import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import selectors from './selectors';

const { object } = PropTypes;

// const access = {
//   V: 'П', // view and read
//   W: 'Р', // write
//   R: 'Н', // read
//   N: 'X', // no access
// };

const STYLE_COLOR_WHITE = { color: 'white' };

const propTypes = {
  user: object,
};

const Authorization = ({ componentAccessName }) => (WrappedComponent) => {
  const WithAuthorization = ({ user: { role }, ...restProps }) => {
    const roleAccess = role[componentAccessName];

    if (!roleAccess) {
      return (
        <h1 style={STYLE_COLOR_WHITE}>
          В доступі відмовлено. Доступ до даної сторінки не назначений.
        </h1>
      );
    } else if (!['R', 'N'].includes(roleAccess)) {
      return <WrappedComponent isEditableRoleAccess={roleAccess === 'W'} {...restProps} />;
    }

    return <h1 style={STYLE_COLOR_WHITE}>В доступі відмовлено. Зверніться до адміністратора.</h1>;
  };

  WithAuthorization.propTypes = propTypes;
  return connect(selectors)(WithAuthorization);
};

export default Authorization;
