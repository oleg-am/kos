import { createSelector, createStructuredSelector } from 'reselect';

const APP = 'app';

const user = state => state[APP].user;

export default createStructuredSelector({
  user: createSelector(user, items => items),
});
