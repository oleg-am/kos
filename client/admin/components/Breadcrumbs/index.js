import React, {PropTypes} from 'react';
import { Link } from 'react-router'

const Breadcrumbs = ({ children, components }) => {
  const {
    title = '',
    links = [{
      name: '',
      to: ''
    }],
 } = components;
  return (
    <div className="breadcrumbs">
      <h1>{title}</h1>
      <ol className="breadcrumb">
        {links.map((link, i, arr) => {

          if (i != arr.length - 1) {
            return (
              <li key={i}>
                <Link className="" to={link.to}>
                  {link.name}
                </Link>
              </li>
            )
          } else {
            if (link.to) {
              return (
                <li className="active" key={i}>
                  <Link className="" to={link.to}>
                    {link.name}
                  </Link>
                </li>
              )
            }
            return <li key={i} className="active">{link.name}</li>
          }
        })}

        {/* <li>
          <Link className="" to={`/admin/orders`}>
            Меню
          </Link>
        </li>
        <li className="active">Заказы</li> */}

      </ol>
    </div>

  );
};

export default Breadcrumbs;
