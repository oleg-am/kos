import React, { PropTypes } from 'react';
// import classNames from 'classnames';

import Icon from '../../elements/Icon';

const { node, bool, func } = PropTypes;

const propTypes = {
  children: node.isRequired,
  isEdited: bool,
  isSaving: bool,
  onSave:   func.isRequired,
};

const defaultProps = {
  onSave: null,

};

const ConfirmationGroup = ({ children, isEdited, onSave, isSaving }) => (
  <div
    className="input-group"
    // style={{ ...(isEdited ? {} : { paddingRight: '51px' }) }}
    style={{ display: isEdited ? 'table' : 'block' }}
  >
    {children}
    <span className="input-group-btn">
      {isEdited &&
        <button
          type="button"
          title="Зберегти"
          className="btn bg-blue bg-font-blue"
          disabled={isSaving}
          onClick={onSave}
        >
          {isSaving
            ? <i className="fa fa-spinner fa-pulse fa-lg fa-fw" />
            : <Icon icon="check" size="lg" collor="default" />
          }
        </button>
      }
    </span>
  </div>
);

ConfirmationGroup.propTypes = propTypes;
ConfirmationGroup.defaultProps = defaultProps;

export default ConfirmationGroup;
