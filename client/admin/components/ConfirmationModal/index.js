import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { Button } from 'react-bootstrap';

import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';

const { string, bool, oneOf, func } = PropTypes;

const propTypes = {
  isOpenModal: bool.isRequired,
  type:        oneOf(['edit, create']),
  title:       string,
  messages:    string,

  onToggleModal: func.isRequired,
  onConfirm:     func.isRequired,
};

const defaultProps = {
  isOpenModal: true,
  title:       '',
  messages:    '',

  onToggleModal: null,
  onConfirm:     null,

};

const ConfirmationModal = ({ isOpenModal, type, title, messages, onToggleModal, onConfirm }) => (
  <Modal show={isOpenModal} onHide={onToggleModal(true)}>
    <Header closeButton>
      <Title
        className={classNames('text-center caption-subject bold',
          { 'font-yellow-lemon': type === 'edit' },
          { 'font-green-haze': type === 'create' },
        )}
      >
        {title}
      </Title>
    </Header>
    <Body>
      <p className="text-danger lead text-center"> {messages} </p>
    </Body>
    <Footer>
      <Button className="green-meadow pull-left" onClick={onConfirm}>Так</Button>
      <Button className="" onClick={onToggleModal(false)}>Ні</Button>
    </Footer>
  </Modal>
);

ConfirmationModal.propTypes = propTypes;
ConfirmationModal.defaultProps = defaultProps;

export default ConfirmationModal;
