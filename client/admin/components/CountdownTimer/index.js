import React, { PropTypes, Component } from 'react';

export default class CountdownTimer extends Component {
  static propTypes = {
    seconds: PropTypes.number,
  }

  state = {
    time: {
      h: 0,
      m: 0,
      s: 0,
    },
    seconds: 0,
  };

  componentWillMount() {
    const { seconds } = this.props;

    if (seconds) {
      this.setState(
        { seconds: seconds + 1 },
        this.setIntervalId,
      );
    }
  }

  componentWillReceiveProps(next) {
    const { seconds: nextSeconds } = next;

    if (nextSeconds) {
      if (this.timer) {
        clearInterval(this.timer);
      }
      this.setState(
        { seconds: nextSeconds + 1 },
        this.setIntervalId,
      );
    }
  }

  componentWillUnmount() {
    this.timer && clearInterval(this.timer);
  }

  setIntervalId = () => {
    this.timer = setInterval(this.countDown, 1000);
  }

  countDown = () => {
    this.setState((prevState) => {
      const seconds = prevState.seconds - 1;
      // Remove one second, set state so a re-render happens.
      // Check if we're at zero.
      seconds === 0 && clearInterval(this.timer);

      return {
        time: this.transformSecondsToTime(seconds),
        seconds,
      };
    });
  }

  transformSecondsToTime = (secs) => {
    const hours = Math.floor(secs / (60 * 60));

    const divisorForMinutes = secs % (60 * 60);
    const minutes = Math.floor(divisorForMinutes / 60);

    const divisorForSeconds = divisorForMinutes % 60;
    const seconds = Math.ceil(divisorForSeconds);

    return {
      h: hours,
      m: minutes,
      s: seconds,
    };
  }

  render() {
    const { time } = this.state;

    return (
      <span
        className="label label-lg label-warning"
        style={{ whiteSpace: 'nowrap', display: 'block' }}
      >
        На паузі: <br /> <br />
        {time.m ? `${time.m} хв ` : ''}{time.s ? `${time.s} сек` : '. '}
      </span>
    );
  }
}
