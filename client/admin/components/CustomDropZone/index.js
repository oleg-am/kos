import React from 'react';

import classNames from 'classnames';
import Dropzone from 'react-dropzone';

import Alert from 'react-s-alert';

class CustomDropZone extends React.Component {

  state = {
    files: []
  }

  constructor(props, context) {
    super(props, context);
  }

  onDrop = (files) => {
    this.setState({files});
  }
  onSave = () => {
    let data = new FormData();
    data.append('file', this.state.files[0]);
    const clearFiles = () => {
      this.setState({ files: []})
    }
    this.props.saveImg(data, clearFiles);
  }
  onOpenClick = () => {
    this.dropzone.open();
  }
  // showSuccessMessage() {
  //   Alert.success(
  //     <span>
  //       <i className="fa fa-check"></i>
  //       Дані збережені
  //     </span>
  //   );
  // }
  // showErrorMessage() {
  //   Alert.error(
  //     <span>
  //       <i className="fa fa-exclamation-triangle"></i>
  //       Помилка. Дані не збережені!
  //     </span>
  //   );
  // }
  render() {
    const {
      imgUrl,
      url_main,
    } = this.props;

    return (
      <div>
        <div className="form-group">
          <div style={{display: 'initial'}} className={`fileinput fileinput-${this.state.files.length ? 'new' : 'exist'}`} data-provides="fileinput">
            <div className="fileinput-preview thumbnail"
              style={{width: '100%', height: '200px', lineHeight: '200px'}}
            >
              {!this.state.files.length &&
                <img height="200px" src={url_main && `${imgUrl}${url_main}`} />
              }
              {this.state.files.map((file, i) =>
                <img key={`file_${i}`} height="200px" src={file.preview} />
              )}
            </div>
            <div className="form-group">
              <span style={{marginRight: '5px'}} className="btn green btn-file" onClick={this.onOpenClick}>
              {this.state.files.length ? 'Змінити файл': 'Виберіть файл'}
              </span>
              {!!this.state.files.length &&
                <button className="btn green-meadow" onClick={this.onSave}>
                  <i className="fa fa-check"></i> Зберегти
                </button>
              }
            </div>
            {!!this.state.files.length && (
              <div>
                <span className="fileinput-filename">
                  {this.state.files[0].name} <i title="Відмінити" className="fa fa-times close"></i>
                </span>
              </div>
            )}
          </div>
        </div>
        <div className="form-group">
          <Dropzone
            style={{display: 'none'}}
            ref={node => { this.dropzone = node }}
            multiple={false}
            accept="image/*"
            onDrop={this.onDrop}
          >
            {/* <div>Try dropping some files here, or click to select files to upload.</div> */}
          </Dropzone>
        </div>
      </div>
    );
  }
}

export default CustomDropZone;
