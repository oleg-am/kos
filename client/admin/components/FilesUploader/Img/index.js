import React, { PropTypes } from 'react';

import classNames from 'classnames';
import Dropzone from 'react-dropzone';

import Alert from 'react-s-alert';

const { bool } = PropTypes;

const propTypes = {
  editable: bool,
};

const Img = ({ files, imgUrl, url_main, onOpenClick, onSave, clearFiles, editable }) => {

  // {console.log('props Img: ', files, imgUrl, url_main)}

  return (
    <div className="form-group">
      <div style={{display: 'initial'}} className={`fileinput fileinput-${files.length ? 'new' : 'exist'}`} data-provides="fileinput">
        <div className="fileinput-preview thumbnail"
          style={{width: '100%', height: '200px', lineHeight: '200px'}}
        >
          {!files.length &&
            <img height="200px" src={url_main && `${imgUrl}${url_main}`} />
          }
          {files.map((file, i) =>
            <img key={`file_${i}`} height="200px" src={file.preview} />
          )}
        </div>
        {editable &&
          <div className="form-group">
            <span style={{ marginRight: '5px' }} className="btn green btn-file" onClick={onOpenClick}>
              {files.length ? 'Змінити файл' : 'Виберіть файл'}
            </span>
            {!!files.length &&
              <button className="btn green-meadow" onClick={onSave}>
                <i className="fa fa-check" /> Зберегти
              </button>
            }
          </div>
        }
        {!!files.length && (
          <div>
            <span className="fileinput-filename">
              {files[0].name} <i onClick={clearFiles} title="Відмінити" className="fa fa-times close" />
            </span>
          </div>
        )}
      </div>
    </div>
  );
};

Img.propTypes = propTypes;

export default Img;
