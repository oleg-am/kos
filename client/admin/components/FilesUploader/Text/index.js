import React, { PropTypes } from 'react';

const { bool } = PropTypes;

const propTypes = {
  editable: bool,
};

const Img = ({ files, imgUrl, url_main, onOpenClick, onSave, clearFiles, editable }) => {

  // {console.log('props Img: ', files, imgUrl, url_main)}

  return (
    <div className="form-group">
      <div style={{ display: 'initial' }} className={`fileinput fileinput-${files.length ? 'new' : 'exist'}`} data-provides="fileinput">
        {editable &&
          <span style={{ marginRight: '5px' }} className="btn green btn-file" onClick={onOpenClick}>
            {(files.length || url_main) ? 'Змінити файл' : 'Додати файл'}
          </span>
        }
        {!!files.length &&
          <button className="btn green-meadow" onClick={onSave}>
            <i className="fa fa-check" /> Зберегти
          </button>
        }
        {(!!files.length || url_main) && (
          <span style={{ marginLeft: '10px' }} className="fileinput-filename">
            {files.length
            ? <span>{files[0].name} <i onClick={clearFiles} title="Відмінити" className="fa fa-times close" /></span>
            : <a href={url_main && `${imgUrl}${url_main}`} download><i className="icon-doc" /> Інструкція</a>
          }
          </span>
        )}
      </div>
    </div>
  );
};

Img.propTypes = propTypes;

export default Img;
