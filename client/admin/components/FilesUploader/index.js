import React from 'react';

import classNames from 'classnames';
import Dropzone from 'react-dropzone';

import Alert from 'react-s-alert';

import Img from './Img';
import Text from './Text';

const dropZoneHOC = ({ accept }) => (FilesType) => {

  class DropZone extends React.Component {

    state = {
      files: []
    }

    constructor(props, context) {
      super(props, context);
    }

    onDrop = (files) => {
      this.setState({files});
    }
    onSave = () => {
      let data = new FormData();
      data.append('file', this.state.files[0]);
      // const clearFiles = () => {
      //   this.setState({ files: []})
      // }
      this.props.saveFile(data, this.clearFiles);
    }
    clearFiles = () => {
      this.setState({ files: []})
    }
    onOpenClick = () => {
      this.dropzone.open();
    }
    // showSuccessMessage() {
    //   Alert.success(
    //     <span>
    //       <i className="fa fa-check"></i>
    //       Дані збережені
    //     </span>
    //   );
    // }
    // showErrorMessage() {
    //   Alert.error(
    //     <span>
    //       <i className="fa fa-exclamation-triangle"></i>
    //       Помилка. Дані не збережені!
    //     </span>
    //   );
    // }
    render() {
      return (
        <div>
          <FilesType
            {...this.props}
            files={this.state.files}
            onOpenClick={this.onOpenClick}
            onSave={this.onSave}
            clearFiles={this.clearFiles}
          />
          <Dropzone
            style={{ display: 'none' }}
            ref={(node) => { this.dropzone = node; }}
            multiple={false}
            accept={accept}
            onDrop={this.onDrop}
          >
            {/* <div>Try dropping some files here, or click to select files to upload.</div> */}
            {/* "image/*" */}
          </Dropzone>
        </div>
      );
    }
  }
  return DropZone;
};

export const FilesUploaderImg = dropZoneHOC({
  accept: 'image/*',
})(Img);

export const FilesUploaderPdfImgZip = dropZoneHOC({
  accept: 'application/pdf,image/*,application/zip',
})(Text);
