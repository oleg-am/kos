import React, { PropTypes } from 'react';
// import classNames from 'classnames';

const propTypes = {
  value:            PropTypes.string,
  comparison:       PropTypes.oneOf(['begins', 'like']),
  onChangeValue:    PropTypes.func.isRequired,
  onChangeCheckbox: PropTypes.func.isRequired,
  onKeyUpValue:     PropTypes.func.isRequired,
};

const defaultProps = {
  value:            '',
  comparison:       'begins',
  onChangeValue:    null,
  onChangeCheckbox: null,
  onKeyUpValue:     null,
};

const FilterSearch = ({
  value,
  comparison,
  onChangeValue,
  onChangeCheckbox,
  onKeyUpValue,
}) => {
  const onChangeChecked = () => (
    onChangeCheckbox &&
      onChangeCheckbox(comparison === 'begins' ? 'like' : 'begins')
  );

  return (
    <div className="input-group">
      <input
        type="text"
        className="form-control"
        style={{ height: '36px' }}
        placeholder="Фільтр найменування"
        value={value}
        onChange={onChangeValue}
        onKeyUp={onKeyUpValue}
      />
      <span className="input-group-addon" >
        <label
          htmlFor="filters_name_2"
          title={`Переключити на пошук ${comparison === 'begins'
            ? 'по всьому тексту'
            : 'з початку тексту'}`
          }
          className="mt-checkbox"
          style={{ paddingLeft: '17px' }}
        >
          <input
            id="filters_name_2"
            onChange={onChangeChecked}
            type="checkbox"
            checked={comparison === 'begins'}
          />
          <span />
        </label>
      </span>
    </div>
  );
};

FilterSearch.propTypes = propTypes;
FilterSearch.defaultProps = defaultProps;

export default FilterSearch;
