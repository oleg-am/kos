import React from 'react';
import moment from '../../helpers/moment';

export const priceFormatter = (cell, row) => {
  return '<i class="glyphicon glyphicon-usd"></i> ' + cell;
};
export const dateFormatter = (cell, row) => (
  cell ? moment(cell).format('L') : '---'
);
export const timeFormatter = (cell, row) => (
  moment(cell).format('LTS')
);
export const dateAndTimeFormatter = (cell, row) => (
  // dateFormatter(cell, row) + ' ' + timeFormatter(cell, row)
  moment(cell).format('LLL')
);
export const extraDataObjFormatter = (field) => (cell, row, extraData) => {
  if (!extraData[cell]) {
    return '';
  }
  return extraData[cell][field];
};

export const formTextFormatter = ({ handleChange, cell, row }) => {
  return (
    <div style={{ 'padding': '0px', 'marginBottom': '0px' }} className="form-group form-md-line-input">
      <input type="text" className="form-control"
        style={{ height: 'auto', padding: '2px 8px 2px 12px'}}
        value={cell}
        onChange={handleChange} />
      <label></label>
    </div>
  )
}

export statusFormatter from './statusFormatter.js';
