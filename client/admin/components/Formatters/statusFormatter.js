import React from 'react';

const statusObj = {
  creation:               { name: 'Створення', color: 'label-green-turquoise'}, // (зелений)
  created:                { name: 'Створено', color: 'label-green-turquoise'}, // (зелений)
  // asdf: { name: 'Отримано', color:  'label-green-turquoise'}, // (зелений)
  in_process_by_operator: { name: 'Обробляється Оператором', color:  'label-grey-steel'}, //(сірий)
  processed_by_operator:  { name: 'Опрацьовано Оператором', color:  'label-grey-steel'}, // (сірий)
  got_in_pharmacy:        { name: 'Отримано для обробки в аптеці', color:  'label-grey-steel'},// (сірий)
  got_in_stock:           { name: 'Отримано для обробки на складі', color:  'label-grey-steel'},// (сірий)
  in_process_by_pharmacy: { name: 'Обробляється в аптеці', color:  'label-grey-steel'},// (сірий)
  in_process_by_stock:    { name: 'Обробляється на складi', color:  'label-grey-steel'},// (сірий)
  returned_to_edit:       { name: 'Відправлений на редагування', color:  'label-warning'},// (жовтий)
  editing_by_operator:    { name: 'Обробляється Call-центром після відправки на редагування', color:  'label-grey-steel'},// (сірий)
  processed_after_edit:   { name: 'Опрацьовано Call-центром після відправки на редагування', color:  'label-grey-steel'},// (сірий)
  cancel_after_edit:      { name: 'Відмова після відправки на редагування', color:  'label-grey-steel'},// (сірий)
  processed_by_stock:     { name: 'Опрацьовано на складі  (Вн-розхід)', color:  'label-grey-steel'},// (сірий)
  processed_by_pharmacy:  { name: 'Опрацьовано в аптеці  (Резерв)', color:  'label-grey-steel'},// (сірий)
  check:                  { name: 'Продаж', color:  'label-grey-steel'},// (сірий)
  canceled:               { name: 'Відмова', color:  'label-grey-steel'},// (сірий)
};

const statusFormatter = statusesConfig => ({ name = 'creation', statusName }) => (
  <span
    className={`label label-lg ${(statusesConfig[name] || {}).color || ''}`}
    style={{ whiteSpace: 'normal', display: 'block' }}
  >
    {statusName || 'Створення'}
  </span>
);

export default statusFormatter(statusObj);
