import React, { Component } from 'react';

import {
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps';

import SearchBox from 'react-google-maps/lib/places/SearchBox';

const INPUT_STYLE = {
  boxSizing:    'border-box',
  MozBoxSizing: 'border-box',
  border:       '1px solid transparent',
  width:        '350px',
  height:       '32px',
  marginTop:    '10px',
  padding:      '8px',
  borderRadius: '2px',
  // boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)',
  boxShadow:    'rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px',
  fontSize:     '14px',
  outline:      'none',
  textOverflow: 'ellipses',
};

const SearchBoxGoogleMapWrapper = withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapMounted}
    defaultZoom={8}
    center={props.center}
    onBoundsChanged={props.onBoundsChanged}
    // onClick={props.onMapClick}
  >
    <SearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      controlPosition={google.maps.ControlPosition.TOP_LEFT}
      onPlacesChanged={props.onPlacesChanged}
      inputPlaceholder="Введіть адресу"
      inputStyle={INPUT_STYLE}
    />
    {[...props.markers, ...props.markersSechBox].map((marker, index) => {
      const isInfo = !!marker.info;

      const onClick = () => {
        isInfo && props.onMarkerClick(marker.info);
      };

      return (
        <Marker
          key={index}
          position={marker.position}
          title={marker.info ? marker.info.name : ''}
          onClick={onClick}
        >
          { isInfo && (props.selectedMarker === marker.info.id) && (
            <InfoWindow onCloseClick={onClick}>
              <div>
                <strong> {marker.info.name} </strong>
                <br />
                <em> {marker.info.address} </em>
              </div>
            </InfoWindow>
          )}
        </Marker>
      );
    })}
  </GoogleMap>
));

/*
 * https://developers.google.com/maps/documentation/javascript/examples/places-searchbox
 *
 * Add <script src="https://maps.googleapis.com/maps/api/js"></script> to your HTML to provide google.maps reference
 */
export default class MultiMarkersMap extends Component {

  constructor(props, context) {
    console.log('props MultiMarkersMap: ', props);
    super(props, context);

    const { center } = props;
    center && this.setState({ center });
  }

  state = {
    bounds: null,
    center: {
      lat: 50.4501,
      lng: 30.5234,
    },
    markers:        [],
    markersSechBox: [],
  }

  componentWillReceiveProps(nextProps) {
    const { markers: nextMarkers, center: nextCenter } = nextProps;

    let markers;
    let center;

    if (nextMarkers !== this.state.markers) {
      markers = nextMarkers;
      if (nextMarkers.length) {
        center = nextMarkers[0].position;
      }
    }

    if (nextCenter && nextCenter !== this.state.nextCenter) {
      center = nextCenter;
    }

    markers && this.setState({ markers });
    center && this.setState({ center });
  }

  handleMapClick = (event) => {
    const { onPlacesChanged } = this.props;
    console.log('eveng: ', event);
    if (onPlacesChanged) {
      onPlacesChanged({
        lat: event.latLng.lat(),
        lng: event.latLng.lng(),
      });
      this.setState({
       // center: event.latLng,
        markers: [{ position: event.latLng },
        ],
      });
    }
  };

// handleMarkerRightClick = (targetMarker) => {
//     const nextMarkers = this.state.markers.filter(marker => marker !== targetMarker);
//     this.setState({
//         markers: nextMarkers,
//     });
// }

  handleMapMounted = (map) => {
    this._map = map;
  }

  handleBoundsChanged = () => {
    this.setState({
      bounds: this._map.getBounds(),
      center: this._map.getCenter(),
    });
  }

  handleSearchBoxMounted = (searchBox) => {
    this._searchBox = searchBox;
  }

  handlePlacesChanged = () => {
    const { onPlacesChanged } = this.props;
    const places = this._searchBox.getPlaces();
    console.log('places: ', places);

    // Add a marker for each place returned from search bar
    const markers = places.map(place => ({
      ...place,
      position: place.geometry.location,
    }));
    onPlacesChanged && onPlacesChanged({
      lat: places[0].geometry.location.lat(),
      lng: places[0].geometry.location.lng(),
    });

    // Set markers; set map center to first search result
    const mapCenter = markers.length > 0 ? markers[0].position : this.state.center;
    console.log('markers: ', markers);
    this.setState({
      center: mapCenter,
      markersSechBox: markers,
    });
  }

  render() {
    const { selectedMarker, onMarkerClick } = this.props;
    return (
      <SearchBoxGoogleMapWrapper
        containerElement={
          <div style={{ height: '100%' }} />
        }
        mapElement={
          <div style={{ height: '100%' }} />
        }
        center={this.state.center}
        onMapMounted={this.handleMapMounted}
        onMapClick={this.handleMapClick}
        onBoundsChanged={this.handleBoundsChanged}
        onSearchBoxMounted={this.handleSearchBoxMounted}
        bounds={this.state.bounds}
        onPlacesChanged={this.handlePlacesChanged}
        markers={this.state.markers}
        selectedMarker={selectedMarker}
        onMarkerClick={onMarkerClick}
        markersSechBox={this.state.markersSechBox}
      />
    );
  }
}
