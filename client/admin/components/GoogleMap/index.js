import React, { Component } from 'react';

import {
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

import SearchBox from 'react-google-maps/lib/places/SearchBox';

const INPUT_STYLE = {
  boxSizing:    'border-box',
  MozBoxSizing: 'border-box',
  border:       '1px solid transparent',
  width:        '250px',
  height:       '32px',
  marginTop:    '10px',
  padding:      '8px',
  borderRadius: '2px',
  // boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)',
  boxShadow:    'rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px',
  fontSize:     '14px',
  outline:      'none',
  textOverflow: 'ellipses',
};

const SearchBoxGoogleMapWrapper = withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapMounted}
    defaultZoom={15}
    center={props.center}
    onBoundsChanged={props.onBoundsChanged}
    onClick={props.onMapClick}
  >
    <SearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      controlPosition={google.maps.ControlPosition.TOP_LEFT}
      onPlacesChanged={props.onPlacesChanged}
      inputPlaceholder="Введіть адресу"
      inputStyle={INPUT_STYLE}
    />
    {props.markers.map((marker, index) => (
      <Marker position={marker.position} key={index} />
    ))}
  </GoogleMap>
));

/*
 * https://developers.google.com/maps/documentation/javascript/examples/places-searchbox
 *
 * Add <script src="https://maps.googleapis.com/maps/api/js"></script> to your HTML to provide google.maps reference
 */
export default class SearchBoxMap extends Component {

  // state = {
  //   bounds: null,
  //   center: {
  //     lat: 50.4501,
  //     lng: 30.5234,
  //   },
  //   markers: [],
  // };
  //

  constructor(props, context) {
    console.log('props SearchBoxMap: ', props);
    super(props, context);

    const { lat, lng } = props;
    const markers = lat && lng
    ? [{
      position: {
        lat,
        lng,
      },
      key: 'Kyiv',
      defaultAnimation: 2,
    }]
    : [];

    this.state = {
      bounds: null,
      center: {
        lat: lat || 50.4501,
        lng: lng || 30.5234,
      },
      markers,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.lat === '' && this.props.lat === '' &&
    nextProps.lat !== this.props.lat && nextProps.lng !== this.props.lng) {
      this.setState({
        markers: [{
          position: {
            lat: nextProps.lat,
            lng: nextProps.lng,
          },
          key: 'Kyiv',
          defaultAnimation: 2,
        }],
        center: {
          lat: nextProps.lat,
          lng: nextProps.lng,
        },
      });
    }
  }

  handleMapClick = (event) => {
    const { onPlacesChanged } = this.props;
    console.log('eveng: ', event);
    if (onPlacesChanged) {
      onPlacesChanged({
        lat: event.latLng.lat(),
        lng: event.latLng.lng(),
      });
      this.setState({
       // center: event.latLng,
        markers: [{ position: event.latLng },
        ],
      });
    }
  };

// handleMarkerRightClick = (targetMarker) => {
//     const nextMarkers = this.state.markers.filter(marker => marker !== targetMarker);
//     this.setState({
//         markers: nextMarkers,
//     });
// }

  handleMapMounted = (map) => {
    this._map = map;
  }

  handleBoundsChanged = () => {
    this.setState({
      bounds: this._map.getBounds(),
      center: this._map.getCenter(),
    });
  }

  handleSearchBoxMounted = (searchBox) => {
    this._searchBox = searchBox;
  }

  handlePlacesChanged = () => {
    const { onPlacesChanged } = this.props;
    const places = this._searchBox.getPlaces();
    console.log('places: ', places);

    // Add a marker for each place returned from search bar
    const markers = places.map(place => ({
      position: place.geometry.location,
    }));
    onPlacesChanged && onPlacesChanged({
      lat: places[0].geometry.location.lat(),
      lng: places[0].geometry.location.lng(),
    });

    // Set markers; set map center to first search result
    const mapCenter = markers.length > 0 ? markers[0].position : this.state.center;
    console.log('markers: ', markers);
    this.setState({
      center: mapCenter,
      markers,
    });
  }

  render() {
    return (
      <SearchBoxGoogleMapWrapper
        containerElement={
          <div style={{ height: '100%' }} />
        }
        mapElement={
          <div style={{ height: '100%' }} />
        }
        center={this.state.center}
        onMapMounted={this.handleMapMounted}
        onMapClick={this.handleMapClick}
        onBoundsChanged={this.handleBoundsChanged}
        onSearchBoxMounted={this.handleSearchBoxMounted}
        bounds={this.state.bounds}
        onPlacesChanged={this.handlePlacesChanged}
        markers={this.state.markers}
      />
    );
  }
}
