import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import NavLink from '../NavLink';

const { number, string, shape, oneOfType } = PropTypes;

const propTypes = {
  version:  number,
  userInfo: shape({
    id: oneOfType([number, string]),
  }),
  user: shape({
    name:   string,
    avatar: string,

    role: shape({
      id:   oneOfType([number, string]),
      name: string,

      dashboard:    string,
      reports:      string,
      orders:       string,
      nomenclature: string,
      pharmacies:   string,
      clients:      string,
      config:       string,

    }),
  }),
};

// const access = {
//   V: 'П', // view and read
//   W: 'Р', // write
//   R: 'Н', // read
//   N: 'X', // no access
// };

const checkAccess = role => page => !['R', 'N'].includes(role[page]);

const Header = ({ whatComponentIsOpen, user, userInfo, version }) => {
  const { name, position, role } = user;
  const checkRoleAccess = checkAccess(role);

  return (
    <header className="page-header">
      <nav className="navbar mega-menu" role="navigation">
        <div className="container-fluid">
          <div className="clearfix navbar-fixed-top ">
            <div className="pull-left">
              <a id="index" className="page-logo pull-left" href="index.html">
                <img
                  className="pull-left"
                  src="/build/admin/images/logo_header.png"
                  style={{
                    height: `${50}px`,
                    width:  'auto',
                  }}
                  alt="Logo"
                />
              </a>
            </div>
            <div className="pull-left" style={{ color: 'white', marginRight: '5%' }} >
              <span style={{ fontSize: '1.5em', display: 'block' }}>Модуль обробки замовлень</span>
              <span style={{ fontSize: '1.25em', color: 'red' }}>Версія: {version}</span>
            </div>
            <div
              style={{ color: 'white', paddingLeft: '0' }}
            >
              <span style={{ fontSize: '1.5em' }} />
                 Ви увійшли як:
              <span style={{ color: 'green', fontSize: '1em', marginLeft: '0' }}> {name}</span>&nbsp;
               Посада:
               <span style={{ color: 'green', fontSize: '1em' }}> {position}</span>

            </div>
            <div className="navbar-toggle" data-toggle="collapse"><button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
              <span className="sr-only">Toggle navigation</span>
              <span className="toggle-icon">
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </span>
            </button>
            </div>
            <div className="topbar-actions ">


              <div className="btn-group-img btn-group ">
                <button type="button" className="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                  <span>Меню користувача</span>
                  {/* <img src="/assets/layouts/layout5/img/avatar1.jpg" alt="" /> */}
                </button>
                <ul className="dropdown-menu-v2" role="menu">
                  {/* <li>
                    <Link to="/admin/users">
                      <i className="icon-users" />
                      Користувачі
                    </Link>
                  </li> */}
                  <li>
                    <Link to={'/admin/me'}>
                      <i className="icon-user" />
                      Мій профіль
                      {/* <span className="badge badge-danger">1</span> */}
                    </Link>
                  </li>
                  {/* <li>
                    <a href="app_calendar.html">
                      <i className="icon-calendar" />
                      My Calendar
                    </a>
                  </li>
                  <li>
                    <a href="app_inbox.html">
                      <i className="icon-envelope-open" />
                      My Inbox
                      <span className="badge badge-danger">
                        3
                      </span>
                    </a>
                  </li>
                  <li>
                    <a href="app_todo_2.html">
                      <i className="icon-rocket" />
                      My Tasks
                      <span className="badge badge-success">
                        7
                      </span>
                    </a>
                  </li>
                  <li className="divider" />
                  <li>
                    <a href="page_user_lock_1.html">
                      <i className="icon-lock" />
                      Lock Screen
                    </a>
                  </li> */}
                  <li>
                    <a href="/oauth/logout">
                      <i className="icon-key" />
                      Вихід
                    </a>
                  </li>
                </ul>
              </div>

              {/* <button type="button" className="quick-sidebar-toggler md-skip" data-toggle="collapse">
                <span className="sr-only">Toggle Quick Sidebar</span>
                <i className="icon-logout" />
              </button> */}

            </div>

          </div>

          <div className="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
            <ul className="nav navbar-nav">

              {checkRoleAccess('dashboard') &&
                <NavLink to="/admin" className="dropdown dropdown-fw " activeClass="active open selected">
                  <i className="fa fa-book" aria-hidden="true" />
                  Робочий стіл
                </NavLink>
              }
              {checkRoleAccess('orders') &&
                <NavLink to="/admin/orders" className="dropdown dropdown-fw order-link" activeClass="active open selected">
                  <span className="glyphicon glyphicon-list-alt" aria-hidden="true" />
                  Замовлення
                </NavLink>
              }
              {checkRoleAccess('reports') &&
                <NavLink to="/admin/reports" className="dropdown dropdown-fw advertieseng-link" activeClass="active open selected">
                  <i className="fa fa-bar-chart" aria-hidden="true" />
                  Звіти
                </NavLink>
              }
              {checkRoleAccess('pharmacies') &&
                <NavLink to="/admin/pharmacies" className="dropdown dropdown-fw advertieseng-link" activeClass="active open selected">
                  <i className="fa fa-medkit" aria-hidden="true" />
                  Аптеки {/* icon-briefcase */}
                </NavLink>
              }
              {checkRoleAccess('nomenclature') &&
                <NavLink to="/admin/nomenclature" className="dropdown dropdown-fw " activeClass="active open selected">
                  <i className="fa fa-book" aria-hidden="true" />
                  Номенклатура
                </NavLink>
              }
              {checkRoleAccess('clients') &&
                <NavLink to="/admin/clients" className="dropdown dropdown-fw " activeClass="active open selected">
                  <i className="fa fa-users" aria-hidden="true" />
                  Клієнти
                </NavLink>
              }
              {checkRoleAccess('config') &&
                <NavLink
                  to="/admin/settings/general"
                  className="dropdown dropdown-fw dictionaries-link"
                  isOpen={!!whatComponentIsOpen.menu}
                  activeClass="active open selected"
                >
                  <i className="fa fa-wrench" aria-hidden="true" /> Налаштування
                </NavLink>
              }
              {checkRoleAccess('config') &&
                <li className="dropdown dropdown-fw  active open selected menu-sublist dictionaries-sublist">
                  <ul className="dropdown-menu dropdown-menu-fw">
                    <NavLink
                      to="/admin/settings/general"
                      className="dropdown dropdown-fw dictionaries-link"
                      activeClassName="active"
                      activeClass="active open selected"
                    >
                      <i className="icon-wallet" /> Загальні
                    </NavLink>
                    <NavLink
                      to="/admin/settings/marketPlace"
                      className="dropdown dropdown-fw dictionaries-link"
                      activeClassName="active"
                      activeClass="active open selected"
                    >
                      <i className="icon-home" /> Майданчики
                    </NavLink>
                    <NavLink
                      to="/admin/settings/users"
                      className="dropdown dropdown-fw dictionaries-link"
                      activeClassName="active"
                      activeClass="active open selected"
                    >
                      <i className="icon-users" /> Користувачi
                    </NavLink>
                    <NavLink
                      to="/admin/settings/user-roles"
                      className="dropdown dropdown-fw dictionaries-link"
                      activeClassName="active"
                      activeClass="active open selected"
                    >
                      <i className="icon-speech" /> Ролі
                    </NavLink>

                    {/* <li className="dropdown dropdown-fw">
                      <Link to="/admin/settings/users" activeClassName="active">Користувачi</Link>
                    </li>
                    <li className="dropdown dropdown-fw">
                      <Link to="/admin/settings" activeClassName="active">Ролі</Link>
                    </li>
                    <li className="dropdown dropdown-fw">
                      <Link to="/admin/settings/prices" activeClassName="active">Ціни</Link>
                    </li> */}
                    {/* <li className="dropdown dropdown-fw">
                          <Link to="/admin/dictionary/nosology_and_synonyms">
                            Справочник нозологий и синонимов
                          </Link>
                        </li>
                        <li className="dropdown dropdown-fw">
                            <Link to="/admin/dictionary/pharmacy">Справочник аптек</Link>
                        </li>
                        <li className="dropdown dropdown-fw">

                            <Link to="/admin/dictionary/cities">Справочник городов</Link>
                        </li> */}
                  </ul>
                </li>
              }
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

Header.propTypes = propTypes;

export default Header;
