import React, { PropTypes } from 'react';
// import { Link } from 'react-router';
// import classNames from 'classnames';

const propTypes = {
  min:          PropTypes.number,
  max:          PropTypes.number,
  optionsValue: PropTypes.arrayOf(PropTypes.number),
  value:        PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onChange:     PropTypes.func,
  // className: PropTypes.string,
};

const defaultProps = {
  min:          10,
  max:          20,
  optionsValue: [],
  // value:    10,
  onChange:     null,
  // className: ''
};

const LimitSelect = ({ min, max, optionsValue, value, onChange }) => (
  <select
    className="form-control"
    style={{ width: 'inherit', display: 'inline-block' }}
    value={value}
    onChange={onChange}
  >
    {optionsValue.length
      ? optionsValue.map(val => (
        <option key={val} value={val}>{val}</option>
      ))
      : Array.from({ length: max - min + 1 }, (v1, k1) => k1).map(val => (
        <option key={val} value={min + val}>{min + val}</option>
      ))
    }

  </select>
);

LimitSelect.propTypes = propTypes;
LimitSelect.defaultProps = defaultProps;

export default LimitSelect;
