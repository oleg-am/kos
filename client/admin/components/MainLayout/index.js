import React, { PropTypes } from 'react';

const propTypes = {
  children: PropTypes.node,
};

const MainLayout = ({ children }) => (
  <div className="page-content" >
    {children}
  </div>
);

MainLayout.propTypes = propTypes;

export default MainLayout;
