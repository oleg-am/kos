import React, { PropTypes } from 'react';
// import {Link} from 'react-router';
import classNames from 'classnames';

const propTypes = {
  children: PropTypes.node,
  md:       PropTypes.number,
}

const defaultProps = {
  children: null,
  md:       2,
}

const ControlLabel = ({ children, md }) => {
  return (
    <label
      className={classNames(`col-md-${md}`, 'control-label')}
      // forHtml="form_control_1"
    >
      {children}
    </label>
  );
};

ControlLabel.propTypes = propTypes;
ControlLabel.defaultProps = defaultProps;

export default ControlLabel;
