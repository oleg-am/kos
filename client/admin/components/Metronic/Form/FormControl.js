import React, { PropTypes } from 'react';
import Select from 'react-select';
// import {Link} from 'react-router';
import classNames from 'classnames';

const propTypes = {
  onChange: PropTypes.func,
  type:     PropTypes.string,
  title:    PropTypes.string,
  value:    PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  isStatic:    PropTypes.bool,
  isClearable: PropTypes.bool,
  component:   PropTypes.string,
  options:     PropTypes.arrayOf(PropTypes.object),
  key:         PropTypes.string,
  field:       PropTypes.string,
  matchProp:   PropTypes.string,
  className:   PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  styles: PropTypes.object,
  error:  PropTypes.string,
};

const defaultProps = {
  onChange:    null,
  type:        'text',
  title:       '',
  value:       '',
  placeholder: null,
  isStatic:    false,
  isClearable: false,
  key:         'id',
  className:   {},
  styles:      {},
  matchProp:   'any', // search by value and label in Select input
  error:       '',
};

const FormControl = ({
  onChange,
  type,
  value,
  title,
  placeholder,
  isStatic,
  component,
  options,
  isClearable,
  className,
  styles,
  matchProp,
  error,
  key,
  field
}) => {
  let formControl = (
    <input
      className="form-control"
      type={type}
      value={value}
      title={title}
      placeholder={placeholder}
      onChange={onChange}
      // {...{onChange, type, value, placeholder }}
    />
  );

  if (component === 'reactSelect') {
    formControl = (
      <Select
        name="form-field-name"
        placeholder={placeholder}
        value={value}
        options={options.map(item => ({ label: item.name, value: item.id, className: className }))}
        // onInputChange={this.setFilterManufacturers(['filters', 'name'], true)}
        onChange={onChange}
        // onOpen={this.loadManufacturers}
        noResultsText="Нічого не знайдено"
        clearValueText="Стерти"
        // isLoading={isLoadingManufacturer}
        clearable={isClearable}
        matchProp={matchProp}
        style={styles}
        // onInputKeyDown={this.setFilterAfterESC(['filters', 'manufacturer'])}
      />
    );
  } else if (component === 'select') {
    formControl = (
      <select value={value} title={title} className="form-control" onChange={onChange} >
        {!options.filter(o => o.id === value).length &&
        <option disabled key="-1" value="-1">{placeholder}</option>}
        {options.map(item => (
          <option key={item.id} value={item.id}>{item.name}</option>
        ))}
      </select>
    );
  } else if (component === 'textarea') {
    formControl = (
      <textarea
        className="form-control"
        value={value}
        title={title}
        placeholder={placeholder}
        onChange={onChange}
        // {...{onChange, type, value, placeholder }}
      />
    );
  }

  return (
    <div>
      {isStatic
        ? <div className="form-control form-control-static">
          {component === 'select' || component === 'reactSelect'
            ? (options.length ? (options.find(o => o.id === value) || {}).name : '')
            : value
          }
        </div>
        : formControl
      }
      <div className="form-control-focus" />
      {error &&
        <span
          className={classNames('help-block', { 'has-error': error })}
          style={{ color: 'red', opacity: 1, margin: 0, fontSize: '12px' }}
        >
          {error}
        </span>
      }
    </div>
  );
};

FormControl.propTypes = propTypes;
FormControl.defaultProps = defaultProps;

export default FormControl;
