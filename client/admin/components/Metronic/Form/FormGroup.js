import React, { PropTypes } from 'react';
// import {Link} from 'react-router';
import classNames from 'classnames';

const propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object,
  error: PropTypes.string,
}

const defaultProps = {
  children:  null,
  className: '',
  style: {}
}

const FormGroup = ({ children, className, style, error }) => {
  return (
    <div
      className={classNames('form-group form-md-line-input', className, { 'has-error': error })}
      style={style}
    >
      {children}
    </div>
  );
};

FormGroup.propTypes = propTypes;
FormGroup.defaultProps = defaultProps;

export default FormGroup;
