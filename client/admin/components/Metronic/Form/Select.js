import React, { PropTypes } from 'react';
// import {Link} from 'react-router';
// import classNames from 'classnames';

const propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  data: PropTypes.object,
  field: PropTypes.string,
  key: PropTypes.string,
  placeholder: PropTypes.string
};

const Select = ({
  onChange,
  value,
  data,
  field,
  key = 'id',
  placeholder = 'Оберіть',
}) => {
  return (
    <div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input has-info">
      <select value={value} style={{ height: 'auto', padding: '2px 8px 2px 12px'}} className="form-control" onChange={onChange} >
      {!data[value] &&
        <option disabled key="-1" value="-1">{placeholder}</option>}
      {Object.keys(data).map(id=>(
        <option key={data[id][key]} value={data[id][key]}>{data[id][field]}</option>
      ))}
      </select>
      <label></label>
  </div>
  );
};

Select.propTypes = propTypes;

export default Select;
