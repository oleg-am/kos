import React, { PropTypes } from 'react';
// import {Link} from 'react-router';
// import classNames from 'classnames';

const propTypes = {
  onChange:    PropTypes.func,
  value:       PropTypes.string,
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
}

const defaultProps = {
  onChange:    null,
  value:       '',
  placeholder: null
}

const InputText = ({ onChange, value, placeholder }) => {
  return (
    <div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input">
      <input type="text" className="form-control"
        style={{ height: 'auto', padding: '2px 8px 2px 12px'}}
        value={value}
        placeholder={placeholder}
        onChange={onChange} />
      <label></label>
    </div>
  );
};

InputText.propTypes = propTypes;
InputText.defaultProps = defaultProps;

export default InputText;
