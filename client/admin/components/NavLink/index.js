import {Link, IndexLink} from "react-router";
import React from 'react';

export default React.createClass({
    contextTypes: {
        router: React.PropTypes.object
    },

    render: function () {
        let isActive = this.context.router.isActive(this.props.to, true),
            className = (isActive || this.props.isOpen) ? this.props.activeClass : "";
        return (

            <li className={this.props.className + " " + className}>
                <Link to={this.props.to} activeClassName={this.props.activeClassName}>
                    {this.props.children}
                </Link>
            </li>
        );
    }
});
