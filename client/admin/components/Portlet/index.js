import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

const propTypes = {
  className:      PropTypes.string,
  titleClassName: PropTypes.string,
  captionClassName: PropTypes.string,
  bodyClassName:  PropTypes.string,
  title:          PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.element,

  ]),
  isTitle:      PropTypes.bool,
  actionsLeft:  PropTypes.array,
  actionsRight: PropTypes.array,
  tools:        PropTypes.array,
};

const defaultProps = {
  className:             '',
  titleClassName:        '',
  captionClassName:      '',
  bodyClassName:         '',
  title:                 '',
  isTitle:               true,
  actionsLeft:           [],
  actionsRight:          [],
  tools:                 [],
  actionsLeftClassName:  '',
  actionsRightClassName: '',
};

const Portlet = ({
  className,
  titleClassName,
  captionClassName,
  bodyClassName,
  title,
  isTitle,
  actionsLeft,
  actionsRight,
  actionsLeftClassName,
  actionsRightClassName,
  tools,
  children,
}) => {
  return (
    <div className={classNames('portlet', className)}>
      {isTitle &&
        <div className={classNames('portlet-title', titleClassName)}>
          {title &&
            <div className="caption">
              {Array.isArray(title)
                ? title.map(val => val)
                : title}
              {/* <i className="fa fa-bell-o"></i>Advance Table  */}
            </div>
          }
          {actionsLeft.length > 0 &&
            <div style={{float:'left'}} className={classNames('actions', actionsLeftClassName)}>
              {/* <div className="btn-group btn-group-devided" data-toggle="buttons"> */}
                {actionsLeft.map(val => val)}
              {/* </div> */}
            </div>
          }
          {actionsRight.length > 0 &&
            <div className={classNames('actions', actionsRightClassName)}>
              {/* <div className="btn-group btn-group-devided" data-toggle="buttons"> */}
                {actionsRight.map(val => val)}
              {/* </div> */}
            </div>
          }
          {tools.length > 0 &&
            <div className="tools">
              {tools.map(val => val)}
              {/* <a href="" className="collapse" data-original-title="" title=""> </a>
              <a href="#portlet-config" data-toggle="modal" className="config" data-original-title="" title=""> </a>
              <a href="" className="reload" data-original-title="" title=""> </a>
              <a href="" className="fullscreen" data-original-title="" title=""> </a>
              <a href="" className="remove" data-original-title="" title=""> </a> */}
            </div>
          }
        </div>
      }


    <div className={classNames('portlet-body', bodyClassName)}>
      {children}
    </div>
  </div>
  );
};

Portlet.propTypes = propTypes;
Portlet.defaultProps = defaultProps;

export default Portlet;
