import React, {PropTypes} from 'react';

const PortletBody = ({ children }) => {
  return (
    <div className="portlet-body">
      <div className="tabbable-custom">
        <div className="portlet light bordered">
          {children}
        </div>
      </div>
    </div>
  );
};

export default PortletBody;
