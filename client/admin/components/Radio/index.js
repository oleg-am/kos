import React, { PropTypes } from 'react';
import classNames from 'classnames';

const Radio = () => {
 //  const {
 //    title = '',
 //    actionsLeft = [],
 //    actionsRight = [],
 // } = components;
  return (
    <div style={{paddingTop: 0, paddingBottom: '10px'}} className="form-group form-md-line-input">
      <label className="col-md-2 control-label" htmlFor="htmlForm_control_1">Варіанти відображення</label>
      <div className="col-md-10">
          <div className="md-radio-inline">
              <div className="md-radio">
                  <input onClick={this.setOption} type="radio" id="radio53" name="radio2" checked={this.state.option == 1} value="1" className="md-radiobtn" />
                  <label htmlFor="radio53">
                      <span className="inc"></span>
                      <span className="check"></span>
                      <span className="box"></span> 1 Таблиці </label>
              </div>
              <div className="md-radio">
                  <input onClick={this.setOption} type="radio" id="radio54" name="radio2" checked={this.state.option == 2} value="2" className="md-radiobtn" />
                  <label htmlFor="radio54">
                      <span className="inc"></span>
                      <span className="check"></span>
                      <span className="box"></span> 2 Таблиця + форми </label>
              </div>
              <div className="md-radio">
                  <input onClick={this.setOption} type="radio" id="radio55" name="radio2" checked={this.state.option == 3} value="3" className="md-radiobtn" />
                  <label htmlFor="radio55">
                      <span className="inc"></span>
                      <span className="check"></span>
                      <span className="box"></span> Option 3 </label>
              </div>
          </div>
      </div>
    </div>
  );
};

export default Radio;
