import React, { PropTypes } from 'react';

const propTypes = {
  children: PropTypes.node,
};

const SettingsLayout = ({ children }) => (
  <div className="page-content">
    {children}
  </div>
);

SettingsLayout.propTypes = propTypes;

export default SettingsLayout;
