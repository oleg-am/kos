import React, {PropTypes} from 'react';

const Table = ({ children }) => {

  return (
    <div className="table-scrollable">
      <table className="table table-responsive table-hover">
        <colgroup>
          <col width="5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
          <col width="9.5%" />
        </colgroup>
        <thead>
          <tr>
            <th> № Заказа </th>
            <th> Дата поставки </th>
            <th> Время поставки </th>
            <th> Аптека(и) </th>
            <th> Сумма </th>
            <th> Клиент ФИО </th>
            <th> Конт.тел </th>
            <th> E-mail </th>
            <th> Оператор </th>
            <th> Статус </th>
            <th> Источник </th>
            <th> Комментарий </th>
            {/* <th name="publish-head"> Публикация </th> */}
          </tr>
        </thead>
        <tbody>
          {children}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
