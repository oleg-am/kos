import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

// const propTypes = {
//   className:      PropTypes.string,
//   titleClassName: PropTypes.string,
//   captionClassName: PropTypes.string,
//   bodyClassName:  PropTypes.string,
//   title:          PropTypes.oneOfType([
//     PropTypes.string,
//     PropTypes.array,
//     PropTypes.element,
//
//   ]),
//   actionsLeft:  PropTypes.array,
//   actionsRight: PropTypes.array,
//   tools:        PropTypes.array,
// };
//
// const defaultProps = {
//   className:      '',
//   titleClassName: '',
//   captionClassName: '',
//   bodyClassName: '',
//   title:          '',
//   actionsLeft:    [],
//   actionsRight:   [],
//   tools:          [],
//   actionsLeftClassName: '',
//   actionsRightClassName: '',
// };

const XEdit = ({
  className,
  titleClassName,
  captionClassName,
  bodyClassName,
  title,
  actionsLeft,
  actionsRight,
  actionsLeftClassName,
  actionsRightClassName,
  tools,
  children,
}) => {
  return (

  // {/* <span className="editable-container editable-inline" > */}
  //   <div>
  //     {/* <div className="editableform-loading" style={{display: 'block'}}></div> */}
  //     <div className="form-group">
  //       <form className="form-inline" >
  //         {/* <div> */}
  //           <div className="editable-input">
  //             {/* <input type="text" className="form-control" style="display: none;" /> */}
  //             <span className="combodate">
  //               <select className="form-control day">
  //                 <option value=""></option>
  //                 <option value="1">1</option>
  //                 <option value="29">29</option>
  //                 <option value="30">30</option>
  //                 <option value="31">31</option>
  //               </select>
  //               <select className="form-control day">
  //                 <option value=""></option>
  //                 <option value="1">1</option>
  //                 <option value="29">29</option>
  //                 <option value="30">30</option>
  //                 <option value="31">31</option>
  //               </select>
  //             </span>
  //           </div>
  //           </form>
  //           <div>
  //             <button className="btn blue">
  //               <i className="fa fa-check"></i>
  //             </button>
  //             <button className="btn default">
  //               <i className="fa fa-times"></i>
  //             </button>
  //           </div>
  //         {/* </div> */}
  //       </div>
  //   </div>
  {/* </span> */}
  );
};

XEdit.propTypes = propTypes;
XEdit.defaultProps = defaultProps;

export default XEdit;
