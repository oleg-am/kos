import React from 'react';
import {Link} from 'react-router';
import moment from '../../helpers/moment';

// console.log(moment().format());
const Row = ({ data, click, statusClass, editable, handleChangeOrderParam, totalSum, pharmacies, statusObj }) => {
  const onChangeOrderParam = (param, e) => {
    handleChangeOrderParam(param, e.target.value)
  }
  // console.log( data.pharmacy);
    return (
        <tr onClick={click}>
        {/* {console.log('data.id', data.id)} */}
          <td>{data.id}</td>
          <td>{moment(data.date).format('L')}</td>
          <td>{moment(data.time).format('LTS')}</td>
          {/* {console.log('data.pharmacy', data.pharmacy)} */}
          <td>
            {editable ?
              (<div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input has-info">
                <select value={data.pharmacy} style={{ height: 'auto', padding: '2px 8px 2px 12px'}} className="form-control" onChange={onChangeOrderParam.bind(this, 'pharmacy')} >
                {data.pharmacy == -1 && <option key={data.pharmacy} value={data.pharmacy}>Виберіть аптеку</option>}
                {Object.keys(pharmacies).map(id=>(
                  <option key={pharmacies[id].id} value={pharmacies[id].id}>{pharmacies[id].name}</option>
                ))}
                </select>
                <label></label>
            </div>)
              : (pharmacies[data.pharmacy] ? pharmacies[data.pharmacy].name : '')
            }
          </td>

          <td>{totalSum || data.sum}</td>
          <td>
            {editable ?
              (<div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input">
                <input className="form-control" style={{ height: 'auto', padding: '2px 8px 2px 12px'}} type="text" value={data.fullName} onChange={onChangeOrderParam.bind(this, 'fullName')} />
                <label></label>
              </div>)
              :
              data.fullName
            }
          </td>
          <td>
            {editable ?
              (<div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input">
                <input className="form-control" style={{ height: 'auto', padding: '2px 8px 2px 12px'}} type="text" value={data.phone} onChange={onChangeOrderParam.bind(this, 'phone')} />
                <label></label>
              </div>)
              :
              data.phone
            }
          </td>
          <td>
            {editable ?
              (<div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input">
                <input className="form-control" style={{ height: 'auto', padding: '2px 8px 2px 12px'}} type="text" value={data.email} onChange={onChangeOrderParam.bind(this, 'email')} />
                <label></label>
              </div>)
              :
              data.email
            }
          </td>

          {/* <td>{data.email}</td> */}
          <td>{data.operator}</td>
          {/* <td style={statusStyle} >{data.status}</td> */}
          <td>
            <span style={{whiteSpace: 'normal', display: 'block'}} className={`label label-lg ${statusClass}`}> {statusObj[data.status].name} </span>
          </td>
          <td>
            {editable ?
              (<div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input">
                <input className="form-control" style={{ height: 'auto', padding: '2px 8px 2px 12px'}} type="text" value={data.source} onChange={onChangeOrderParam.bind(this, 'source')} />
                <label></label>
              </div>)
              :
              data.source
            }
          </td>
          <td>
            {editable ?
              (<div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input">
                <input className="form-control" style={{ height: 'auto', padding: '2px 8px 2px 12px'}} type="text" value={data.comment} onChange={onChangeOrderParam.bind(this, 'comment')} />
                <label></label>
              </div>)
              :
              data.comment
            }
          </td>

            {/* <td>{data.id}</td>
            <td>{data.article}</td>
            <td>{data.name}</td>
            <td>{data.manufacturer.name}</td>
            <td>
                <Link className="btn btn-sm green-meadow" to={'/admin/dictionary/nomenclature/' + data.id}><i className="fa fa-edit"></i></Link>
            </td>
            <td>
                <label className="mt-checkbox">
                    <input type="checkbox" />
                    <span></span>
                </label>
            </td> */}
        </tr>
    );
};

export default Row;
