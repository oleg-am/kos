import React from 'react';
import {Link} from 'react-router';

const RowGoods = ({
  data,
  className,
  style,
  handlerCheckCountQuantity,
  handlerCheckCountPharmacy,
  handlerCheckCountStock,
  editable,
}) => {
  const handlerChangeCountQuantity = (e) => {
    // console.log(e.target.value)
    handlerCheckCountQuantity(+e.target.value)
  }
  const handlerChangeCountPharmacy = (e) => {
    // console.log(e.target.value)
    handlerCheckCountPharmacy(+e.target.value)
  }
  const handlerChangeCountStock = (e) => {
    // console.log(e.target.value)
    handlerCheckCountStock(+e.target.value)
  }
  const getbadge = (className, value) => (
    <span className={`pull-right badge badge-${className}`}>{className == 'warning' ? '+': ''}{value}</span>
  )
  const getDeficit = (value) => {
    if (!value && value != 0) {
      return '';
    } else if (value == 0) {
      return getbadge('success', value);
    } else if (value > 0) {
      return getbadge('warning', value);
    } else if (value < 0) {
      return getbadge('danger', value);
    }
  }
  const renderEditableCell = (data, cell, value, deficitValue, renderDeficit, onChange) => {
    // console.log('cell: ', cell, 'deficitValue: ', deficitValue);
    const stock = cell ? `${cell.substring(5).toLowerCase()}Stock` : '999999';
    // console.log('stock: ', stock);
    return (
      data.name == 'Итого' || !editable ?
      <span style={{display: 'block', marginLeft: '10px'}}>{value} {renderDeficit && getDeficit(+deficitValue)}</span>
      :
      (<div>
        <div style={{'padding': '0px', 'marginBottom': '0px', width: '65%'}} className="pull-left form-group form-md-line-input">
          <input min="0" max={data[stock]} className="form-control" style={{ height: 'auto', padding: '2px 8px 2px 12px'}} type="number" value={value} onChange={onChange} />
          <label></label>
        </div>
        {getDeficit(+deficitValue)}
      </div>)
    )
  }
    return (
        <tr className={className} style={style}>
          {/* <td>
              <label className="mt-checkbox">
                  <input type="checkbox" />
                  <span></span>
              </label>
          </td> */}

          <td>{data.name}</td>
          <td>{data.reservePrice}</td>
          {/* <td>{data.quantity}</td> */}
          <td>
            {renderEditableCell(data, '', data.quantity, data.countDeficit, true,  handlerChangeCountQuantity)}
          </td>
          <td>
            {renderEditableCell(data, "countPharmacy",  data.countPharmacy, data.pharmacyDeficit, false, handlerChangeCountPharmacy)}
          </td>
          <td>
            {renderEditableCell(data, "countStorage", data.countStorage, data.stockDeficit, false, handlerChangeCountStock)}
          </td>
          {/* <td>{data.countStorage}</td> */}
          {/* <td>
            {getDeficit(+data.deficit)}
          </td> */}
          <td>{data.price}</td>
          <td>{data.pharmacyStock}</td>
          <td>{data.storageStock}</td>

            {/* <td>{data.id}</td>
            <td>{data.article}</td>
            <td>{data.name}</td>
            <td>{data.manufacturer.name}</td>*/}
            {/* <td>
                <Link className="btn btn-sm green-meadow" to={'/admin/dictionary/nomenclature/' + data.id}><i className="fa fa-edit"></i></Link>
            </td> */}
            <td>
              {data.name != 'Итого' && <Link
                className="btn btn-sm green-meadow" >
                  <i style={{verticalAlign: '-3px'}} className="hover fa fa-search fa-lg"></i>
              </Link>}
            </td>
            {/* <td>
                <label className="mt-checkbox">
                    <input type="checkbox" />
                    <span></span>
                </label>
            </td>  */}
        </tr>
    );
};

export default RowGoods;
