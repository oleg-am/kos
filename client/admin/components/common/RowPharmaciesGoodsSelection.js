import React from 'react';
import {Link} from 'react-router';

const RowPharmaciesGoodsSelection = ({data, checkHandler}) => {
  const onChange = (e) => {
    checkHandler(e);
  }
    return (
        <tr>
          <td>
              <label className="mt-checkbox">
                  <input onChange={onChange} type="checkbox" />
                  <span></span>
              </label>
          </td>

          <td>{data.name}</td>
          <td>{data.pharmacyStock}</td>
          {/* <td>{data.storageStock}</td> */}
          <td>{data.reservePrice}</td>
          {/* <td>
              <Link className="btn btn-sm green-meadow" to={'/admin/pharmacies/product/' + data.id}>
                <i className="fa fa-edit"></i> Найти
              </Link>
          </td> */}


            {/* <td>{data.id}</td>
            <td>{data.article}</td>
            <td>{data.name}</td>
            <td>{data.manufacturer.name}</td>
            <td>
                <Link className="btn btn-sm green-meadow" to={'/admin/dictionary/nomenclature/' + data.id}><i className="fa fa-edit"></i></Link>
            </td>
            <td>
                <label className="mt-checkbox">
                    <input type="checkbox" />
                    <span></span>
                </label>
            </td> */}
        </tr>
    );
};

export default RowPharmaciesGoodsSelection;
