import React from 'react';
import {Link} from 'react-router';
import moment from '../../helpers/moment';

const TdEditable = ({ editable = false, dataValue, onChangeValue }) => {
  const handleChange = (e) => {
    onChangeValue(e)
  }
  return (
    <td>
      {editable ?
        (<div style={{'padding': '0px', 'marginBottom': '0px'}} className="form-group form-md-line-input">
          <input className="form-control" style={{ height: 'auto', padding: '2px 8px 2px 12px'}} type="text" value={dataValue} onChange={handleChange.bind(this)} />
          <label></label>
        </div>)
        :
        dataValue
      }
    </td>
  )
};

export default TdEditable;
