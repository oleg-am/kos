export * as app from './containers/App/constants';

export * as orders from './containers/Orders/constants';
export * as ordersItem from './containers/OrdersItem/constants';

export * as orderInfo from './containers/OrderInfo/constants';

export * as nomenclatureSelectionInPharmacy from './containers/NomenclatureSelectionInPharmacy/constants';
export * as goodsSelection from './containers/GoodsSelection/constants';
export * as goodsSelectionInPharmacies from './containers/PharmaciesGoodsSelection/constants';


export * as nomenclature from './containers/Nomenclature/constants';
export * as nomenclatureItem from './containers/NomenclatureItem/constants';
export * as nomenclatureRemainders from './containers/NomenclatureRemainders/constants';

export * as pharmacies from './containers/Pharmacies/constants';
export * as pharmaciesMap from './containers/PharmaciesMap/constants';
export * as pharmaciesItem from './containers/PharmaciesItem/constants';
export * as pharmaciesRemainders from './containers/PharmaciesRemainders/constants';

export * as clients from './containers/Clients/constants';
export * as clientItem from './containers/ClientItem/constants';

export * as marketPlace from './containers/MarketPlace/constants';
export * as marketPlaceItem from './containers/MarketPlaceItem/constants';
export * as users from './containers/Users/constants';
export * as usersItem from './containers/UsersItem/constants';
export * as userRoles from './containers/UserRoles/constants';
export * as userRoleItem from './containers/UserRoleItem/constants';
export * as configGeneral from './containers/ConfigGeneral/constants';
