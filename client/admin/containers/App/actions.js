import { browserHistory } from 'react-router';

import * as types from './constants';
import { CONFIGS_ARR, STATUSES_ARR } from './schemas';

export const showSuccessMessage = ({ messages, meta }) => ({
  type: types.SCHOW_SUCCESS_MESSAGE, messages, meta,
});

export const showErrorMessage = ({ messages, meta }) => ({
  type: types.SCHOW_ERROR_MESSAGE, messages, meta,
});

export const closeMessage = () => ({
  type: types.CLOSE_MESSAGE,
});

export const changeConfigsParam = (key, value) => ({
  type: types.CHANGE_CONFIGS_PARAM,
  key,
  value,
});

export const browserHystoryGoBack = () => {
  browserHistory.goBack();
  return ({
    type: types.BROWSER_HYSTORY_GO_BACK,
  });
};

// export const load = () => ({
//   types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
//   promise: api => api.get('/api/me/check_rights'),
// });

export const loadUser = () => ({
  types:   [types.LOAD_USER_REQUEST, types.LOAD_USER_SUCCESS, types.LOAD_USER_FAILURE],
  promise: api => api.get('/api/me/check_rights'),
});

export const loadUserInfo = () => ({
  types: [
    types.LOAD_USER_INFO_REQUEST,
    types.LOAD_USER_INFO_SUCCESS,
    types.LOAD_USER_INFO_FAILURE,
  ],
  promise: api => api.get('/api/me/info'),
});

export const loadConfigs = () => ({
  types: [
    types.LOAD_CONFIGS_REQUEST,
    types.LOAD_CONFIGS_SUCCESS,
    types.LOAD_CONFIGS_FAILURE,
  ],
  promise: api => api.get('/api/admin/configs'),
  schema:  CONFIGS_ARR,
});

export const loadStatuses = () => ({
  types:   [types.LOAD_STATUSES_REQUEST, types.LOAD_STATUSES_SUCCESS, types.LOAD_STATUSES_FAILURE],
  promise: api => api.get('/api/admin/order_statuses'),
  schema:  STATUSES_ARR,
});

export const loadImgUrl = () => ({
  types:   [types.LOAD_IMG_URL_REQUEST, types.LOAD_IMG_URL_SUCCESS, types.LOAD_IMG_URL_FAILURE],
  promise: api => api.get('/api/img-url'),
});

export const logOut = () => ({
  types:   [types.LOAD_LOG_OUT_REQUEST, types.LOAD_LOG_OUT_SUCCESS, types.LOAD_LOG_OUT_FAILURE],
  promise: api => api.get('/oauth/logout'),
});
