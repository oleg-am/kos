const name = 'app';

// export const LOAD_REQUEST = `${name}/LOAD_REQUEST`;
// export const LOAD_SUCCESS = `${name}/LOAD_SUCCESS`;
// export const LOAD_FAILURE = `${name}/LOAD_FAILURE`;
export const LOAD_USER_REQUEST = `${name}/LOAD_USER_REQUEST`;
export const LOAD_USER_SUCCESS = `${name}/LOAD_USER_SUCCESS`;
export const LOAD_USER_FAILURE = `${name}/LOAD_USER_FAILURE`;

export const LOAD_USER_INFO_REQUEST = `${name}/LOAD_USER_INFO_REQUEST`;
export const LOAD_USER_INFO_SUCCESS = `${name}/LOAD_USER_INFO_SUCCESS`;
export const LOAD_USER_INFO_FAILURE = `${name}/LOAD_USER_INFO_FAILURE`;

export const LOAD_CONFIGS_REQUEST = `${name}/LOAD_CONFIGS_REQUEST`;
export const LOAD_CONFIGS_SUCCESS = `${name}/LOAD_CONFIGS_SUCCESS`;
export const LOAD_CONFIGS_FAILURE = `${name}/LOAD_CONFIGS_FAILURE`;

export const LOAD_STATUSES_REQUEST = `${name}/LOAD_STATUSES_REQUEST`;
export const LOAD_STATUSES_SUCCESS = `${name}/LOAD_STATUSES_SUCCESS`;
export const LOAD_STATUSES_FAILURE = `${name}/LOAD_STATUSES_FAILURE`;

export const LOAD_IMG_URL_REQUEST = `${name}/LOAD_IMG_URL_REQUEST`;
export const LOAD_IMG_URL_SUCCESS = `${name}/LOAD_IMG_URL_SUCCESS`;
export const LOAD_IMG_URL_FAILURE = `${name}/LOAD_IMG_URL_FAILURE`;

export const LOAD_LOG_OUT_REQUEST = `${name}/LOAD_LOG_OUT_REQUEST`;
export const LOAD_LOG_OUT_SUCCESS = `${name}/LOAD_LOG_OUT_SUCCESS`;
export const LOAD_LOG_OUT_FAILURE = `${name}/LOAD_LOG_OUT_FAILURE`;

export const SCHOW_SUCCESS_MESSAGE = `${name}/SCHOW_SUCCESS_MESSAGE`;
export const SCHOW_ERROR_MESSAGE   = `${name}/SCHOW_ERROR_MESSAGE`;

export const CLOSE_MESSAGE = `${name}/CLOSE_MESSAGE`;

export const CHANGE_CONFIGS_PARAM = `${name}/CHANGE_CONFIGS_PARAM`;

export const BROWSER_HYSTORY_GO_BACK = `${name}/BROWSER_HYSTORY_GO_BACK`;

export default name;
