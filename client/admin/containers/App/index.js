import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import cookie from 'react-cookie';

import Alert from 'react-s-alert';
import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';

import Spinner from '../../components/Spinner';
import Header from '../../components/Header';

import ruLng from '../../i18n/ru';
import uaLng from '../../i18n/ua';

const { node, number, string, arrayOf, shape, oneOfType, func } = PropTypes;

const translate = (lngFrom = {}, lngTo = {}) => (msges = []) => (
  msges.map((msg) => {
    const key = Object.keys(lngFrom).find(lngKey => lngFrom[lngKey] === msg);
    return lngTo[key] || msg;
  })
);

const tranlateFromRuToUa = translate(ruLng, uaLng);

const MARGIN_RIGHT_5PX = { marginRight: '5px' };
const SaveIcon = () => <i style={MARGIN_RIGHT_5PX} className="fa fa-check" />;
const ErrorIcon = () => <i style={MARGIN_RIGHT_5PX} className="fa fa-exclamation-triangle" />;

class App extends Component {
  static propTypes = {
    children: node.isRequired,
    version:  number,
    userInfo: shape({
      id: oneOfType([number, string]),
    }),
    user: shape({
      name:   string,
      avatar: string,

      role: shape({
        name: string,
        id:   oneOfType([number, string]),
      }),
    }),
    meta:         string,
    messages:     arrayOf(string),
    loadConfigs:  func.isRequired,
    loadStatuses: func.isRequired,
    loadImgUrl:   func.isRequired,
    loadUser:     func.isRequired,
    loadUserInfo: func.isRequired,

    browserHystoryGoBack: func.isRequired,
  }

  constructor(props) {
    super(props);

    const { loadConfigs, loadStatuses, loadImgUrl, loadUser, loadUserInfo } = this.props;

    this.debounceSetLogOutInterval = _debounce(this.setLogOutInterval, 400);

    loadConfigs();
    loadStatuses();
    loadImgUrl();
    loadUser();
    loadUserInfo();
  }

  componentDidMount() {
    // Logout user after 15 min idle
    this.debounceSetLogOutInterval({});

    const eventListener = () => { this.debounceSetLogOutInterval({}); };

    ['mousedown', 'keydown', 'touchstart'].forEach((event) => {
      window.addEventListener(event, eventListener);
    });

    // When press Esc, then go back in history
    window.addEventListener('keyup', (e) => {
      if (e.keyCode === 27 && e.target.nodeName !== 'INPUT') {
        this.props.browserHystoryGoBack();
      }
    });
  }

  componentWillReceiveProps(next) {
    const {
      isSuccessMessage,
      isErrorMessage,
      closeMessage,
      meta,
      messages,
    } = next;

    if (isSuccessMessage) {
      const type = 'success';
      const title = meta === 'save'
        ? <span> <SaveIcon /> Дані збережені </span>
        : null;

      this.showMessage({ type, title, messages });
      closeMessage();
    }

    if (isErrorMessage) {
      const type = 'error';
      const title = (
        <span>
          <ErrorIcon />
          { meta === 'save' ? 'Помилка. Дані не збережені!' : 'Помилка:' }
        </span>
      );
      // const title = meta === 'save'
      // ? <span> <ErrorIcon /> Помилка. Дані не збережені! </span>
      // : <span> <ErrorIcon /> Помилка: </span>;

      this.showMessage({ type, title, messages: tranlateFromRuToUa(messages) });
      closeMessage();
    }
  }

  getMessage = (message) => {
    const MESSAGES = {
      'Error code 401: you are not authenticated':
        'Ваша сесія закінчилася. Ви будете перенаправлені на сторінку входу',
    };
    return MESSAGES[message] || message;
  }

  showMessage = ({ type, title, messages }) => {
    Alert[type](
      <div>
        {title}
        {this.renderMessages(messages)}
      </div>,
    );
  }

  isUserIdle = () => (
    !!cookie.load('idle')
  )

  setLogOutInterval = ({
    //                 min   sec
    cookieMaxAge     = 44 * 60,
    timeoutTime      = 45 * 60 * 1000,
    timeoutTimeShort = 2 * 60 * 1000,
  }) => {
    this.timeoutTimer && clearTimeout(this.timeoutTimer);
    this.timeoutInterval && clearInterval(this.timeoutInterval);

    cookie.save('idle', 'not idle', { maxAge: cookieMaxAge });

    this.timeoutTimer = setTimeout(() => {
      if (!this.isUserIdle()) {
        window.location.href = '/oauth/logout';
      } else {
        this.timeoutInterval = setInterval(() => {
          if (!this.isUserIdle()) {
            window.location.href = '/oauth/logout';
          }
        }, timeoutTimeShort);
      }
    }, timeoutTime);
  }

  renderMessages = messages => (
    messages.length
      ? <ul style={{ marginTop: '15px' }}>
        {messages.map((message, i) =>
          <li key={`message_${i}`}> {this.getMessage(message)} </li>,
        )}
      </ul>
      : null
  )

  render() {
    const { user, userInfo, version } = this.props;

    if (!user.role.id) {
      return <Spinner />;
    }

    return (
      <div className="wrapper">
        <Header
          version={version}
          user={user}
          userInfo={userInfo}
          whatComponentIsOpen={this.props.children.props.routes[1]}
        />
        <div className="container-fluid">
          {this.props.children}
        </div>
        <Alert
          position="top-right"
          effect="scale" stack={{ limit: 3 }}
          // timeout="none"
          timeout={3000}
        />
      </div>
    );
  }
}

export default connect(selectors, actions)(App);
