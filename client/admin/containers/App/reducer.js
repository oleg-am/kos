import { combineReducers } from 'redux';
import * as types from './constants';

import { setValue } from '../../helpers/setObjValue';

const initialState = {
  version:          '',
  imgUrl:           '',
  isSuccessMessage: false,
  isErrorMessage:   false,
  meta:             '',
  messages:         [],

  userInfo: {
    id: '',
  },
  user: {
    name:   '',
    avatar: '',
    role:   {
      name: '',
      id:   '',

      chiefOperator: false,
    },
  },
  configs: {
    basicDiscount: {
      value: '0',
    },
    ordersAutoReloadInterval: {
      value: '60',
    },
    ordersAutoReloadEnable: {
      value: 'false',
    },
    pharmacyInactivityInterval: {},
    smsEnabled:                 {},
    smsFrom:                    {},
    smsLogin:                   {},
    smsPassword:                {},
    smsText:                    {},
    smsURL:                     {},
  },
  statuses: {},
};

export default combineReducers({
  version(state = initialState.version, action) {
    switch (action.type) {
      default:
        return state;
    }
  },
  imgUrl(state = initialState.imgUrl, action) {
    switch (action.type) {
      case types.LOAD_IMG_URL_SUCCESS:
        return action.data;
      default:
        return state;
    }
  },
  user(state = initialState.user, action) {
    switch (action.type) {
      case types.LOAD_USER_SUCCESS:
        return action.data;
      default:
        return state;
    }
  },
  userInfo(state = initialState.userInfo, action) {
    switch (action.type) {
      case types.LOAD_USER_INFO_SUCCESS:
        return { ...state, ...action.data };
      default:
        return state;
    }
  },
  isSuccessMessage(state = initialState.isSuccessMessage, action) {
    switch (action.type) {
      case types.SCHOW_SUCCESS_MESSAGE:
        return true;
      case types.CLOSE_MESSAGE:
        return false;
      default:
        return state;
    }
  },
  isErrorMessage(state = initialState.isErrorMessage, action) {
    switch (action.type) {
      case types.SCHOW_ERROR_MESSAGE:
        return true;
      case types.CLOSE_MESSAGE:
        return false;
      default:
        return state;
    }
  },
  configs(state = initialState.configs, action) {
    switch (action.type) {
      case types.LOAD_CONFIGS_SUCCESS:
        return {
          ...state,
          ...action.entities.configs,
        };
      case types.CHANGE_CONFIGS_PARAM:
        return {
          ...state,
          ...setValue(state, action.key, action.value),
        };
      default:
        return state;
    }
  },
  statuses(state = initialState.statuses, action) {
    switch (action.type) {
      case types.LOAD_STATUSES_SUCCESS:
        return {
          ...state,
          ...action.entities.statuses,
        };
      default:
        return state;
    }
  },
  meta(state = initialState.meta, action) {
    switch (action.type) {
      case types.SCHOW_ERROR_MESSAGE:
      case types.SCHOW_SUCCESS_MESSAGE:
        return action.meta || '';
      case types.CLOSE_MESSAGE:
        return '';
      default:
        return state;
    }
  },
  messages(state = initialState.messages, action) {
    switch (action.type) {
      case types.SCHOW_ERROR_MESSAGE:
      case types.SCHOW_SUCCESS_MESSAGE:
        return action.messages;
      case types.CLOSE_MESSAGE:
        return [];
      default:
        return state;
    }
  },
});
