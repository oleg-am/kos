import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

import * as actions from '../actions';
import { configGeneral } from '../../../constants';

function* changeConfigsParam({ key, value }) {
  yield put(actions.changeConfigsParam(key, value));
}

function* toggleOrdersAutoReloadEnable({ param, data }) {
  if (param === 'ordersAutoReloadEnable') {
    yield put(actions.changeConfigsParam(['ordersAutoReloadEnable', 'value'], data.value));
  }
  if (param === 'smsEnabled') {
    yield put(actions.changeConfigsParam(['smsEnabled', 'value'], data.value));
  }
}

function* rootSagas() {
  yield [
    takeEvery(configGeneral.CHANGE_PARAM, changeConfigsParam),
    takeEvery(configGeneral.SAVE_SUCCESS, toggleOrdersAutoReloadEnable),
  ];
}

export default rootSagas;
