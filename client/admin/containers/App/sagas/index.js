import { fork } from 'redux-saga/effects';

import messages from './messages';
import configs from './configs';
import redirect from './redirect';

function* rootSagas() {
  yield [
    fork(messages),
    fork(configs),
    fork(redirect),
  ];
}

export default rootSagas;
