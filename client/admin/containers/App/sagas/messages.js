import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

import { SCHOW_SUCCESS_MESSAGE } from '../constants';
import * as actions from '../actions';
import * as projectConstants from '../../../constants';

const constants = Object.values(projectConstants).reduce((arr, containerConstants) => ([
  ...arr, ...Object.values(containerConstants),
]), []);

const EXCEPTIONS = [
  SCHOW_SUCCESS_MESSAGE,
  projectConstants.configGeneral.LOAD_LOG_FAILURE,
];
const LOAD_SUCCESS_ACTIONS = constants.filter(item => (/LOAD.*SUCCESS/.test(item)));

const SAVE_SUCCESS_ACTIONS = constants.filter(item => (
  !LOAD_SUCCESS_ACTIONS.includes(item) &&
  !EXCEPTIONS.includes(item) &&
  /_SUCCESS/.test(item)
));

export const FAILURE_ACTIONS = constants.filter(item => (
  !EXCEPTIONS.includes(item) &&
  /_FAILURE/.test(item)
));

// const SAVE_ERROR_ACTIONS = SAVE_SUCCESS_ACTIONS.map(a => a.replace('_SUCCESS', '_FAILURE'));

function* showSuccessMessage(action) {
  yield put(actions.showSuccessMessage({
    meta:     'save',
    messages: action.successMessage || [],
  }));
}

const showErrorMessage = meta => (
  function* showErrorMessageGen(action) {
    // якщо в екшині, який робить запрос вказати: { isDismissMessage: true },
    // то повідомлення з серверною помилкою виводитись не буде
    if (action.isDismissMessage === undefined) {
      const { LOAD_TAKE_ORDER_FAILURE } = projectConstants.orders;

      const messages = action.type === LOAD_TAKE_ORDER_FAILURE
        ? ['Нових замовлень не знайдено']
        : action.errors || [];

      yield put(actions.showErrorMessage({ meta, messages }));
    }
  }
);

function* rootSagas() {
  yield [
    takeEvery(SAVE_SUCCESS_ACTIONS, showSuccessMessage),
    // takeEvery(SAVE_FAILURE_ACTIONS, showErrorMessage('save')),
    takeEvery(FAILURE_ACTIONS, showErrorMessage('')),
  ];
}

export default rootSagas;
