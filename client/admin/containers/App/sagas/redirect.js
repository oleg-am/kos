import { takeEvery } from 'redux-saga';
import { browserHistory } from 'react-router';

import { LOAD_USER_SUCCESS } from '../constants';
import { FAILURE_ACTIONS } from './messages';

const ROLE_ROUTES = {
  dashboard:    '/admin',
  reports:      '/admin/reports',
  orders:       '/admin/orders',
  nomenclature: '/admin/nomenclature',
  pharmacies:   '/admin/pharmacies',
  clients:      '/admin/clients',
  // config:       '/admin/settings/general',
};

const ERROR_MSG = 'Error code 401: you are not authenticated';

function* redirectToLoginPage({ errors }) {
  if (errors && Array.isArray(errors) && errors.find(item => item === ERROR_MSG)) {
    yield setTimeout(() => { window.location.href = '/oauth/login'; }, 3000);
    // location.reload();
  }
}

// [KSM-234] Шукаємо найближчу сторінку, до якої є доступ: редагування або перегляд
function* redirectToAvailablePage({ data: { role } }) {
  // Якщо робочий стіл заблокований
  const isBlockedDashboardPage = role && ['R', 'N'].includes(role.dashboard);
  // І переходимо на робочий стіл
  const isCarrentPageDashboard = ~window.location.href.search(/(admin|admin\/)$/);

  if (isBlockedDashboardPage && isCarrentPageDashboard) {
    const availablePage = Object.keys(ROLE_ROUTES).find(page => ['V', 'W'].includes(role[page]));

    if (availablePage) {
      yield browserHistory.push(ROLE_ROUTES[availablePage]);
    }
  }
}

function* rootSagas() {
  yield [
    takeEvery(FAILURE_ACTIONS, redirectToLoginPage),
    takeEvery(LOAD_USER_SUCCESS, redirectToAvailablePage),
  ];
}

export default rootSagas;
