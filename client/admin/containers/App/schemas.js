import { Schema, arrayOf } from 'normalizr';

const configs = new Schema('configs', { idAttribute: 'param' });
const statuses = new Schema('statuses', { idAttribute: 'name' });

export const CONFIGS_ARR = { data: arrayOf(configs) };
export const STATUSES_ARR = { data: arrayOf(statuses) };
