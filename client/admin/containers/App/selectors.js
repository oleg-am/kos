import { createSelector, createStructuredSelector } from 'reselect';

const REDUCER = 'app';

const isSuccessMessage = state => state[REDUCER].isSuccessMessage;
const isErrorMessage   = state => state[REDUCER].isErrorMessage;
const meta             = state => state[REDUCER].meta;
const messages         = state => state[REDUCER].messages;

const user = state => state[REDUCER].user;
const userInfo = state => state[REDUCER].userInfo;

const version = state => state[REDUCER].version;

// const imgUrl            = state => state[REDUCER].imgUrl;

// const userRoles = createSelector(
//   userRolesEntities,
//   userRolesIds,
//   (items, ids) => ids.map(id => items[id]),
// );

export default createStructuredSelector({
  isSuccessMessage,
  isErrorMessage,
  meta,
  messages,
  user,
  userInfo,
  version,
  // imgUrl,
});
