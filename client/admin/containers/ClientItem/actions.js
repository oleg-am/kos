import * as types from './constants';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});

export const goToClients = () => ({
  type: types.GO_TO_CLIENTS,
});

export const getValidateErrors = errors => ({
  type: types.GET_VALIDATE_ERRORS,
  errors,
});

export const load = id => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/clients/${id}`),
});

export const edit = (id, data) => ({
  types:   [types.EDIT_REQUEST, types.EDIT_SUCCESS, types.EDIT_FAILURE],
  promise: api => api.put(`/api/admin/clients/${id}`, data),
});

export const closeEditForm = () => ({
  type: types.CLOSE_EDIT_FORM,
});
