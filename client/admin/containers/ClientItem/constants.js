const MODULE = 'clientItem';

export const LOAD_REQUEST        = `${MODULE}/LOAD_REQUEST`;
export const LOAD_SUCCESS        = `${MODULE}/LOAD_SUCCESS`;
export const LOAD_FAILURE        = `${MODULE}/LOAD_FAILURE`;

export const EDIT_REQUEST        = `${MODULE}/EDIT_REQUEST`;
export const EDIT_SUCCESS        = `${MODULE}/EDIT_SUCCESS`;
export const EDIT_FAILURE        = `${MODULE}/EDIT_FAILURE`;

export const GET_VALIDATE_ERRORS = `${MODULE}/GET_VALIDATE_ERRORS`;

export const GO_TO_CLIENTS       = `${MODULE}/GO_TO_CLIENTS`;

export const CLOSE_EDIT_FORM     = `${MODULE}/CLOSE_EDIT_FORM`;
export const SET_FILTER          = `${MODULE}/SET_FILTER`;
export const CHANGE_PARAM        = `${MODULE}/CHANGE_PARAM`;

export default MODULE;
