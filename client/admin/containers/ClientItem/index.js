import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import InputElement from 'react-input-mask';
import { Link } from 'react-router';

import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';

import * as actions from './actions';
import selectors from './selectors';
import putValidator from './schemas';

import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

class ClientItem extends React.Component {
  static propTypes = {
    isEditableRoleAccess: PropTypes.bool,
    // errors: PropTypes.arrayOf(PropTypes.object).isRequired,
    client:        PropTypes.object.isRequired,
    filters:       PropTypes.object.isRequired,
    validateErors: PropTypes.object.isRequired,
    params:        PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
    load:          PropTypes.func.isRequired,
    setFilter:     PropTypes.func.isRequired,
    changeParam:   PropTypes.func.isRequired,
    edit:          PropTypes.func.isRequired,
    closeEditForm: PropTypes.func.isRequired,

    getValidateErrors: PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const { params: { id }, load } = this.props;

    load(id);
  }

  onChangeParam = (key, value) => (e) => {
    this.props.changeParam(key, value || e.target.value);
  }

  onSave = () => {
    const { client, edit, getValidateErrors, params: { id } } = this.props;
    const validationClient = {
      ...client,
      tel: client.tel.replace(/([^0-9])/g, ''),
    };
    const validData = putValidator.validate({ ...validationClient });

    if (validData) {
      edit(id, validData);
    } else {
      getValidateErrors(putValidator.getErrors());
    }
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  refreshData = () => {
    this.props.load(this.props.filters);
  }
  closeEditForm = () => {
    const { closeEditForm } = this.props;
    closeEditForm();
  }
  // reset = () => {
  //   this.props.reset();
  // }
  render() {
    const { validateErors, client, isEditableRoleAccess } = this.props;

    const renderFormGroup = ({ label, placeholder, error, component, ...rest }) => (
      <div className={classNames('form-group', { 'has-error': error })}>
        <label className="col-md-3 col-lg-3 control-label"> {label} </label>
        <div className="col-md-9 col-lg-9">
          {component === 'phone' &&
            <comp>
              <InputElement
                {...rest}
                className="form-control"
                placeholder={placeholder}
                mask="38 (099) 999-99-99"
                // value={value}
                // onChange={onChange}
              />
              { error &&
                <span
                  className={classNames({ 'help-block': true, 'has-error': error })}
                  style={{ color: 'red' }}
                > { error === 'TOO_SHORT' && `Поле "${label}" повинне мати 12 цифр.` }
                  { error === 'REQUIRED' && `Поле "${label}" не може бути пустим.` }
                </span>
              }
            </comp>
          }
          {component !== 'phone' &&
            <comp>
              <input {...rest} type="text" className="form-control" placeholder={placeholder} />
              { error &&
                <span
                  className={classNames({ 'help-block': true, 'has-error': error })}
                  style={{ color: 'red' }}
                > { error === 'REQUIRED' && `Поле "${label}" не може бути пустим.` }
                  { error === 'WRONG_EMAIL' && `Поле "${label}" повинно бути в форматi example@domain.com` }
                </span>
              }
            </comp>
          }

        </div>
      </div>
    );

    const renderFormGroupTextarea = ({ label, placeholder, ...rest }) => (
      <div className="form-group">
        <label> {label} </label>
        <textarea {...rest} className="form-control" placeholder={placeholder} />
      </div>
    );

    return (
      <div>
        <Breadcrumbs
          title=""
          components={{
            title: 'Картка клієнта',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Всі клієнти',
                to:   '/admin/clients',
              }, {
                name: 'Картка клієнта',
              },
            ],
          }}
        />

        <Portlet
          actionsRight={[
            isEditableRoleAccess && <button
              key="actionsRight_2"
              style={{ marginRight: '10px' }}
              className="btn green-meadow"
              onClick={this.onSave}
            >
              <i className="fa fa-check" /> Зберегти
            </button>,
            <Link key="actionsRight_3" onClick={this.closeEditForm} className="btn btn-sm default" to="/admin/clients">
              <i className="fa fa-times" /> Закрити
            </Link>,

          ]}
        >
          <Row style={{ marginTop: '15px' }}>
            <Col md={6}>
              <form role="form" className="form-horizontal" style={{ marginTop: '24px' }}>
                {renderFormGroup({
                  label:       'ПІБ',
                  placeholder: 'Введіть ПІБ',
                  readOnly:    !isEditableRoleAccess,
                  value:       client.firstName,
                  onChange:    this.onChangeParam('firstName'),
                  error:       validateErors.firstName,
                })}
                {renderFormGroup({
                  label:       'Конт. тел.',
                  placeholder: 'Введіть контактний телефон',
                  readOnly:    !isEditableRoleAccess,
                  value:       client.tel,
                  onChange:    this.onChangeParam('tel'),
                  error:       validateErors.tel,
                  component:   'phone',
                })}
                {renderFormGroup({
                  label:       'E-mail',
                  placeholder: 'Введіть e-mail',
                  readOnly:    !isEditableRoleAccess,
                  value:       client.email,
                  onChange:    this.onChangeParam('email'),
                  error:       validateErors.email,
                })}
                {renderFormGroup({
                  label:       'Адреса доставки',
                  placeholder: 'Введіть адресу доставки',
                  readOnly:    !isEditableRoleAccess,
                  value:       client.deliveryAddress,
                  onChange:    this.onChangeParam('deliveryAddress'),
                })}
              </form>
            </Col>
            <Col md={6}>
              {renderFormGroupTextarea({
                label:       'Коментар',
                placeholder: 'Введіть текст',
                rows:        '10',
                readOnly:    !isEditableRoleAccess,
                value:       client.comment,
                onChange:    this.onChangeParam('comment'),
              })}
            </Col>
          </Row>

        </Portlet>

      </div>
    );
  }
}

export default connect(selectors, actions)(ClientItem);
