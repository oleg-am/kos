import { combineReducers } from 'redux';
import * as types from './constants';

import { setFiltersValue } from '../../helpers/setObjValue';

const initialState = {
  client: {
    lastName:        '',
    firstName:       '',
    middleName:      '',
    tel:             '',
    email:           '',
    deliveryAddress: '',
    comment:         '',
  },
  filters: {
    page:  1,
    limit: 10,

    // with: {
    //   '*': {
    //     value: '*',
    //   }
    // },
    order_by: {
      id: {
        value: 'ASC',
      },
    },
    // filters: {
    //   name: {
    //     comparison: 'like',
    //     value: '',
    //     modifier: 'lower',
    //   },
    //   address: {
    //     comparison: 'like',
    //     value: '',
    //     modifier: 'lower',
    //   },
    // }
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const validateErors = (state = {}, action) => {
  switch (action.type) {
    case types.GET_VALIDATE_ERRORS:
      return action.errors;
    case types.LOAD_SUCCESS:
    case types.EDIT_SUCCESS:
      return {};
    default:
      return state;
  }
};

const client = (state = initialState.client, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.data;
    case types.CLOSE_EDIT_FORM:
      return initialState.client;
    case types.CHANGE_PARAM:
      return {
        ...state,
        [action.key]: action.value,
      };
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    default:
      return state;
  }
};

export default combineReducers({
  errors, // []
  validateErors,
  client, // {}
  filters, // {}
  isFiltersDebounce, // false
});
