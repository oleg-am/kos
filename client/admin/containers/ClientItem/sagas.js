import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

import { browserHistory } from 'react-router';

import { EDIT_SUCCESS } from './constants';
import { goToClients as goToClientsAction } from './actions';


function* goToClients() {
  yield put(goToClientsAction());
  yield browserHistory.push('/admin/clients');
}

function* rootSagas() {
  yield takeEvery(EDIT_SUCCESS, goToClients);
}

export default rootSagas;
