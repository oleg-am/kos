import LIVR from 'livr';

LIVR.Validator.defaultAutoTrim(true);

const clientSchema = {
  id:              ['required', 'positive_integer'],
  lastName:        'string',
  firstName:       ['required', 'string'],
  middleName:      'string',
  tel:             ['required', { min_length: 12 }],
  email:           'email', // ['string', 'email'],
  deliveryAddress: 'string',
  comment:         'string',
};

const putValidator = new LIVR.Validator(clientSchema);

export default putValidator;
