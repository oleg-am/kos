import { createSelector, createStructuredSelector } from 'reselect';
import MODULE from './constants';

const client            = state => state[MODULE].client;

const filtersState      = state => state[MODULE].filters;

const isFiltersDebounce = state => state[MODULE].isFiltersDebounce;
const errors            = state => state[MODULE].errors;
const validateErors     = state => state[MODULE].validateErors;

// const clients = createSelector(
//   clientsEntities,
//   clientsIds,
//   (items, ids) => ids.map(id => items[id]),
// );

const filters = createSelector(filtersState, items => items);

export default createStructuredSelector({
  filters,
  isFiltersDebounce,
  errors,
  validateErors,
  client,
});
