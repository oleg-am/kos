import * as types from './constants';
import { createParams } from '../../helpers/api';

import CLIENTS_ARR from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const load = filters => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/clients${createParams(filters)}`),
  schema:  CLIENTS_ARR,
});

export const resetFilter = () => ({
  type: types.RESET_FILTER,
});
