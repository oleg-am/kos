import React from 'react';
import { connect } from 'react-redux';

import { Link, browserHistory } from 'react-router';
// import { push } from 'react-router-redux';

// import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
// import Select from 'react-select';

import _debounce from 'lodash/debounce';
import HighlightWords from 'react-highlight-words';

import * as actions from './actions';
import selectors from './selectors';

// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

import Paginate from '../../components/Paginate';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import LimitSelect from '../../components/LimitSelect';

class Clients extends React.Component {
  static propTypes = {
    errors:      React.PropTypes.array.isRequired,
    clients:     React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    // params: React.PropTypes.shape({
    //   id: React.PropTypes.string.isRequired,
    // }),
    filters:     React.PropTypes.object.isRequired,
    paging:      React.PropTypes.object.isRequired,
    load:        React.PropTypes.func.isRequired,
    setFilter:   React.PropTypes.func.isRequired,
    resetFilter: React.PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const { load, filters } = this.props;

    this.debounceLoad = _debounce(load, 200);

    load(filters);
  }
  state = {
    searchOption: '',
    isOpenSearch: false,
  };
  componentWillReceiveProps(next) {
    const { isFiltersDebounce, filters: nextFilters } = next;
    const { load, filters } = this.props;

    if (filters !== nextFilters) {
      const newfilters = filters.page === nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters;

      isFiltersDebounce
      ? this.debounceLoad(newfilters)
      : load(newfilters);
    }
  }

  onEdit = id => (e) => {
    e.preventDefault();
    browserHistory.push(`/admin/clients/${id || ''}`);
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  refreshData = () => {
    this.props.load(this.props.filters);
  }
  fieldSearch = () => {
    const { isOpenSearch } = this.state;
    isOpenSearch && this.props.resetFilter();
    this.setState({
      isOpenSearch: !isOpenSearch,
    });
  }

  searchOption = (e) => {
    this.setState({ searchOption: e.target.value });
    this.props.resetFilter();
  };

  render() {
    const {
      errors,
      clients,
      paging,
      filters,
    } = this.props;
    const { searchOption, isOpenSearch } = this.state;

    const check = cell => (
      <i className={`fa fa-${cell ? 'check font-green-jungle' : 'close font-red-intense'}`}></i>
    );

    const selectTrueFalse = (onChange, values = [true, false]) => (
      <select onChange={onChange} className="form-control">
        <option key="All" value="">Всі</option>
        <option key="True" value={values[0]}>Так</option>
        <option key="False" value={values[1]}>Ні</option>
      </select>
    );

    const searchButtonStyle = {
      boxShadow:               'none',
      borderTopLeftRadius:     '0px',
      borderTopRightRadius:    '4px',
      borderBottomLeftRadius:  '0px',
      borderBottomRightRadius: '4px',
      height:                  '34px',
      borderLeft:              '0px',
    };

    const placeholderSeachInput = {
      tel: 'Введiть телефон',
    };

    return (
      <div>
        <Breadcrumbs
          title=""
          components={{
            title: 'Всі Клієнти',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Клієнти',
              },
            ],
          }}
        />
        {/* <PortletBody> */}

        <Portlet
          actionsLeft={[
            <div key={1}>
              <div key={2} className="input-group">
                <LimitSelect key="actionsLeft_1" optionsValue={[10, 15, 20]} value={filters.limit} onChange={this.setFilter('limit')} />
                {isOpenSearch &&
                  <select
                    key="actionsLeft_3"
                    value={searchOption}
                    style={{ width: 'inherit', borderLeft: '0px' }}
                    onChange={this.searchOption}
                    className="form-control"
                  >
                    <option key={'default'} disabled value="" >Оберiть фiльтр</option>
                    <option key={1} value="tel" >Телефон</option>
                    </select>
                }
                { isOpenSearch &&
                  searchOption.length > 0 &&
                    <input
                      key="actionsLeft_2"
                      style={{ width: 'inherit', borderLeft: '0px' }}
                      type="text"
                      value={filters.filters[searchOption].value}
                      onChange={this.setFilter(['filters', searchOption], true)}
                      className="form-control"
                      placeholder={placeholderSeachInput[searchOption]}
                    />
                }
                {!isOpenSearch ?
                  <button
                    key={3}
                    style={searchButtonStyle}
                    onClick={this.fieldSearch}
                    type="button"
                    className="btn btn-default"
                    title="Вiдкрити пошук"
                  ><i className="fa fa-search" aria-hidden="true" />
                  </button>
                  :
                  <button
                    key={4}
                    style={searchButtonStyle}
                    onClick={this.fieldSearch}
                    type="button"
                    className="btn btn-default"
                    title="Закрити пошук"
                  ><i className="fa fa-times" aria-hidden="true" />
                  </button>
                }
              </div>
            </div>
          ]}

          actionsRight={[
            <Link key="3" onClick={this.refreshData} className="btn btn-sm blue-sharp">
              <i className="fa fa-refresh" />
            </Link>,
          ]}
        >

          <CustomTable
            tableClass="table table-striped table-bordered table-hover"
            data={errors.length ? [] : clients}
            striped
            hover
            pagination
            filters={false}
          >
            <HeaderColumn dataField="id" width="5%" dataAlign="center" isKey> № </HeaderColumn>
            {/* <HeaderColumn dataField="name" width="25%" filters={<input type="text" value={filters.filters.name.value} onChange={this.setFilter(['filters', 'name'], true)} className="form-control" placeholder="Введіть найменування" />}> Найменування </HeaderColumn> */}
            <HeaderColumn dataField="firstName" width="25%"> ПІБ </HeaderColumn>
            <HeaderColumn
              dataField="tel"
              dataFormat={cell => (
                <HighlightWords
                  searchWords={[filters.filters.tel.value]}
                  textToHighlight={cell}
                />
              )}
            > Конт. тел. </HeaderColumn>
            <HeaderColumn dataField="email"> E-mail </HeaderColumn>
            <HeaderColumn dataField="deliveryAddress"> Адреса </HeaderColumn>
            <HeaderColumn dataField="comment"> Коментар </HeaderColumn>
            {/* <HeaderColumn dataField="connected" dataAlign="center" dataFormat={check} > Зв'язок </HeaderColumn> */}
            {/* <HeaderColumn
              dataField="active" dataAlign="center"
              filters={selectTrueFalse(this.setFilter(['filters', 'active']))}
              dataFormat={(cell, row) => (
                <span className={`badge badge-empty ${cell ? 'badge-success' : 'badge-danger'}`}></span>
              )}
            >
              Статус
            </HeaderColumn> */}
            <HeaderColumn
              dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle"
              dataFormat={cell => (
                <a onClick={this.onEdit(cell)} href="" className="btn btn-icon-only yellow">
                  <i className="fa fa-edit" />
                </a>
              )}
            />
          </CustomTable>

        </Portlet>

        {!errors.length && !!clients.length &&
          <Paginate
            pageNum={paging.totalPages}
            clickCallback={this.setFilterPage}
            forceSelected={paging.page - 1}
          />
        }
      </div>
    );
  }
}

export default connect(selectors, actions)(Clients);
