import { combineReducers } from 'redux';
import * as types from './constants';

import { setFiltersValue } from '../../helpers/setObjValue';

const initialState = {
  entities: {
    userRoles: {},
  },
  filters: {
    page:  1,
    limit: 10,

    // with: {
    //   '*': {
    //     value: '*',
    //   }
    // },
    order_by: {
      id: {
        value: 'ASC',
      },
    },
    filters: {
      tel: {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
    //  address: {
    //     comparison: 'like',
    //     value: '',
    //     modifier: 'lower',
    //   },
    },
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const entities = (state = initialState.entities, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities;
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data;
    default:
      return state;
  }
};

const paging = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.paging;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.RESET_FILTER:
      return {
        ...state,
        filters: initialState.filters.filters,
      };
    default:
      return state;
  }
};

export default combineReducers({
  errors, // []
  entities, // {}
  ids, // []
  paging, // {}
  filters, // {}
  isFiltersDebounce, // false
});
