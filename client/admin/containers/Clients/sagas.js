import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

// import { browserHistory } from 'react-router';

import { LOAD_SUCCESS } from './constants';
import * as actionsClientItem from '../ClientItem/actions';

function* closeEditForm() {
  yield put(actionsClientItem.closeEditForm());
}

function* rootSagas() {
  yield takeEvery([LOAD_SUCCESS], closeEditForm);
}

export default rootSagas;
