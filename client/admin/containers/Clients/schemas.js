import { Schema, arrayOf } from 'normalizr';

const clients = new Schema('clients');

const CLIENTS_ARR = { data: arrayOf(clients) };

export default CLIENTS_ARR;
