import { createSelector, createStructuredSelector } from 'reselect';

const REDUCER = 'clients';

const clientsEntities = state => state[REDUCER].entities.clients;
const clientsIds      = state => state[REDUCER].ids;

const filtersState      = state => state[REDUCER].filters;

const paging            = state => state[REDUCER].paging;
const isFiltersDebounce = state => state[REDUCER].isFiltersDebounce;
const errors            = state => state[REDUCER].errors;

const clients = createSelector(
  clientsEntities,
  clientsIds,
  (items, ids) => ids.map(id => items[id]),
);

const filters = createSelector(filtersState, items => items);

export default createStructuredSelector({
  filters,
  paging,
  isFiltersDebounce,
  errors,
  clients,
});
