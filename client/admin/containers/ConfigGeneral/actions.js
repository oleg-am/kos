import * as types from './constants';
import { createParams } from '../../helpers/api';

import { LOG_SHEMAS } from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});
export const changeParamLogs = (key, label, value) => ({
  type: types.CHANGE_PARAM_LOGS,
  key,
  label,
  value,
});

export const addLogs = () => ({
  type: types.ADD_LOGS,
});

export const postLogs = email => ({
  types:   [types.POST_LOGS_REQUEST, types.POST_LOGS_SUCCESS, types.POST_LOGS_FAILURE],
  promise: api => api.post(`/api/admin/log_receivers`, email),
  email,
});

export const putLogs = (id, email) => ({
  types:   [types.PUT_LOGS_REQUEST, types.PUT_LOGS_SUCCESS, types.PUT_LOGS_FAILURE],
  promise: api => api.put(`/api/admin/log_receivers/${id}`, email),
});

export const deleteLogs = id => ({
  types:   [types.DEL_LOGS_REQUEST, types.DEL_LOGS_SUCCESS, types.DEL_LOGS_FAILURE],
  promise: api => api.delete(`/api/admin/log_receivers/${id}`),
  id,
});

export const deleteTempLogs = id => ({
  type: types.DEL_TEMP_LOGS,
  id,
});

export const openModal = () => ({
  type: types.OPEN_MODAL,
});

export const closeModal = () => ({
  type: types.CLOSE_MODAL,
});

export const saveValidatorErrors = errors => ({
  type: types.SAVE_VALIDATE_ERRORS,
  errors,
});

export const loadLog = filters => ({
  types:   [types.LOAD_LOG_REQUEST, types.LOAD_LOG_SUCCESS, types.LOAD_LOG_FAILURE],
  promise: api => api.get(`/api/admin/log_receivers${createParams(filters)}`),
  schema:  LOG_SHEMAS,

});

export const save = (param, data) => ({
  types:   [types.SAVE_REQUEST, types.SAVE_SUCCESS, types.SAVE_FAILURE],
  promise: api => api.put(`/api/admin/configs/${param}`, data),
  param,
});
