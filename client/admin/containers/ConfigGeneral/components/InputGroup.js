import React, { PropTypes } from 'react';
import classNames from 'classnames';

import Icon from '../../../elements/Icon';

const propTypes = {
  param:    PropTypes.string.isRequired,
  value:    PropTypes.string.isRequired,
  label:    PropTypes.string,
  helpText: PropTypes.string,
  error:    PropTypes.string,
  labelCol: PropTypes.string,
  valueCol: PropTypes.string,
  type:     PropTypes.string,
  isSaving: PropTypes.bool,
  isEdited: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onSave:   PropTypes.func.isRequired,
};

const defaultProps = {
  param:    '',
  value:    '',
  label:    '',
  helpText: '',
  error:    '',
  isSaving: false,
  isEdited: false,
  disabled: false,
  onChange: null,
  onSave:   null,
  labelCol: '',
  valueCol: '',
};

const InputGroup = ({
  param,
  value,
  label,
  helpText,
  error,
  isSaving,
  disabled,
  isEdited,
  onChange,
  onSave,
  labelCol,
  valueCol,
  type,
  ...rest
}) => (
  <div className={classNames('form-group', { 'has-error': error })}>
    { label &&
      <label
        htmlFor={param}
        className={classNames('control-label', { 'col-sm-6 col-md-6 col-lg-6': !labelCol }, labelCol)}
      > {label} </label>
    }
    <div
      className={classNames('input-group', { 'col-sm-5 col-md-6 col-lg-6': !valueCol }, valueCol)}
      style={{ ...(isEdited ? {} : { paddingRight: '51px' }) }}
    >
      <input
        id={param}
        value={type === 'number' ? +value : value}
        type={type}
        disabled={disabled}
        onChange={onChange}
        {...rest}
        className="form-control"
      />
      <span className="input-group-btn">
        {isEdited &&
          <button
            type="button"
            title="Зберегти"
            className="btn bg-blue bg-font-blue"
            disabled={isSaving}
            onClick={onSave}
          >
            {isSaving
              ? <i className="fa fa-spinner fa-pulse fa-lg fa-fw" />
              : <Icon icon="check" size="lg" collor="default" />
            }
          </button>
        }
      </span>
      {helpText && <span className="help-block"> {helpText} </span>}
    </div>
  </div>
);

InputGroup.propTypes = propTypes;
InputGroup.defaultProps = defaultProps;

export default InputGroup;
