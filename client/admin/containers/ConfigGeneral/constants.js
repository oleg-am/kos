const module = 'configGeneral';

export const LOAD_LOG_REQUEST = `${module}/LOAD_LOG_REQUEST`;
export const LOAD_LOG_SUCCESS = `${module}/LOAD_LOG_SUCCESS`;
export const LOAD_LOG_FAILURE = `${module}/LOAD_LOG_FAILURE`;

export const POST_LOGS_REQUEST = `${module}/POST_LOGS_REQUEST`;
export const POST_LOGS_SUCCESS = `${module}/POST_LOGS_SUCCESS`;
export const POST_LOGS_FAILURE = `${module}/POST_LOGS_FAILURE`;

export const PUT_LOGS_REQUEST = `${module}/PUT_LOGS_REQUEST`;
export const PUT_LOGS_SUCCESS = `${module}/PUT_LOGS_SUCCESS`;
export const PUT_LOGS_FAILURE = `${module}/PUT_LOGS_FAILURE`;

export const DEL_LOGS_REQUEST = `${module}/DEL_LOGS_REQUEST`;
export const DEL_LOGS_SUCCESS = `${module}/DEL_LOGS_SUCCESS`;
export const DEL_LOGS_FAILURE = `${module}/DEL_LOGS_FAILURE`;

export const SAVE_REQUEST = `${module}/SAVE_REQUEST`;
export const SAVE_SUCCESS = `${module}/SAVE_SUCCESS`;
export const SAVE_FAILURE = `${module}/SAVE_FAILURE`;

export const SET_FILTER   = `${module}/SET_FILTER`;

export const OPEN_MODAL   = `${module}/OPEN_MODAL`;
export const CLOSE_MODAL   = `${module}/CLOSE_MODAL`;

export const SAVE_VALIDATE_ERRORS   = `${module}/SAVE_VALIDATE_ERRORS`;

export const CHANGE_PARAM = `${module}/CHANGE_PARAM`;
export const CHANGE_PARAM_LOGS = `${module}/CHANGE_PARAM_LOGS`;
export const ADD_LOGS = `${module}/ADD_LOGS`;
export const DEL_TEMP_LOGS = `${module}/DEL_TEMP_LOGS`;

export default module;
