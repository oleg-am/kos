import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
// import { Link } from 'react-router';

import { Button, Row, Col } from 'react-bootstrap';
import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';

// import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';
import { validator, emailValidator } from './schemas';
import Icon from '../../elements/Icon';
import InputGroup from './components/InputGroup';
import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import Paginate from '../../components/Paginate';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

class Prices extends React.Component {
  static propTypes = {
    isSaving:             PropTypes.objectOf(PropTypes.bool).isRequired,
    isEdited:             PropTypes.objectOf(PropTypes.bool).isRequired,
    validateErors:        PropTypes.object,
    filters:              PropTypes.object,
    configs:              PropTypes.objectOf(PropTypes.object).isRequired,
    settingLog:           PropTypes.objectOf(PropTypes.object).isRequired,
    loadLog:              PropTypes.func.isRequired,
    setFilter:            PropTypes.func.isRequired,
    changeParam:          PropTypes.func.isRequired,
    changeParamLogs:      PropTypes.func.isRequired,
    save:                 PropTypes.func.isRequired,
    saveValidatorErrors:  PropTypes.func.isRequired,
    deleteLogs:           PropTypes.func.isRequired,
    addLogs:              PropTypes.func.isRequired,
    postLogs:             PropTypes.func.isRequired,
    putLogs:              PropTypes.func.isRequired,
    closeModal:           PropTypes.func.isRequired,
    openModal:            PropTypes.func.isRequired,
    deleteTempLogs:       PropTypes.func.isRequired,
    isOpenModal:          PropTypes.bool,
    isEditableRoleAccess: PropTypes.bool,
  }

  constructor(props, context) {
    super(props, context);
    const { loadLog, filters } = this.props;
    loadLog(filters);
  }

  state = {
    currentLog: {},
  }

  componentWillReceiveProps = (next) => {
    const { loadLog, filters } = this.props;
    if (filters !== next.filters) {
      loadLog(next.filters);
    }
  }

  onChangeParam = (key, value) => (e) => {
    this.props.changeParam(key, value || e.target.value);
  }

  onChangeParamLogs = (label, key, enabled) => (e) => {
    let val = '';
    if (e.target.type === 'checkbox') {
      val = e.target.checked;
    } else if (e.target.value) {
      val = e.target.value;
    } else {
      val = enabled;
      e.preventDefault();
    }
    this.props.changeParamLogs(key, label, val);
  }

  onSaveLogs = id => () => {
    const {
      postLogs,
      putLogs,
      settingLog,
      saveValidatorErrors,
      deleteTempLogs,
    } = this.props;
    const isNew = typeof id === 'string';
    const email = settingLog.entities.log[id];
    const isEmailValid = emailValidator.validate(email);
    if (isEmailValid) {
      if (isNew) {
        postLogs(isEmailValid);
        deleteTempLogs(id);
      } else {
        putLogs(id, isEmailValid);
      }
    } else {
      saveValidatorErrors({
        ...emailValidator.getErrors(),
        [`rowId${id}`]: id,
      });
    }
  }

  onSave = (param, value) => (e) => {
    const { save, saveValidatorErrors } = this.props;
    const paramValidator = validator[param];
    const validData = paramValidator.validate({ [param]: value });
    validData
      ? save(param, { value: validData[param] })
      : saveValidatorErrors(paramValidator.getErrors());

    e.preventDefault();
  }

  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  // ищем позицию курсора в textArea
  getCursorPosition = textArea => {
    let CurrentPos = 0;
    if (document.selection) {
      textArea.focus();
      const Sel = document.selection.createRange();
      Sel.moveStart('character', -textArea.value.length);
      CurrentPos = Sel.text.length;
    } else if (textArea.selectionStart || textArea.selectionStart === '0') {
      CurrentPos = textArea.selectionStart;
    }
    return CurrentPos;
  }
  // добавляем в поле textArea маски в зависимости от нажатой кнопки.
  addTextMask = (textArea, mask, key) => () => {
    const position = this.getCursorPosition(document.getElementById(textArea));
    const text = document.getElementById(textArea).value;
    const addText = String.prototype.addSubStr = (pos, str) => {
      const beforeSubStr = str.substring(0, pos);
      const afterSubStr = str.substring(pos, str.length);
      return beforeSubStr + mask + afterSubStr;
    };
    const endText = addText(position, text);
    this.props.changeParam(key, endText);
    document.getElementById(textArea).focus();
  }

  deleteLogs = id => (e) => {
    e & e.preventDefault();
    const { deleteLogs, closeModal } = this.props;
    deleteLogs(id);
    closeModal();
  }

  setFilterPage = ({ selected }) => {
    const { setFilter } = this.props;
    setFilter('page', selected + 1);
  }

  openModal = row => () => {
    const { openModal, deleteTempLogs } = this.props;
    const isNew = typeof row.id === 'string';
    if (isNew) {
      deleteTempLogs(row.id);
    } else {
      openModal();
      this.setState({
        currentLog: row,
      });
    }
  }

  render() {
    const {
      configs: {
        basicDiscount,
        ordersAutoReloadInterval,
        ordersAutoReloadEnable,
        smsEnabled,
        smsFrom,
        smsLogin,
        smsPassword,
        smsText,
        smsURL,
      },
      logEntity,
      logPaging,
      addLogs,
      isAddingLog,
      isSaving,
      isEdited,
      validateErors,
      closeModal,
      isOpenModal,
      isEditableRoleAccess,
    } = this.props;
    const { currentLog } = this.state;
    console.log('this.props', this.props);
    return (
      <div style={{ paddingTop: '40px' }} >
        <Breadcrumbs
          title=""
          components={{
            title: 'Загальні',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Загальні',
              },
            ],
          }}
        />

        <Portlet isTitle={false} >
          <Row>
            <Col md={12} sm={12}>
              <Portlet
                className={'box blue-dark'}
                title={[
                  <i key={'osnovne_1'} className="fa fa-cogs" />,
                  <span key={'osnovne_2'} >Налаштування знижки</span>,
                ]}
              >
                <Row>
                  <Col md={6} xs={10}>
                    <form className="form-horizontal">
                      <InputGroup
                        param="basicDiscount"
                        type="number"
                        min="0"
                        max="100"
                        step="0.1"
                        disabled={!isEditableRoleAccess}
                        label="Налаштування знижки, %:"
                        placeholder="Введіть знижку"
                        title="Введіть знижку"
                        value={basicDiscount.value}
                        onChange={this.onChangeParam(['basicDiscount', 'value'])}
                        onSave={this.onSave('basicDiscount', basicDiscount.value)}
                        isSaving={isSaving.basicDiscount}
                        isEdited={isEdited.basicDiscount}
                        // helpText="Максимум 12 символів, приклад: 380671234567"
                        // error={validateErors.tel}
                      />
                    </form>
                  </Col>
                </Row>
              </Portlet>
            </Col>
          </Row>

          <Row>
            <Col md={12} sm={12}>
              <Portlet
                className={'box blue-dark'}
                title={[
                  <i key={'osnovne_1'} className="fa fa-cogs" />,
                  <span key={'osnovne_2'} >Налаштування сторiнки замовлень</span>,
                ]}
              >
                <Row>
                  <Col md={6} xs={10}>
                    <form className="form-horizontal">
                      <InputGroup
                        param="ordersAutoReloadInterval"
                        type="number"
                        min="0"
                        // max="100"
                        // step="0.1"
                        disabled={!isEditableRoleAccess}
                        label="Налаштування автооновлення, сек:"
                        placeholder="Введіть інтервал"
                        title="Введіть інтервал"
                        value={ordersAutoReloadInterval.value}
                        onChange={this.onChangeParam(['ordersAutoReloadInterval', 'value'])}
                        onSave={this.onSave('ordersAutoReloadInterval', ordersAutoReloadInterval.value)}
                        isSaving={isSaving.ordersAutoReloadInterval}
                        isEdited={isEdited.ordersAutoReloadInterval}
                        // helpText="Максимум 12 символів, приклад: 380671234567"
                        // error={validateErors.tel}
                      />
                    </form>
                  </Col>
                  <Col xs={2} sm={2} md={1}>
                    <form className="form-horizontal">
                      {ordersAutoReloadEnable.value === 'true' ?
                        <button
                          disabled={!isEditableRoleAccess}
                          onClick={this.onSave('ordersAutoReloadEnable', 'false')}
                          className="btn green-meadow" title="Вимкнути"
                        >
                          <i className="fa fa-toggle-on" /> Активовано
                        </button>
                        : <button
                          disabled={!isEditableRoleAccess}
                          onClick={this.onSave('ordersAutoReloadEnable', 'true')}
                          className="btn red"
                          title="Активувати"
                        >
                          <i className="fa fa-toggle-off" /> Деактивовано
                        </button>
                      }
                    </form>
                  </Col>
                </Row>
              </Portlet>
            </Col>
          </Row>

          <Row>
            <Col md={12} sm={12}>
              <Portlet
                className={'box blue-dark'}
                title={[
                  <i key={'sms_1'} className="fa fa-cogs" />,
                  <span key={'sms_2'} >Налаштування одержувачiв логування</span>,
                ]}
                actionsRight={[
                  <button
                    key={'btn1'}
                    onClick={addLogs}
                    disabled={isEditableRoleAccess ? isAddingLog : true}
                    type="button"
                    className="btn green-meadow"
                    title="Додати"
                    style={{ marginRight: '5px' }}
                  >
                    <i key={'btn2'} className="fa fa-plus" /> Додати одержувача
                  </button>,
                ]}
              >
                <Row>
                  <Col md={12}>
                    <CustomTable
                      tableHeaderClass="bg-blue bg-font-blue"
                      tableClass="table table-striped  table-hover"
                      trClassName="vertical-middle"
                      data={logEntity}
                      striped
                      hover
                      columnClassName="vertical-middle"
                    >

                      <HeaderColumn
                        dataField="email"
                        width="50%"
                        dataSort
                        columnClassName="vertical-middle"
                        dataFormat={(cell, row) => (
                          <div
                            style={{ marginBottom: 0, paddingTop: 0 }}
                            className={classNames('form-group form-md-line-input', {
                              'has-error': row.id === validateErors[`rowId${row.id}`] && validateErors.email,
                            })}
                          >
                            <input
                              onChange={this.onChangeParamLogs('email', row.id)}
                              value={cell}
                              disabled={!isEditableRoleAccess}
                              type="text"
                              className="form-control"
                              placeholder="Введіть e-mail"
                              title="Введіть e-mail"
                            />
                            {
                              row.id === validateErors[`rowId${row.id}`] &&
                              <span
                                className={classNames({ 'has-error': validateErors.email })}
                                style={{ color: 'red' }}
                              > {
                                  validateErors.email === 'REQUIRED' &&
                                  'Поле обов`язкове для заповнення.'
                                }
                              </span>
                            }

                          </div>
                        )}
                      >
                        <div style={{ minWidth: '300px' }} >
                          <div style={{ float: 'left' }}>
                            E-mail одержувача
                          </div>
                          <div style={{ float: 'right' }}>
                            Отримувати логи по:
                          </div>
                        </div>
                      </HeaderColumn>

                      <HeaderColumn
                        dataField="dictionaryLogs"
                        width="5%"
                        dataSort
                        dataAlign="center"
                        columnClassName="vertical-middle"
                        dataFormat={(cell, row) => (
                          <label
                            htmlFor={`log_receivers_1${row.id}`}
                            title={'Номенклатура'}
                            className="mt-checkbox"
                          >
                            <input
                              id={`log_receivers_1${row.id}`}
                              onChange={this.onChangeParamLogs('dictionaryLogs', row.id)}
                              type="checkbox"
                              disabled={!isEditableRoleAccess}
                              checked={cell}
                            />
                            <span />
                          </label>
                        )}
                      >
                        Номенклатура
                      </HeaderColumn>

                      <HeaderColumn
                        dataField="remaindersLogs"
                        width="5%"
                        dataSort
                        dataAlign="center"
                        columnClassName="vertical-middle"
                        dataFormat={(cell, row) => (
                          <label
                            htmlFor={`log_receivers_2${row.id}`}
                            title={'Залишки'}
                            className="mt-checkbox"
                          >
                            <input
                              id={`log_receivers_2${row.id}`}
                              onChange={this.onChangeParamLogs('remaindersLogs', row.id)}
                              type="checkbox"
                              disabled={!isEditableRoleAccess}
                              checked={cell}
                            />
                            <span />
                          </label>
                        )}
                      >
                        Залишки
                      </HeaderColumn>

                      <HeaderColumn
                        dataField="statusLogs"
                        width="5%"
                        dataSort
                        dataAlign="center"
                        columnClassName="vertical-middle"
                        dataFormat={(cell, row) => (
                          <label
                            htmlFor={`log_receivers_3${row.id}`}
                            title={'Статуси'}
                            className="mt-checkbox"
                          >
                            <input
                              id={`log_receivers_3${row.id}`}
                              onChange={this.onChangeParamLogs('statusLogs', row.id)}
                              type="checkbox"
                              disabled={!isEditableRoleAccess}
                              checked={cell}
                            />
                            <span />
                          </label>
                        )}
                      >
                        Статуси
                      </HeaderColumn>

                      <HeaderColumn
                        dataField="pharmacyConnectionLogs"
                        width="5%"
                        dataSort
                        dataAlign="center"
                        columnClassName="vertical-middle"
                        dataFormat={(cell, row) => (
                          <label
                            htmlFor={`log_receivers_4${row.id}`}
                            title={'Аптеки'}
                            className="mt-checkbox"
                          >
                            <input
                              id={`log_receivers_4${row.id}`}
                              onChange={this.onChangeParamLogs('pharmacyConnectionLogs', row.id)}
                              type="checkbox"
                              disabled={!isEditableRoleAccess}
                              checked={cell}
                            />
                            <span />
                          </label>
                        )}
                      >
                        Аптеки
                      </HeaderColumn>

                      <HeaderColumn
                        dataField="id"
                        width="5%"
                        dataSort
                        isKey
                        dataAlign="center"
                        columnClassName="vertical-middle"
                        dataFormat={(cell, row) => (
                          <btnGroup style={{ whiteSpace: 'nowrap' }}>
                            <button
                              onClick={this.onChangeParamLogs('enabled', row.id, !row.enabled)}
                              type="button"
                              disabled={!isEditableRoleAccess}
                              className={classNames('btn', {
                                'green-meadow': row.enabled,
                                'red':          !row.enabled,
                              })}
                              title={row.enabled ? 'Включено' : 'Виключено'}
                              style={{ width: '128px' }}
                            >
                              <i
                                className={classNames('fa', {
                                  'fa-toggle-on':  row.enabled,
                                  'fa-toggle-off': !row.enabled,
                                })}
                              />
                              {row.enabled ? 'Включено' : 'Виключено'}

                            </button>

                            <button
                              type="button"
                              title="Зберегти"
                              className="btn bg-blue bg-font-blue"
                              disabled={isEditableRoleAccess ? (row.isEdit === 'undefined' ? true : !row.isEdit) : true}
                              onClick={this.onSaveLogs(row.id)}
                            >
                              <Icon icon="check" size="lg" collor="default" />
                              {row.isEdit}
                            </button>

                            <button
                              className="btn btn-icon-only red"
                              title="Видалити"
                              disabled={!isEditableRoleAccess}
                              onClick={this.openModal(row)}
                            ><i className="fa fa-times" aria-hidden="true" />
                            </button>
                          </btnGroup>
                        )}
                      >
                        <i className="fa fa-cogs" aria-hidden="true" />
                      </HeaderColumn>
                    </CustomTable>

                    {logPaging &&
                    <Paginate
                      pageNum={logPaging.totalPages}
                      clickCallback={this.setFilterPage}
                      forceSelected={logPaging.page - 1}
                    />
                    }
                  </Col>
                </Row>
              </Portlet>
            </Col>
          </Row>

          <Row>
            <Col md={12} sm={12}>
              <Portlet
                className={'box blue-dark'}
                title={[
                  <i key={'sms_1'} className="fa fa-cogs" />,
                  <span key={'sms_2'} >Налаштування СМС</span>,
                ]}
                actionsRight={[
                  <div key={'statusSmsButton_1'} className={'form-group'}>
                    <label
                      key={'statusSmsButton_2'}
                      htmlFor={'statusSms'}
                      className={'col-sm-9 col-md-9 col-lg-9'}
                      style={{ marginTop: '6px', paddingRight: '0px' }}
                    >
                      Статус СМС розсилки:
                    </label>
                    <div key={'statusSmsButton_3'} className={'col-sm-2 col-md-2 col-lg-2'}>
                      <span id="statusSms">
                        {
                          smsEnabled &&
                          <button
                            disabled={!isEditableRoleAccess}
                            onClick={this.onSave('smsEnabled', smsEnabled.value === 'true' ? 'false' : 'true')}
                            className={classNames('btn btn-icon-only', {
                              'green-meadow': smsEnabled.value === 'true' ? true : false,
                              'red':          smsEnabled.value === 'false' ? true : false,
                            })}
                            title={smsEnabled.value === 'true' ? 'Включено' : 'Виключено'}
                          >
                            <i
                              className={classNames('fa', {
                                'fa-toggle-on':  smsEnabled.value === 'true' ? true : false,
                                'fa-toggle-off': smsEnabled.value === 'false' ? true : false,
                              })}
                            />
                          </button>
                        }
                      </span>
                    </div>
                  </div>,
                ]}
              >
                <Row>
                  <Col md={6}>
                    <form className="form-horizontal">
                      <InputGroup
                        param="smsFrom"
                        type="text"
                        disabled={!isEditableRoleAccess}
                        label="Вкажiть AlphaName:"
                        placeholder="Вкажiть AlphaName"
                        title="Вкажiть AlphaName"
                        value={smsFrom.value}
                        onChange={this.onChangeParam(['smsFrom', 'value'])}
                        onSave={this.onSave('smsFrom', smsFrom.value)}
                        isSaving={isSaving.smsFrom}
                        isEdited={isEdited.smsFrom}
                        valueCol={'col-sm-6 col-md-6 col-lg-6'}
                      />
                      <InputGroup
                        param="smsLogin"
                        type="text"
                        disabled={!isEditableRoleAccess}
                        label="Вкажiть логiн:"
                        placeholder="Вкажiть логiн"
                        title="Вкажiть логiн"
                        value={smsLogin.value}
                        onChange={this.onChangeParam(['smsLogin', 'value'])}
                        onSave={this.onSave('smsLogin', smsLogin.value)}
                        isSaving={isSaving.smsLogin}
                        isEdited={isEdited.smsLogin}
                      />
                      <InputGroup
                        param="smsPassword"
                        type="text"
                        disabled={!isEditableRoleAccess}
                        label="Вкажiть пароль:"
                        placeholder="Вкажiть пароль"
                        title="Вкажiть пароль"
                        value={smsPassword.value}
                        onChange={this.onChangeParam(['smsPassword', 'value'])}
                        onSave={this.onSave('smsPassword', smsPassword.value)}
                        isSaving={isSaving.smsPassword}
                        isEdited={isEdited.smsPassword}
                      />
                      <InputGroup
                        param="smsURL"
                        type="text"
                        disabled={!isEditableRoleAccess}
                        label="Url:"
                        placeholder="Вкажiть url на який вiдправляти смс"
                        title="Вкажiть url на який вiдправляти смс"
                        value={smsURL.value}
                        onChange={this.onChangeParam(['smsURL', 'value'])}
                        onSave={this.onSave('smsURL', smsURL.value)}
                        isSaving={isSaving.smsURL}
                        isEdited={isEdited.smsURL}
                      />
                    </form>
                  </Col>

                  <Col md={6}>
                    <Row>
                      <Col md={3}>
                        <label>Текст СМС</label>
                      </Col>
                    </Row>

                    <Row>
                      <Col>
                        <form className="form-horizontal">
                          <div className="textArea-with-button">
                            <textarea
                              className="form-control textArea-with-button__input"
                              id="smsText"
                              disabled={!isEditableRoleAccess}
                              onChange={this.onChangeParam(['smsText', 'value'])}
                              value={smsText.value}
                              // className="form-control"
                              rows="5"
                              placeholder="Введіть текст смс"
                            />
                            <Button
                              onClick={this.addTextMask('smsText', '[:order:]', ['smsText', 'value'])}
                              className="btn bg-blue bg-font-blue textArea-with-button__button"
                              title="Вставити № замовлення"
                              disabled={!isEditableRoleAccess}
                            >
                              № замовлення
                            </Button>
                            <Button
                              onClick={this.addTextMask('smsText', '[:totalPrice:]', ['smsText', 'value'])}
                              className="btn bg-blue bg-font-blue textArea-with-button__button"
                              title="Вставити суму замовлення"
                              disabled={!isEditableRoleAccess}
                              style={{ left: '154px' }}
                            >
                              Сума замовлення
                            </Button>
                            <Button
                              onClick={this.addTextMask('smsText', '[:pharmacy:]', ['smsText', 'value'])}
                              className="btn bg-blue bg-font-blue textArea-with-button__button"
                              title="Вставити адресу доставки"
                              disabled={!isEditableRoleAccess}
                              style={{ left: '305px' }}
                            >
                              Адреса доставки
                            </Button>
                          </div>
                          <span className="input-group-btn">
                            {isEdited.smsText &&
                              <button
                                type="button"
                                title="Зберегти"
                                className="btn bg-blue bg-font-blue"
                                disabled={isSaving.smsText}
                                onClick={this.onSave('smsText', smsText.value)}
                              >
                                {isSaving.smsText
                                  ? <i className="fa fa-spinner fa-pulse fa-lg fa-fw" />
                                  : <Icon icon="check" size="lg" collor="default" />
                                }
                              </button>
                            }
                          </span>
                        </form>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Portlet>
            </Col>
          </Row>
        </Portlet>

        <Modal show={isOpenModal} onHide={closeModal}>
          <Header closeButton>
            <Title>
              Ви впевненi, що бажаєте видалити отримувача "{currentLog.email}"
            </Title>
          </Header>
          <Footer>
            <Button onClick={this.deleteLogs(currentLog.id)} className="green-meadow pull-left">Підтвердити</Button>
            <Button onClick={closeModal} className="">Закрити</Button>
          </Footer>
        </Modal>

      </div>
    );
  }
}

export default connect(selectors, actions)(Prices);
