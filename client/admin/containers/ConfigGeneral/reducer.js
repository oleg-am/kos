import { combineReducers } from 'redux';
import * as types from './constants';
import _uniqueId from 'lodash/uniqueId';
import { setFiltersValue } from '../../helpers/setObjValue';

const initialState = {
  validateErors: {},
  isSaving:      {
    basicDiscount: false,
    smsURL:        false,
    smsLogin:      false,
    smsPassword:   false,
    smsFrom:       false,
    smsText:       false,
  },
  isEdited: {
    basicDiscount: false,
    smsURL:        false,
    smsLogin:      false,
    smsPassword:   false,
    smsFrom:       false,
    smsText:       false,
  },
  settingLog: {
    entities: {
      log: {},
    },
  },
  isAddingLog: false,
  filters: {
    page:     1,
    limit:    '10',
    order_by: {
      id: {
        value: 'ASC',
      },
    },
  },
  isOpenModal: false,
};

export default combineReducers({
  filters(state = initialState.filters, action) {
    switch (action.type) {
      case types.SET_FILTER:
        return {
          ...state,
          ...setFiltersValue(state, action.key, action.value),
        };
      default:
        return state;
    }
  },
  isOpenModal(state = initialState.isOpenModal, action) {
    switch (action.type) {
      case types.OPEN_MODAL:
        return true;
      case types.CLOSE_MODAL:
        return false;
      default:
        return state;
    }
  },
  isAddingLog(state = initialState.isAddingLog, action) {
    switch (action.type) {
      case types.ADD_LOGS:
        return true;
      case types.DEL_TEMP_LOGS:
      case types.DEL_LOGS_SUCCESS:
        return false;
      default:
        return state;
    }
  },
  errors(state = [], action) {
    switch (action.type) {
      case types.LOAD_FAILURE:
        return action.errors;
      case types.LOAD_SUCCESS:
        return [];
      default:
        return state;
    }
  },
  validateErors(state = {}, action) {
    switch (action.type) {
      case types.POST_LOGS_SUCCESS:
      case types.PUT_LOGS_SUCCESS:
        return {};
      case types.SAVE_VALIDATE_ERRORS:
        return {
          ...state,
          ...action.errors,
        };
      default:
        return state;
    }
  },
  logPaging(state = {}, action) {
    switch (action.type) {
      case types.LOAD_LOG_SUCCESS:
        return action.result.paging;
      default:
        return state;
    }
  },
  delTempLog(state = [], action) {
    switch (action.type) {
      case types.DEL_TEMP_LOGS:
        return [...state, action.id];
      default:
        return state;
    }
  },
  settingLog(state = initialState.settingLog, action) {
    switch (action.type) {
      case types.LOAD_LOG_SUCCESS:
        return {
          ...state,
          entities: {
            ...state.entities,
            ...action.entities,
          },
        };
      case types.PUT_LOGS_SUCCESS:
        return {
          ...state,
          entities: {
            ...state.entities,
            log: {
              ...state.entities.log,
              [action.data.id]: action.data,
            },
          },
        };
      case types.ADD_LOGS:
        const id = _uniqueId('id');
        return {
          ...state,
          entities: {
            ...state.entities,
            log: {
              ...state.entities.log,
              [id]: {
                id,
                email:                  '',
                enabled:                false,
                dictionaryLogs:         false,
                remaindersLogs:         false,
                statusLogs:             false,
                pharmacyConnectionLogs: false,
                isEdit:                 true,
              }
            },
          },
        };
      case types.CHANGE_PARAM_LOGS:
        return {
          ...state,
          entities: {
            ...state.entities,
            log: {
              ...state.entities.log,
              [action.key]: {
                ...state.entities.log[action.key],
                [action.label]: action.value,
                isEdit: true,
              }
            },
          },
        };
      default:
        return state;
    }
  },
  isSaving(state = initialState.isSaving, action) {
    switch (action.type) {
      case types.SAVE_REQUEST:
        return {
          ...state,
          [action.param]: true,
        };
      case types.SAVE_SUCCESS:
      case types.SAVE_FAILURE:
        return {
          ...state,
          [action.param]: false,
        };
      default:
        return state;
    }
  },
  isEdited(state = initialState.isEdited, action) {
    switch (action.type) {
      case types.CHANGE_PARAM:
        return {
          ...state,
          [action.key[0]]: true,
        };
      case types.SAVE_SUCCESS:
        return {
          ...state,
          [action.param]: false,
        };
      default:
        return state;
    }
  },
  isFiltersDebounce(state = false, action) {
    switch (action.type) {
      case types.SET_FILTER:
      case types.SET_FILTER_MANUFACTURES:
        return action.isFiltersDebounce;
      default:
        return state;
    }
  },
});
