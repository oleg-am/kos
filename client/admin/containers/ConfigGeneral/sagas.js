import { takeEvery } from 'redux-saga';
import { put, select } from 'redux-saga/effects';

// import { browserHistory } from 'react-router';

import REDUCER, {
  DEL_LOGS_SUCCESS,
  LOAD_LOG_FAILURE,
  POST_LOGS_SUCCESS,
} from './constants';

import * as actions from './actions';

function* reload() {
  const state = yield select();
  const filters = state[REDUCER].filters;
  yield put(actions.loadLog(filters));
}

function* loadPrevPage() {
  const state = yield select();
  const f = state[REDUCER].filters;
  if (f.page === 1){
    return;
  } else {
    yield put(actions.setFilter('page', f.page - 1));
  }
}

function* loadMaxPage() {
  const state = yield select();
  const totalRecords = state[REDUCER].logPaging.totalRecords;
  yield put(actions.setFilter('page', (~~(totalRecords/10) < 1  ? 1 : ~~(totalRecords/10) + 1)));
}

function* rootSagas() {
  yield takeEvery([DEL_LOGS_SUCCESS], reload);
  yield takeEvery([LOAD_LOG_FAILURE], loadPrevPage);
  yield takeEvery([POST_LOGS_SUCCESS], loadMaxPage);
}

export default rootSagas;
