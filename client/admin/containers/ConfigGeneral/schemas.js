import { Schema, arrayOf } from 'normalizr';
import LIVR from 'livr';

LIVR.Validator.defaultAutoTrim(true);

const log = new Schema('log');
const logs = {
  data: arrayOf(log),
};
export const LOG_SHEMAS = logs;
const email = {
  email:                  ['required', 'string'],
  id:                     ['required', 'string'],
  enabled:                { one_of: [true, false] },
  dictionaryLogs:         { one_of: [true, false] },
  remaindersLogs:         { one_of: [true, false] },
  statusLogs:             { one_of: [true, false] },
  pharmacyConnectionLogs: { one_of: [true, false] },
  isEdit:                 ['string'],
};

const schemas = {
  basicDiscount:            ['string', { number_between: [0, 100] }],
  ordersAutoReloadInterval: ['string', { min_number: 0 }],
  ordersAutoReloadEnable:   { one_of: ['true', 'false'] },
  smsEnabled:               { one_of: ['true', 'false'] },
  smsURL:                   ['string'],
  smsLogin:                 ['string'],
  smsPassword:              ['string'],
  smsFrom:                  ['string'],
  smsText:                  ['string'],
};
const createIndividualValidators = schema =>
  Object.keys(schema).reduce((obj, key) => {
    obj[key] = new LIVR.Validator({ [key]: schema[key] });
    return obj;
  }, {});

export const validator      = createIndividualValidators(schemas);
export const emailValidator = new LIVR.Validator(email);
