import { createSelector, createStructuredSelector } from 'reselect';
import _omit from 'lodash/omit';
import REDUCER from './constants';

const APP = 'app';

const settingLog    = state => state[REDUCER].settingLog;
const settingLogEnt = state => state[REDUCER].settingLog.entities.log;
const delTempLog    = state => state[REDUCER].delTempLog;

const logEntity = createSelector(
  settingLogEnt,
  delTempLog,
  (i, del) => (
    Object.values(i).length > 0 ?
    Object.values(del ? _omit(i, del) : i).map(e => e)
    : []
  )
);

export default createStructuredSelector({
  configs: state => state[APP].configs,

  isSaving:          state => state[REDUCER].isSaving,
  isEdited:          state => state[REDUCER].isEdited,
  isAddingLog:       state => state[REDUCER].isAddingLog,
  isFiltersDebounce: state => state[REDUCER].isFiltersDebounce,
  errors:            state => state[REDUCER].errors,
  filters:           state => state[REDUCER].filters,
  isOpenModal:       state => state[REDUCER].isOpenModal,
  validateErors:     state => state[REDUCER].validateErors,
  logPaging:         state => state[REDUCER].logPaging,
  settingLog,
  logEntity,
});
