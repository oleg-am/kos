import * as types from './constants';

import { createParams } from '../../helpers/api';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const load = filters => ({
  types:   [types.LOAD_REPORTS_REQUEST, types.LOAD_REPORTS_SUCCESS, types.LOAD_REPORTS_FAILURE],
  promise: api => api.get(`/api/admin/order_reports${createParams(filters)}`),
});
