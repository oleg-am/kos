const name = 'desktop';

export const LOAD_REPORTS_REQUEST = `${name}/LOAD_REPORTS_REQUEST`;
export const LOAD_REPORTS_SUCCESS = `${name}/LOAD_REPORTS_SUCCESS`;
export const LOAD_REPORTS_FAILURE = `${name}/LOAD_REPORTS_FAILURE`;

export const SET_FILTER = `${name}/SET_FILTER`;

export default name;
