import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import { Bar } from 'react-chartjs-2';
import { Link, browserHistory } from 'react-router'
// import { push } from 'react-router-redux';

// import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
// import Select from 'react-select';
// import Dropzone from 'react-dropzone';

// import _debounce from 'lodash/debounce';
// import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import * as actions from './actions';
import selectors from './selectors';
// import { pharmacyValidator } from './schemas.js';

// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

// import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import LimitSelect from '../../components/LimitSelect';
const modalStyles = {
  content: {
    top:         '50%',
    left:        '50%',
    right:       'auto',
    bottom:      'auto',
    marginRight: '-10%',
    transform:   'translate(-50%, -50%)',
    width:       '50%',
    height:      '50%'
  }
}

class Desktop extends React.Component {
  static propTypes = {
    isEditableRoleAccess: PropTypes.bool,
    reports:              PropTypes.arrayOf(PropTypes.object),
    load:                 PropTypes.func.isRequired,
    filters:              PropTypes.objectOf(PropTypes.any).isRequired,
  }
  constructor(props, context) {
    super(props, context);
    const { load, filters, reports } = this.props;
    this.state = {
      showAddReportModal: false,
      graphData:          [] ,
    }

    load(filters);

  }
  componentWillReceiveProps(next){
    this.setState({ graphData: next.reports })

  }


  openModal = () => {
    this.setState({ showAddReportModal: true });
  }
  closeModal = () => {

  }
  showChosenGraph = id => () => {

    this.setState({ showAddReportModal: false });
    this.setState({ graphData: this.props.reports });

  }

  render() {
    const { showAddReportModal, graphData } = this.state
    const { isEditableRoleAccess } = this.props;

    const data    = {
      labels:   graphData.map(each => each.date),
      datasets: [
        {
          label:                     'Сума замовлень,грн',
          type:                      'line',
          data:                      graphData.map(each => each.totalCost),
          fill:                      false,
          borderColor:               '#EC932F',
          backgroundColor:           '#EC932F',
          pointBorderColor:          '#EC932F',
          pointBackgroundColor:      '#EC932F',
          pointHoverBackgroundColor: '#EC932F',
          pointHoverBorderColor:     '#EC932F',
          yAxisID:                   'y-axis-2'
        },
        {
          type:                 'bar',
          label:                'Кількість замовлень,шт',
          data:                 graphData.map(each => each.count),
          fill:                 false,
          backgroundColor:      '#71B37C',
          borderColor:          '#71B37C',
          hoverBackgroundColor: '#71B37C',
          hoverBorderColor:     '#71B37C',
          yAxisID:              'y-axis-1'
        }]
    };

    const options = {
       // maintainAspectRatio: false,
      responsive: true,
      tooltips:   {
        mode: 'label'
      },
      elements: {
        line: {
          fill: false
        }
      },
      scales: {
        xAxes: [
          {
            display:   true,
            gridLines: {
              display: false
            },
            labels: {
              show: true
            }
          }
        ],
        yAxes: [
          {
              ticks: {
                beginAtZero: true
              },
            type:      'linear',
            display:   true,
            position:  'left',
            id:        'y-axis-1',
            gridLines: {
              display: false
            },
            labels: {
              show: true
            }
          },
          {
              ticks: {
                beginAtZero: true
              },
            type:      'linear',
            display:   true,
            position:  'right',
            id:        'y-axis-2',
            gridLines: {
              display: false
            },
            labels: {
              show: true
            }
          }
        ]
      }
    };
    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Modal
          contentLabel="Виберіть звіт для відображення"
          isOpen={showAddReportModal}
          onRequestClose={this.сloseModal}
          style={modalStyles}
        >
          <a onClick={this.showChosenGraph(1)}>Звіт за вказаною датою</a>
        </Modal>
        <Breadcrumbs title="" components={{
          title: 'Робочий стіл',
          links: [
            {
              name: 'Меню',
              to:   '/admin/reports'
            }, {
              name: 'Робочий стіл'
            }
          ]
        }}
        />
        <Portlet
          actionsRight={[
            isEditableRoleAccess && <button
              style={{ marginRight: '10px' }}
              key="actionsRight_1"
              className="btn green-meadow"
              onClick={this.openModal}
            >
              <i className="fa fa-plus"></i> Додати звіт
            </button>,

          ]}
        />
        {
          graphData.length > 0 &&

          <Link
            to="admin/reports/orderbypharmacy"
            style={{ display: 'block', width: '500px' }}
          >
            <b>Звіт за вказаною датою</b>
            <Bar

              data={data}
              options={options}
              // width={500}
              // height={250}
            />
          </Link>
      }
      {console.log('this.props:', this.props)}
      </div>
    );
  }
}

export default connect(selectors, actions)(Desktop);
