import { combineReducers } from 'redux';
import * as types from './constants';
import { startDefault, endDefault } from './utils/defaultDate';
import { setFiltersValue } from '../../helpers/setObjValue';

const initialState = {
  filters: {
    startDate: `${startDefault}`,
    endDate:   `${endDefault}`,
    pharmacy:  '',
    filters:   {
      name: {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },

    },
  },
  reports: [],

};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    default:
      return state;
  }
};

const reports = (state = initialState.reports, action) => {
  switch (action.type) {
    case types.LOAD_REPORTS_SUCCESS:
      return action.data;
    case types.LOAD_REPORTS_FAILURE:
      return initialState.reports;
    default:
      return state;
  }
};

export default combineReducers({
   filters, // {}
  reports, // []

});
