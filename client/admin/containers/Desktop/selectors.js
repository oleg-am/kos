import { createSelector, createStructuredSelector } from 'reselect';

import REDUCER from './constants';

// const APP = 'app';

const reports    = state => state[REDUCER].reports;
const filters    = state => state[REDUCER].filters;

// const pharmacies = createSelector(
//   state => state[REDUCER].pharmacies,
//   items => items.map(({ name, id }) => ({ label: name, value: id })),
// );

export default createStructuredSelector({
  filters: createSelector(filters, items => items),
  reports: createSelector(reports, items => items),
  //pharmacies,

});
