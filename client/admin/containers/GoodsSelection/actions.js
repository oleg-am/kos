import * as types from './constants';
import { PHARMACY_ARRAY } from './schemas.js';

import { normalize } from 'normalizr';

export const loadData = (data) => {
  return {type: types.LOAD_DATA, data: normalize(data, PHARMACY_ARRAY)}
}
export const toogleSelectedItems = (checked, id) => (
  {type: types.TOOGLE_CHECKED_ITEMS, checked, id}
)
