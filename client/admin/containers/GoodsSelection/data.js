import faker from 'faker';

const createObj = () => {
  let id = faker.random.number({min:4000, max:10050});
  return {
    id: faker.random.number({min:4000, max:4050}), //faker.random.number() indx*5000 + 4000+i
    name: faker.commerce.productName(),
  }
}

const getIds = (count) => {
  return Array.from({ length: count }, (v, k) => k).map((_, i) => ({
      id: 4000+i, //faker.random.number() indx*5000 + 4000+i
      name: faker.commerce.productName(),
  }))
}

const getArrId = () => {
  const arr =   [
      [4011, 4012, 4023, 4024, 4027, 4031, 4037, 4044, 4045, 4048],
      [4015, 4022, 4026, 4027, 4028, 4033, 4036, 4037, 4038, 4041],
      [4002, 4007, 4011, 4015, 4018, 4019, 4036, 4039, 4042, 4047],
      [4000, 4011, 4017, 4019, 4030, 4039, 4041, 4043, 4044, 4047],
      [4003, 4009, 4019, 4027, 4031, 4036, 4041, 4046, 4047, 4048],
      [4004, 4007, 4019, 4021, 4022, 4023, 4031, 4045, 4047, 4049],
      [4001, 4006, 4009, 4014, 4023, 4031, 4034, 4038, 4039, 4048],
      [4003, 4014, 4016, 4022, 4033, 4036, 4040, 4043, 4044, 4045],
      [4003, 4004, 4008, 4009, 4010, 4015, 4016, 4017, 4028, 4041],
      [4004, 4010, 4011, 4012, 4014, 4018, 4025, 4029, 4031, 4045],
    ]
return arr
}



export const getRandomGoods = (count) => {
  const idsObj = fromArrayToObj(getIds(50));
  const arrId = getArrId();
  return Array.from({ length: count }, (v, k) => k).map((_, index) => {
    let pharmacyId = 100+index;
    return {
      id: pharmacyId,
      name: faker.company.companyName(),
      goods: Array.from({ length: count }, (v, k) => k).map((_, i) => {
        return {
          // id: faker.random.number(),
          id: idsObj[arrId[index][i]].id, //faker.random.number() index*5000 + 4000+i
          name: idsObj[arrId[index][i]].name,
          pharmacy: pharmacyId,
          reservePrice: faker.finance.amount(10, 50),
          pharmacyStock: faker.finance.amount(0, 10),
          storageStock: faker.finance.amount(10, 50),
          quantity: 0,
          countPharmacy: 0,
          countStorage: 0,
        }
      })
    }
  })
}

function fromArrayToObj(data) {
  return data.reduce((obj, data) => {
    obj = obj || {};
    obj[data.id] = data;
    return obj
  }, 0 );
}
