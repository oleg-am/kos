import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ReactPaginate from 'react-paginate';

import { browserHistory, Link } from 'react-router'

import RowGoodsSelection from '../../components/common/RowGoodsSelection';

// import { addGoods } from '../../reducers/ordersReducer';

import { getRandomGoods } from './data';
import * as actions from './actions';
import { getSelectedGoods } from './selectors';

import { addGoods } from '../Orders/actions';
import { getOrderById } from '../Orders/selectors';

class GoodsSelection extends React.Component {

    constructor(props, context) {
        super(props, context);
        const { loadData, getRandomGoods, toogleSelectedItems } = this.props;
        this.getNewPage = this.getNewPage.bind(this);
        if (!props.goodsSelection.length) {
          loadData(getRandomGoods(10));
        }
    }

    getNewPage(data) {
        let page = data.selected + 1;
        this.props.actions.loadNomenclature(page);
    }
    hendlerClick(id) {
      browserHistory.push(`/admin/orders/${id}`);
    }
    handlerCheck(id, e) {
      this.props.toogleSelectedItems(e.target.checked, id);
    }
    handlerAddGoods(id, selectedGoods) {
      this.props.addGoods(id, selectedGoods);
    }

    render() {
      const { pharmacy ,goodsSelection, routeParams, selectedGoods } = this.props;

      return (
			<div className="page-content" style={{paddingTop: '20px'}}>
            <div className="breadcrumbs">
              <h1> Подбор товаров в аптеке <span style={{color: '#ed6b75'}}>"{pharmacy}"</span></h1>
                <ol className="breadcrumb">
                  <li>
                    <Link className="" to={`/admin/orders`}>
                      Меню
                    </Link>
                  </li>
                  <li>
                    <Link className="" to={`/admin/orders`}>
                      Заказы
                    </Link>
                  </li>
                  <li>
                    <Link className="" to={`/admin/orders/${routeParams.id}`}>
                      № {routeParams.id}
                    </Link>
                  </li>
                    <li className="active">Подбор товаров</li>
                </ol>
              </div>

							<table className="table">
								<colgroup>
									<col width="15%" />
									<col width="40%" />
									<col width="20%" />
									<col width="25%" />
								</colgroup>
								<tbody>
                <tr>
									<td></td>
									<td>
										<input type="text" className="form-control input-md input-inline" placeholder="Фильтр по названию"/>
									</td>
									<td>
										<input type="text" className="form-control input-md input-inline" placeholder="Фильтр по производителю"/>
									</td>
									<td></td>
								</tr>
                </tbody>
							</table>
							<div className="table-scrollable">
								<table className="table table-hover">
									<colgroup>
										<col width="5%" />
										<col width="35" />
										<col width="15%" />
										<col width="15%" />
										<col width="15%" />
										<col width="15%" />

									</colgroup>
									<thead>
										<tr>
											<th>  </th>
											<th> Наименование </th>
											<th> Остаток в аптеке </th>
											<th> Остаток на складе </th>
											<th> Цена резерва </th>
											<th> Поиск в других аптеках </th>
											{/* <th name="publish-head"> Публикация </th> */}
										</tr>
									</thead>
									<tbody>
									{goodsSelection.map((item) => (
								    <RowGoodsSelection
                      key={item.id}
                      orderId={routeParams.id}
                      data={item}
                      checkHandler={this.handlerCheck.bind(this, item.id)}
                      click={this.hendlerClick.bind(null, item.id)} />
									))}
									</tbody>
								</table>
							</div>
							{/* <div className="center">
								<ReactPaginate previousLabel={"Предыдущая"}
											   nextLabel={"Следующая"}
											   breakLabel={<a href="">...</a>}
											   breakClassName={"break-me"}
											  //  pageNum={this.props.pageNum}
									   		   marginPagesDisplayed={2}
											  //  forceSelected={this.props.paging.page - 1}
											   pageRangeDisplayed={5}
											   clickCallback={this.getNewPage}
											   containerClassName={"pagination"}
											   subContainerClassName={"pages pagination"}
											   activeClassName={"active"} />
							</div> */}
              <Link onClick={this.handlerAddGoods.bind(this, routeParams.id, selectedGoods)} to={`/admin/orders/${routeParams.id}`} className="btn btn-sm green-meadow">
                + Добавить в заказ
              </Link>
              <Link className="pull-right btn btn-sm red btn-outline" to={`/admin/orders/${routeParams.id}`}>
                <i className="fa fa-close"></i> Закрыть
              </Link>

						</div>
        );
    }
}


function mapStateToProps(state, ownProps) {
    const {pharmacy: pharmacyObj } = state.orders.entities;

    const order = state.orders.entities.orders[ownProps.params.id];
    // const goodsSelection = state.goodsSelection.pharmacy.goods;
    const goodsSelectionObj = state.goodsSelection.entities.goods;
    const goods = state.goodsSelection.entities.pharmacies[ownProps.params.pharmacyId] || '';
    const goods2 = goods.goods || [];
    const goodsSelection = goods2.map(val => goodsSelectionObj[val])

    return {
        pharmacy: pharmacyObj[order.pharmacy].name,
        goodsSelection,
        selectedGoods: getSelectedGoods(goodsSelection, state.goodsSelection.selected),
        // pageNum: state.goodsSelection.data.length/5,
        // paging: state.nomenclature.paging || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        // actions: bindActionCreators(dictionaryActions, dispatch),
        addGoods: bindActionCreators(addGoods, dispatch),
        ...bindActionCreators(actions, dispatch),
        getRandomGoods,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(GoodsSelection);
