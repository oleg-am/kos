import { Schema, arrayOf } from 'normalizr';

const PHARMACY = new Schema('pharmacies', { idAttribute: 'id' });
const PRODUCT = new Schema('goods', { idAttribute: 'id' });


PRODUCT.define({
    // author: postAuthorSchema,
    pharmacy: PHARMACY
});
PHARMACY.define({
    // author: postAuthorSchema,
    goods: arrayOf(PRODUCT)
});

const PHARMACY_ARRAY = arrayOf(PHARMACY);

export { PHARMACY, PHARMACY_ARRAY};


// export let data = [{
//   id: 1,
//   title: 'Some Article',
//   author: {
//     id: 1,
//     name: 'Dan'
//   }
// }, {
//   id: 2,
//   title: 'Other Article',
//   author: {
//     id: 1,
//     name: 'Dan'
//   }
// }];
