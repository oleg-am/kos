import * as types from './constants';
import { createParams } from '../../helpers/api';

import MARKET_PLACES_ARR from './schemas';

export const load = filters => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/internet_grounds${createParams(filters)}`),
  schema:  MARKET_PLACES_ARR,
});

export const deleteRequest = id => ({
  types:   [types.DELETE_REQUEST, types.DELETE_SUCCESS, types.DELETE_FAILURE],
  promise: api => api.delete(`/api/admin/internet_grounds/${id}`),
});

export const updateRequest = (id, mp) => ({
  types:   [types.UPDATE_REQUEST, types.UPDATE_SUCCESS, types.UPDATE_FAILURE],
  promise: api => api.put(`/api/admin/internet_grounds/${id}`, mp),
});

export const openModal = () => ({
  type: types.OPEN_MODAL,
});

export const closeForm = () => ({
  type: types.CLOSE_FORM,
});

export const deleteMP = mp => ({
  type:     types.DELETE_MP,
  activeMP: mp,
});

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});
