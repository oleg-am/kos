const module = 'MARKETPLACE';

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const DELETE_REQUEST = `${module}/DELETE_REQUEST`;
export const DELETE_SUCCESS = `${module}/DELETE_SUCCESS`;
export const DELETE_FAILURE = `${module}/DELETE_FAILURE`;

export const UPDATE_REQUEST = `${module}/UPDATE_REQUEST`;
export const UPDATE_SUCCESS = `${module}/UPDATE_SUCCESS`;
export const UPDATE_FAILURE = `${module}/UPDATE_FAILURE`;

export const OPEN_MODAL = `${module}/OPEN_MODAL`;
export const CLOSE_FORM = `${module}/CLOSE_FORM`;
export const DELETE_MP = `${module}/DELETE_MP`;
export const SET_FILTER = `${module}/SET_FILTER`;
