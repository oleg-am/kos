import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import classNames from 'classnames';
import * as actions from './actions';
import selectors from './selectors';
import Breadcrumbs from '../../components/Breadcrumbs';
import Portlet from '../../components/Portlet/index';
import Paginate from '../../components/Paginate';
import LimitSelect from '../../components/LimitSelect';
import { Button } from 'react-bootstrap';
import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';
import { CustomTable, HeaderColumn } from '../../components/CustomTable';

class MarketPlace extends Component {
  static propTypes = {
    load:          PropTypes.func.isRequired,
    openModal:     PropTypes.func.isRequired,
    deleteMP:      PropTypes.func.isRequired,
    updateRequest: PropTypes.func.isRequired,
    deleteRequest: PropTypes.func.isRequired,
    closeForm:     PropTypes.func.isRequired,
    setFilter:     PropTypes.func.isRequired,
    marketPlaces:  PropTypes.array.isRequired,
    filters:       PropTypes.shape({
      page:     PropTypes.number,
      limit:    PropTypes.string,
      order_by: PropTypes.shape({
        id: PropTypes.shape({
          value: PropTypes.string,
        }),
      }),
    }),
    activeMP: PropTypes.shape({
      id:   PropTypes.number,
      name: PropTypes.string,
    }),
    paging: PropTypes.shape({
      limit:        PropTypes.number,
      page:         PropTypes.number,
      records:      PropTypes.number,
      totalPages:   PropTypes.number,
      totalRecords: PropTypes.number,
    }),
    isOpenModal: PropTypes.bool,
    isEditableRoleAccess: PropTypes.bool,
  }

  constructor(props) {
    super(props);
    const { load, filters } = this.props;
    load(filters);
  }

  componentWillReceiveProps = (next) => {
    const { load, filters } = this.props;
    const { filters: nextFilters } = next;
    if (filters !== nextFilters) {
      const newfilters = filters.page === nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters;
      load(newfilters);
    }
  }

  setFilterPage = ({ selected }) => {
    const { setFilter } = this.props;
    setFilter('page', selected + 1);
  }

  setFilter = key => (e) => {
    const { setFilter } = this.props;
    setFilter(key, e.target.value);
  };

  deleteMP = (row) => {
    const { openModal, deleteMP } = this.props;
    deleteMP(row);
    openModal();
  }

  enabled = row => {
    const { updateRequest } = this.props;
    const mp = {
      ...row,
      enabled: !row.enabled,
    };
    updateRequest(row.id, mp);
  }

  render() {
    const {
      marketPlaces,
      isOpenModal,
      closeForm,
      filters,
      paging,
      activeMP,
      deleteRequest,
      isEditableRoleAccess,
    } = this.props;
    return (
      <div style={{ paddingTop: '40px' }}>
        <Breadcrumbs
          title="" components={{
            title: 'Майданчики',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              },
              {
                name: 'Майданчики',
              },
            ],
          }}
        />

        <Portlet
          key="marketPlace"

          actionsLeft={[
            <LimitSelect
              key="actionsLeft_1"
              optionsValue={[10, 15, 20]}
              value={filters.limit}
              onChange={this.setFilter('limit')}
            />,
          ]}
          actionsRight={[
            isEditableRoleAccess &&
            <Link
              key="actionsRight_1"
              to={'/admin/settings/marketPlaceItem/create'}
              className="btn btn-sm green-meadow"
            >Створити майданчик
            </Link>,
          ]}
        >
          <CustomTable
            tableHeaderClass="bg-blue bg-font-blue"
            tableClass="table table-striped  table-hover"
            trClassName="vertical-middle"
            data={marketPlaces}
            striped
            hover
            columnClassName="vertical-middle"
          >
            <HeaderColumn
              dataField="name"
              width="25%"
              dataSort
              columnClassName="vertical-middle"
            >Назва майданчика
            </HeaderColumn>
            <HeaderColumn
              dataField="url"
              width="25%"
              dataSort
              columnClassName="vertical-middle"
            >Адреса Майданчика (Web)
            </HeaderColumn>
            <HeaderColumn
              dataField="remaindersPeriod"
              width="20%"
              dataSort
              columnClassName="vertical-middle"
            >Залишки(хв)
            </HeaderColumn>
            <HeaderColumn
              dataField="orderPeriod"
              width="15%"
              dataSort
              columnClassName="vertical-middle"
            >Замовлення(хв)
            </HeaderColumn>
            <HeaderColumn
              dataField="id"
              width="15%"
              dataAlign="center"
              dataSort
              isKey
              columnClassName="vertical-middle"
              dataFormat={(cell, row) => (
                <btnGroup key="btn0" style={{ whiteSpace: 'nowrap' }}>
                  <Link
                    key="btn1"
                    to={`/admin/settings/marketPlaceItem/${row.id}`}
                    className="btn btn-icon-only yellow"
                    title="Редагувати"
                  ><i className="fa fa-edit" aria-hidden="true" />
                  </Link>
                  { row &&
                    <button
                      key="btn2"
                      disabled={!isEditableRoleAccess}
                      className={classNames('btn btn-icon-only', row.enabled ? ' green-meadow' : ' red')}
                      title={row.enabled ? 'Вiдключити' : 'Пiдключити'}
                      onClick={() => this.enabled(row)}
                    ><i className={classNames('fa', row.enabled ? 'fa-toggle-on' : 'fa-toggle-off')} aria-hidden="true" />
                    </button>
                  }
                  <button
                    key="btn3"
                    disabled={!isEditableRoleAccess}
                    className="btn btn-icon-only red"
                    title="Видалити"
                    onClick={() => this.deleteMP(row)}
                  ><i className="fa fa-times" aria-hidden="true" />
                  </button>
                </btnGroup>
              )}
            ><i className="fa fa-cogs" aria-hidden="true" />
            </HeaderColumn>
          </CustomTable>

        </Portlet>
        {paging &&
        <Paginate
          pageNum={paging.totalPages}
          clickCallback={this.setFilterPage}
          forceSelected={paging.page - 1}
        />
        }

        <Modal show={isOpenModal} onHide={closeForm}>
          <Header closeButton >
            <Title className="text-center caption-subject bold">
              Ви підтверджуєте видалення майданчика?
            </Title>
          </Header>
          <Body>
            <p className="text-danger lead text-center">{activeMP.name}</p>
          </Body>
          <Footer>
            <Button onClick={() => deleteRequest(activeMP.id)} className="green-meadow pull-left" >Так</Button>
            <Button onClick={closeForm} >Закрити</Button>
          </Footer>
        </Modal>
      </div>
    );
  }
}

export default connect(selectors, actions)(MarketPlace);
