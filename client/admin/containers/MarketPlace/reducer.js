import { combineReducers } from 'redux';
import * as types from './constants';
import { setFiltersValue } from '../../helpers/setObjValue';

const initialState = {
  entities: {
    marketPlaces: {},
  },
  isOpenModal: false,
  activeMP:    {},
  filters:     {
    page:     1,
    limit:    '10',
    order_by: {
      id: {
        value: 'ASC',
      },
    },
  },
};

const entities = (state = initialState.entities, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities;
    case types.LOAD_FAILURE:
      return state;
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data;
    case types.LOAD_FAILURE:
      return [];
    default:
      return state;
  }
};

const paging = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.paging;
    default:
      return state;
  }
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
    case types.UPDATE_FAILURE:
    case types.DELETE_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
    case types.UPDATE_SUCCESS:
    case types.DELETE_SUCCESS:
      return [];
    default:
      return state;
  }
};

const isOpenModal = (state = initialState.isOpenModal, action) => {
  switch (action.type) {
    case types.OPEN_MODAL:
      return true;
    case types.CLOSE_FORM:
    case types.DELETE_SUCCESS:
    case types.UPDATE_SUCCESS:
      return false;
    default:
      return state;
  }
};

const activeMP = (state = initialState.activeMP, action) => {
  switch (action.type) {
    case types.DELETE_MP:
      return action.activeMP;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    default:
      return state;
  }
};

export default combineReducers({
  entities,
  ids,
  paging,
  errors,
  isOpenModal,
  activeMP,
  filters,

});
