import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

// import { browserHistory } from 'react-router';

import { LOAD_SUCCESS, DELETE_SUCCESS, UPDATE_SUCCESS } from './constants';
import * as actions from './actions';
import * as actionsMP from '../MarketPlaceItem/actions';

function* reload() {
  yield put(actions.load());
}

function* closeAll() {
  yield put(actionsMP.closeAll());
}

function* rootSagas() {
  yield takeEvery([DELETE_SUCCESS, UPDATE_SUCCESS], reload);
  yield takeEvery([LOAD_SUCCESS], closeAll);
}

export default rootSagas;
