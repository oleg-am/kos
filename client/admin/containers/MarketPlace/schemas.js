import { Schema, arrayOf } from 'normalizr';

const marketPlaces = new Schema('marketPlaces');

const MARKET_PLACES_ARR = { data: arrayOf(marketPlaces) };

export default MARKET_PLACES_ARR;
