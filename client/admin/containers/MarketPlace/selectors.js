import { createSelector, createStructuredSelector } from 'reselect';

// import _omit from 'lodash/omit';

const REDUCER = 'marketPlace';

const marketPlacesEntities = state => state[REDUCER].entities.marketPlaces;
const marketPlacesIds      = state => state[REDUCER].ids;
const isOpenModal      = state => state[REDUCER].isOpenModal;
const activeMP      = state => state[REDUCER].activeMP;
const filters      = state => state[REDUCER].filters;

const paging            = state => state[REDUCER].paging;
const errors            = state => state[REDUCER].errors;

const marketPlaces = createSelector(
  marketPlacesEntities,
  marketPlacesIds,
  (items, ids) => ids.map(id => items[id]),
);

export default createStructuredSelector({
  marketPlaces,
  paging,
  errors,
  isOpenModal,
  activeMP,
  filters
});
