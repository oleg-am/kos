import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import { Button } from 'react-bootstrap';
import InputElement from 'react-input-mask';

const getRequiredMark = () => (
  <span style={{ color: 'red' }} >*</span>
);

class FormMdLineInputStatic extends Component {
  static propTypes = {
    value: PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
      React.PropTypes.bool,
    ]),
    placeholder: PropTypes.string,
    labelCol:    PropTypes.string,
    valueCol:    PropTypes.string,
    label:       PropTypes.string.isRequired,
    component:   PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string,
    ]),
    isError: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string,
    ]),
    isEdit:   PropTypes.bool,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    options:  PropTypes.array,
    styles:   PropTypes.object,
    type:     PropTypes.string,
  };

  static defaultProps = {
    value:       '',
    label:       '',
    isError:     false,
    isEdit:      true,
    required:    false,
    disabled:    false,
    onChange:    null,
    placeholder: '',
    component:   '',
    labelCol:    '',
    valueCol:    '',
    styles:      {},
    options:     [],
    type:        'text',
  };

  render() {
    const {
      label,
      component,
      value,
      isEdit,
      disabled,
      placeholder,
      isError,
      styles,
      type,
      options,
      required,
      onChange,
      labelCol,
      valueCol,
    } = this.props;
    return (
      <div
        style={{ marginBottom: 0, paddingTop: '5px' }}
        className={classNames('form-group form-md-line-input', { 'has-error': isError })}
      >
        <comp>
          <label
            className={classNames({ 'col-lg-6 col-md-6 col-sm-6': !labelCol }, labelCol, 'control-label')}
            style={styles}
          > { label }
            {
              required && getRequiredMark()
            }
          </label>
          {
            component !== 'info' &&
            <div className={classNames({ 'col-lg-6 col-md-6 col-sm-6': !valueCol }, valueCol)}>
              {
                !isEdit &&
                <div
                  className="form-control form-control-static"
                  style={{ wordWrap: 'break-word' }}
                > {value} </div>
              }
              {
                component === 'input' &&
                <com>
                  <input
                    onChange={onChange}
                    value={type === 'number' ? +value : value} // фикс для мозилы для type=number
                    type={type}
                    disabled={disabled}
                    className="form-control"
                    placeholder={placeholder}
                  />
                  { isError &&
                    <span
                      className={classNames({ 'has-error': isError })}
                      style={{ color: 'red' }}
                    > {isError === 'REQUIRED' &&
                        'Поле обов`язкове для заповнення.'
                      }
                    </span>
                  }
                </com>
              }
              {
                component === 'select' &&
                <com>
                  <select
                    value={value}
                    onChange={onChange}
                    disabled={disabled}
                    className="form-control"
                  > <option disabled key="-1" value="-1"> { placeholder } </option>
                    {options.map(item => (
                      <option key={item.id} value={item.id}>{item.name}</option>
                    ))}
                  </select>
                  { isError &&
                    <span
                      className={classNames({ 'has-error': isError })}
                      style={{ color: 'red' }}
                    > {isError === 'NOT_ALLOWED_VALUE' &&
                        'Поле обов`язкове до вибору.'
                      }
                    </span>
                  }
                </com>

              }
              {
                component === 'checkbox' &&
                  <label
                    htmlFor="filters_name_33"
                    className="mt-checkbox mt-checkbox-outline "
                  >
                    <input
                      id="filters_name_33"
                      type="checkbox"
                      disabled={disabled}
                      checked={value}
                      onChange={onChange}
                    />
                    <span className="checkbox" style={{ minHeight: 'auto' }} />
                  </label>
              }
              {
                component === 'phone' &&
                  <com>
                    <InputElement
                      className="form-control"
                      placeholder={placeholder}
                      mask="38 (099) 999-99-99"
                      type={type}
                      disabled={disabled}
                      value={value}
                      onChange={onChange}
                    />
                    { isError &&
                      <span
                        className={classNames({ 'has-error': isError })}
                        style={{ color: 'red' }}
                      > {isError === 'TOO_SHORT' &&
                          'Поле повинно мати 12 цифр.'
                        }
                      </span>
                    }
                  </com>
              }
              {
                component === 'button' &&
                <Button
                  disabled={disabled}
                  onClick={onChange}
                >
                  { placeholder }
                </Button>
              }
              <div className="form-control-focus" />
            </div>
          }
        </comp>
      </div>
    );
  }
}

export default FormMdLineInputStatic;
