import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';
import FormMdLineInputStatic from './FormMdLineInputStatic';

class RemaindersTable extends Component {
  static propTypes = {
    fields:               PropTypes.object,
    label:                PropTypes.string,
    editButton:           PropTypes.func,
    addButton:            PropTypes.func,
    delButton:            PropTypes.func,
    isEditableRoleAccess: PropTypes.bool,
  }

  render() {
    const {
      editButton,
      delButton,
      addButton,
      label,
      fields,
      isEditableRoleAccess,
    } = this.props;
    return (
      <div
        className="portlet box dark"
        style={{
          marginBottom: '0px',
        }}
      >
        <div className="portlet-title">
          <div className="caption">
            <i className="fa fa-cubes" />
            { label }
          </div>
          <div className="actions">
            <a
              disabled={!isEditableRoleAccess}
              className="btn btn-default btn-sm"
              onClick={isEditableRoleAccess && addButton}
              style={{ color: '#ffffff', borderColor: '#ffffff' }}
              title=""
            >
              <i className="fa fa-plus" style={{ color: '#ffffff' }} />
              Додати поле
            </a>
          </div>
        </div>
        <div className="portlet-body">
          <div className="table-scrollable">
            <table className="table table-striped table-hover">
              <thead>
                <tr>
                  <th> # </th>
                  <th> Поле в системi </th>
                  <th> Поле на майданчику </th>
                  <th> Тип поля </th>
                  {
                    isEditableRoleAccess &&
                    <th colSpan={2} style={{ textAlign: 'center' }}>
                      <i className="fa fa-cogs" />
                    </th>
                  }
                </tr>
              </thead>
              <tbody>

                {
                  Object.values(fields).map(({ id, fieldName, paramName, fieldType }, key) => (
                    <tr key={id}>
                      <td>{ key + 1 }</td>
                      <td>{ fieldName }</td>
                      <td>{ paramName }</td>
                      <td>{ fieldType }</td>
                      {
                        isEditableRoleAccess &&
                        <td>
                          <i
                            // редактируем выбраное поле
                            onClick={editButton(fields[id], 'edit')}
                            className="fa fa-pencil pull-right font-hover-green-jungle"
                            style={{ cursor: 'pointer' }}
                          />
                        </td>
                      }
                      {
                        isEditableRoleAccess &&
                        <td>
                          <i
                            // id для удаления, key для вывода в модалку подтвержения.
                            onClick={delButton(id, key + 1)}
                            className="fa fa-times pull-right font-hover-red"
                            style={{ cursor: 'pointer' }}
                          />
                        </td>
                      }
                    </tr>
                  ))
                }
                {
                  Object.values(fields).length === 0 &&
                  <tr key="err">
                    <td style={{ textAlign: 'center' }} colSpan={5}>Нiчого не знайдено.</td>
                  </tr>
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default RemaindersTable;
