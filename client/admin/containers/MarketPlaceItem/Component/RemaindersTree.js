import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';
import FormMdLineInputStatic from './FormMdLineInputStatic';

class RemaindersTree extends Component {
  static propTypes = {
    remaindersChainEntity: PropTypes.array,
    onChange:              PropTypes.func,
    currentField:          PropTypes.object,
    formErrors:            PropTypes.object,
  }

  state = {}

  componentWillReceiveProps(next) {
    this.readArr(next.remaindersChainEntity);
  }

  readArr = (ar, name) => {
    const { currentField } = this.props;
    return ar.map(item => {
      const key = name ? name + '.' + item.name : item.name;
      const perent = name ? name : item.name;
      // пока выкл.
      // if (key === currentField.fieldName) {
        // если есть выбраное поле -> открываем дерево к нему.
        // работает только до 2й вложености.
        // когда 3я и более открывается только родитель самого итема.
        // this.setState({
        //   [key]:    false,
        //   [perent]: true,
        // });
      // } else {
        this.setState({ [key]: false });
      // }
      // если есть еще вложеность запускаемся поновой и читаем пока не кончится.
      item.fields && this.readArr(item.fields, key);
    });
  }

  toggle = item => () => {
    // открываем сабдерево.
    this.setState({ [item]: !this.state[item] });
  }

  readChildren = (child, parent, onChange) => {
    const { currentField } = this.props;
    const render = child.map((item, key) => (
      <ol
        key={key + item.name}
        className="dd-list"
        style={{ display: this.state[parent] === true ? 'block' : 'none' }}
      >
        <li className="dd-item" >
          <div className="dd-handle">
            {
              item.fields &&
              <i
                className={classNames(
                'fa',
                { 'fa-plus': this.state[parent + '.' + item.name] === false },
                { 'fa-minus': this.state[parent + '.' + item.name] === true },
                )}
                onClick={this.toggle(parent ? parent + '.' + item.name : item.name)}
              />
            }
            {
              !item.fields &&
              <input
                type="radio"
                onChange={onChange(['currentRemaindersField', 'fieldName'])}
                name="remainders"
                checked={parent + '.' + item.name === currentField.fieldName}
                value={parent + '.' + item.name}
              />
            }
            { ' ' + item.name }
          </div>
          {
            item.fields &&
            this.readChildren(item.fields, parent ? parent + '.' + item.name : item.name, onChange)
          }
        </li>
      </ol>
    ));
    return render;
  }

  render() {
    const {
      remaindersChainEntity,
      onChange,
      currentField,
      formErrors,
    } = this.props;
    return (
      <div className="portlet light bordered">
        <div className="portlet-body ">
          <Row>
            <Col md={6}>
              <div className="caption">
                <span
                  className="caption-subject"
                  style={{ color: formErrors.fieldName && 'red' }}
                  >
                  Назва поля в системi:
                </span>
              </div>
              <div className="dd" id="nestable_list_1">
                <ol className="dd-list">
                  {
                    remaindersChainEntity.map((item, key) => (
                      <li key={item.name + key} className="dd-item" data-id="1">
                        <div className="dd-handle">
                          {
                            item.fields &&
                            <i
                              className={classNames(
                                'fa',
                                { 'fa-plus': this.state[item.name] === false },
                                { 'fa-minus': this.state[item.name] === true })}
                              onClick={this.toggle(item.name)}
                            />
                          }
                          {
                            !item.fields &&
                            <input
                              type="radio"
                              onChange={onChange(['currentRemaindersField', 'fieldName'])}
                              name="remainders"
                              checked={item.name === currentField.fieldName}
                              value={item.name}
                            />
                          }
                          {' '+item.name }
                          </div>
                        {item.fields && this.readChildren(item.fields, item.name, onChange)}
                      </li>
                      ))
                    }
                </ol>
              </div>
            </Col>
            <Col md={6}>
              <form className="form-horizontal">
                <FormMdLineInputStatic
                  label="Назва поля на майданчику "
                  component="input"
                  required
                  isError={formErrors.paramName}
                  value={currentField.paramName}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={onChange(['currentRemaindersField', 'paramName'])}
                  labelCol="col-lg-12 col-md-12 col-sm-12"
                  valueCol="col-lg-12 col-md-12 col-sm-12"
                />
                <FormMdLineInputStatic
                  label="Тип поля "
                  component="select"
                  required
                  isError={formErrors.fieldType}
                  value={currentField.fieldType}
                  options={[
                  { id: 'integer', name: 'integer' },
                  { id: 'float', name: 'float' },
                  { id: 'boolean', name: 'boolean' },
                  { id: 'string', name: 'string' },
                  ]}
                  styles={{ textAlign: 'left' }}
                  placeholder="Виберiть тип"
                  onChange={onChange(['currentRemaindersField', 'fieldType'])}
                  labelCol="col-lg-12 col-md-12 col-sm-12"
                  valueCol="col-lg-12 col-md-12 col-sm-12"
                />
              </form>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default RemaindersTree;
