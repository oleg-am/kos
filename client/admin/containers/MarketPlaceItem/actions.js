import * as types from './constants';
// import { createParams } from '../../helpers/api';
import {
  MARKET_PLACE,
} from './schemas';

export const load = id => ({
  types: [
    types.LOAD_REQUEST,
    types.LOAD_SUCCESS,
    types.LOAD_FAILURE,
  ],
  promise: api => api.get(`/api/admin/internet_grounds/${id}?with[*]=*`),
  schema:  MARKET_PLACE,
});

export const loadOrderStatuses = () => ({
  types: [
    types.LOAD_ORDER_STATUSES_REQUEST,
    types.LOAD_ORDER_STATUSES_SUCCESS,
    types.LOAD_ORDER_STATUSES_FAILURE,
  ],
  promise: api => api.get('/api/admin/order_statuses'),
});

export const loadRemaindersChain = () => ({
  types: [
    types.LOAD_REMAINDERS_CHAIN_REQUEST,
    types.LOAD_REMAINDERS_CHAIN_SUCCESS,
    types.LOAD_REMAINDERS_CHAIN_FAILURE,
  ],
  promise: api => api.get('/api/admin/remainders_chain'),
});

export const addMarketPlace = mp => ({
  types: [
    types.CREATE_MP_REQUEST,
    types.CREATE_MP_SUCCESS,
    types.CREATE_MP_FAILURE,
  ],
  promise: api => api.post('/api/admin/internet_grounds', mp),
});

export const putMarketPlace = (id, mp) => ({
  types: [
    types.PUT_MP_REQUEST,
    types.PUT_MP_SUCCESS,
    types.PUT_MP_FAILURE,
  ],
  promise: api => api.put(`/api/admin/internet_grounds/${id}`, mp),
});

// =====================CONTACTS_START=========================================
export const addContacts = () => ({
  type: types.ADD_CONTACTS,
});

export const delContacts = id => ({
  type: types.DEL_CONTACTS, id,
});

export const editContacts = id => ({
  type: types.EDIT_CONTACTS, id,
});

export const postContactOnServer = (data, tempId) => ({
  types: [
    types.POST_CONTACTS_ON_SERVER_REQUEST,
    types.POST_CONTACTS_ON_SERVER_SUCCESS,
    types.POST_CONTACTS_ON_SERVER_FAILURE,
  ],
  promise: api => api.post('/api/admin/internet_ground_contacts', data),
  tempId,
});

export const putContactOnServer = (id, data) => ({
  types: [
    types.PUT_CONTACTS_ON_SERVER_REQUEST,
    types.PUT_CONTACTS_ON_SERVER_SUCCESS,
    types.PUT_CONTACTS_ON_SERVER_FAILURE,
  ],
  promise: api => api.put(`/api/admin/internet_ground_contacts/${id}`, data),
});

export const delContactOnServer = id => ({
  types: [
    types.DEL_CONTACTS_ON_SERVER_REQUEST,
    types.DEL_CONTACTS_ON_SERVER_SUCCESS,
    types.DEL_CONTACTS_ON_SERVER_FAILURE,
  ],
  promise: api => api.delete(`/api/admin/internet_ground_contacts/${id}`),
});
// =====================CONTACTS_END===========================================

// =====================STATUS_ALIASES_START===================================
export const addStatusAliases = () => ({
  type: types.ADD_STATUS_ALIASES,
});

export const editStatusAliases = id => ({
  type: types.EDIT_STATUS_ALIASES, id,
});

export const delStatusAliases = id => ({
  type: types.DEL_STATUS_ALIASES, id,
});

export const postStatusAliasesOnServer = (data, tempId) => ({
  types: [
    types.POST_STATUS_ALIASES_ON_SERVER_REQUEST,
    types.POST_STATUS_ALIASES_ON_SERVER_SUCCESS,
    types.POST_STATUS_ALIASES_ON_SERVER_FAILURE,
  ],
  promise: api => api.post('/api/admin/status_aliases', data),
  tempId,
});

export const putStatusAliasesOnServer = (id, data) => ({
  types: [
    types.PUT_STATUS_ALIASES_ON_SERVER_REQUEST,
    types.PUT_STATUS_ALIASES_ON_SERVER_SUCCESS,
    types.PUT_STATUS_ALIASES_ON_SERVER_FAILURE,
  ],
  promise: api => api.put(`/api/admin/status_aliases/${id}`, data),
});

export const delStatusAliasesOnServer = id => ({
  types: [
    types.DEL_STATUS_ALIASES_ON_SERVER_REQUEST,
    types.DEL_STATUS_ALIASES_ON_SERVER_SUCCESS,
    types.DEL_STATUS_ALIASES_ON_SERVER_FAILURE,
  ],
  promise: api => api.delete(`/api/admin/status_aliases/${id}`),
});
// =====================STATUS_ALIASES_END===================================

// =====================REMAINDERS_FIELDS_START================================
export const addRemaindersFieldsOnServer = data => ({
  types: [
    types.ADD_REMAINDERS_FIELDS_ON_SERVER_REQUEST,
    types.ADD_REMAINDERS_FIELDS_ON_SERVER_SUCCESS,
    types.ADD_REMAINDERS_FIELDS_ON_SERVER_FAILURE,
  ],
  promise: api => api.post('/api/admin/remainders_fields', data),
});

export const delRemaindersFieldsOnServer = id => ({
  types: [
    types.DEL_REMAINDERS_FIELDS_ON_SERVER_REQUEST,
    types.DEL_REMAINDERS_FIELDS_ON_SERVER_SUCCESS,
    types.DEL_REMAINDERS_FIELDS_ON_SERVER_FAILURE,
  ],
  promise: api => api.delete(`/api/admin/remainders_fields/${id}`),
});

export const putRemaindersFieldsOnServer = (id, data) => ({
  types: [
    types.PUT_REMAINDERS_FIELDS_ON_SERVER_REQUEST,
    types.PUT_REMAINDERS_FIELDS_ON_SERVER_SUCCESS,
    types.PUT_REMAINDERS_FIELDS_ON_SERVER_FAILURE,
  ],
  promise: api => api.put(`/api/admin/remainders_fields/${id}`, data),
});

export const addRemaindersFields = () => ({
  type: types.ADD_REMAINDERS_FIELDS,
});

export const delRemaindersFields = id => ({
  type: types.DEL_REMAINDERS_FIELDS,
  id,
});

export const editRemaindersFields = (field, status) => ({
  type: types.EDIT_REMAINDERS_FIELDS,
  field,
  status,
});
// =====================REMAINDERS_FIELDS_END==================================

// =====================REMAINDERS_LOCAL_DIFF_START============================
export const putRemaindersLocalDiffOnServer = (id, data) => ({
  types: [
    types.PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_REQUEST,
    types.PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_SUCCESS,
    types.PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_FAILURE,
  ],
  promise: api => api.put(`/api/admin/remainders_local_diff_fields/${id}`, data),
});

export const postRemaindersLocalDiffOnServer = data => ({
  types: [
    types.POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_REQUEST,
    types.POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_SUCCESS,
    types.POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_FAILURE,
  ],
  promise: api => api.post('/api/admin/remainders_local_diff_fields', data),
});

// =====================REMAINDERS_LOCAL_DIFF_END==============================

// =====================ORDER_FIELDS_START============================
export const putOrderFieldsOnServer = (id, data) => ({
  types: [
    types.PUT_ORDER_FIELDS_ON_SERVER_REQUEST,
    types.PUT_ORDER_FIELDS_ON_SERVER_SUCCESS,
    types.PUT_ORDER_FIELDS_ON_SERVER_FAILURE,
  ],
  promise: api => api.put(`/api/admin/order_fields/${id}`, data),
});

export const postOrderFieldsOnServer = data => ({
  types: [
    types.POST_ORDER_FIELDS_ON_SERVER_REQUEST,
    types.POST_ORDER_FIELDS_ON_SERVER_SUCCESS,
    types.POST_ORDER_FIELDS_ON_SERVER_FAILURE,
  ],
  promise: api => api.post('/api/admin/order_fields', data),
});

// =====================ORDER_FIELDS_END==============================

// =====================STATUS_FIELDS_START============================
export const putStatusFieldsOnServer = (id, data) => ({
  types: [
    types.PUT_STATUS_FIELDS_ON_SERVER_REQUEST,
    types.PUT_STATUS_FIELDS_ON_SERVER_SUCCESS,
    types.PUT_STATUS_FIELDS_ON_SERVER_FAILURE,
  ],
  promise: api => api.put(`/api/admin/status_fields/${id}`, data),
});

export const postStatusFieldsOnServer = data => ({
  types: [
    types.POST_STATUS_FIELDS_ON_SERVER_REQUEST,
    types.POST_STATUS_FIELDS_ON_SERVER_SUCCESS,
    types.POST_STATUS_FIELDS_ON_SERVER_FAILURE,
  ],
  promise: api => api.post('/api/admin/status_fields', data),
});

// =====================STATUS_FIELDS_END==============================

export const closeAll = () => ({
  type: types.CLOSE_ALL,
});

export const openModal = field => ({
  type: types.OPEN_MODAL,
  field,
});
export const closeModal = () => ({
  type: types.CLOSE_MODAL,
});

export const createForm = () => ({
  type: types.CREATE_FORM,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});

// =====================BLOCK_START============================================
export const editBlock = (fieldName, status) => ({
  type: types.EDIT_BLOCK,
  fieldName,
  status,
});

export const saveBlock = (fieldName, status) => ({
  type: types.SAVE_BLOCK,
  fieldName,
  status,
});

export const cancelBlock = (fieldName, status) => ({
  type: types.CANCEL_BLOCK,
  fieldName,
  status,
});
// =====================BLOCK_END==============================================

export const saveFormErrors = formErrors => ({
  type: types.SAVE_FORM_ERRORS,
  formErrors,
});
