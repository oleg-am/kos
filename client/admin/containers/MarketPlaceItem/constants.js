const module = 'MARKETPLACEITEM';

export const CREATE_FORM                   = `${module}/CREATE_FORM`;

export const LOAD_REQUEST                  = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS                  = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE                  = `${module}/LOAD_FAILURE`;

export const LOAD_REMAINDERS_CHAIN_REQUEST = `${module}/LOAD_REMAINDERS_CHAIN_REQUEST`;
export const LOAD_REMAINDERS_CHAIN_SUCCESS = `${module}/LOAD_REMAINDERS_CHAIN_SUCCESS`;
export const LOAD_REMAINDERS_CHAIN_FAILURE = `${module}/LOAD_REMAINDERS_CHAIN_FAILURE`;

export const LOAD_ORDER_STATUSES_REQUEST   = `${module}/LOAD_ORDER_STATUSES_REQUEST`;
export const LOAD_ORDER_STATUSES_SUCCESS   = `${module}/LOAD_ORDER_STATUSES_SUCCESS`;
export const LOAD_ORDER_STATUSES_FAILURE   = `${module}/LOAD_ORDER_STATUSES_FAILURE`;

export const CREATE_MP_REQUEST             = `${module}/CREATE_MP_REQUEST`;
export const CREATE_MP_SUCCESS             = `${module}/CREATE_MP_SUCCESS`;
export const CREATE_MP_FAILURE             = `${module}/CREATE_MP_FAILURE`;

export const PUT_MP_REQUEST                = `${module}/PUT_MP_REQUEST`;
export const PUT_MP_SUCCESS                = `${module}/PUT_MP_SUCCESS`;
export const PUT_MP_FAILURE                = `${module}/PUT_MP_FAILURE`;

export const CHANGE_PARAM                  = `${module}/CHANGE_PARAM`;
export const SAVE_FORM_ERRORS              = `${module}/SAVE_FORM_ERRORS`;
export const DEL_REMAINDERS_FIELDS         = `${module}/DEL_REMAINDERS_FIELDS`;

export const OPEN_MODAL                    = `${module}/OPEN_MODAL`;
export const CLOSE_MODAL                   = `${module}/CLOSE_MODAL`;
export const CLOSE_ALL                     = `${module}/CLOSE_ALL`;
// ...........................................................................
// =====================STATUS_FIELDS_START====================================
export const PUT_STATUS_FIELDS_ON_SERVER_REQUEST  = `${module}/PUT_STATUS_FIELDS_ON_SERVER_REQUEST`;
export const PUT_STATUS_FIELDS_ON_SERVER_SUCCESS  = `${module}/PUT_STATUS_FIELDS_ON_SERVER_SUCCESS`;
export const PUT_STATUS_FIELDS_ON_SERVER_FAILURE  = `${module}/PUT_STATUS_FIELDS_ON_SERVER_FAILURE`;

export const POST_STATUS_FIELDS_ON_SERVER_REQUEST = `${module}/POST_STATUS_FIELDS_ON_SERVER_REQUEST`;
export const POST_STATUS_FIELDS_ON_SERVER_SUCCESS = `${module}/POST_STATUS_FIELDS_ON_SERVER_SUCCESS`;
export const POST_STATUS_FIELDS_ON_SERVER_FAILURE = `${module}/POST_STATUS_FIELDS_ON_SERVER_FAILURE`;

// =====================STATUS_FIELDS_END======================================

// ...........................................................................
// =====================ORDER_FIELDS_START====================================
export const PUT_ORDER_FIELDS_ON_SERVER_REQUEST  = `${module}/PUT_ORDER_FIELDS_ON_SERVER_REQUEST`;
export const PUT_ORDER_FIELDS_ON_SERVER_SUCCESS  = `${module}/PUT_ORDER_FIELDS_ON_SERVER_SUCCESS`;
export const PUT_ORDER_FIELDS_ON_SERVER_FAILURE  = `${module}/PUT_ORDER_FIELDS_ON_SERVER_FAILURE`;

export const POST_ORDER_FIELDS_ON_SERVER_REQUEST = `${module}/POST_ORDER_FIELDS_ON_SERVER_REQUEST`;
export const POST_ORDER_FIELDS_ON_SERVER_SUCCESS = `${module}/POST_ORDER_FIELDS_ON_SERVER_SUCCESS`;
export const POST_ORDER_FIELDS_ON_SERVER_FAILURE = `${module}/POST_ORDER_FIELDS_ON_SERVER_FAILURE`;

// =====================ORDER_FIELDS_END======================================

// ...........................................................................
// =====================REMAINDER_LOCAL_DIFF_START============================
export const PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_REQUEST  = `${module}/PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_REQUEST`;
export const PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_SUCCESS  = `${module}/PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_SUCCESS`;
export const PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_FAILURE  = `${module}/PUT_REMAINDERS_LOCAL_DIFF_ON_SERVER_FAILURE`;

export const POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_REQUEST = `${module}/POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_REQUEST`;
export const POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_SUCCESS = `${module}/POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_SUCCESS`;
export const POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_FAILURE = `${module}/POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_FAILURE`;

// =====================REMAINDER_LOCAL_DIFF_END===============================

// ...........................................................................
// =====================BLOCK_START============================================
export const EDIT_BLOCK   = `${module}/EDIT_BLOCK`;
export const SAVE_BLOCK   = `${module}/SAVE_BLOCK`;
export const CANCEL_BLOCK = `${module}/CANCEL_BLOCK`;
// =====================BLOCK_END==============================================
// ...........................................................................
// =====================CONTACTS_START=========================================
export const ADD_CONTACTS                    = `${module}/ADD_CONTACTS`;
export const DEL_CONTACTS                    = `${module}/DEL_CONTACTS`;
export const EDIT_CONTACTS                   = `${module}/EDIT_CONTACTS`;

export const POST_CONTACTS_ON_SERVER_REQUEST = `${module}/POST_CONTACTS_ON_SERVER_REQUEST`;
export const POST_CONTACTS_ON_SERVER_SUCCESS = `${module}/POST_CONTACTS_ON_SERVER_SUCCESS`;
export const POST_CONTACTS_ON_SERVER_FAILURE = `${module}/POST_CONTACTS_ON_SERVER_FAILURE`;

export const PUT_CONTACTS_ON_SERVER_REQUEST  = `${module}/PUT_CONTACTS_ON_SERVER_REQUEST`;
export const PUT_CONTACTS_ON_SERVER_SUCCESS  = `${module}/PUT_CONTACTS_ON_SERVER_SUCCESS`;
export const PUT_CONTACTS_ON_SERVER_FAILURE  = `${module}/PUT_CONTACTS_ON_SERVER_FAILURE`;

export const DEL_CONTACTS_ON_SERVER_REQUEST  = `${module}/DEL_CONTACTS_ON_SERVER_REQUEST`;
export const DEL_CONTACTS_ON_SERVER_SUCCESS  = `${module}/DEL_CONTACTS_ON_SERVER_SUCCESS`;
export const DEL_CONTACTS_ON_SERVER_FAILURE  = `${module}/DEL_CONTACTS_ON_SERVER_FAILURE`;
// =====================CONTACTS_END===========================================
// ...........................................................................
// =====================STATUS_ALIASES_START====================================
export const ADD_STATUS_ALIASES                    = `${module}/ADD_STATUS_ALIASES`;
export const DEL_STATUS_ALIASES                    = `${module}/DEL_STATUS_ALIASES`;
export const EDIT_STATUS_ALIASES                    = `${module}/EDIT_STATUS_ALIASES`;

export const POST_STATUS_ALIASES_ON_SERVER_REQUEST = `${module}/POST_STATUS_ALIASES_ON_SERVER_REQUEST`;
export const POST_STATUS_ALIASES_ON_SERVER_SUCCESS = `${module}/POST_STATUS_ALIASES_ON_SERVER_SUCCESS`;
export const POST_STATUS_ALIASES_ON_SERVER_FAILURE = `${module}/POST_STATUS_ALIASES_ON_SERVER_FAILURE`;

export const PUT_STATUS_ALIASES_ON_SERVER_REQUEST  = `${module}/PUT_STATUS_ALIASES_ON_SERVER_REQUEST`;
export const PUT_STATUS_ALIASES_ON_SERVER_SUCCESS  = `${module}/PUT_STATUS_ALIASES_ON_SERVER_SUCCESS`;
export const PUT_STATUS_ALIASES_ON_SERVER_FAILURE  = `${module}/PUT_STATUS_ALIASES_ON_SERVER_FAILURE`;

export const DEL_STATUS_ALIASES_ON_SERVER_REQUEST  = `${module}/DEL_STATUS_ALIASES_ON_SERVER_REQUEST`;
export const DEL_STATUS_ALIASES_ON_SERVER_SUCCESS  = `${module}/DEL_STATUS_ALIASES_ON_SERVER_SUCCESS`;
export const DEL_STATUS_ALIASES_ON_SERVER_FAILURE  = `${module}/DEL_STATUS_ALIASES_ON_SERVER_FAILURE`;
// =====================STATUS_ALIASES_END=====================================
// ...........................................................................
// =====================REMAINDERS_FIELDS_START================================
export const ADD_REMAINDERS_FIELDS                   = `${module}/ADD_REMAINDERS_FIELDS`;
export const EDIT_REMAINDERS_FIELDS                  = `${module}/EDIT_REMAINDERS_FIELDS`;
export const CONFIRM_EDITED_REMAINDERS_FIELDS        = `${module}/CONFIRM_EDITED_REMAINDERS_FIELDS`;

export const ADD_REMAINDERS_FIELDS_ON_SERVER_REQUEST = `${module}/ADD_REMAINDERS_FIELDS_ON_SERVER_REQUEST`;
export const ADD_REMAINDERS_FIELDS_ON_SERVER_SUCCESS = `${module}/ADD_REMAINDERS_FIELDS_ON_SERVER_SUCCESS`;
export const ADD_REMAINDERS_FIELDS_ON_SERVER_FAILURE = `${module}/ADD_REMAINDERS_FIELDS_ON_SERVER_FAILURE`;

export const DEL_REMAINDERS_FIELDS_ON_SERVER_REQUEST = `${module}/DEL_REMAINDERS_FIELDS_ON_SERVER_REQUEST`;
export const DEL_REMAINDERS_FIELDS_ON_SERVER_SUCCESS = `${module}/DEL_REMAINDERS_FIELDS_ON_SERVER_SUCCESS`;
export const DEL_REMAINDERS_FIELDS_ON_SERVER_FAILURE = `${module}/DEL_REMAINDERS_FIELDS_ON_SERVER_FAILURE`;

export const PUT_REMAINDERS_FIELDS_ON_SERVER_REQUEST = `${module}/PUT_REMAINDERS_FIELDS_ON_SERVER_REQUEST`;
export const PUT_REMAINDERS_FIELDS_ON_SERVER_SUCCESS = `${module}/PUT_REMAINDERS_FIELDS_ON_SERVER_SUCCESS`;
export const PUT_REMAINDERS_FIELDS_ON_SERVER_FAILURE = `${module}/PUT_REMAINDERS_FIELDS_ON_SERVER_FAILURE`;
// =====================REMAINDERS_FIELDS_END==================================
// ...........................................................................
