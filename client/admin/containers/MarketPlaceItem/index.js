import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import classNames from 'classnames';
import { Button, Row, Col } from 'react-bootstrap';
import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';
import * as actions from './actions';
import selectors from './selectors';
import FormMdLineInputStatic from './Component/FormMdLineInputStatic';
import RemaindersTree from './Component/RemaindersTree';
import RemaindersTable from './Component/RemaindersTable';
import translateStatus from './Component/Statusies';
import {
  postMpValidator,
  localDiffValidator,
  orderFieldsValidator,
  statusFieldsValidator,
  contactsValidator,
  statusAliasesValidator,
  remainderTreeValidator,
} from './schemas';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

class MarketPlaceItem extends Component {
  static propTypes = {
    isLoad:                          PropTypes.bool,
    isOpenModal:                     PropTypes.bool,
    isLoadRemaindersChain:           PropTypes.bool,
    isEditableRoleAccess:            PropTypes.bool,
    load:                            PropTypes.func.isRequired,
    closeAll:                        PropTypes.func.isRequired,
    openModal:                       PropTypes.func.isRequired,
    saveBlock:                       PropTypes.func.isRequired,
    editBlock:                       PropTypes.func.isRequired,
    cancelBlock:                     PropTypes.func.isRequired,
    createForm:                      PropTypes.func.isRequired,
    closeModal:                      PropTypes.func.isRequired,
    changeParam:                     PropTypes.func.isRequired,
    addContacts:                     PropTypes.func.isRequired,
    delContacts:                     PropTypes.func.isRequired,
    editContacts:                    PropTypes.func.isRequired,
    saveFormErrors:                  PropTypes.func.isRequired,
    addMarketPlace:                  PropTypes.func.isRequired,
    addStatusAliases:                PropTypes.func.isRequired,
    delStatusAliases:                PropTypes.func.isRequired,
    editStatusAliases:               PropTypes.func.isRequired,
    loadOrderStatuses:               PropTypes.func.isRequired,
    delContactOnServer:              PropTypes.func.isRequired,
    putContactOnServer:              PropTypes.func.isRequired,
    postContactOnServer:             PropTypes.func.isRequired,
    loadRemaindersChain:             PropTypes.func.isRequired,
    delRemaindersFields:             PropTypes.func.isRequired,
    addRemaindersFields:             PropTypes.func.isRequired,
    editRemaindersFields:            PropTypes.func.isRequired,
    putOrderFieldsOnServer:          PropTypes.func.isRequired,
    postOrderFieldsOnServer:         PropTypes.func.isRequired,
    putStatusFieldsOnServer:         PropTypes.func.isRequired,
    postStatusFieldsOnServer:        PropTypes.func.isRequired,
    delStatusAliasesOnServer:        PropTypes.func.isRequired,
    putStatusAliasesOnServer:        PropTypes.func.isRequired,
    postStatusAliasesOnServer:       PropTypes.func.isRequired,
    delRemaindersFieldsOnServer:     PropTypes.func.isRequired,
    putRemaindersFieldsOnServer:     PropTypes.func.isRequired,
    addRemaindersFieldsOnServer:     PropTypes.func.isRequired,
    putRemaindersLocalDiffOnServer:  PropTypes.func.isRequired,
    postRemaindersLocalDiffOnServer: PropTypes.func.isRequired,
    contactsEntity:                  PropTypes.shape({
      createdAt:      PropTypes.string,
      id:             PropTypes.number,
      name:           PropTypes.string,
      position:       PropTypes.string,
      updatedAt:      PropTypes.string,
      internetGround: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
      ]),
    }),
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
    isEditBlock: PropTypes.shape({
      orderFields:               PropTypes.bool,
      statusFields:              PropTypes.bool,
      remaindersLocalDiffFields: PropTypes.bool,
    }),
    statusAliasesEntity: PropTypes.shape({
      alias:          PropTypes.string,
      createdAt:      PropTypes.string,
      id:             PropTypes.number,
      internetGround: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
      ]),
      itemAlias: PropTypes.string,
      status:    PropTypes.shape({
        id:   PropTypes.number,
        name: PropTypes.string,
      }),
    }),
    remaindersFieldsEntity: PropTypes.shape({
      createdAt: PropTypes.string,
      fieldName: PropTypes.string,
      fieldType: PropTypes.string,
      id:        PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      internetGround: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
      ]),
      paramName: PropTypes.string,
      updatedAt: PropTypes.string,
    }),
    currentRemaindersField: PropTypes.shape({
      fieldName: PropTypes.string,
      fieldType: PropTypes.string,
      id:        PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      paramName:      PropTypes.string,
      internetGround: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
      ]),
    }),
    fieldInModal: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
    ]),
    remaindersChainEntity: PropTypes.array,
    orderStatuses:         PropTypes.array,
    formErrors:            PropTypes.objectOf(PropTypes.any),
    thisMp:                PropTypes.objectOf(PropTypes.any).isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const {
      params,
      createForm,
      load,
      loadOrderStatuses,
      loadRemaindersChain,
    } = props;
    // если нажата кнопка на создать Площадку создаем форму с пустыми полями,
    // иначе загружаем данные с сервера по id площадки.
    params.id === 'create' ? createForm() : load(params.id);
    // загружаем статусы заказов.
    loadOrderStatuses();
    // загружаем поля системы по остаткам.
    loadRemaindersChain();
  }

  onChangeParam = (key, value) => (e) => {
    const { changeParam } = this.props;
    // записуем введенные параметры юзером в нашу форму по названию поля(для text).
    changeParam(key, value || e.target.value);
  }

  onChangeParamSelect = (key, value) => (e) => {
    e.preventDefault();
    // записуем введенные параметры юзером в нашу форму по названию поля(для select).
    this.props.changeParam(key, value);
  }

  onChangeParamChecked = key => (e) => {
    // записуем введенные параметры юзером в нашу форму по названию поля(для checked).
    this.props.changeParam(key, e.target.checked);
  }

  closeAll = () => {
    const { closeAll } = this.props;
    // очищаем форму при выходе со страницы.
    closeAll();
    // переходим на вкладку все хплощадок.
    browserHistory.push('/admin/settings/marketPlace');
  }
  // открываем на редактирование контакт.
  editContact = id => () => {
    const { editContacts } = this.props;
    editContacts(id);
  }
  // сохраняем юзера.
  saveContact = id => () => {
    const {
      thisMp,
      postContactOnServer,
      saveFormErrors,
      putContactOnServer,
    } = this.props;
    const isNew = typeof id === 'string';
    const currentContact = {
      ...thisMp.entities.contacts[id],
      pib: thisMp.entities.contacts[id].name,
      tel: thisMp.entities.contacts[id].tel.replace(/([^0-9])/g, ''),
    };
    let isContactsValid = contactsValidator.validate(currentContact);
    // если форма создания площадки то проверяем на валидность.
    if (isContactsValid) {
      isContactsValid = {
        ...isContactsValid,
        name: isContactsValid.pib,
      };
      delete isContactsValid.pib;
      // если форма прошла валидацию - отправляем форму на сервер.
      if (isNew) {
        // если новая то добавляем
        postContactOnServer(isContactsValid, id);
      } else {
        // если не новая -> изменяем.
        putContactOnServer(id, isContactsValid);
      }
    } else {
      console.log('contactsValidator.getErrors()', contactsValidator.getErrors());
      // если форма непрошла валидацию - формируем офибки и выводим их на экран.
      saveFormErrors({
        ...contactsValidator.getErrors(),
        [`id${id}`]: id,
      });
    }
  }

  delContactsInModal = (id, name) => () => {
    const { openModal, delContacts } = this.props;
    // если тип id === string => контакт новый.
    const isNew = typeof id === 'string';
    if (!isNew) {
      // если не новодобавленный контакт то спрашиваем согласия на удаление.
      openModal({
        action:  'delContacts',
        id:      id,
        name:    name,
        onClick: this.delContacts,
      });
    } else {
      // чистим локально масив.
      delContacts(id);
    }
  }

  // удаляем выбраный контакт
  delContacts = id => () => {
    const { delContactOnServer, delContacts, closeModal } = this.props;
    // Удаляем на сервере
    delContactOnServer(id);
    // чистим локально масив.
    delContacts(id);
    // закрываем модальное окно
    closeModal();
  }

  // открываем на редактирование статус.
  editStatusAliases = id => () => {
    const { editStatusAliases } = this.props;
    editStatusAliases(id);
  }
  // сохраняем статус
  saveStatusAliases = id => () => {
    const {
      thisMp,
      saveFormErrors,
      postStatusAliasesOnServer,
      putStatusAliasesOnServer,
    } = this.props;
    const isNew = typeof id === 'string';
    const currentStatusAliases = {
      ...thisMp.entities.statusAliases[id],
      statusInStatusAliases: thisMp.entities.statusAliases[id].status.id,
    };
    let isStatusAliases = statusAliasesValidator.validate(currentStatusAliases);
    // если форма создания площадки то проверяем на валидность.
    if (isStatusAliases) {
      isStatusAliases = {
        ...isStatusAliases,
        status: isStatusAliases.statusInStatusAliases,
      };
      delete isStatusAliases.statusInStatusAliases;
      // если форма прошла валидацию - отправляем форму на сервер.
      if (isNew) {
        // если новая то добавляем
        postStatusAliasesOnServer(isStatusAliases, id);
      } else {
        // если не новая -> изменяем.
        putStatusAliasesOnServer(id, isStatusAliases);
      }
    } else {
      // если форма непрошла валидацию - формируем офибки и выводим их на экран.
      saveFormErrors({
        ...statusAliasesValidator.getErrors(),
        [`ids${id}`]: id,
      });
    }
  }

  delStatusAliasesInModal = (id, name) => () => {
    const { openModal, delStatusAliases } = this.props;
    // если тип id === string => контакт новый.
    const isNew = typeof id === 'string';
    if (!isNew) {
      // если не новодобавленный контакт то спрашиваем согласия на удаление.
      openModal({
        action:  'delStatusAliases',
        id:      id,
        name:    name,
        onClick: this.delStatusAliases,
      });
    } else {
      // чистим локально масив.
      delStatusAliases(id);
    }
  }
  // удаляем статус алиаса
  delStatusAliases = id => () => {
    const {
      closeModal,
      delStatusAliases,
      delStatusAliasesOnServer,
    } = this.props;
    // Удаляем на сервере
    delStatusAliasesOnServer(id);
    // чистим локально масив.
    delStatusAliases(id);
    // закрываем модальное окно
    closeModal();
  }

  // создаем новую форму для нового поля остатки.
  addRemaindersFields = () => {
    const { openModal, addRemaindersFields } = this.props;
    // открываем модальное окно для блока остатки.
    openModal({ action: 'remainders' });
    // создаем пустое поле
    addRemaindersFields();
  }
  // добавляем заполненое поле остатки в масив всех полей.
  pushNewRemaindersField = () => {
    const {
      closeModal,
      currentRemaindersField,
      addRemaindersFieldsOnServer,
      saveFormErrors,
    } = this.props;
    const isRemaindersFieldValid = remainderTreeValidator.validate(currentRemaindersField);
    if (isRemaindersFieldValid) {
      // отправляем запрос на сервер на добавление.
      addRemaindersFieldsOnServer(isRemaindersFieldValid);
      // закрываем модальное окно и очищаем форму добавления нового поля "остатки"
      closeModal();
    } else {
      // если форма непрошла валидацию - формируем офибки и выводим их на экран.
      saveFormErrors(remainderTreeValidator.getErrors());
    }
  }

  editRemaindersFields = (field, status) => () => {
    const {
      openModal,
      editRemaindersFields,
    } = this.props;
    // задаем дефолтные значения или подставляем выбраные для выбраного поля поля
    editRemaindersFields(field, status);
    // открываем модальное окно для блока остатки.
    openModal({ action: 'remainders' });
  }

  confirmEditedRemaindersField = () => {
    const {
      closeModal,
      putRemaindersFieldsOnServer,
      currentRemaindersField,
      saveFormErrors,
    } = this.props;
    // Изменяем обьект с id равным тому что мы редактировали.
    const id = currentRemaindersField.id;
    const field = {
      // сама форма.
      ...currentRemaindersField,
      internetGround: currentRemaindersField.internetGround.id,
    };
    const isRemaindersFieldValid = remainderTreeValidator.validate(field);
    if (isRemaindersFieldValid) {
      // отправляем запрос на сервер на добавление.
      putRemaindersFieldsOnServer(id, isRemaindersFieldValid);
      // закрываем модальное окно и очищаем форму добавления нового поля "остатки"
      closeModal();
    } else {
      // если форма непрошла валидацию - формируем офибки и выводим их на экран.
      saveFormErrors(remainderTreeValidator.getErrors());
    }
  }

  delRemaindersInModal = (id, nomer) => () => {
    const { openModal } = this.props;
    openModal({
      action:  'delRemainders',
      id:      id,
      name:    nomer,
      onClick: this.delRemaindersFields,
    });
  }

  // удаляем поле остатки.
  delRemaindersFields = id => () => {
    const {
      closeModal,
      delRemaindersFields,
      delRemaindersFieldsOnServer,
    } = this.props;
    // удаляем выбраное поле остатков и заносим его в масив удаленных.
    delRemaindersFields(id); // удаляем локально
    delRemaindersFieldsOnServer(id); // делаем del запрос на сервер по id
    closeModal();
  }
  // сохраняем дефолную форму.
  saveForm = () => {
    const {
      thisMp,
      params,
      addMarketPlace,
      putMarketPlace,
      saveFormErrors,
    } = this.props;
    const id = thisMp.id;
    const isPostMpValid = postMpValidator.validate(thisMp);
    // если форма создания площадки то проверяем на валидность.
    if (isPostMpValid) {
      // если форма прошла валидацию - отправляем форму на сервер.
      if (params.id === 'create') {
        addMarketPlace(isPostMpValid);
      } else {
        putMarketPlace(id, isPostMpValid);
      }
    } else {
      // если форма непрошла валидацию - формируем офибки и выводим их на экран.
      saveFormErrors(postMpValidator.getErrors());
    }
  }
  // Сохраняем блок "Локальнi резерви"
  saveBlockLocalDiff = () => {
    const {
      saveBlock,
      putRemaindersLocalDiffOnServer,
      postRemaindersLocalDiffOnServer,
      saveFormErrors,
      thisMp,
    } = this.props;
    const id = thisMp.remaindersLocalDiffFields.id;
    const field = {
      ...thisMp.remaindersLocalDiffFields,
      internetGround: thisMp.id,
    };
    const isLocalDiffValid = localDiffValidator.validate(field);

    if (isLocalDiffValid) {
      if (id) {
        // если id есть есть значит put
        putRemaindersLocalDiffOnServer(id, isLocalDiffValid);
      } else {
        // если нету id значит будет post
        postRemaindersLocalDiffOnServer(isLocalDiffValid);
      }
      // закрываем на редактирование блок.
      saveBlock('remaindersLocalDiffFields', false);
    } else {
      // если форма непрошла валидацию - формируем офибки и выводим их на экран.
      saveFormErrors(localDiffValidator.getErrors());
    }
  }
  // сохраняем блок "Замовлення"
  saveBlockOrderFields= () => {
    const {
      saveBlock,
      putOrderFieldsOnServer,
      postOrderFieldsOnServer,
      saveFormErrors,
      thisMp,
    } = this.props;
    const id = thisMp.orderFields.id;
    const field = {
      ...thisMp.orderFields,
      internetGround:          thisMp.id,
      itemNomenclatureInOrder: thisMp.orderFields.itemNomenclature,
    };
    let isOrderFieldsValid = orderFieldsValidator.validate(field);

    if (isOrderFieldsValid) {
      // переименовуем поле в такое как хочет сервер и удаляем то что мы отвалидировали.
      isOrderFieldsValid = {
        ...isOrderFieldsValid,
        itemNomenclature: isOrderFieldsValid.itemNomenclatureInOrder,
      };
      delete isOrderFieldsValid.itemNomenclatureInOrder;
      if (id) {
        // если id есть есть значит put
        putOrderFieldsOnServer(id, isOrderFieldsValid);
      } else {
        // если нету id значит будет post
        postOrderFieldsOnServer(isOrderFieldsValid);
      }
      // закрываем на редактирование блок.
      saveBlock('orderFields', false);
    } else {
      // если форма непрошла валидацию - формируем офибки и выводим их на экран.
      saveFormErrors(orderFieldsValidator.getErrors());
    }
  }
  // сохраняем блок "Статуси"
  saveBlockStatusFields = () => {
    const {
      saveBlock,
      putStatusFieldsOnServer,
      postStatusFieldsOnServer,
      saveFormErrors,
      thisMp,
    } = this.props;
    const id = thisMp.statusFields.id;
    const field = {
      ...thisMp.statusFields,
      internetGround:                 thisMp.id,
      itemSectionNameInStatusFields:  thisMp.statusFields.itemSectionName,
      itemNomenclatureInStatusFields: thisMp.statusFields.itemNomenclature,
      codeInStatusFields:             thisMp.statusFields.code,
    };
    let isStatusFieldsValid = statusFieldsValidator.validate(field);

    if (isStatusFieldsValid) {
      // переименовуем поле в такое как хочет сервер и удаляем то что мы отвалидировали.
      isStatusFieldsValid = {
        ...isStatusFieldsValid,
        itemSectionName:  isStatusFieldsValid.itemSectionNameInStatusFields,
        itemNomenclature: isStatusFieldsValid.itemNomenclatureInStatusFields,
        code:             isStatusFieldsValid.codeInStatusFields,
      };
      delete isStatusFieldsValid.itemSectionNameInStatusFields;
      delete isStatusFieldsValid.itemNomenclatureInStatusFields;
      delete isStatusFieldsValid.codeInStatusFields;

      if (id) {
        // если id есть есть значит put
        putStatusFieldsOnServer(id, isStatusFieldsValid);
      } else {
        // если нету id значит будет post
        postStatusFieldsOnServer(isStatusFieldsValid);
      }
      // закрываем на редактирование блок.
      saveBlock('statusFields', false);
    } else {
      // если форма непрошла валидацию - формируем офибки и выводим их на экран.
      saveFormErrors(statusFieldsValidator.getErrors());
    }
  }

  render() {
    const {
      thisMp,
      formErrors,
      isLoad,
      params,
      isEditBlock,
      editBlock,
      cancelBlock,
      isOpenModal,
      fieldInModal,
      closeModal,
      statusAliasesEntity,
      addStatusAliases,
      addContacts,
      contactsEntity,
      orderStatuses,
      remaindersChainEntity,
      remaindersFieldsEntity,
      isLoadRemaindersChain,
      isEditableRoleAccess,
      currentRemaindersField,
    } = this.props;

    const isNew = params.id === 'create';

    return (
  isLoad ?
    <img
      src="/build/admin/images/cube.gif"
      style={{ top: '20%' }}
      className="preloader"
      alt="preloader"
    />
  : <div style={{ paddingTop: '40px' }}>
    <Breadcrumbs
      title=""
      components={{
        title: 'Картка майданчика',
        links: [
          {
            name: 'Налаштування',
            to:   '/admin/settings/general',
          }, {
            name: 'Майданчики',
            to:   '/admin/settings/marketPlace',
          }, {
            name: 'Картка майданчика',
          },
        ],
      }}
    />
    <Portlet
      actionsRight={[
        <button
          style={{ marginRight: '10px' }}
          key="actionsRight_3"
          className="btn green-meadow"
          onClick={this.saveForm}
          disabled={!isEditableRoleAccess}
        >
          <i className="fa fa-check" /> Зберегти
        </button>,

        <Link
          key="actionsRight_4"
          className="btn btn-sm default"
          onClick={this.closeAll}
        >
          <i className="fa fa-times" /> Закрити
        </Link>,

      ]}
    >
      <Row>
        <Col md={12} sm={12}>
          <Portlet
            className={'box blue-dark'}
            title={[
              <i key={'osnovne_1'} className="fa fa-cogs" />,
              <span key={'osnovne_2'} >Основне</span>,
            ]}
          > <Row>
            <Col md={6}>
              <form className="form-horizontal">
                <FormMdLineInputStatic
                  label="Назва майданчика:"
                  component="input"
                  required
                  disabled={!isEditableRoleAccess}
                  isError={formErrors.name}
                  value={thisMp.name}
                  styles={{ textAlign: 'left' }}
                  placeholder="Введiть назву майданчика"
                  onChange={this.onChangeParam('name')}
                  labelCol="col-lg-4 col-md-4 col-sm-4"
                  valueCol="col-lg-8 col-md-8 col-sm-8"
                />
                <FormMdLineInputStatic
                  label="Адреса майданчика:"
                  component="input"
                  required
                  disabled={!isEditableRoleAccess}
                  isError={formErrors.url}
                  value={thisMp.url}
                  styles={{ textAlign: 'left' }}
                  placeholder="Введiть адресу майданчика"
                  onChange={this.onChangeParam('url')}
                  labelCol="col-lg-4 col-md-4 col-sm-4"
                  valueCol="col-lg-8 col-md-8 col-sm-8"
                />
              </form>
            </Col>
            <Col md={3}>
              <Row>
                <form className="form-horizontal">
                  <label style={{ textAlign: 'left' }} className="col-lg-4 col-md-4 col-sm-4 control-label">Статус:</label>
                  <button
                    disabled={!isEditableRoleAccess}
                    className={classNames('btn btn-icon-only', thisMp.enabled ? ' green-meadow' : ' red')}
                    title={thisMp.enabled ? 'Вiдключити' : 'Пiдключити'}
                    onClick={this.onChangeParamSelect('enabled', !thisMp.enabled)}
                  ><i className={classNames('fa', thisMp.enabled ? 'fa-toggle-on' : 'fa-toggle-off')} aria-hidden="true" />
                  </button>
                </form>
              </Row>
            </Col>
          </Row>
           <hr />
          <Row>
            <Col md={4}>
              <Portlet
                className={'box blue'}
                title={[
                  <i key="zamovlenya1" className="fa fa-shopping-cart" />,
                  <span key="zamovlenya2">Замовлення</span>,
                ]}
              >
                <Row>
                  <Col>
                    <FormMdLineInputStatic
                      label="Вкажіть URL:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      isError={formErrors.orderUrl}
                      value={thisMp.orderUrl}
                      styles={{ textAlign: 'left' }}
                      placeholder=""
                      onChange={this.onChangeParam('orderUrl')}
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      valueCol="col-lg-8 col-md-8 col-sm-8"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormMdLineInputStatic
                      label="Вкажіть період збору данних(хв.):"
                      component="input"
                      type="number"
                      disabled={!isEditableRoleAccess}
                      // required
                      isError={formErrors.orderPeriod}
                      value={thisMp.orderPeriod}
                      styles={{ textAlign: 'left' }}
                      placeholder=""
                      onChange={this.onChangeParam('orderPeriod')}
                      labelCol="col-lg-7 col-md-7 col-sm-7"
                      valueCol="col-lg-5 col-md-5 col-sm-5"
                    />
                  </Col>
                </Row>
              </Portlet>
            </Col>
            <Col md={4}>
              <Portlet
                className={'box blue'}
                title={[
                  <i key="localDiff1" className="fa fa-industry" />,
                  <span key="localDiff2">Локальнi резерви</span>,
                ]}
              >
                <Row>
                  <Col>
                    <FormMdLineInputStatic
                      label="Вкажіть URL:"
                      component="input"
                      value={thisMp.remaindersLocalDiffUrl}
                      styles={{ textAlign: 'left' }}
                      placeholder=""
                      disabled={!isEditableRoleAccess}
                      onChange={this.onChangeParam('remaindersLocalDiffUrl')}
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      valueCol="col-lg-8 col-md-8 col-sm-8"
                    />
                  </Col>
                </Row>
              </Portlet>
            </Col>
            <Col md={4}>
              <Portlet
                className={'box blue'}
                title={[
                  <i key="status1" className="fa fa-flag" />,
                  <span key="status2">Статуси</span>,
                ]}
              >
                <Row>
                  <Col>
                    <FormMdLineInputStatic
                      label="Вкажіть URL:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      value={thisMp.statusUrl}
                      styles={{ textAlign: 'left' }}
                      placeholder=""
                      onChange={this.onChangeParam('statusUrl')}
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      valueCol="col-lg-8 col-md-8 col-sm-8"
                    />
                  </Col>
                </Row>
              </Portlet>
            </Col>
          </Row>

            <Row>
              {Object.values(contactsEntity).map(({ id, name, position, tel, isEditThisContact }) => (
                <Col key={id} md={3}>
                  <Portlet
                    className={'box yellow-mint'}
                    actionsLeft={[
                      <i key={`info_${id}`} className="fa fa-user" aria-hidden="true" />,
                      <span key={`info_text_${id}`}> Контактнi данi спiвробiтникiв:</span>,
                    ]}
                    actionsRight={[
                      <div key={'div'}>
                        {
                          isEditThisContact ?
                          // если статус = true --> показуем кнопку Save
                            <button
                              disabled={!isEditableRoleAccess}
                              key={`check_b_${id}`}
                              style={{ borderColor: '#FFF', marginRight: '5px' }}
                              onClick={this.saveContact(id)}
                              className="btn btn-icon-only btn-default btn-sm"
                              title="Зберегти"
                            >
                              <i style={{ color: '#FFF' }} className="fa fa-save" aria-hidden="true" />
                            </button>
                          :
                          // если нажали на кнопку Save показуем Edit
                            <button
                              disabled={!isEditableRoleAccess}
                              key={`check_e_${id}`}
                              style={{ borderColor: '#FFF', marginRight: '5px' }}
                              onClick={this.editContact(id)}
                              className="btn btn-icon-only btn-default btn-sm"
                              title="Редагувати"
                            >
                              <i style={{ color: '#FFF' }} className="fa fa-edit" aria-hidden="true" />
                            </button>
                        }
                        <button
                          disabled={!isEditableRoleAccess}
                          key={`check_d_${id}`}
                          style={{ borderColor: '#FFF' }}
                          onClick={this.delContactsInModal(id, name)}
                          className="btn btn-icon-only btn-default btn-sm"
                          title="Видалити"
                        >
                          <i style={{ color: '#FFF' }} className="fa fa-times" aria-hidden="true" />
                        </button>
                      </div>,
                    ]}
                  >
                    <form className="form-horizontal">
                      <FormMdLineInputStatic
                        label="ПIБ:"
                        component={isEditThisContact && "input"}
                        isEdit={isEditThisContact || false}
                        isError={id === formErrors[`id${id}`] && formErrors.pib}
                        required={isEditThisContact}
                        value={name}
                        disabled={!isEditableRoleAccess}
                        placeholder="Введiть ПIБ"
                        labelCol="col-lg-3 col-md-3 col-sm-3"
                        valueCol="col-lg-9 col-md-9 col-sm-9"
                        onChange={this.onChangeParam(['entities', 'contacts', id, 'name'])}
                      />
                      <FormMdLineInputStatic
                        label="Посада:"
                        component={isEditThisContact && "input"}
                        isEdit={isEditThisContact || false}
                        value={position}
                        disabled={!isEditableRoleAccess}
                        placeholder="Введiть посаду"
                        labelCol="col-lg-3 col-md-3 col-sm-3"
                        valueCol="col-lg-9 col-md-9 col-sm-9"
                        onChange={this.onChangeParam(['entities', 'contacts', id, 'position'])}
                      />
                      <FormMdLineInputStatic
                        label="Телефон:"
                        component={isEditThisContact && "phone"}
                        isEdit={isEditThisContact || false}
                        isError={id === formErrors[`id${id}`] && formErrors.tel}
                        value={tel}
                        disabled={!isEditableRoleAccess}
                        placeholder="Введiть телефон"
                        labelCol="col-lg-3 col-md-3 col-sm-3"
                        valueCol="col-lg-9 col-md-9 col-sm-9"
                        onChange={this.onChangeParam(['entities', 'contacts', id, 'tel'])}
                      />
                    </form>
                  </Portlet>
                </Col>
              ))}
              {
                !isNew &&
                <Col md={3}>
                  <Button
                    onClick={addContacts}
                    disabled={!isEditableRoleAccess}
                    className="btn btn-sm green-meadow"
                  > <i className={'fa fa-plus'} />
                    Додати спiвробiтника
                  </Button>
                </Col>
              }
            </Row>
          </Portlet>
        </Col>
      </Row>

      <Row>
        <Col md={12} sm={12}>
          <Portlet
            className={'box blue-dark'}
            title={[
              <i key={'settings_1'} className="fa fa-cogs" />,
              <span key={'settings_2'} >Налаштування майданчика</span>,
            ]}
          >
            <Row>
              <Col md={12}>
                <form className="form-horizontal">
                  <FormMdLineInputStatic
                    label="Авторизацiя:"
                    component="select"
                    required
                    disabled={!isEditableRoleAccess}
                    isError={formErrors.authType}
                    value={thisMp.authType}
                    options={[
                      { id: 'basic', name: 'Basic' },
                      { id: 'oauth', name: 'OAuth' },
                      { id: 'token', name: 'Token' },
                    ]}
                    placeholder="Тип авторизації"
                    labelCol="col-lg-4 col-md-4 col-sm-4"
                    onChange={this.onChangeParam('authType')}
                  />

                  {thisMp.authType === 'basic' &&
                  <c>
                    <FormMdLineInputStatic
                      label="Вкажiть логiн:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.authLogin}
                      placeholder="Login"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('authLogin')}
                    />
                    <FormMdLineInputStatic
                      label="Вкажiть пароль:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.authPassword}
                      placeholder="Password"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('authPassword')}
                    />
                  </c>
                  }
                  {thisMp.authType === 'oauth' &&
                  <c>
                    <FormMdLineInputStatic
                      label="Вкажiть логiн:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.authLogin}
                      placeholder="Login"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('authLogin')}
                    />
                    <FormMdLineInputStatic
                      label="Вкажiть пароль:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.authPassword}
                      placeholder="Password"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('authPassword')}
                    />
                    <FormMdLineInputStatic
                      label="Вкажiть ClientID:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.oauthClientID}
                      placeholder="ClientID"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('oauthClientID')}
                    />
                    <FormMdLineInputStatic
                      label="Вкажiть ClientSecret:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.oauthClientSecret}
                      placeholder="ClientSecret"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('oauthClientSecret')}
                    />
                    <FormMdLineInputStatic
                      label="Вкажiть URL запроса:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.oauthUrl}
                      placeholder="URL запроса"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('oauthUrl')}
                    />
                    {/* <FormMdLineInputStatic
                      label="Вкажiть grant_type:"
                      component="input"
                      // required
                      value=""
                      placeholder="grant_type not work"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      // onChange={this.onChangeParam('remaindersPeriod')}
                    /> */}
                    <FormMdLineInputStatic
                      label="Вкажiть звiдки брати Token:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.tokenParamName}
                      placeholder="звiдки брати Token"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('tokenParamName')}
                    />
                    <FormMdLineInputStatic
                      label="Вкажiть iм'я параметра для Token:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.oauthTokenFieldName}
                      placeholder="iм'я параметра для Token"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('oauthTokenFieldName')}
                    />
                  </c>
                  }
                  {thisMp.authType === 'token' &&
                  <c>
                    <FormMdLineInputStatic
                      label="Вкажiть токен:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.token}
                      placeholder="Token"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('token')}
                    />
                    <FormMdLineInputStatic
                      label="Вкажiть iм'я параметра:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      // required
                      value={thisMp.tokenParamName}
                      placeholder="Iм'я параметра"
                      labelCol="col-lg-4 col-md-4 col-sm-4"
                      onChange={this.onChangeParam('tokenParamName')}
                    />
                  </c>
                  }
                </form>
              </Col>
            </Row>
          </Portlet>
        </Col>
      </Row>

      <Row>
        <Col md={12} sm={12}>
          <Portlet
            className={'box blue-dark'}
            title={[
              <i key={'ostatki_1'} className="fa fa-cogs" />,
              <span key={'ostatki_2'} >Залишки</span>,
            ]}
          >
            <Row>
              <Col md={6}>
                {
                  !isNew &&
                  <RemaindersTable
                    label="Залишки:"
                    isEditableRoleAccess={isEditableRoleAccess}
                    fields={remaindersFieldsEntity}
                    addButton={this.addRemaindersFields}
                    editButton={this.editRemaindersFields}
                    delButton={this.delRemaindersInModal}
                  />
                }
                <form className="form-horizontal">
                  <FormMdLineInputStatic
                    label="Вигрузка залишкiв та цiн:"
                    component="info"
                    styles={{ textAlign: 'left', fontWeight: 'bold' }}
                  />
                  <FormMdLineInputStatic
                    label="Вкажiть перiод вигрузки (хв):"
                    component="input"
                    disabled={!isEditableRoleAccess}
                    type="number"
                    required
                    isError={formErrors.remaindersPeriod}
                    value={thisMp.remaindersPeriod}
                    styles={{ textAlign: 'left' }}
                    placeholder=""
                    onChange={this.onChangeParam('remaindersPeriod')}
                  />

                  <FormMdLineInputStatic
                    label="Враховувати локальнi резерви:"
                    component="checkbox"
                    disabled={!isEditableRoleAccess}
                    value={thisMp.ignoreSelfRemaindersLocalDiff}
                    styles={{ textAlign: 'left' }}
                    onChange={this.onChangeParamChecked('ignoreSelfRemaindersLocalDiff')}
                  />

                  <FormMdLineInputStatic
                    label="Тип приймача:"
                    component="select"
                    disabled={!isEditableRoleAccess}
                    required
                    isError={formErrors.remaindersTarget}
                    value={thisMp.remaindersTarget}
                    styles={{ textAlign: 'left' }}
                    options={[
                      { id: 'api', name: 'Api' },
                      { id: 'ftp', name: 'Ftp' },
                      { id: 'email', name: 'E-mail' },
                    ]}
                    placeholder="Виберiть тип"
                    onChange={this.onChangeParam('remaindersTarget')}
                  />
                  {
                    thisMp.remaindersTarget === 'ftp' &&
                    <FormMdLineInputStatic
                      label="Вкажiть логiн FTP:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      value={thisMp.ftpLogin}
                      styles={{ textAlign: 'left' }}
                      onChange={this.onChangeParam('ftpLogin')}
                    />
                  }
                  {
                    thisMp.remaindersTarget === 'ftp' &&
                    <FormMdLineInputStatic
                      label="Вкажiть пароль FTP:"
                      component="input"
                      disabled={!isEditableRoleAccess}
                      value={thisMp.ftpPassword}
                      styles={{ textAlign: 'left' }}
                      onChange={this.onChangeParam('ftpPassword')}
                    />
                  }
                  <FormMdLineInputStatic
                    label="Вкажiть формат для вiдправки данних:"
                    component="select"
                    disabled={!isEditableRoleAccess}
                    required
                    isError={formErrors.format}
                    value={thisMp.format}
                    styles={{ textAlign: 'left' }}
                    options={[
                      { id: 'csv', name: 'csv' },
                      { id: 'xml', name: 'xml' },
                      { id: 'json', name: 'json' },
                    ]}
                    placeholder="Виберіть формат"
                    onChange={this.onChangeParam('format')}
                  />
                  <FormMdLineInputStatic
                    label="Вкажiть iм'я файлу для залишкiв:"
                    component="input"
                    disabled={isEditableRoleAccess ? thisMp.remaindersTarget === 'api' : true}
                    value={thisMp.remaindersFileName}
                    styles={{ textAlign: 'left' }}
                    onChange={this.onChangeParam('remaindersFileName')}
                  />
                  <FormMdLineInputStatic
                    label="Вкажiть URL або E-mail або ftp для вигрузки:"
                    component="input"
                    disabled={!isEditableRoleAccess}
                    required
                    isError={formErrors.remaindersUrl}
                    value={thisMp.remaindersUrl}
                    styles={{ textAlign: 'left' }}
                    placeholder="URL / E-mail / Ftp"
                    onChange={this.onChangeParam('remaindersUrl')}
                  />
                </form>
              </Col>
              {
                !isNew &&
                <Col md={6}>
                  <Portlet
                    className={'box dark'}
                    title={[
                      <i key={'localDiff_1'} className="fa fa-industry" />,
                      <span key={'localDiff_2'} >Локальнi резерви:</span>,
                    ]}
                    actionsRight={[
                      <div key={'div'}>
                        {
                          !isEditBlock.remaindersLocalDiffFields ?
                          // отображаем кнопку Edit
                            <button
                              key={'localDiff_4'}
                              disabled={!isEditableRoleAccess}
                              style={{ borderColor: '#FFF' }}
                              onClick={() => editBlock('remaindersLocalDiffFields', true)}
                              className="btn btn-icon-only btn-default btn-sm"
                              title="Редагувати"
                            >
                              <i style={{ color: '#FFF' }} className="fa fa-edit" aria-hidden="true" />
                            </button>
                          :
                          // если нажали на кнопку Edit показуем Save and Exit
                            <div>
                              <button
                                key={'localDiff_3'}
                                disabled={!isEditableRoleAccess}
                                style={{ borderColor: '#FFF', marginRight: '5px' }}
                                onClick={this.saveBlockLocalDiff}
                                className="btn btn-icon-only btn-default btn-sm"
                                title="Зберегти"
                              >
                                <i style={{ color: '#FFF' }} className="fa fa-save" aria-hidden="true" />
                              </button>
                              <button
                                key={'localDiff_5'}
                                disabled={!isEditableRoleAccess}
                                style={{ borderColor: '#FFF' }}
                                onClick={() => cancelBlock('remaindersLocalDiffFields', false)}
                                className="btn btn-icon-only btn-default btn-sm"
                                title="Закрити"
                              >
                                <i style={{ color: '#FFF' }} className="fa fa-times" aria-hidden="true" />
                              </button>
                            </div>
                        }
                      </div>

                    ]}
                  >
                    <Row>
                      <Col>
                        <FormMdLineInputStatic
                          label="Iм`я поля з S/N аптеки:"
                          component={isEditBlock.remaindersLocalDiffFields && "input"}
                          disabled={!isEditableRoleAccess}
                          required={isEditBlock.remaindersLocalDiffFields}
                          isEdit={isEditBlock.remaindersLocalDiffFields}
                          isError={formErrors.itemPharmacy}
                          value={thisMp.remaindersLocalDiffFields.itemPharmacy}
                          styles={{ textAlign: 'left' }}
                          placeholder=""
                          onChange={this.onChangeParam(['remaindersLocalDiffFields', 'itemPharmacy'])}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormMdLineInputStatic
                          label="Iм`я поля з кодом товару в 1С:"
                          component={isEditBlock.remaindersLocalDiffFields && "input"}
                          disabled={!isEditableRoleAccess}
                          required={isEditBlock.remaindersLocalDiffFields}
                          isEdit={isEditBlock.remaindersLocalDiffFields}
                          isError={formErrors.itemNomenclature}
                          value={thisMp.remaindersLocalDiffFields.itemNomenclature}
                          styles={{ textAlign: 'left' }}
                          placeholder=""
                          onChange={this.onChangeParam(['remaindersLocalDiffFields', 'itemNomenclature'])}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormMdLineInputStatic
                          label="Iм`я поля з рiзницею залишку в аптецi:"
                          component={isEditBlock.remaindersLocalDiffFields && "input"}
                          disabled={!isEditableRoleAccess}
                          required={isEditBlock.remaindersLocalDiffFields}
                          isEdit={isEditBlock.remaindersLocalDiffFields}
                          isError={formErrors.itemQuantityDiff}
                          value={thisMp.remaindersLocalDiffFields.itemQuantityDiff}
                          styles={{ textAlign: 'left' }}
                          placeholder=""
                          onChange={this.onChangeParam(['remaindersLocalDiffFields', 'itemQuantityDiff'])}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormMdLineInputStatic
                          label="Iм`я поля з рiзницею залишку на складi:"
                          component={isEditBlock.remaindersLocalDiffFields && "input"}
                          disabled={!isEditableRoleAccess}
                          required={isEditBlock.remaindersLocalDiffFields}
                          isEdit={isEditBlock.remaindersLocalDiffFields}
                          isError={formErrors.itemStockQuantityDiff}
                          value={thisMp.remaindersLocalDiffFields.itemStockQuantityDiff}
                          styles={{ textAlign: 'left' }}
                          placeholder=""
                          onChange={this.onChangeParam(['remaindersLocalDiffFields', 'itemStockQuantityDiff'])}
                        />
                      </Col>
                    </Row>
                  </Portlet>
                </Col>
              }
            </Row>
          </Portlet>
        </Col>
      </Row>
      {
        !isNew &&
        <Row>
          <Col md={6} sm={6}>
            <Portlet
              className={'box blue-dark'}
              title={[
                <i key={'zamovlenya_1'} className="fa fa-cogs" />,
                <span key={'zamovlenya_2'} >Замовлення</span>,
              ]}
              actionsRight={[
                <div key={'div'}>
                  {
                    !isEditBlock.orderFields ?
                    // отображаем кнопку Edit
                      <button
                        key={'localDiff_4'}
                        disabled={!isEditableRoleAccess}
                        style={{ borderColor: '#FFF' }}
                        onClick={() => editBlock('orderFields', true)}
                        className="btn btn-icon-only btn-default btn-sm"
                        title="Редагувати"
                      >
                        <i style={{ color: '#FFF' }} className="fa fa-edit" aria-hidden="true" />
                      </button>
                    :
                    // если нажали на кнопку Edit показуем Save and Exit
                      <div>
                        <button
                          key={'localDiff_3'}
                          disabled={!isEditableRoleAccess}
                          style={{ borderColor: '#FFF', marginRight: '5px' }}
                          onClick={this.saveBlockOrderFields}
                          className="btn btn-icon-only btn-default btn-sm"
                          title="Зберегти"
                        >
                          <i style={{ color: '#FFF' }} className="fa fa-save" aria-hidden="true" />
                        </button>
                        <button
                          key={'localDiff_5'}
                          disabled={!isEditableRoleAccess}
                          style={{ borderColor: '#FFF' }}
                          onClick={() => cancelBlock('orderFields', false)}
                          className="btn btn-icon-only btn-default btn-sm"
                          title="Закрити"
                        >
                          <i style={{ color: '#FFF' }} className="fa fa-times" aria-hidden="true" />
                        </button>
                      </div>
                  }
                </div>
              ]}
            >
              <form className="form-horizontal">
                <FormMdLineInputStatic
                  label="Товари:"
                  component="info"
                  styles={{ textAlign: 'left', fontWeight: 'bold' }}
                />
                <FormMdLineInputStatic
                  label="Iм'я секції з товарами:"
                  component={isEditBlock.orderFields && "input"}
                  required={isEditBlock.orderFields}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.itemSectionName}
                  value={thisMp.orderFields.itemSectionName}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'itemSectionName'])}
                />
                <FormMdLineInputStatic
                  label="Iм'я поля коду 1С:"
                  component={isEditBlock.orderFields && "input"}
                  required={isEditBlock.orderFields}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.itemNomenclatureInOrder}
                  value={thisMp.orderFields.itemNomenclature}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'itemNomenclature'])}
                />
                <FormMdLineInputStatic
                  label="Iм'я поля кількості товару:"
                  component={isEditBlock.orderFields && "input"}
                  required={isEditBlock.orderFields}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.itemQuantity}
                  value={thisMp.orderFields.itemQuantity}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'itemQuantity'])}
                />
                <FormMdLineInputStatic
                  label="Iм'я поля ціна товару:"
                  component={isEditBlock.orderFields && "input"}
                  required={isEditBlock.orderFields}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.itemPrice}
                  value={thisMp.orderFields.itemPrice}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'itemPrice'])}
                />
                <FormMdLineInputStatic
                  label="Клієнт:"
                  component="info"
                  styles={{ textAlign: 'left', fontWeight: 'bold' }}
                />
                <hr />
                <FormMdLineInputStatic
                  label="Iм'я поля з iменем клiєнта:"
                  component={isEditBlock.orderFields && "input"}
                  required={isEditBlock.orderFields}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.clientName}
                  value={thisMp.orderFields.clientName}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'clientName'])}
                />
                <FormMdLineInputStatic
                  label="Iм'я поля з телефоном клiєнта:"
                  component={isEditBlock.orderFields && "input"}
                  required={isEditBlock.orderFields}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.clientTel}
                  value={thisMp.orderFields.clientTel}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'clientTel'])}
                />
                <FormMdLineInputStatic
                  label="Iм'я поля з e-mail клiєнта:"
                  component={isEditBlock.orderFields && "input"}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.clientEmail}
                  value={thisMp.orderFields.clientEmail}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'clientEmail'])}
                />
                <FormMdLineInputStatic
                  label="Замовлення:"
                  component="info"
                  styles={{ textAlign: 'left', fontWeight: 'bold' }}
                />
                <hr />
                <FormMdLineInputStatic
                  label="Iм'я поля з кодом замовлення:"
                  component={isEditBlock.orderFields && "input"}
                  required={isEditBlock.orderFields}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.code}
                  value={thisMp.orderFields.code}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'code'])}
                />
                <FormMdLineInputStatic
                  label="Iм'я поля з коментарем клiєнта:"
                  component={isEditBlock.orderFields && "input"}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.comment}
                  value={thisMp.orderFields.comment}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'comment'])}
                />
                <FormMdLineInputStatic
                  label="Iм'я поля з ознакою що замовлення комплектується на складі:"
                  component={isEditBlock.orderFields && "input"}
                  isEdit={isEditBlock.orderFields}
                  isError={formErrors.stockFlag}
                  value={thisMp.orderFields.stockFlag}
                  styles={{ textAlign: 'left' }}
                  placeholder=""
                  onChange={this.onChangeParam(['orderFields', 'stockFlag'])}
                />
              </form>
            </Portlet>
          </Col>

          <Col md={6} sm={6}>
            <Portlet
              className={'box blue-dark'}
              title={[
                <i key={'statysu_1'} className="fa fa-cogs" />,
                <span key={'statysu_2'} >Статуси</span>,
              ]}
              actionsRight={[
                <div key={'div'}>
                  {
                    !isEditBlock.statusFields ?
                    // отображаем кнопку Edit
                      <button
                        key={'statysu_4'}
                        disabled={!isEditableRoleAccess}
                        style={{ borderColor: '#FFF' }}
                        onClick={() => editBlock('statusFields', true)}
                        className="btn btn-icon-only btn-default btn-sm"
                        title="Редагувати"
                      >
                        <i style={{ color: '#FFF' }} className="fa fa-edit" aria-hidden="true" />
                      </button>
                    :
                    // если нажали на кнопку Edit показуем Save and Exit
                      <div>
                        <button
                          key={'statysu_3'}
                          disabled={!isEditableRoleAccess}
                          style={{ borderColor: '#FFF', marginRight: '5px' }}
                          onClick={this.saveBlockStatusFields}
                          className="btn btn-icon-only btn-default btn-sm"
                          title="Зберегти"
                        >
                          <i style={{ color: '#FFF' }} className="fa fa-save" aria-hidden="true" />
                        </button>
                        <button
                          key={'statysu_5'}
                          disabled={!isEditableRoleAccess}
                          style={{ borderColor: '#FFF' }}
                          onClick={() => cancelBlock('statusFields', false)}
                          className="btn btn-icon-only btn-default btn-sm"
                          title="Закрити"
                        >
                          <i style={{ color: '#FFF' }} className="fa fa-times" aria-hidden="true" />
                        </button>
                      </div>
                  }
                </div>
              ]}
            > <form className="form-horizontal">
              <FormMdLineInputStatic
                label="Товари:"
                component="info"
                styles={{ textAlign: 'left', fontWeight: 'bold' }}

              />
              <FormMdLineInputStatic
                label="Iм'я секції з товарами:"
                component={isEditBlock.statusFields && "input"}
                required={isEditBlock.statusFields}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.itemSectionNameInStatusFields}
                value={thisMp.statusFields.itemSectionName}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'itemSectionName'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля з кодом замовлення:"
                component={isEditBlock.statusFields && "input"}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.itemOrderCode}
                value={thisMp.statusFields.itemOrderCode}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'itemOrderCode'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля з кодом в 1С:"
                component={isEditBlock.statusFields && "input"}
                required={isEditBlock.statusFields}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.itemNomenclatureInStatusFields}
                value={thisMp.statusFields.itemNomenclature}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'itemNomenclature'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля з проданою товару:"
                component={isEditBlock.statusFields && "input"}
                required={isEditBlock.statusFields}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.itemQuantityShip}
                value={thisMp.statusFields.itemQuantityShip}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'itemQuantityShip'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля з ціною продажу:"
                component={isEditBlock.statusFields && "input"}
                required={isEditBlock.statusFields}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.itemPriceShip}
                value={thisMp.statusFields.itemPriceShip}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'itemPriceShip'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля зі статусом товару:"
                component={isEditBlock.statusFields && "input"}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.itemStatus}
                value={thisMp.statusFields.itemStatus}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'itemStatus'])}
              />
              <FormMdLineInputStatic
                label="Замовлення:"
                component="info"
                styles={{ textAlign: 'left', fontWeight: 'bold' }}
              />
              <hr />
              <FormMdLineInputStatic
                label="Iм'я поля з кодом замовлення:"
                component={isEditBlock.statusFields && "input"}
                required={isEditBlock.statusFields}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.codeInStatusFields}
                value={thisMp.statusFields.code}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'code'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля з серійним номером аптеки:"
                component={isEditBlock.statusFields && "input"}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.pharmacy}
                value={thisMp.statusFields.pharmacy}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'pharmacy'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля з ознакою 'Відмінити замовлення':"
                component={isEditBlock.statusFields && "input"}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.cancelFlag}
                value={thisMp.statusFields.cancelFlag}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'cancelFlag'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля з причиною відміни замовлення:"
                component={isEditBlock.statusFields && "input"}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.cancelReason}
                value={thisMp.statusFields.cancelReason}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'cancelReason'])}
              />
              <FormMdLineInputStatic
                label="Iм'я поля з статусом замовлення:"
                component={isEditBlock.statusFields && "input"}
                required={isEditBlock.statusFields}
                isEdit={isEditBlock.statusFields}
                isError={formErrors.status}
                value={thisMp.statusFields.status}
                styles={{ textAlign: 'left' }}
                placeholder=""
                onChange={this.onChangeParam(['statusFields', 'status'])}
              />
            </form>
            </Portlet>
          </Col>
        </Row>
      }
      {
        !isNew &&
        <Row>
          {Object.values(statusAliasesEntity).map(({ id, alias, itemAlias, status, isEditThisStatusAliases }) => (
            <Col key={'statusAliases' + status.id + id} lg={3} md={4} sm={6}>
              <Portlet
                className={'box blue-dark'}
                actionsLeft={[
                  <i key={`info_a_${id}`} className="fa fa-cogs" aria-hidden="true" />,
                  <span key={`info_a_text_${id}`}> Алiаси статусiв</span>,
                ]}
                actionsRight={[
                  <div key={'div'}>
                    {
                      isEditThisStatusAliases ?
                      // если статус = true --> показуем кнопку Save
                        <button
                          key={`check_status_${id}`}
                          disabled={!isEditableRoleAccess}
                          style={{ borderColor: '#FFF', marginRight: '5px' }}
                          onClick={this.saveStatusAliases(id)}
                          className="btn btn-icon-only btn-default btn-sm"
                          title="Зберегти"
                        >
                          <i style={{ color: '#FFF' }} className="fa fa-save" aria-hidden="true" />
                        </button>
                      :
                      // если нажали на кнопку Save показуем Edit
                        <button
                          key={`check_status_${id}`}
                          disabled={!isEditableRoleAccess}
                          style={{ borderColor: '#FFF', marginRight: '5px' }}
                          onClick={this.editStatusAliases(id)}
                          className="btn btn-icon-only btn-default btn-sm"
                          title="Редагувати"
                        >
                          <i style={{ color: '#FFF' }} className="fa fa-edit" aria-hidden="true" />
                        </button>
                    }
                    <button
                      key={`del_status_${id}`}
                      disabled={!isEditableRoleAccess}
                      style={{ borderColor: '#FFF' }}
                      onClick={this.delStatusAliasesInModal(id, status)}
                      className="btn btn-icon-only btn-default btn-sm"
                      title="Видалити"
                    >
                      <i style={{ color: '#FFF' }} className="fa fa-times" aria-hidden="true" />
                    </button>
                  </div>,
                ]}
              >
                <form className="form-horizontal">
                  <FormMdLineInputStatic
                    label="Статус:"
                    component={isEditThisStatusAliases && "select"}
                    isEdit={isEditThisStatusAliases || false}
                    isError={id === formErrors[`ids${id}`] && formErrors.statusInStatusAliases}
                    required={isEditThisStatusAliases}
                    value={isEditThisStatusAliases ? status.id : translateStatus('ua', status.id)}
                    styles={{ textAlign: 'left' }}
                    options={orderStatuses && orderStatuses.map(i => ({
                      id:   i.id,
                      name: translateStatus('ua', i.id),
                    }))}
                    placeholder="Оберiть статус"
                    labelCol="col-lg-3 col-md-3 col-sm-3"
                    valueCol="col-lg-9 col-md-9 col-sm-9"
                    onChange={this.onChangeParam(['entities', 'statusAliases', id, 'status', 'id'])}
                  />
                  <FormMdLineInputStatic
                    label="Алiас для замовлення:"
                    component={isEditThisStatusAliases && "input"}
                    isEdit={!!isEditThisStatusAliases}
                    isError={id === formErrors[`ids${id}`] && formErrors.alias}
                    value={alias}
                    styles={{ textAlign: 'left' }}
                    placeholder=""
                    labelCol="col-lg-7 col-md-7 col-sm-7"
                    valueCol="col-lg-5 col-md-5 col-sm-5"
                    onChange={this.onChangeParam(['entities', 'statusAliases', id, 'alias'])}
                  />
                  <FormMdLineInputStatic
                    label="Алiас для товару:"
                    component={isEditThisStatusAliases && "input"}
                    isEdit={isEditThisStatusAliases || false}
                    isError={id === formErrors[`ids${id}`] && formErrors.itemAlias}
                    value={itemAlias}
                    styles={{ textAlign: 'left' }}
                    placeholder=""
                    labelCol="col-lg-7 col-md-7 col-sm-7"
                    valueCol="col-lg-5 col-md-5 col-sm-5"
                    onChange={this.onChangeParam(['entities', 'statusAliases', id, 'itemAlias'])}
                  />
                </form>
              </Portlet>
            </Col>
          ))}
          <Col md={1} sm={1} >
            <Button
              onClick={addStatusAliases}
              disabled={!isEditableRoleAccess}
              className="btn btn-sm green-meadow"
            ><i className={'fa fa-plus'} /> Додати алiас</Button>
          </Col>
        </Row>
      }
    </Portlet>

    <Modal show={isOpenModal} onHide={closeModal}>
      <Header closeButton>
        <Title>
          {
            fieldInModal.action === 'remainders' &&
              'Виберiть поле в системi та вкажiть на майданчику'
          }
          {
            fieldInModal.action === 'delRemainders' &&
              `Ви впевненi, що бажаєте видалити поле # ${fieldInModal.name}`
          }
          {
            fieldInModal.action === 'delContacts' &&
              `Ви впевненi, що бажаєте видалити контакти спiвробiтника "${fieldInModal.name}"`
          }
          {
            fieldInModal.action === 'delStatusAliases' &&
              `Ви впевненi, що бажаєте видалити статус "${translateStatus('ua', fieldInModal.name.id)}"`
          }
        </Title>
      </Header>
      {
        fieldInModal.action === 'remainders' &&
        <Body>
          {
            // если не загружено, выводим гифку загрузки.
            !isLoadRemaindersChain ?
              <img
                src="/build/admin/images/cube.gif"
                style={{ top: '20%' }}
                className="preloader"
                alt="preloader"
              />
            :
              <RemaindersTree
                onChange={this.onChangeParam}
                currentField={currentRemaindersField}
                remaindersChainEntity={remaindersChainEntity}
                formErrors={formErrors}
              />
          }
        </Body>
      }

      <Footer>
        {
          (
            fieldInModal.action === 'delRemainders' ||
            fieldInModal.action === 'delContacts' ||
            fieldInModal.action === 'delStatusAliases'
          ) &&
            <Button
              onClick={fieldInModal.onClick(fieldInModal.id)}
              className="green-meadow pull-left"
            >
              Підтвердити
            </Button>
        }
        {
          currentRemaindersField.status === 'new' &&
            <Button
              onClick={this.pushNewRemaindersField}
              className="green-meadow pull-left"
            >
              Підтвердити
            </Button>
        }
        {
          currentRemaindersField.status === 'edit' &&
            <Button
              onClick={this.confirmEditedRemaindersField}
              className="green-meadow pull-left"
            >
              Підтвердити
            </Button>
        }
        <Button onClick={closeModal} >Закрити</Button>
      </Footer>
    </Modal>
  </div>
    );
  }
}

export default connect(selectors, actions)(MarketPlaceItem);
