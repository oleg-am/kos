import { combineReducers } from 'redux';
import _uniqueId from 'lodash/uniqueId';
import _omit from 'lodash/omit';
import * as types from './constants';
import { setValue } from '../../helpers/setObjValue';

const initialState = {
  thisMp: {
    authType:                      '-1',
    enabled:                       false,
    ignoreSelfRemaindersLocalDiff: false,
    format:                        '-1', // input type='select' and default value='-1'
    id:                            '',
    name:                          '',
    orderPeriod:                   '',
    orderUrl:                      '',
    url:                           '',
    token:                         '',
    tokenParamName:                '',
    oauthTokenFieldName:           '',
    oauthUrl:                      '',
    oauthClientSecret:             '',
    oauthClientID:                 '',
    authPassword:                  '',
    authLogin:                     '',
    ftpLogin:                      '',
    ftpPassword:                   '',
    remaindersPeriod:              '',
    remaindersTarget:              '-1', // input type='select' and default value='-1'
    remaindersUrl:                 '',
    remaindersFileName:            '',
    remaindersLocalDiffUrl:        '',
    remaindersLocalDiffFields:     {
      itemPharmacy:          '',
      itemNomenclature:      '',
      itemQuantityDiff:      '',
      itemStockQuantityDiff: '',
    },
    orderFields: {
      itemSectionName:  '',
      itemNomenclature: '',
      itemQuantity:     '',
      itemPrice:        '',
      clientName:       '',
      clientTel:        '',
      clientEmail:      '',
      code:             '',
      comment:          '',
      stockFlag:        '',
    },
    statusUrl:    '',
    statusFields: {
      itemSectionName:  '',
      itemOrderCode:    '',
      itemNomenclature: '',
      itemQuantityShip: '',
      itemPriceShip:    '',
      itemStatus:       '',
      code:             '',
      pharmacy:         '',
      cancelFlag:       '',
      cancelReason:     '',
      status:           '',
    },
    entities: {
      contacts:         {},
      statusAliases:    {},
      remaindersFields: {},
    },
    contacts:               [],
    statusAliases:          [],
    remaindersFields:       [],
    formErrors:             {},
    currentRemaindersField: {},
    copyBlockBeforEdit:     {
      remaindersLocalDiffFields: {},
      orderFields:               {},
      statusFields:              {},
    },
  },
  ordeStatuses:          [],
  remaindersChain:       [],
  isLoad:                true,
  isLoadRemaindersChain: false,
  modal:                 {
    isOpenModal:  false,
    fieldInModal: {},
  },
  isEditBlock: {
    orderFields:               false,
    statusFields:              false,
    remaindersLocalDiffFields: false,
  },
};

// ############################################################################
const isEditBlock = (state = initialState.isEditBlock, action) => {
  switch (action.type) {
// ...........................................................................
    case types.EDIT_BLOCK:
    case types.SAVE_BLOCK:
    case types.CANCEL_BLOCK:
      return {
        ...state,
        [action.fieldName]: action.status,
      };
// ...........................................................................
    case types.CLOSE_ALL:
      return initialState.isEditBlock;
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const remaindersChain = (state = initialState.remaindersChain, action) => {
  switch (action.type) {
// ...........................................................................
    case types.LOAD_REMAINDERS_CHAIN_SUCCESS:
      return action.data;
// ...........................................................................
    case types.CLOSE_ALL:
      return initialState.remaindersChain;
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const isLoadRemaindersChain = (state = initialState.isLoadRemaindersChain, action) => {
  switch (action.type) {
// ...........................................................................
    case types.LOAD_REMAINDERS_CHAIN_SUCCESS:
    case types.LOAD_REMAINDERS_CHAIN_FAILURE:
      return true;
// ...........................................................................
    case types.LOAD_REMAINDERS_CHAIN_REQUEST:
      return false;
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const orderStatuses = (state = initialState.ordeStatuses, action) => {
  switch (action.type) {
// ...........................................................................
    case types.LOAD_ORDER_STATUSES_SUCCESS:
      return action.data;
// ...........................................................................
    default:
      return state;
  }
};
// ############################################################################
const formErrors = (state = initialState.thisMp.formErrors, action) => {
  switch (action.type) {
// ...........................................................................
    case types.CLOSE_ALL:
    case types.SAVE_BLOCK:
    case types.CANCEL_BLOCK:
    case types.PUT_MP_SUCCESS:
    case types.PUT_CONTACTS_ON_SERVER_SUCCESS:
    case types.POST_CONTACTS_ON_SERVER_SUCCESS:
    case types.PUT_STATUS_ALIASES_ON_SERVER_SUCCESS:
    case types.POST_STATUS_ALIASES_ON_SERVER_SUCCESS:
    case types.ADD_REMAINDERS_FIELDS_ON_SERVER_SUCCESS:
    case types.PUT_REMAINDERS_FIELDS_ON_SERVER_SUCCESS:
      return {};
// ...........................................................................
    case types.SAVE_FORM_ERRORS:
      return {
        ...state,
        ...action.formErrors,
      };
// ...........................................................................
    default:
      return state;
  }
};
// ############################################################################
const thisMp = (state = initialState.thisMp, action) => {
  switch (action.type) {
// ...........................................................................
    case types.LOAD_SUCCESS:
      return {
        ...state,
        ...action.result,
        entities: {
          ...state.entities,
          ...action.entities,
        },
      };
// ...........................................................................
    case types.CLOSE_ALL:
      return initialState.thisMp;
// ...........................................................................
    case types.CHANGE_PARAM:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
// ...........................................................................
    case types.EDIT_STATUS_ALIASES:
      return {
        ...state,
        entities: {
          ...state.entities,
          statusAliases: {
            ...state.entities.statusAliases,
            [action.id]: {
              ...state.entities.statusAliases[action.id],
              internetGround:          state.id,
              isEditThisStatusAliases: true,
            },
          },
        },
      };
// ...........................................................................
    case types.POST_STATUS_ALIASES_ON_SERVER_SUCCESS:
      // удаляем временный обьект с временныйм ID и вставляем то что прислал сервер.
      delete state.entities.statusAliases[action.tempId];
      return {
        ...state,
        entities: {
          ...state.entities,
          statusAliases: {
            ...state.entities.statusAliases,
            [action.data.id]: action.data,
          },
        },
      };
// ...........................................................................
    case types.PUT_STATUS_ALIASES_ON_SERVER_SUCCESS:
      return {
        ...state,
        entities: {
          ...state.entities,
          statusAliases: {
            ...state.entities.statusAliases,
            [action.data.id]: action.data,
          },
        },
      };
// ...........................................................................
    case types.EDIT_CONTACTS:
      return {
        ...state,
        entities: {
          ...state.entities,
          contacts: {
            ...state.entities.contacts,
            [action.id]: {
              ...state.entities.contacts[action.id],
              internetGround:    state.id,
              isEditThisContact: true,
            },
          },
        },
      };
// ...........................................................................
    case types.POST_STATUS_FIELDS_ON_SERVER_SUCCESS:
      return {
        ...state,
        statusFields: {
          ...state.statusFields,
          ...action.data,
        },
      };
// ...........................................................................
    case types.PUT_CONTACTS_ON_SERVER_SUCCESS:
      return {
        ...state,
        entities: {
          ...state.entities,
          contacts: {
            ...state.entities.contacts,
            [action.data.id]: action.data,
          },
        },
      };
// ...........................................................................
    case types.POST_CONTACTS_ON_SERVER_SUCCESS:
      // удаляем временный обьект с временныйм ID и вставляем то что прислал сервер.
      delete state.entities.contacts[action.tempId];
      return {
        ...state,
        entities: {
          ...state.entities,
          contacts: {
            ...state.entities.contacts,
            [action.data.id]: action.data,
          },
        },
      };
// ...........................................................................
    case types.POST_ORDER_FIELDS_ON_SERVER_SUCCESS:
      return {
        ...state,
        orderFields: {
          ...state.orderFields,
          ...action.data,
        },
      };
// ...........................................................................
    case types.POST_REMAINDERS_LOCAL_DIFF_ON_SERVER_SUCCESS:
      return {
        ...state,
        remaindersLocalDiffFields: {
          ...state.remaindersLocalDiffFields,
          ...action.data,
        },
      };
// ...........................................................................
    case types.ADD_CONTACTS:
      const id = _uniqueId('cid0');
      return {
        ...state,
        entities: {
          ...state.entities,
          contacts: {
            ...state.entities.contacts,
            [id]: {
              id,
              name:              '',
              position:          '',
              tel:               '',
              isEditThisContact: true,
              internetGround:    state.id,
            },
          },
        },
      };
// ...........................................................................
    case types.ADD_STATUS_ALIASES:
      const id2 = _uniqueId('sid0');
      return {
        ...state,
        entities: {
          ...state.entities,
          statusAliases: {
            ...state.entities.statusAliases,
            [id2]: {
              id:        id2,
              alias:     '',
              itemAlias: '',
              status:    {
                id:   '-1',
                name: '',
              },
              isEditThisStatusAliases: true,
              internetGround:          state.id,
            },
          },
        },
      };
// ...........................................................................
    case types.ADD_REMAINDERS_FIELDS_ON_SERVER_SUCCESS:
    case types.PUT_REMAINDERS_FIELDS_ON_SERVER_SUCCESS:
      return {
        ...state,
        entities: {
          ...state.entities,
          remaindersFields: {
            ...state.entities.remaindersFields,
            [action.data.id]: action.data,
          },
        },
      };
// ...........................................................................
    case types.ADD_REMAINDERS_FIELDS:
      const id3 = _uniqueId('rid0');
      return {
        ...state,
        currentRemaindersField: {
          fieldName:      '',
          fieldType:      '-1', // input type='select' and default value='-1'
          id:             id3,
          paramName:      '',
          status:         'new',
          internetGround: state.id,
        },
      };
// ...........................................................................
    case types.CLOSE_MODAL:
      return {
        ...state,
        currentRemaindersField: initialState.thisMp.currentRemaindersField,
      };
// ...........................................................................
    case types.EDIT_REMAINDERS_FIELDS:
      return {
        ...state,
        currentRemaindersField: {
          ...action.field,
          status: action.status,
        },
      };
// ...........................................................................
    case types.EDIT_BLOCK:
      return {
        ...state,
        copyBlockBeforEdit: {
          ...state.copyBlockBeforEdit,
          [action.fieldName]: state[action.fieldName],
        },
      };
// ...........................................................................
    case types.CANCEL_BLOCK:
      return {
        ...state,
        [action.fieldName]: state.copyBlockBeforEdit[action.fieldName],
      };
// ...........................................................................
    case types.SAVE_BLOCK:
      return {
        ...state,
        copyBlockBeforEdit: {
          ...state.copyBlockBeforEdit,
          [action.fieldName]: {},
        },
      };
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const delContacts = (state = [], action) => {
  switch (action.type) {
// ...........................................................................
    case types.DEL_CONTACTS:
      return [...state, action.id];
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const delRemaindersFields = (state = [], action) => {
  switch (action.type) {
// ...........................................................................
    case types.DEL_REMAINDERS_FIELDS:
      return [...state, action.id];
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const delStatusAliases = (state = [], action) => {
  switch (action.type) {
// ...........................................................................
    case types.DEL_STATUS_ALIASES:
      return [...state, action.id];
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const isLoad = (state = initialState.isLoad, action) => {
  switch (action.type) {
// ...........................................................................
    case types.LOAD_SUCCESS:
    case types.LOAD_FAILURE:
    case types.CREATE_FORM:
      return false;
// ...........................................................................
    case types.CLOSE_ALL:
      return true;
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const isOpenModal = (state = initialState.modal.isOpenModal, action) => {
  switch (action.type) {
// ...........................................................................
    case types.OPEN_MODAL:
      return true;
// ...........................................................................
    case types.CLOSE_MODAL:
      return false;
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################
const fieldInModal = (state = initialState.modal.fieldInModal, action) => {
  switch (action.type) {
// ...........................................................................
    case types.OPEN_MODAL:
      return action.field;
// ...........................................................................
    case types.CLOSE_MODAL:
      return '';
// ...........................................................................
    default:
      return state;
  }
};

// ############################################################################

export default combineReducers({
  thisMp,
  delContacts,
  delStatusAliases,
  delRemaindersFields,
  remaindersChain,
  isLoad,
  isOpenModal,
  fieldInModal,
  isLoadRemaindersChain,
  isEditBlock,
  orderStatuses,
  formErrors,
});
