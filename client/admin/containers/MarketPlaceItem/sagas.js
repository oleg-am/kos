import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

import { browserHistory } from 'react-router';

import { CREATE_MP_SUCCESS, PUT_MP_SUCCESS } from './constants';
import * as actions from './actions';

function* goToMP() {
  yield put(actions.closeAll());
  yield browserHistory.push('/admin/settings/marketPlace');
}

function* rootSagas() {
  yield takeEvery([CREATE_MP_SUCCESS, PUT_MP_SUCCESS], goToMP);
}

export default rootSagas;
