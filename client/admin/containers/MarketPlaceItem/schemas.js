import { Schema, arrayOf } from 'normalizr';
import LIVR from 'livr';

LIVR.Validator.defaultAutoTrim(true);

const contacts = new Schema('contacts');
const statusAliases = new Schema('statusAliases');
const remaindersFields = new Schema('remaindersFields');

const mpItem = {
  contacts:         arrayOf(contacts),
  statusAliases:    arrayOf(statusAliases),
  remaindersFields: arrayOf(remaindersFields),
};

export const MARKET_PLACE = mpItem;

const newMarcetPlaceSchema = {
  name:                          ['required', 'string'],
  url:                           ['required', 'string'],
  enabled:                       ['required', { one_of: [true, false] }],
  format:                        ['required', { one_of: ['csv', 'xml', 'json'] }],
  remaindersPeriod:              ['required', 'string'],
  remaindersUrl:                 ['required', 'string'],
  remaindersTarget:              ['required', { one_of: ['api', 'email', 'ftp'] }],
  remaindersFileName:            'string',
  remaindersLocalDiffUrl:        'string',
  ignoreSelfRemaindersLocalDiff: 'string', // 'bool',
  statusUrl:                     'string',
  orderUrl:                      ['string'],
  orderPeriod:                   ['string'],
  authType:                      ['required', { one_of: ['basic', 'oauth', 'token'] }],
  authLogin:                     'string',
  authPassword:                  'string',
  oauthClientID:                 'string',
  oauthClientSecret:             'string',
  oauthUrl:                      'string',
  oauthTokenFieldName:           'string',
  tokenParamName:                'string',
  token:                         'string',
  ftpLogin:                      'string',
  ftpPassword:                   'string',
};

const newLocalDiffSchema = {
  itemNomenclature:      ['required', 'string'],
  itemQuantityDiff:      ['required', 'string'],
  itemStockQuantityDiff: ['required', 'string'],
  itemPharmacy:          ['required', 'string'],
  internetGround:        ['required', { min_number: 0 }],
};

const neworderFieldsSchema = {
  itemSectionName:         ['required', 'string'],
  itemNomenclatureInOrder: ['required', 'string'],
  itemQuantity:            ['required', 'string'],
  itemPrice:               ['required', 'string'],
  clientName:              ['required', 'string'],
  clientTel:               ['required', 'string'],
  clientEmail:             ['string'],
  code:                    ['required', 'string'],
  comment:                 ['string'],
  stockFlag:               ['string'],
  internetGround:          ['required', { min_number: 0 }],
};

const newStatusFieldsSchema = {
  itemSectionNameInStatusFields:  ['required', 'string'],
  itemOrderCode:                  ['string'],
  itemNomenclatureInStatusFields: ['required', 'string'],
  itemQuantityShip:               ['required', 'string'],
  itemPriceShip:                  ['required', 'string'],
  itemStatus:                     ['string'],
  codeInStatusFields:             ['required', 'string'],
  pharmacy:                       ['string'],
  cancelFlag:                     ['string'],
  cancelReason:                   ['string'],
  status:                         ['required', 'string'],
  internetGround:                 ['required', { min_number: 0 }],
};

const newContactSchema = {
  pib:            ['required', 'string'],
  position:       ['string'],
  tel:            [{ min_length: 12 }],
  internetGround: ['required', { min_number: 0 }],
};

const newStatusAliasesSchema = {
  statusInStatusAliases: ['required', { number_between: [1, 15] }],
  alias:                 ['string'],
  itemAlias:             ['string'],
  internetGround:        ['required', { min_number: 0 }],
};

const newRemainderTreeSchema = {
  fieldType:      ['required', { one_of: ['integer', 'float', 'boolean', 'string'] }],
  paramName:      ['required', 'string'],
  fieldName:      ['required', 'string'],
  internetGround: ['required', { min_number: 0 }],
};

export const postMpValidator        = new LIVR.Validator(newMarcetPlaceSchema);
export const localDiffValidator     = new LIVR.Validator(newLocalDiffSchema);
export const orderFieldsValidator   = new LIVR.Validator(neworderFieldsSchema);
export const contactsValidator      = new LIVR.Validator(newContactSchema);
export const statusFieldsValidator  = new LIVR.Validator(newStatusFieldsSchema);
export const statusAliasesValidator = new LIVR.Validator(newStatusAliasesSchema);
export const remainderTreeValidator = new LIVR.Validator(newRemainderTreeSchema);
// export const putValidator = new LIVR.Validator({ ...newMarcetPlaceSchema, ...idSchema });

// const getValidator = (schema) => (max) => new LIVR.Validator({
//    ...schema,
//    statusInStatusAliases: ['required', { 'number_between': [1, max] }]
//  });
//
// export const statusAliasesValidator = getValidator(newStatusAliasesSchema);
