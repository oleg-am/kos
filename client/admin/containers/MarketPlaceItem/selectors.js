import { createSelector, createStructuredSelector } from 'reselect';
import _omit from 'lodash/omit';

const REDUCER = 'marketPlaceItem';

const thisMp                    = state => state[REDUCER].thisMp;
const thisMpEntities            = state => state[REDUCER].thisMp.entities;
const formErrors                = state => state[REDUCER].formErrors;
const currentRemaindersField    = state => state[REDUCER].thisMp.currentRemaindersField;
const isLoad                    = state => state[REDUCER].isLoad;
const isOpenModal               = state => state[REDUCER].isOpenModal;
const fieldInModal              = state => state[REDUCER].fieldInModal;
const remaindersChainEntity     = state => state[REDUCER].remaindersChain;
const orderStatuses             = state => state[REDUCER].orderStatuses;
const delContactsEntity         = state => state[REDUCER].delContacts;
const delStatusAliasesEntity    = state => state[REDUCER].delStatusAliases;
const delRemaindersFieldsEntity = state => state[REDUCER].delRemaindersFields;
const isLoadRemaindersChain     = state => state[REDUCER].isLoadRemaindersChain;
const isEditBlock               = state => state[REDUCER].isEditBlock;

const contactsEntity = createSelector(
  thisMp,
  delContactsEntity,
  thisMpEntities,
  ({ contacts: ids }, del, { contacts }) =>
  del ? _omit(contacts, del) : contacts,
);

const statusAliasesEntity = createSelector(
  thisMp,
  delStatusAliasesEntity,
  thisMpEntities,
  ({ statusAliases: ids }, del, { statusAliases }) =>
  del ? _omit(statusAliases, del) : statusAliases,
);

const remaindersFieldsEntity = createSelector(
  thisMp,
  delRemaindersFieldsEntity,
  thisMpEntities,
  ({ remaindersFields: ids }, del, { remaindersFields }) =>
  del ? _omit(remaindersFields, del) : remaindersFields,
);

export default createStructuredSelector({
  thisMp,
  isLoad,
  formErrors,
  isOpenModal,
  fieldInModal,
  orderStatuses,
  isEditBlock,
  contactsEntity,
  statusAliasesEntity,
  isLoadRemaindersChain,
  remaindersChainEntity,
  remaindersFieldsEntity,
  currentRemaindersField,
});
