import * as types from './constants';

import { createParams } from '../../helpers/api';
import { normalize } from 'normalizr';
import axios from 'axios';

import { NOMENCLATURE_ARR, MANUFACTURER_ARR } from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});
export const changeParamManufactures = (key, value) => ({
  type: types.CHANGE_PARAM_MANUFACTURES,
  key,
  value,
});

export const setFilterManufacturers = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER_MANUFACTURES,
  key,
  value,
  isFiltersDebounce
});

export const edit = (nomenclature) => ({
  type: types.EDIT,
  nomenclature,
})

export const loadNomenclature = filters => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/nomenclature${createParams(filters)}`),
  schema:  NOMENCLATURE_ARR,
});

// export const loadManufacturers = filters => ({
//   types: [
//     types.LOAD_REQUEST_MANUFACTURES,
//     types.LOAD_SUCCESS_MANUFACTURES,
//     types.LOAD_FAILURE_MANUFACTURES,
//   ],
//   promise: api => api.get(`/api/admin/manufacturers${createParams(filters)}`),
//   schema:  MANUFACTURER_ARR,
// });

// export const loadNomenclature = (filters) => (dispatch) => (
// 	axios
// 		.get(`/api/admin/nomenclature${createParams(filters)}`)
// 		.then((res) => {
//       console.log('data: ', res.data);
// 			return dispatch({
// 				type: types.LOAD_SUCCESS,
//         errors: [],
// 				...normalize(res.data, NOMENCLATURE_ARR)
// 			})
// 			// dispatch(loadCitySuccess(res.data));
// 		}).catch((error) => {
//
//       console.log(error);
//       return dispatch({
// 				type: types.LOAD_FAILURE,
// 				errors: error.response.data.errors
// 			})
//     })
//   )

export const loadManufacturers = (filters) => (dispatch) => {
  dispatch({
    type: types.LOAD_REQUEST_MANUFACTURES,
    filters
  })

  axios
		.get(`/api/admin/manufacturers${createParams(filters)}`)
		.then((res) => {
      console.log('data: ', res.data);
      console.log('normalize: ', normalize(res.data, MANUFACTURER_ARR));

			return dispatch({
				type: types.LOAD_SUCCESS_MANUFACTURES,
        errors: [],
				...normalize(res.data, MANUFACTURER_ARR)
			})
		}).catch((error) => {

      console.log(error);
      return dispatch({
				type: types.LOAD_FAILURE_MANUFACTURES,
				errors: error.response.data.errors
			})
    })
  }
