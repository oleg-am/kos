import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';

import { Link, browserHistory } from 'react-router';
// import { push } from 'react-router-redux';

// import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
import Select from 'react-select';

import HighlightWords from 'react-highlight-words';

import _debounce from 'lodash/debounce';

// import DatePicker from 'react-datepicker';

import * as actions from './actions';
import * as selectors from './selectors';
import { createSelectors } from '../../helpers/createSelectors';

// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';
// import Table from '../../components/Table/index';

import Paginate from '../../components/Paginate';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import LimitSelect from '../../components/LimitSelect';
import FilterSearch from '../../components/FilterSearch';

import LinkEdit from '../../elements/LinkEdit';
import IconCheck from '../../elements/IconCheck';
import IconStatus from '../../elements/IconStatus';
import Icon from '../../elements/Icon';
import LinkSearch from '../../elements/LinkSearch';

const checkboxStyle = {
  float:                  'right',
  width:                  'inherit',
  marginLeft:             '5px',
  height:                 '34px',
  borderBottomLeftRadius: '4px',
  borderTopLeftRadius:    '4px',
  borderLeft:             '1px solid rgb(204, 204, 204)',
};

class Nomenclature extends Component {
  static propTypes = {
    imgUrl:                  PropTypes.string,
    errors:                  PropTypes.arrayOf(PropTypes.object),
    filters:                 PropTypes.objectOf(PropTypes.any).isRequired,
    filterManufacturers:     PropTypes.objectOf(PropTypes.any).isRequired,
    paging:                  PropTypes.objectOf(PropTypes.any),
    setFilter:               PropTypes.func.isRequired,
    setFilterManufacturers:  PropTypes.func.isRequired,
    loadNomenclature:        PropTypes.func.isRequired,
    loadManufacturers:       PropTypes.func.isRequired,
    changeParam:             PropTypes.func.isRequired,
    changeParamManufactures: PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const {
      loadNomenclature,
      loadManufacturers,
      filters,
    //  filterManufacturers
    } = this.props;

    this.debounceLoadNomenclature = _debounce(loadNomenclature, 500);
    this.debounceLoadManufacturers = _debounce(loadManufacturers, 300);

    loadNomenclature(filters);
    // loadManufacturers(filterManufacturers);
  }
  componentWillReceiveProps(next) {
    const {
      loadNomenclature,
      loadManufacturers,
      paging,
      filters,
      filterManufacturers,
    } = this.props;
    const {
      isFiltersDebounce,
      paging: nextPaging,
      filters: nextFilters,
      filterManufacturers: nextFilterManufacturers,
    } = next;

    if (filters !== nextFilters) {
      const newfilters = (paging.page === nextFilters.page)
        || (nextPaging.totalRecords <= nextFilters.limit)
          ? { ...nextFilters, page: 1 }
          : nextFilters;

      if (isFiltersDebounce) {
        this.debounceLoadNomenclature(newfilters);
      } else {
        loadNomenclature(newfilters);
      }
    }

    if (filterManufacturers !== nextFilterManufacturers) {
      console.log(nextFilterManufacturers.filters.name.value);
      if (typeof(nextFilterManufacturers.filters.name.value) === 'string') {
        if (isFiltersDebounce) {
          this.debounceLoadManufacturers(nextFilterManufacturers);
        } else {
          loadManufacturers(nextFilterManufacturers);
        }
      }
    }
    // filters != nextFilters && loadNomenclature(nextFilters);
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterRemainders = key => (e) => {
    this.props.setFilter(key, e.target.checked ? 'not null' : '');
  }
  setFilterAfterESC = key => (e) => {
    e.keyCode === 27 && this.props.setFilter(key, '');
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  setFilterManufacturers = (key, isFiltersDebounce) => (value = '') => {
    this.props.setFilterManufacturers(key, value, isFiltersDebounce);
  }
  setFilterSelect = (key, isFiltersDebounce) => (obj) => {
    this.props.setFilter(key, obj ? obj.value : '', isFiltersDebounce);
    key[key.length-1] = "name";
    this.props.setFilterManufacturers(key, obj ? obj : '', isFiltersDebounce);
  }
  toggleComparison = key => (value) => {
    this.props.changeParam(key, value);
  }
  toggleComparisonManufactures = key => (value) => {
    this.props.changeParamManufactures(key, value);
  }
  refreshData = () => {
    this.props.loadNomenclature(this.props.filters);
  }
  onChangeChecked = (onChangeCheckbox, comparison) => () => {
    onChangeCheckbox &&
      onChangeCheckbox(comparison === 'begins' ? 'like' : 'begins')
  };

  render() {
    const {
      isLoadingManufacturer,
      errors,
      nomenclature,
      manufacturersOptions,
      paging,
      filters,
      filterManufacturers,
      imgUrl,
    } = this.props;

    const check = cell => (
      <IconCheck isValue={!!cell} />
    );

    const instructionIcon = hostUrl => (cell, row) => {
      const icons = [];

      row.instruction && icons.push(<Icon icon="fileText" title="Інструкція" />);
      row.instructionFile && icons.push(
        <a href={hostUrl ? `${hostUrl}${row.instructionFile}` : ''} download>
          <Icon icon="download" title="Файл" />
        </a>,
      );
      row.stateNomenclature && row.stateNomenclature.instruction && icons.push(
        <a href={row.stateNomenclature.instruction} download>
          <Icon icon="download" collor="font-red-flamingo" title="ГосНоменклатура" />
        </a>,
      );

      !icons.length && icons.push(<Icon icon="close" title="Відсутня" />);

      return (
        <span>
          {icons.map((icon, i) => (
            i === 1
            ? <span key={`instruction_${i}`} style={{ marginLeft: '10px' }}> {icon} </span>
            : <span key={`instruction_${i}`}> {icon} </span>
          ))}
        </span>
      );
    };

    const instructionIconWithHostUrl = instructionIcon(imgUrl);

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          components={{
            title: 'Номенклатура',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Номенклатура',
              },
            ],
          }}
        />

        <Portlet
          actionsLeft={[
            <div key="actionsLeft">
              <LimitSelect key="actionsLeft_1" optionsValue={[25, 50, 100]} value={filters.limit} onChange={this.setFilter('limit')} />
              <span key={'actionsLeft_3'} className="input-group-addon" style={checkboxStyle} >
                <label
                  htmlFor="actionsLeft_2"
                  title={!filters.filters.remainders.value ? 'Вiдображати номенклатуру в наявностi' : 'Вiдображати всю номенклатуру'}
                  className="mt-checkbox"
                  style={{ paddingLeft: '18px' }}
                >
                  <t
                    style={{ marginLeft: '10px', lineHeight: '1.4' }}
                  >
                    {!filters.filters.remainders.value ? 'Показана вся номенклатура' : 'Показана номенклатура в наявностi'}
                  </t>
                  <input
                    id="actionsLeft_2"
                    onChange={this.setFilterRemainders(['filters', 'remainders'])}
                    type="checkbox"
                    checked={filters.filters.remainders.value === 'not null'}
                  />
                  <span />
                </label>
              </span>
            </div>,
          ]}

          actionsRight={[
            <Link key="3" onClick={this.refreshData} className="btn btn-sm blue-sharp">
              <i className="fa fa-refresh" />
            </Link>,
          ]}
        >

          <CustomTable
            tableClass="table table-striped table-bordered table-hover"
            data={errors.length ? [] : nomenclature}
            striped
            hover
            pagination
            filters
            minHeight={(paging.records < 3 || errors.length) ? '250px' : ''}
          >
            <HeaderColumn dataField="id" width="5%" dataAlign="center" isKey> № </HeaderColumn>
            <HeaderColumn dataField="code1C"> code1C </HeaderColumn>
            <HeaderColumn dataField="codeMorion"> codeMorion </HeaderColumn>
            <HeaderColumn
              dataField="codeTES"
              width="8%"
              filters={
                <input
                  type="text"
                  className="form-control"
                  placeholder="Фільтр"
                  value={filters.filters.codeTES.value}
                  onChange={this.setFilter(['filters', 'codeTES'], true)}
                  onKeyUp={this.setFilterAfterESC(['filters', 'codeTES'])}
                />
              }
            > codeTES </HeaderColumn>
            <HeaderColumn
              dataField="name"
              width="25%"
              dataFormat={cell => (
                <HighlightWords
                  searchWords={[filters.filters.name.value]}
                  textToHighlight={cell}
                />
              )}
              filters={
                <FilterSearch
                  value={filters.filters.name.value}
                  onChangeValue={this.setFilter(['filters', 'name'], true)}
                  onKeyUpValue={this.setFilterAfterESC(['filters', 'name'])}
                  comparison={filters.filters.name.comparison}
                  onChangeCheckbox={this.toggleComparison(['filters', 'name', 'comparison'])}
                />
              }
            > Найменування </HeaderColumn>
            <HeaderColumn
              dataField="manufacturer" width="20%" filters={
              // <input type="text" value={filters.filters['manufacturer.name'].value}  onChange={this.setFilter(['filters', 'manufacturer.name'], true)} className="form-control" placeholder="Фільтр найменування" />
              // FIXME: Don't work in api "filters['manufacturer.name']=like|value|lower"
                <div className="input-group">
                  <Select
                    name="form-field-name"
                    placeholder="Фільтр по бренду"
                    // value={{value: 0, label: '22222222' }}
                    value={filterManufacturers.filters.name.value}
                    options={filterManufacturers.filters.name.value ? manufacturersOptions : [{ label: 'Введіть назву бренду', disabled: true }]}
                    onInputChange={this.setFilterManufacturers(['filters', 'name'], true)}
                    onChange={this.setFilterSelect(['filters', 'manufacturer'])}
                    // onOpen={this.loadManufacturers}
                    noResultsText="Нічого не знайдено"
                    clearValueText="Стерти"
                    isLoading={isLoadingManufacturer}
                    clearable
                    onInputKeyDown={this.setFilterAfterESC(['filters', 'manufacturer'])}
                    style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0 }}
                  />
                  <span className="input-group-addon" >
                    <label
                      htmlFor="filters_name_3"
                      title={`Переключити на пошук ${filterManufacturers.filters.name.comparison === 'begins'
                        ? 'по всьому тексту'
                        : 'з початку тексту'}`
                      }
                      className="mt-checkbox"
                      style={{ paddingLeft: '17px' }}
                    >
                      <input
                        id="filters_name_3"
                        onChange={this.onChangeChecked(this.toggleComparisonManufactures(['filters', 'name', 'comparison']), filterManufacturers.filters.name.comparison)}
                        type="checkbox"
                        checked={filterManufacturers.filters.name.comparison === 'begins'}
                      />
                      <span />
                    </label>
                  </span>
                </div>
              }
            > Бренд </HeaderColumn>
            <HeaderColumn dataField="country"> Країна </HeaderColumn>
            <HeaderColumn dataField="measure"> Упаковка </HeaderColumn>
            <HeaderColumn dataField="image" dataAlign="center" dataFormat={check}> Фото </HeaderColumn>
            <HeaderColumn
              dataField="instruction" dataAlign="center"
              dataFormat={instructionIconWithHostUrl}
            > Інструкція </HeaderColumn>
            <HeaderColumn
              dataField="active" dataAlign="center"
              dataFormat={cell => (
                <IconStatus isValue={!!cell} />
              )}
            > Статус </HeaderColumn>
            {/* <HeaderColumnEdit pathWithoutId="/admin/nomenclature/" /> */}
            <HeaderColumn
              dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle"
              dataFormat={id => (
                <LinkSearch to={`/admin/nomenclature/${id}/remainders`} title="Залишки" />
              )}
            > Залишки </HeaderColumn>
            <HeaderColumn
              dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle"
              dataFormat={id => (
                <LinkEdit to={`/admin/nomenclature/${id}`} />
              )}
            />

          </CustomTable>

        </Portlet>

        {!errors.length && !!nomenclature.length &&
          <Paginate
            pageNum={paging.totalPages}
            clickCallback={this.setFilterPage}
            forceSelected={paging.page - 1}
          />
        }
      </div>
    );
  }
}

export default connect(createSelectors(selectors), actions)(Nomenclature);
