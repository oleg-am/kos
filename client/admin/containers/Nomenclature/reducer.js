import { combineReducers } from 'redux';
import * as types from './constants';

import { setFiltersValue, setValue } from '../../helpers/setObjValue';

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const nomenclatureEntities = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities.nomenclature;
    default:
      return state;
  }
};

const measureEntities = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities.measure;
    default:
      return state;
  }
};

const manufacturerEntities = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities.manufacturer;
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data;
    default:
      return state;
  }
};

const paging = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.paging;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const filtersInit = {
  page:  1,
  limit: 25,
  with:  {
    '*': {
      value: '*',
    },
  },
  filters: {
    name: {
      comparison: 'begins', //'like',
      value:      '',
      modifier:   'lower',
    },
    codeTES: {
      comparison: 'like',
      value:      '',
      // modifier: 'lower',
    },
    'manufacturer.name': {
      comparison: 'like',
      value:      '',
      modifier:   'lower',
    },
    manufacturer: {
      //comparison: 'like',
      value:      '',
      //modifier:   'lower',
    },
    remainders: {
      value: 'not null',
    },
  }
};

const filters = (state = filtersInit, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.CHANGE_PARAM:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
    default:
      return state;
  }
};

const filterManufacturersInit = {
  // page: 1,
  limit: 20,
  // with: {
  //   '*': {
  //     value: '*',
  //   }
  // },
  filters: {
    name: {
      comparison: 'begins',
      value:    '',
      modifier: 'lower',
    },

  },
};

const filterManufacturers = (state = filterManufacturersInit, action) => {
  switch (action.type) {
    case types.SET_FILTER_MANUFACTURES:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.CHANGE_PARAM_MANUFACTURES:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
    default:
      return state;
  }
};

const activeNomenclature = (state = {}, action) => {
  switch (action.type) {
    case types.EDIT:
      return action.nomenclature;
    default:
      return state;
  }
};

const manufacturerInit = {
  entities: {},
  ids: [],
};

const manufacturer = (state = manufacturerInit, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS_MANUFACTURES:
      return {
        ...state,
        entities: action.entities.manufacturer,
        ids: action.result.data,
      };
    default:
      return state;
  }
};

const isLoadingManufacturer = (state = false, action) => {
  switch (action.type) {
    case types.LOAD_REQUEST_MANUFACTURES:
      return true;
    case types.LOAD_SUCCESS_MANUFACTURES:
    case types.LOAD_FAILURE_MANUFACTURES:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  isLoadingManufacturer, // false
  errors, // []
  nomenclatureEntities, // {}
  measureEntities, // {}
  manufacturerEntities, // {}
  manufacturer, // {}
  ids, // []
  paging, // {}
  activeNomenclature, // {}
  filters, // {}
  filterManufacturers, // {}
  isFiltersDebounce, // false
});
