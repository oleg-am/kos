import { Schema, arrayOf } from 'normalizr';

const nomenclature = new Schema('nomenclature');
const manufacturer = new Schema('manufacturer');
const measure = new Schema('measure');

nomenclature.define({
    manufacturer,
    measure
});

export const NOMENCLATURE_ARR = { data: arrayOf(nomenclature) };
export const MANUFACTURER_ARR = { data: arrayOf(manufacturer) };
