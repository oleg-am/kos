import { createSelector } from 'reselect';

export const imgUrl = state => state.app.imgUrl;

export const nomenclatureEntities = state => state.nomenclature.nomenclatureEntities;
export const measureEntities      = state => state.nomenclature.measureEntities;
export const manufacturerEntities = state => state.nomenclature.manufacturerEntities;

export const manufacturer = state => state.nomenclature.manufacturer.entities;

// export const nomenclature = createSelector(
//   nomenclatureEntities,
//   // measureEntities,
//   // manufacturerEntities,
//   items => Object.values(items)
// );

export const nomenclature = createSelector(
  nomenclatureEntities,
  measureEntities,
  manufacturerEntities,
  (items, measure, manufacturer) => Object.values(items).map(item => ({
    ...item,
    measure:      measure[item.measure].name,
    manufacturer: manufacturer[item.manufacturer].name,
    country:      manufacturer[item.manufacturer].country,
  }))
);

export const manufacturersOptions = createSelector(
  manufacturer,
  (items) => Object.values(items).map(item => ({
    value: item.id,
    label: item.name,
  }))
);


export const nomenclatureIds = state => state.nomenclature.ids;
export const paging = state => state.nomenclature.paging;
export const filters1 = state => state.nomenclature.filters;
export const filterManufacturers1 = state => state.nomenclature.filterManufacturers;

export const filters = createSelector(filters1, (items) => items)
export const filterManufacturers = createSelector(filterManufacturers1, (items) => items)

export const activeNomenclature = state => state.nomenclature.activeNomenclature;
export const errors = state => state.nomenclature.errors;
export const isFiltersDebounce = state => state.nomenclature.isFiltersDebounce;

export const isLoadingManufacturer = state => state.nomenclature.isLoadingManufacturer;
