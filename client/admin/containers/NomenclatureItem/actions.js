import * as types from './constants';

// import { createParams } from '../../helpers/api';
import { normalize } from 'normalizr';
import axios from 'axios';
// import _omit from 'lodash/omit';
// import _union from 'lodash/union';

import {
  NOMENCLATURE_ITEM_OBJ,
} from './schemas.js';

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
})

export const closeMessage = () => ({
  type: types.CLOSE_MESSAGE,
})

export const reset = () => ({
  type: types.RESET,
})

export const loadNomenclatureItem = (id) => (dispatch) => (
	axios
  	.get(`/api/admin/nomenclature/${id}?with[*]=*`)
  	.then((res) => {
      // console.log('normalize: ', normalize(res.data, NOMENCLATURE_ITEM_OBJ));
  		return dispatch({
  			type: types.LOAD_SUCCESS,
        res,
        errors: [],
  			data: res.data
  		})
  	}).catch((error) => {
      return dispatch({
  			type: types.LOAD_FAILURE,
        error,
  			errors: error
  		})
    })
)


export const loadImgUrl = () => (dispatch) => {
  dispatch({
    type: types.LOAD_REQUEST_IMG_URL,
  })

  axios
		.get(`/api/img-url`)
		.then((res) => {
			return dispatch({
				type: types.LOAD_SUCCESS_IMG_URL,
        res,
        data: res.data || ''
        // errors: [],
			})
		}).catch((error) => {

      console.error(error);
      return dispatch({
				type: types.LOAD_FAILURE_IMG_URL,
        error,
        data: '',
			})
    })
}

export const saveNomenclatureItem = (id, data) => (dispatch) => {
  dispatch({
    type: types.SAVE_NOMENCLATURE_REQUEST
  })
  axios
  .put(`/api/admin/nomenclature/${id}`, data)
  .then((res) => {
    return dispatch({
      type: types.SAVE_NOMENCLATURE_SUCCESS,
      errors: [],
      res: res,
    })
  }).catch((error) => {

    console.error(error);
    return dispatch({
      type: types.SAVE_NOMENCLATURE_FAILURE,
      errors: error
    })
  })
}

export const saveImg = (id, data, clearFiles) => (dispatch) => {
  dispatch({
    type: types.SAVE_IMG_REQUEST
  })
  axios
  .post(`/api/admin/nomenclature/${id}/image`, data)
  .then((res) => {
    clearFiles();
    return dispatch({
      type: types.SAVE_IMG_SUCCESS,
      res,
      errors: [],
      image: res.data.image,
    })
  }).catch((error) => {
    console.error(error);
    return dispatch({
      type: types.SAVE_IMG_FAILURE,
      error,
      errors: error.response.data.errors
    })
  })
}

export const saveFile = (id, data, clearFiles) => (dispatch) => {
  dispatch({
    type: types.SAVE_FILE_REQUEST
  })
  axios
  .post(`/api/admin/nomenclature/${id}/instruction`, data)
  .then((res) => {
    clearFiles();
    return dispatch({
      type: types.SAVE_FILE_SUCCESS,
      errors: [],
      res,
      instructionFile: res.data.instructionFile,
    })
  }).catch((error) => {
    return dispatch({
      type: types.SAVE_FILE_FAILURE,
      error,
      errors: error.response.data.errors
    })
  })
}
