import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router';

import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';

import * as actions from './actions';
import selectors from './selectors';
import { nomenclatureValidator } from './schemas';

import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

import { FilesUploaderImg, FilesUploaderPdfImgZip } from '../../components/FilesUploader';
// import Switch from 'react-bootstrap-switch';

const { bool } = PropTypes;

const FormMdLineInputStatic = ({ label, value, labelCol = '', valueCol = '' }) => (
  <div style={{marginBottom: 0, paddingTop: '5px'}} className="form-group form-md-line-input">
    <label className={classNames({ 'col-lg-4 col-md-4 col-sm-4': !labelCol }, labelCol, 'control-label')} > {label} </label>
    <div className={classNames({ 'col-lg-8 col-md-8 col-sm-8': !valueCol }, valueCol)}>
      <div className="form-control form-control-static"> {value} </div>
      <div className="form-control-focus"> </div>
    </div>
  </div>
)

const BootstrapSwitch = ({ onValue, offValue, active = false, onClick }) => (
  <div onClick={onClick} className = "bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-animate bootstrap-switch-off" style = {{width: '175.222px'}} >
    <div className="bootstrap-switch-container" style={{width: '259px', marginLeft: active ? 0 : '-86px'}}>
      <span className="bootstrap-switch-handle-on bootstrap-switch-primary" style={{width: '86px'}}>Активний</span>
      <span className="bootstrap-switch-label" style={{width: '86px'}}></span>
      <span className="bootstrap-switch-handle-off bootstrap-switch-default" style={{width: '86px'}}>Неактивний</span>
      {/* <input type="checkbox" className="make-switch" data-on-text="External" data-off-text="Internal" /> */}
    </div>
  </div>
)

class NomenclatureItem extends React.Component {
  static propTypes = {
    isEditableRoleAccess: bool,
  }

  state = {
    files: []
  }

  constructor(props, context) {
    super(props, context);

    const {
      imgUrl,
      loadImgUrl,
      loadNomenclatureItem,
      params: { id }
    } = this.props;

    // this.debounceLoadPharmaciesItem = _debounce(loadNomenclatureItem, 400);
    loadNomenclatureItem(id);
    !imgUrl && loadImgUrl();
  }

  onChangeParam = (key, value) => (e) => {
    this.props.changeParam(key, value || e.target.value);
  }
  onChangeActive = (e, state) => {
    this.props.changeParam('active', state);
  }
  onSubmit = () => {
    const { nomenclatureItem, saveNomenclatureItem, params: { id } } = this.props;

    const validData = nomenclatureValidator.validate({
      ...nomenclatureItem,
    });

    if (validData) {
      saveNomenclatureItem(id, validData);
    } else {
      console.log('errors', nomenclatureValidator.getErrors());
    }
  }
  reset = () => {
    this.props.reset();
  }
  onDrop = (files) => {
    this.setState({files});
  }
  saveImg = (data, clearFiles) => {
    this.props.saveImg(this.props.params.id, data, clearFiles)
  }
  saveFile = (data, clearFiles) => {
    this.props.saveFile(this.props.params.id, data, clearFiles)
  }
  onOpenClick = () => {
    this.dropzone.open();
  }
  render() {
    const {
      isEditableRoleAccess,
      params: { id },
      imgUrl,
      errors,
      nomenclatureItem,
    } = this.props;

    const { stateNomenclature } = nomenclatureItem;

    const renderTemperatureRegim = () => (
      <div className="form-group">
        <label>Режим зберігання</label>
        <table className="table table-bordered tr-center td-center">
          <colgroup>
            <col width="33%" />
            <col width="33%" />
            <col width="33%" />
          </colgroup>
          <thead>
            <tr>
              <th> t&deg;<sub>min</sub> </th>
              <th> t&deg;<sub>max</sub> </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <div style={{padding: 0, margin: '0 0 15px'}} className="form-group form-md-line-input">
                  {/* <input onChange={this.onChangeParam('workOnWeekDays')} style={{minWidth: '50%', textAlign: 'center'}} value={nomenclatureItem.workOnWeekDays} type="text" className="form-control" placeholder="Введіть час" /> */}
                  <div className="form-control-focus"> </div>
                </div>
              </td>
              <td>
                <div style={{padding: 0, margin: '0 0 15px'}} className="form-group form-md-line-input">
                  {/* <input onChange={this.onChangeParam('workOnSaturday')} style={{textAlign: 'center'}} value={nomenclatureItem.workOnSaturday} type="text" className="form-control" placeholder="Введіть час" /> */}
                  <div className="form-control-focus"> </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )

    return (
      <div className="page-content" style={{paddingTop: '20px'}}>
        <Breadcrumbs title="" components={{
          title: 'Картка товару',
          links: [
            {
              name: 'Меню',
              to: `/admin/reports`
            }, {
              name: 'Всі товари',
              to: `/admin/nomenclature`
            }, {
              name: 'Картка товару'
            }
          ]
        }}/>
        <Portlet
          title={
            <div>
              <i className="icon-bag"></i>
              <span className="caption-subject"> {nomenclatureItem.name} </span>
              {/* <span style={{fontSize: '12px'}} className="caption-helper"> {nomenclatureItem.addressIn1C} </span> */}
            </div>
          }
          actionsRight={[
            <Link style={{marginRight: '10px'}} key="actionsRight_2" className="btn btn-sm blue" to={`/admin/nomenclature/${id}/remainders`}>
              <i className="fa fa-search"></i> Залишки
            </Link>,
            isEditableRoleAccess && <button
              style={{ marginRight: '10px' }}
              key="actionsRight_3"
              className="btn green-meadow"
              onClick={this.onSubmit}
            >
              <i className="fa fa-check" /> Зберегти
            </button>,
            <Link key="actionsRight_4" className="btn btn-sm default" onClick={this.reset} to="/admin/nomenclature">
              <i className="fa fa-times"></i> Закрити
            </Link>,

          ]}
        >

        <Row>
          <Col md={6}>
            <form className="form-horizontal">
              <FormMdLineInputStatic
                label="Назва товарної позиції:"
                value={nomenclatureItem.name}
              />
              <FormMdLineInputStatic
                label="Код в 1С АНР:"
                value={nomenclatureItem.code1C}
              />
              <FormMdLineInputStatic
                label="Код в Маріона:"
                value={nomenclatureItem.codeMorion}
              />
              <FormMdLineInputStatic
                label="Код в ТЕСА:"
                value={nomenclatureItem.codeTES}
              />
              <FormMdLineInputStatic
                label="Бренд:"
                value={nomenclatureItem.manufacturer.name}
              />
              <FormMdLineInputStatic
                label="Країна виробник:"
                value={nomenclatureItem.manufacturer.country}
              />
              <FormMdLineInputStatic
                label="Упаковка:"
                value={nomenclatureItem.measure.name}
              />
              <FormMdLineInputStatic
                label="Мінімальна частина:"
                value={nomenclatureItem.subQuantity ? (1 / nomenclatureItem.subQuantity).toFixed(2) : 1}
              />
            </form>
            <FilesUploaderImg
              editable={isEditableRoleAccess}
              imgUrl={imgUrl}
              url_main={nomenclatureItem.image.url_main}
              saveFile={this.saveImg}
            />
          </Col>
          <Col md={6}>
            <div className="form-group">
              <label>Статус</label>
              <div className="pull-right">
                <div className="bootstrap-switch bootstrap-switch-on">
                  <span className={classNames(
                    'bootstrap-switch-handle-on',
                    { 'bootstrap-switch-success': nomenclatureItem.active },
                    { 'bootstrap-switch-danger': !nomenclatureItem.active },
                  )}>
                    {nomenclatureItem.active ? 'Активний' : 'Неактивний'}
                  </span>
                </div>
                {/* <Switch
                  value={nomenclatureItem.active}
                  onText="Активний"
                  offText="Неактивний"
                  onColor="success"
                  offColor="danger"
                  onChange={this.onChangeActive}
                /> */}
              </div>
            </div>
            <div className="form-group">
              <label>Інструкція</label>
              <textarea
                onChange={this.onChangeParam('instruction')}
                value={nomenclatureItem.instruction}
                className="form-control"
                rows="5"
                placeholder="Введіть інструкцію"
              >
              </textarea>
            </div>
            <Row>
              <Col md={7}>
                <FilesUploaderPdfImgZip
                  editable={isEditableRoleAccess}
                  imgUrl={imgUrl}
                  url_main={nomenclatureItem.instructionFile}
                  saveFile={this.saveFile}
                />
              </Col>
              {stateNomenclature && stateNomenclature.instruction &&
                <Col md={5} >
                  <a
                    href={`file:${stateNomenclature.instruction}`}
                    style={{ color: 'red', marginTop: '10px', display: 'inline-block', verticalAlign: 'middle' }}
                    download
                  >
                    <i className="icon-doc" /> Інструкція з ГосНоменклатури
                  </a>
                </Col>
              }
            </Row>
            {renderTemperatureRegim()}
            <FormMdLineInputStatic
              label="Мінімальний термін придатності:"
              labelCol="col-lg-6 col-md-6 col-sm-6"
              value=""
              valueCol="col-lg-6 col-md-6 col-sm-6"
            />
          </Col>
        </Row>
      </Portlet>

      </div>
    );
  }
}

export default connect(selectors, actions)(NomenclatureItem);
