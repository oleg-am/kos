import * as types from './constants';
import { combineReducers } from 'redux';

import _uniqueId from 'lodash/uniqueId';
import _omit from 'lodash/omit';
import _pick from 'lodash/pick';
import _union from 'lodash/union';
import _merge from 'lodash/merge';

import { mapObj } from '../../helpers/mapObj';
import { setValue } from '../../helpers/setObjValue';

const errors = (state = [], action ) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
}

const errorsValidation = (state = {}, action ) => {
  switch (action.type) {
    case types.SAVE_DELIVERY_SCHEDULES_FAILURE:
      return action.errors;
    case types.RESET:
    case types.CLOSE_MESSAGE:
      return {};
    default:
      return state;
  }
}

const imgUrl = (state = '', action ) => {
  switch (action.type) {
    case types.LOAD_SUCCESS_IMG_URL:
    case types.LOAD_FAILURE_IMG_URL:
      return action.data;
    default:
      return state;
  }
}

const isFiltersDebounce = (state = false, action ) => {
  switch (action.type) {
    case types.SET_FILTER_CITIES:
    case types.SET_FILTER_REGIONS:
    return action.isFiltersDebounce;
    default:
    return state;
  }
}

const isSuccessMessage = (state = false, action ) => {
  switch (action.type) {
    case types.SAVE_PHARMACY_SUCCESS:
    case types.SAVE_IMG_SUCCESS:
    case types.SAVE_FILE_SUCCESS:
    case types.SAVE_PHONES_SUCCESS:
    case types.SAVE_DELIVERY_SCHEDULES_SUCCESS:
      return true;
    case types.CLOSE_MESSAGE:
      return false;
    default:
      return state;
  }
}

const isErrorMessage = (state = false, action ) => {
  switch (action.type) {
    case types.SAVE_PHARMACY_FAILURE:
    case types.SAVE_IMG_FAILURE:
    case types.SAVE_PHONES_FAILURE:
    case types.SAVE_DELIVERY_SCHEDULES_FAILURE:
      return true;
    case types.CLOSE_MESSAGE:
      return false;
    default:
      return state;
  }
}

const isSaveNomenclature = (state = false, action ) => {
  switch (action.type) {
    case types.CHANGE_PARAM:
      return true;
    case types.SAVE_NOMENCLATURE_SUCCESS:
    case types.RESET:
      return false;
    default:
      return state;
  }
}

const nomenclatureInit = {
  name: '',
  instructionFile:'',
  image: {
    url_main: ''
  },
  manufacturer: {},
  measure: {
    // name: ''
  },
}

const nomenclatureItem = (state = nomenclatureInit, action ) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return {
        ...state,
        ...action.data,
      };
    case types.CHANGE_PARAM:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
    case types.SAVE_IMG_SUCCESS:
      return {
        ...state,
        image: action.image
      };
    case types.SAVE_FILE_SUCCESS:
      return {
        ...state,
        instructionFile: action.instructionFile
      };
    case types.RESET:
      return nomenclatureInit;
    default:
      return state;
  }
}

export default combineReducers({
  isFiltersDebounce, // false
  isSuccessMessage,
  isErrorMessage,
  errors, // []
  errorsValidation, // {}
  imgUrl,
  isSaveNomenclature,
  nomenclatureItem, // {}
})
