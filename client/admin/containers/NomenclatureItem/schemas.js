import { Schema, arrayOf } from 'normalizr';

import LIVR from 'livr';
LIVR.Validator.defaultAutoTrim(true);

const nomenclature = new Schema('nomenclature');
const manufacturer = new Schema('manufacturer');
const measure = new Schema('measure');

nomenclature.define({
    manufacturer,
    measure
});

export const NOMENCLATURE_ITEM_OBJ = nomenclature;

const nomenclatureSchema = {
  instruction: 'string',
}

export const nomenclatureValidator = new LIVR.Validator(nomenclatureSchema);
