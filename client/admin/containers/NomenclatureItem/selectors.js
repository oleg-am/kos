import { createSelector, createStructuredSelector } from 'reselect';
import _omit from 'lodash/omit';
import _pick from 'lodash/pick';

import { mapObj } from '../../helpers/mapObj';

const REDUCER = 'nomenclatureItem';

const nomenclatureItem  = state => state[REDUCER].nomenclatureItem;
const entities          = state => state[REDUCER].nomenclatureItem.entities;
const isFiltersDebounce = state => state[REDUCER].isFiltersDebounce;
const errors            = state => state[REDUCER].errors;
const errorsValidation  = state => state[REDUCER].errorsValidation;
const imgUrl            = state => state[REDUCER].imgUrl;

const isSuccessMessage = state => state[REDUCER].isSuccessMessage;
const isErrorMessage   = state => state[REDUCER].isErrorMessage;
const isSaveNomenclature     = state => state[REDUCER].isSaveNomenclature;

// const networkPhones = createSelector(
//   nomenclatureItem,
//   networks,
//   ({ network: id }, networks) =>
//     (id && Object.keys(networks).length) ? networks[id].phones : []
// )

export default createStructuredSelector({
  imgUrl,
  nomenclatureItem,

  isFiltersDebounce,
  errors,
  errorsValidation,
  isSuccessMessage,
  isErrorMessage,
  isSaveNomenclature,
})
