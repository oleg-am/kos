import * as types from './constants';

import { createParams } from '../../helpers/api';
import REMAINDERS_ARR from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const edit = nomenclature => ({
  type: types.EDIT,
  nomenclature,
});

export const reset = () => ({
  type: types.RESET,
});

export const loadRemainders = (id, filters) => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/nomenclature/${id}/remainders${createParams(filters)}`),
  schema:  REMAINDERS_ARR,
});

export const loadNomenclature = id => ({
  types: [
    types.LOAD_NOMENCLATURE_REQUEST,
    types.LOAD_NOMENCLATURE_SUCCESS,
    types.LOAD_NOMENCLATURE_FAILURE,
  ],
  promise: api => api.get(`/api/admin/nomenclature/${id}?fields=name`),
});
