const module = 'NOMENCLATURE_REMAINDERS';

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const LOAD_NOMENCLATURE_REQUEST = `${module}/LOAD_NOMENCLATURE_REQUEST`;
export const LOAD_NOMENCLATURE_SUCCESS = `${module}/LOAD_NOMENCLATURE_SUCCESS`;
export const LOAD_NOMENCLATURE_FAILURE = `${module}/LOAD_NOMENCLATURE_FAILURE`;

export const SET_FILTER = `${module}/SET_FILTER`;
export const EDIT = `${module}/EDIT`;

export const RESET = `${module}/RESET`;
