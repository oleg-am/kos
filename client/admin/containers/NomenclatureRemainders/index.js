import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

// import { Link, browserHistory } from 'react-router'

// import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
// import Select from 'react-select';

import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';

import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
// import LimitSelect from '../../components/LimitSelect';
// import Paginate from '../../components/Paginate';

// import LinkEdit from '../../elements/LinkEdit';
import LinkClose from '../../elements/LinkClose';

const STYLE_RED = { color: '#ed6b75' };

class PharmaciesRemainders extends React.Component {

  static propTypes = {
    location: PropTypes.objectOf(PropTypes.any).isRequired,
    params:   PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
    errors:           PropTypes.arrayOf(PropTypes.object),
    filters:          PropTypes.objectOf(PropTypes.any).isRequired,
    nomenclature:     PropTypes.objectOf(PropTypes.any),
    remainders:       PropTypes.arrayOf(PropTypes.object).isRequired,
    paging:           PropTypes.objectOf(PropTypes.any),
    setFilter:        PropTypes.func.isRequired,
    reset:            PropTypes.func.isRequired,
    loadRemainders:   PropTypes.func.isRequired,
    loadNomenclature: PropTypes.func.isRequired,
  }

  constructor(props, context) {
    console.log('props nom: ', props);
    super(props, context);
    const { params: { id }, loadRemainders, filters, loadNomenclature } = this.props;

    this.debounceLoadRemainders = _debounce(loadRemainders, 200);

    loadRemainders(id, filters);
    loadNomenclature(id);
  }

  componentWillReceiveProps(next) {
    const { isFiltersDebounce, filters: nextFilters } = next;
    const { loadRemainders, filters, params: { id } } = this.props;

    if (filters !== nextFilters) {
      const newfilters = filters.page === nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters;

      isFiltersDebounce
      ? this.debounceLoadRemainders(id, newfilters)
      : loadRemainders(id, newfilters);
    }
  }

  componentWillUnmount() {
    this.reset();
  }

  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  reset = () => {
    this.props.reset();
  }
  refreshData = () => {
    this.props.loadRemainders(this.props.params.id, this.props.filters);
  }
  render() {
    const {
      params: { id },
      location,
      errors,
      remainders,
      paging,
      filters,
      nomenclature,
    } = this.props;

    const { returnTo, isReading } = location.state || {};

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          components={{
            title: <span> Залишки <span style={STYLE_RED}>{nomenclature.name}</span></span>,
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Список номенклатури',
                to:   '/admin/nomenclature',
              }, {
                name: `Номенклатура №${id}`,
                to:   `/admin/nomenclature/${id}`,
              }, {
                name: 'Залишки',
              },
            ],
          }}
        />
        {/* <PortletBody> */}

        <Portlet
          // actionsLeft={[
          //   <LimitSelect
          //     key="actionsLeft_1"
          //     optionsValue={[10, 15, 20]}
          //     value={filters.limit}
          //     onChange={this.setFilter('limit')}
          //   />,
          // ]}

          actionsRight={[
            <LinkClose key="actionsRight_5" to={returnTo || '/admin/nomenclature'} />,
          ]}
        >
          <CustomTable
            tableClass="table table-striped table-bordered table-hover"
            trClassName={row => (row.isStock ? 'bg-default' : '')}
            data={errors.length ? [] : remainders}
            striped
            hover
            pagination
            filters
          >
            <HeaderColumn dataField="id" width="5%" dataAlign="center" isKey> № </HeaderColumn>
            <HeaderColumn
              dataField="pharmacy"
              width="55%"
              dataFormat={(cell, { pharmacy }) => pharmacy.name}
              filters={
                <input
                  type="text"
                  value={filters.filters['pharmacy.name'].value}
                  onChange={this.setFilter(['filters', 'pharmacy.name'], true)}
                  className="form-control"
                  placeholder="Введіть найменування"
                />
              }
            > Аптеки </HeaderColumn>
            <HeaderColumn dataField="quantity" width="20%"> Кількість </HeaderColumn>
            <HeaderColumn dataField="sell_price" width="20%" > Ціна Резерву</HeaderColumn>
            {/* <HeaderColumn dataField="price" width="20%"> Ціна Аптеки</HeaderColumn> */}
            {/* <HeaderColumn dataField="stockPrice" > Ціна Cкладу</HeaderColumn> */}
            {/* <HeaderColumn dataField="purchasePrice"> Ціна закупки </HeaderColumn> */}
            {/* KSM-81 Лишняя кнопка "LinkEdit" в разделе остатки - номенклатура
            Выключили 22.12.2016 */}
            {/* {!isReading &&
              <HeaderColumn
                dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle"
                dataFormat={(cell, { pharmacy }) => (
                  <LinkEdit to={`/admin/pharmacies/${pharmacy.id}`} />
                )}
              />
            } */}

          </CustomTable>

        </Portlet>

        {/* {!errors.length && !!remainders.length &&
          <Paginate
            pageNum={paging.totalPages}
            clickCallback={this.setFilterPage}
            forceSelected={paging.page - 1}
          />
        } */}
      </div>
    );
  }
}

export default connect(selectors, actions)(PharmaciesRemainders);
