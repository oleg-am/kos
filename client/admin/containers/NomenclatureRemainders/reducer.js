import { combineReducers } from 'redux';
import * as types from './constants';

import { setFiltersValue } from '../../helpers/setObjValue';

const stockRemainders = {
  id:       '',
  quantity: 0,
  price:    0,
  pharmacy: {
    name: 'Склад',
  },
};

const initialState = {
  nomenclature: {
    name: '',
  },
  remaindersEntities: {
    stockRemainders,
  },
  filters: {
    page:  1,
    limit: 10,

    with: {
      '*': {
        value: '*',
      },
    },
    order_by: {
      id: {
        value: 'ASC',
      },
    },
    filters: {
      'pharmacy.name': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
    },
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const nomenclature = (state = initialState.nomenclature, action) => {
  switch (action.type) {
    case types.LOAD_NOMENCLATURE_SUCCESS:
      return action.data;
    case types.RESET:
      return initialState.nomenclature;
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data || state;
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const remaindersEntities = (state = initialState.remaindersEntities, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return {
        ...(action.entities.remainders || {}),
        stockRemainders: action.result.stockRemainders || stockRemainders,
      };
    default:
      return state;
  }
};

// const paging = (state = {}, action) => {
//   switch (action.type) {
//     case types.LOAD_SUCCESS:
//       return action.result.paging;
//     default:
//       return state;
//   }
// };

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.RESET:
      return { ...initialState.filters };
    default:
      return state;
  }
};

export default combineReducers({
  errors, // []
  nomenclature,
  ids, // []
  // paging, // {}
  filters, // {}
  isFiltersDebounce, // false
  remaindersEntities, // {}
});
