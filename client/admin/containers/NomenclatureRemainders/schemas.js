import { Schema, arrayOf } from 'normalizr';

const remainders = new Schema('remainders');
// const pharmacy = new Schema('pharmacy', { idAttribute: 'name' });

// remainders.define({
//   pharmacy,
// });

const REMAINDERS_ARR = { data: arrayOf(remainders) };

export default REMAINDERS_ARR;
