import { createSelector, createStructuredSelector } from 'reselect';

const REDUCER = 'nomenclatureRemainders';

const remaindersIds        = state => state[REDUCER].ids;
const remaindersEntities   = state => state[REDUCER].remaindersEntities;

const stockRemaindersPrice = createSelector(
  remaindersEntities,
  items =>
    items.stockRemainders && (items.stockRemainders.sell_price || items.stockRemainders.price),
);

const remainders = createSelector(
  remaindersIds,
  remaindersEntities,
  stockRemaindersPrice,
  (ids, items, stockRemaindersPrice_) => {
    const newIds = ['stockRemainders', ...ids];

    return newIds.map(id => (id === 'stockRemainders'
      ? { ...items[id], isStock: true, sell_price: stockRemaindersPrice_.toFixed(2) }
      : items[id]
    ));
  },
);

export default createStructuredSelector({
  nomenclature:      state => state[REDUCER].nomenclature,
  isFiltersDebounce: state => state[REDUCER].isFiltersDebounce,
  errors:            state => state[REDUCER].errors,
  filters:           createSelector(state => state[REDUCER].filters, items => items),
  remainders,
});
