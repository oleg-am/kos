const module = 'nomenclatureSelectionInPharmacy';

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const LOAD_PHARMACY_REQUEST = `${module}/LOAD_PHARMACY_REQUEST`;
export const LOAD_PHARMACY_SUCCESS = `${module}/LOAD_PHARMACY_SUCCESS`;
export const LOAD_PHARMACY_FAILURE = `${module}/LOAD_PHARMACY_FAILURE`;

export const TOGGLE_ITEM = `${module}/TOGGLE_ITEM`;
export const ADD_ITEMS = `${module}/ADD_ITEMS`;

export const SET_FILTER = `${module}/SET_FILTER`;

export const CHANGE_PARAM = `${module}/CHANGE_PARAM`;
export const CHANGE_FILTER_PARAM = `${module}/CHANGE_FILTER_PARAM`;

export const EDIT = `${module}/EDIT`;

export const RESET = `${module}/RESET`;
// sagas
export const SELECT_ITEMS = `${module}/SELECT_ITEMS`;

export default module;
