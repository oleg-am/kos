import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router';
import HighlightWords from 'react-highlight-words';

// import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
// import Select from 'react-select';

import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';

import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import LimitSelect from '../../components/LimitSelect';
import Paginate from '../../components/Paginate';

import { dateFormatter } from '../../components/Formatters';
import FilterSearch from '../../components/FilterSearch';

import LinkSearch from '../../elements/LinkSearch';
import LinkClose from '../../elements/LinkClose';

class NomenclatureSelectionInPharmacy extends React.Component {

  static propTypes = {
    errors:        PropTypes.arrayOf(PropTypes.object),
    selectedItems: PropTypes.objectOf(PropTypes.object),
    filters:       PropTypes.objectOf(PropTypes.any).isRequired,
    location:      PropTypes.objectOf(PropTypes.any).isRequired,
    params:        PropTypes.shape({
      id:         PropTypes.string.isRequired,
      pharmacyId: PropTypes.string.isRequired,
    }),
    pharmacy:          PropTypes.objectOf(PropTypes.any),
    remainders:        PropTypes.arrayOf(PropTypes.object).isRequired,
    paging:            PropTypes.objectOf(PropTypes.any),
    setFilter:         PropTypes.func.isRequired,
    changeParam:       PropTypes.func.isRequired,
    changeFilterParam: PropTypes.func.isRequired,
    reset:             PropTypes.func.isRequired,
    loadRemainders:    PropTypes.func.isRequired,
    loadPharmacy:      PropTypes.func.isRequired,
    toggleItem:        PropTypes.func.isRequired,
    addItems:          PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const { params: { pharmacyId }, loadRemainders, filters, loadPharmacy } = this.props;

    this.debounceLoadRemainders = _debounce(loadRemainders, 200);

    loadPharmacy(pharmacyId);
    loadRemainders(pharmacyId, filters);
  }

  componentWillReceiveProps(next) {
    const { isFiltersDebounce, filters: nextFilters } = next;
    const { loadRemainders, filters, params: { pharmacyId } } = this.props;

    if (filters !== nextFilters) {
      const newfilters = filters.page === nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters;

      isFiltersDebounce
      ? this.debounceLoadRemainders(pharmacyId, newfilters)
      : loadRemainders(pharmacyId, newfilters);
    }
  }

  componentWillUnmount() {
    this.reset();
  }
  onChangeParam = (key, max, item) => (e) => {
    const targetValue = +e.target.value;
    let value = targetValue > max ? max : targetValue;
    value = value < 0 ? 0 : value;
    this.props.changeParam(key, value, item);
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterAfterESC = key => (e) => {
    e.keyCode === 27 && this.props.setFilter(key, '');
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  addItems = () => {
    this.props.addItems(this.props.selectedItems);
  }
  toggleItem = (id, item) => () => {
    this.props.toggleItem(id, item);
  }
  reset = () => {
    this.props.reset();
  }
  toggleComparison = key => (value) => {
    this.props.changeFilterParam(key, value);
  }
  refreshData = () => {
    this.props.loadRemainders(this.props.params.pharmacyId, this.props.filters);
  }
  render() {
    const {
      params: { id },
      location,
      errors,
      remainders,
      paging,
      filters,
      pharmacy,
      selectedItems,
    } = this.props;

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          components={{
            title: <span> Підбір товарів <span style={{ color: '#ed6b75' }}>{pharmacy.name}</span></span>,
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Замовлення',
                to:   '/admin/orders',
              }, {
                name: id === 'create' ? 'Створення' : `№${id}`,
                to:   `/admin/orders/${id}`,
              }, {
                name: 'Підбір товарів',
              },
            ],
          }}
        />
        {/* <PortletBody> */}

        <Portlet
          actionsLeft={[
            <LimitSelect key="actionsLeft_1" optionsValue={[10, 15, 20]} value={filters.limit} onChange={this.setFilter('limit')} />,
              // // <input
              //   type="text"
              //   style={{wpidth: 'inherit', display: 'inline-block'}}
              //   className="form-control"
              //   placeholder="Фільтер найменуванню"
              // />
          ]}

          actionsRight={[
            <Link
              key="actionsRight_1"
              className="btn btn-sm green-meadow"
              style={{ marginRight: '10px' }}
              to={`/admin/orders/${id}`}
              onClick={this.addItems}
            >
              + Додати до замовлення
            </Link>,
            <LinkClose key="actionsRight_5" to={`/admin/orders/${id}`} />,
            // <Link
              //   style={{ marginRight: '5px' }}
              //   key="actionsRight_3"
              //   onClick={this.refreshData}
              //   className="btn btn-sm blue-sharp"
              // >
            //   <i className="fa fa-refresh" />
            //   {/* Оновити дані */}
            // </Link>,
          ]}
        >

          <CustomTable
            tableClass="table table-striped table-bordered table-hover"
            data={errors.length ? [] : remainders}
            striped
            hover
            pagination
            filters
          >
            <HeaderColumn
              dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle" isKey
              dataFormat={(cell, row) => (
                <label htmlFor={`check_${cell}`} className="mt-checkbox">
                  <input
                    id={`check_${cell}`}
                    onChange={this.toggleItem(row.nomenclature.id, row)}
                    type="checkbox"
                    checked={!!selectedItems[row.nomenclature.id]}
                  />
                  <span />
                </label>
              )}
            />
            <HeaderColumn
              dataField="nomenclature"
              width="40%"
              dataFormat={(cell, { nomenclature }) => (
                <HighlightWords
                  searchWords={[filters.filters['nomenclature.name'].value]}
                  textToHighlight={nomenclature.name}
                />
              )}
              filters={
                <FilterSearch
                  value={filters.filters['nomenclature.name'].value}
                  onChangeValue={this.setFilter(['filters', 'nomenclature.name'], true)}
                  onKeyUpValue={this.setFilterAfterESC(['filters', 'nomenclature.name'])}
                  comparison={filters.filters['nomenclature.name'].comparison}
                  onChangeCheckbox={this.toggleComparison(['filters', 'nomenclature.name', 'comparison'])}
                />
              }
              // filters={
              //   <input
              //     type="text"
              //     value={filters.filters['nomenclature.name'].value}
              //     onChange={this.setFilter(['filters', 'nomenclature.name'], true)}
              //     className="form-control"
              //     placeholder="Введіть найменування"
              //   />
              // }
            > Найменування </HeaderColumn>
            <HeaderColumn
              dataField="nomenclature"
              dataFormat={(cell, row) => {
                const { quantity, stockRemainders, is_divisible, sub_quantity } = row;
                const item = selectedItems[cell.id];
                const max = quantity + stockRemainders;
                return (
                  <input
                    type="number"
                    className="form-control"
                    style={{ minWidth: '50%', textAlign: 'center' }}
                    min={0}
                    max={max}
                    step={(is_divisible && sub_quantity && 1 / sub_quantity) || 1}
                    value={(item && item.nomenclature.quantity) || 0}
                    onChange={this.onChangeParam([cell.id, 'nomenclature', 'quantity'], max, row)}
                  />
                );
              }}
            > Кількість </HeaderColumn>
            <HeaderColumn dataField="quantity"> Залишок в аптеці </HeaderColumn>
            <HeaderColumn dataField="stockRemainders" > Залишок на складі </HeaderColumn>
            <HeaderColumn dataField="sell_price" > Ціна резерву</HeaderColumn>
            {/* <HeaderColumn dataField="purchasePrice"> Ціна закупки </HeaderColumn> */}
            <HeaderColumn dataField="expiredAt" dataFormat={dateFormatter} > Термін придатності </HeaderColumn>
            <HeaderColumn
              dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle"
              dataFormat={(cell, { nomenclature }) => (
                <LinkSearch
                  title="Пошук в інших аптеках"
                  to={{
                    state: {
                      isReading: true,
                      returnTo:  location.pathname,
                    },
                    pathname: `/admin/nomenclature/${nomenclature.id}/remainders`,
                  }}
                />
              )}
            >
              Пошук в інших аптеках
            </HeaderColumn>

          </CustomTable>

        </Portlet>

        {!errors.length && !!remainders.length &&
          <Paginate
            pageNum={paging.totalPages}
            clickCallback={this.setFilterPage}
            forceSelected={paging.page - 1}
          />
        }
      </div>
    );
  }
}

export default connect(selectors, actions)(NomenclatureSelectionInPharmacy);
