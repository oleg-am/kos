import { combineReducers } from 'redux';
import _omit from 'lodash/omit';

import * as types from './constants';

import { setFiltersValue, setValue } from '../../helpers/setObjValue';

const initialState = {
  selectedItems: {},
  pharmacy: {
    name: '',
  },
  filters: {
    page:          1,
    limit:         10,
    include_stock: true, // [KSM-222] додає товари, яких немає на залишках аптеки, але є на складі

    with: {
      '*': {
        value: '*',
      },
    },
    order_by: {
      id: {
        value: 'ASC',
      },
    },
    filters: {
      'nomenclature.name': {
        comparison: 'begins', // 'like'
        value:      '',
        modifier:   'lower',
      },
    },
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const pharmacy = (state = initialState.pharmacy, action) => {
  switch (action.type) {
    case types.LOAD_PHARMACY_SUCCESS:
      return action.data;
    case types.RESET:
      return initialState.pharmacy;
    default:
      return state;
  }
};

const selectedItems = (state = initialState.selectedItems, action) => {
  switch (action.type) {
    case types.SELECT_ITEMS:
      return action.items.reduce((obj, item) => {
        const id = item.id;
        obj[id] = {
          ...item,
          // ...state.nomenclature[id],
          nomenclature: {
            id,
            name:     item.name,
            quantity: item.quantity || 0,
          },
        };
        return obj;
      }, {});
    case types.TOGGLE_ITEM:
      return state[action.id]
        ? _omit(state, action.id)
        : { ...state, [action.id]: action.item };
    case types.CHANGE_PARAM: {
      const { key, value, item } = action;
      const id = key[0];
      if (state[id] && value > 0) {
        return { ...state, ...setValue(state, key, value) };
      } else if (state[id] && value === 0) {
        return _omit(state, id);
      } else if (!state[id] && value > 0) {
        return { ...state, ...setValue({ [id]: item }, key, value) };
      }
      return state;
    }
    default:
      return state;
  }
};

const remainders = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.data;
    default:
      return state;
  }
};

const paging = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.paging;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.CHANGE_FILTER_PARAM:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
    case types.RESET:
      return { ...initialState.filters };
    default:
      return state;
  }
};

export default combineReducers({
  errors, // []
  pharmacy,
  selectedItems,
  paging, // {}
  filters, // {}
  isFiltersDebounce, // false
  remainders, // {}
});
