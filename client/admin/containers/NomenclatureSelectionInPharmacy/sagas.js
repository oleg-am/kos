import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

import { SELECT_ITEMS } from '../OrdersItem/constants';
import * as actions from './actions';


[SELECT_ITEMS].forEach((actionType) => {
  if (!actionType) {
    throw new Error(`Action type "${actionType}" is not define`);
  }
});

function* selectItems({ items }) {
  yield put(actions.selectItems(Object.values(items)));
}

function* rootSagas() {
  yield [
    takeEvery(SELECT_ITEMS, selectItems),
  ];
}

export default rootSagas;
