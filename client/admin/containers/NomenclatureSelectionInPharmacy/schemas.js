import { Schema, arrayOf } from 'normalizr';

const remainders = new Schema('remainders');
const stock = new Schema('stock');

remainders.define({
  stock,
});

const REMAINDERS_ARR = { data: arrayOf(remainders) };

export default REMAINDERS_ARR;
