import { createSelector, createStructuredSelector } from 'reselect';

// import _omit from 'lodash/omit';

import REDUCER from './constants';
import transformMinusValueToZero from '../../helpers/transformMinusValueToZero';

const pharmacy           = state => state[REDUCER].pharmacy;
const selectedItems      = state => state[REDUCER].selectedItems;

const remaindersState    = state => state[REDUCER].remainders;

const filtersState       = state => state[REDUCER].filters;

const paging             = state => state[REDUCER].paging;
const isFiltersDebounce  = state => state[REDUCER].isFiltersDebounce;
const errors             = state => state[REDUCER].errors;

const remainders = createSelector(
  remaindersState,
  items => items.map((item) => {
    const quantity = item.quantity ? transformMinusValueToZero(item.quantity) : 0;
    const stock = item.stock;
    // знаходимо максимальну дату строку годності
    const expiredAt = ([(item.expiredAt || 0), ((stock && stock.expiredAt) || 0)]
      .sort((a, b) => Date.parse(a) - Date.parse(b))
    )[1];

    return {
      ...item,
      quantity,
      expiredAt,
      id:              item.id || `remainders${item.nomenclature.id}`,
      stockRemainders: stock ? transformMinusValueToZero(stock.quantity) : '',
    };
  }),
);

export const filters = createSelector(filtersState, items => items);

export default createStructuredSelector({
  selectedItems,
  pharmacy,
  paging,
  isFiltersDebounce,
  errors,
  remainders,
  filters,
});
