import * as types from './constants';

export const loadData = (id) => (
  {type: types.LOAD_DATA, id}
)
export const saveData = (data) => (
  {type: types.SAVE_DATA, data}
)

export const addGoods = (orderId, goods) => {
  return {
    type: types.ADD_GOODS,
    orderId,
    goods,
  }
}
