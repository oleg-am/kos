import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router'

// import Row from '../../components/common/Row';.
import RowGoods from '../../components/common/RowGoods';

// import { push } from 'react-router-redux';
import { getOrderById, getGoodsTotal } from './selectors';
import { fromArrayToObj } from '../Orders/selectors';

import { loadData } from './actions';
import { changeCount, changeStatus, changeOrderParam } from '../Orders/actions';
import { getcountPharmacy, getCountStorage, getDeficit, getPrice, editableStatusies, getTotalSum, statusObj } from '../Orders/data';

import Breadcrumbs from '../../components/Breadcrumbs';
import Portlet from '../../components/Portlet/index';
import Radio from '../../components/Radio';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import {
  priceFormatter,
  dateFormatter,
  timeFormatter,
  dateAndTimeFormatter,
  statusFormatter,
  extraDataObjFormatter,
  formTextFormatter
} from '../../components/Formatters';

import {
  ControlLabel,
  FormControl,
  FormGroup,
  InputText,
  Select,
  Textarea,
} from '../../components/Metronic/Form';

import { Row, Col } from 'react-bootstrap';
// import Col from 'react-bootstrap/lib/Col';

class OrderInfo extends React.Component {
  constructor(props, context) {
    super(props, context);
    const { loadData, order } = this.props;
    this.state = {
      option: 1,
      classNames: {
        tableHeaderClassOrder: 'bg-blue bg-font-blue',
        pharmacy: '',
        client: '',
      }
    }

    // loadData(order);
  }

  setOption = (e) => {
    console.log(e.target.value);
    this.setState({
      option: e.target.value
    })
  }

  handlerCheck(orderId, productId, stock, count) {

    // console.log(orderId, productId, stock, count);
    this.props.changeCount(orderId, productId, stock, count);
  }
  changeOrderStatus(orderId) {
    const color = this.props.statusObj[13].color
    this.props.changeStatus(orderId, 13, color);
  }
  onChangeOrderParam = (orderId, param) => (e) => {
    // console.log('arguments: ', arguments);
    this.props.changeOrderParam(orderId, param, e.target.value);
  }
  checkPharmacy = (pharmacyId) => (e) => {
    if (pharmacyId == -1) {
      alert('Виберіть будь ласка аптеку');
      e.preventDefault();
      return false;
    }
  }
  render() {
    const { order, goods, pharmacies, goodsTotal, routeParams, editable, totalSum, statusObj, statuses } = this.props;
    const renderFieldGroup = ({controlLabelMd = 4, colMd = 8, label, ...props}) => {
      return (
        <FormGroup style={{margin: '0 -15px 5px'}}>
          <ControlLabel md={controlLabelMd}>{label}</ControlLabel>
          <Col md={colMd}>
            <FormControl {...props} />
          </Col>
        </FormGroup>
      )
    }
    return (

			<div className="page-content" style={{paddingTop: '20px'}}>
        <Breadcrumbs title="" components={{
          title: 'Інформація про замовлення' ,
          links: [
            {
              name: 'Меню',
              to: `/admin/reports`
            },
            {
              name: 'Замовлення',
              to: `/admin/orders`
            },
            {
              name: `Замовлення №${order.id}`
            }
          ]
        }}/>

        {/* <Portlet className="" components={{title: 'Замовлення'}}> */}

        <CustomTable tableHeaderClass="bg-blue bg-font-blue" trClassName="bold" tableClass="table table-striped table-bordered table-hover" data={[order]} striped={true} hover={true} >
          <HeaderColumn dataField="id"       width="5%"  dataAlign="center" dataSort={true} isKey={true}              >№ Замовлення</HeaderColumn>
          <HeaderColumn dataField="date"     width="10%" dataAlign="center" dataSort={true} dataFormat={dateFormatter}>Дата замовлення</HeaderColumn>
          <HeaderColumn dataField="time"     width="10%" dataAlign="center" dataSort={true} dataFormat={timeFormatter}>Час замовлення</HeaderColumn>
          <HeaderColumn dataField="operator" width="10%" dataAlign="center" dataSort={true}                           >Оператор</HeaderColumn>
          <HeaderColumn dataField="status"   width="10%" dataAlign="center" dataSort={true} dataFormat={statusFormatter} formatExtraData={statuses} >Статус</HeaderColumn>
          <HeaderColumn dataField="source"   width="10%" dataAlign="center" dataSort={true}>Джерело</HeaderColumn>
          <HeaderColumn dataField="comment"  width="45%" dataAlign="center" dataSort={true}
            dataFormat={editable
              ? (cell, row) => <InputText value={cell} onChange={this.onChangeOrderParam(row.id, 'comment')} />
              : (cell) => cell} >Коментар клієнта</HeaderColumn>
        </CustomTable>

        {/* </Portlet> */}

        {/* {goods.length > 0 && <h2>Товарная зона</h2>} */}

        <Row>
          <Col md={6} sm={6}>
            <Portlet className="blue-dark box" title={[
              <i key="1" className="fa fa-cogs"></i>, <span key="2">Аптека</span>
            ]}>
                <form role="form" className="form-horizontal">
                {console.log(Object.values(pharmacies))}
                  {renderFieldGroup({
                    isStatic:!editable,
                    label:"Аптека:",
                    'component': 'select',
                    value: order.pharmacy,

                    options: Object.values(pharmacies),
                    field: "name",
                    placeholder: "Оберіть аптеку",
                    onChange: this.onChangeOrderParam(order.id, 'pharmacy'),
                  })}
                  {renderFieldGroup({
                    isStatic: true,
                    label:"Дата та час доставки:",
                    type:"text",
                    value: dateFormatter(order.date) + ' ' + timeFormatter(order.time),
                  })}
                  {renderFieldGroup({
                    isStatic:!editable,
                    label:"Коментар оператора:",
                    type:"text",
                    value: order.comment,
                    onChange: this.onChangeOrderParam(order.id, 'comment'),
                  })}

                  {/* <FieldGroup key="111"
                    isStatic={!editable}
                    label="Аптека:"
                    componentClass="select"
                    value={order.pharmacy}
                    options={pharmacies}
                    field="name"
                    placeholder="Оберіть аптеку"
                    onChange={this.onChangeOrderParam(order.id, 'pharmacy')}
                  />
                  <FieldGroup key="1112"
                    isStatic
                    label="Дата та час доставки:"
                    value={dateFormatter(order.date) + ' ' + timeFormatter(order.time)}
                  />
                  <FieldGroup key="1141"
                    isStatic={!editable}
                    label="Коментар оператора:"
                    type="text"
                    value={order.comment}
                    onChange={this.onChangeOrderParam(order.id, 'comment')}
                  /> */}
                </form>
            </Portlet>
          </Col>
          <Col md={6} sm={6}>
            <div className="portlet blue-hoki box">
                <div className="portlet-title">
                    <div className="caption">
                        <i className="fa fa-cogs"></i>Клієнт </div>
                    {/* <div className="actions">
                        <a href="javascript:;" className="btn btn-default btn-sm">
                            <i className="fa fa-pencil"></i> Edit </a>
                    </div> */}
                </div>
                <div className="portlet-body">
                  <form role="form" className="form-horizontal">
                    {renderFieldGroup({
                      controlLabelMd: 2,
                      colMd: 10,
                      isStatic:!editable,
                      label:"ПІБ:",
                      type:"text",
                      value: order.fullName,
                      onChange: this.onChangeOrderParam(order.id, 'fullName'),
                    })}
                    {renderFieldGroup({
                      controlLabelMd: 2,
                      colMd: 10,
                      isStatic:!editable,
                      label:"Телефон:",
                      type:"text",
                      value: order.phone,
                      onChange: this.onChangeOrderParam(order.id, 'phone'),
                    })}
                    {renderFieldGroup({
                      controlLabelMd: 2,
                      colMd: 10,
                      isStatic:!editable,
                      label:"E-mail:",
                      type:"text",
                      value: order.email,
                      onChange: this.onChangeOrderParam(order.id, 'email'),
                    })}
                    {/*<FieldGroup key="111fs"
                      isStatic={!editable}
                      label="E-mail:"
                      type="text"
                      value={order.email}
                      onChange={this.onChangeOrderParam(order.id, 'email')}
                    /> */}
                  </form>
                </div>
            </div>
          </Col>
        </Row>

        <Portlet>

				{goods.length > 0 && <div className="table-scrollable tr-center th-middle">
					<table className="table table-hover">
						<colgroup>
							<col width="40%" />
							<col width="12%" />
							<col width="12%" />
							<col width="12%" />
							<col width="12%" />
							<col width="12%" />

						</colgroup>
						<thead className="bg-blue bg-font-blue">
							<tr>
								<th rowSpan="2"> Найменування </th>
								<th rowSpan="2"> Ціна </th>
								<th colSpan="3"> Кількість </th>
                {/* <th> Кількість Аптека </th> */}
								{/* <th> Кількість Склад </th> */}
								<th rowSpan="2"> Вартість </th>
								<th colSpan="2"> Залишок </th>
								{/* <th> Залишок на складі </th> */}
								<th rowSpan="2"> Підбір аналога </th>
							</tr>
							<tr>
								{/* <th> Найменування </th> */}
								{/* <th> Ціна </th> */}
								<th> Замовлення </th>
								<th> Аптека </th>
								<th> Склад </th>
								{/* <th> Вартість </th> */}
								<th> Аптека </th>
								<th> Склад </th>
								{/* <th> Підбір аналога </th> */}
							</tr>
						</thead>
						<tbody>
              {goods.map((item) => {
								return <RowGoods
                  key={item.id}
                  data={item}
                  editable={editable}
                  handlerCheckCountQuantity={this.handlerCheck.bind(this, order.id, item.id, 'quantity')}
                  handlerCheckCountPharmacy={this.handlerCheck.bind(this, order.id, item.id, 'countPharmacy')}
                  handlerCheckCountStock={this.handlerCheck.bind(this, order.id, item.id, 'countStorage')}
                />
							})}
              <RowGoods style={{fontWeight: 'bold'}} className={'success'} key={''} data={goodsTotal} />
						</tbody>
					</table>
				</div>}
        </Portlet>
        <div className={'row'}>
          <div className={'col-md-12'}>
            {editable ?
              (<div>
                {console.log('link order', order)}
                {console.log('link order.pharmacy', order.pharmacy)}
                <Link onClick={this.checkPharmacy(order.pharmacy)} disabled={order.pharmacy == -1} className="btn btn-sm blue-madison" to={`/admin/orders/${order.id}/goods-selection-in-pharmacy/${order.pharmacy}`}>
                  + Додати товари
                </Link>
                <Link onClick={this.changeOrderStatus.bind(this, routeParams.id)} className="pull-right btn btn-sm green-meadow" to={`/admin/orders`}>
                  Відправити замовлення
                </Link>
              </div>)
              :
              (<Link className="pull-right btn btn-sm red btn-outline" to={`/admin/orders`}>
                <i className="fa fa-close"></i> Закрити
              </Link>)


            }
          </div>

        </div>
			</div>
    );
  }
}


function mapStateToProps(state, ownProps) {
  console.log('ownProps: ', ownProps);
  // console.log(getOrderById(state.orders.data, ownProps.params.id));
  // const order = getOrderById(state.orders.data, ownProps.params.id);
  const { orders, goods: goodsObj, pharmacy: pharmaciesObj } = state.orders.entities;
  console.log('pharmaciesObj:', pharmaciesObj);
  console.log('orders[ownProps.params.id].pharmacy: ', orders[ownProps.params.id].pharmacy);
  console.log('state.orders.entities.pharmacy[orders[ownProps.params.id].pharmacy]', state.orders.entities.pharmacy[orders[ownProps.params.id].pharmacy]);
  const order = {
    ...orders[ownProps.params.id],
    // pharmacy: pharmaciesObj[orders[ownProps.params.id].pharmacy].name;
  };
  // const pharmacies = Object.keys(pharmaciesObj).map(pharmacyId => {
  //   return pharmaciesObj[pharmacyId];
  // })
  // console.log('pharmacies:', pharmacies);
  // const goods = order.goods.map(id => ({
  //   ...goodsObj[id],
  // }));
  // console.log();
  const goods = order.goods.map(id => {
    const product = goodsObj[id];
    console.log('product: ', product);
    console.log('product.customStockChange: ', product.customStockChange);
    const countPharmacy = product.customStockChange == 'countPharmacy' ? product.countPharmacy : getcountPharmacy(+product.quantity, +product.pharmacyStock);
    const countStorage = product.customStockChange == 'countStorage' ? product.countStorage : getCountStorage(+product.quantity, +product.storageStock, +countPharmacy);
    // console.group('deficit')
    // console.log('countPharmacy: ', +countPharmacy)
    // console.log('countStorage: ', +countStorage)
    // console.log('product.quantity: ', +product.quantity)
    // console.log('countDeficit: ', +countPharmacy + +countStorage - +product.quantity)
    // console.groupEnd()
    return {
      ...product,
      countPharmacy,
      countStorage,
      deficit: getDeficit(+countPharmacy, +countStorage, +product.quantity),
      price: getPrice(+product.reservePrice, +product.quantity),
      countDeficit:  +countPharmacy + +countStorage - +product.quantity,
      pharmacyDeficit: +product.pharmacyStock - +countPharmacy,
      stockDeficit: +product.storageStock - +countStorage,
    }
  });
  let goodsTotal = getGoodsTotal(goods);
  goodsTotal = {
    ...goodsTotal,
    deficit: getDeficit(+goodsTotal.countPharmacy, +goodsTotal.countStorage, +goodsTotal.quantity),
    countDeficit:  +goodsTotal.countPharmacy + goodsTotal.countStorage - +goodsTotal.quantity,
    pharmacyDeficit: +goodsTotal.pharmacyStock - +goodsTotal.countPharmacy,
    stockDeficit: +goodsTotal.storageStock - +goodsTotal.countStorage,
  }
  console.log('goodsTotal: ', goodsTotal);
  const totalSum = getTotalSum(goods).toFixed(2);
  const editable = !!(editableStatusies.indexOf(order.status) != -1);
  console.log('order info editable: ', editable);
    return {
        order,
        goods,
        pharmacies: pharmaciesObj ,
        goodsTotal,
        editable,
        totalSum
        // activeOrder: state.orderInfo.activeOrder,
        // pageNum: state.orders.data.length/5,
        // paging: state.nomenclature.paging || {}
    };
}

function mapDispatchToProps(dispatch) {
  console.log('fromArrayToObj(statusObj): ', fromArrayToObj(statusObj));
    return {
      loadData: bindActionCreators(loadData, dispatch),
      changeCount: bindActionCreators(changeCount, dispatch),
      changeStatus: bindActionCreators(changeStatus, dispatch),
      changeOrderParam: bindActionCreators(changeOrderParam, dispatch),
      statusObj: fromArrayToObj(statusObj),
      statuses: fromArrayToObj(statusObj, 'name'),
        // actions: bindActionCreators(dictionaryActions, dispatch),
        // onChangeValue: bindActionCreators(onChangeValue, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderInfo);
