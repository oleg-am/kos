// import * as types from '../actions/actionTypes';
import * as types from './constants';

import faker from 'faker';

const initialState = {
  activeOrder: {}

}

export default function orderInfoReducer(state = initialState, action) {
    switch (action.type) {
        case types.LOAD_DATA:
        const data = {...action.data }
            return {...state, activeOrder: data};
        default:
            return state;
    }
}
