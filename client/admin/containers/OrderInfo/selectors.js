export const getOrderById = (data, id) => {
  const order = data.filter(order => {
    return order.id == id;
  })
  return order[0];
}

export const getGoodsTotal = (goods) => {
  let obj = {};
  console.log('goods: ', goods);
  Object.keys(goods[0]||{}).forEach(key => {
    if (['reservePrice', 'quantity', 'price', 'pharmacyStock', 'storageStock',
        'countPharmacy', 'countStorage'].indexOf(key) != -1) {
      obj[key] = goods.reduce((prev, next) => (+prev + (+next[key] || 0) ), 0)
    } else {
      obj[key] = key == 'name' ? 'Итого': '';
    }

  })
  return obj;
}
