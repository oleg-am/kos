import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';

import { CustomTable, HeaderColumn } from '../../../components/CustomTable';

import './OrderTable.css';

class OrderTable extends Component {
  static propTypes = {
    data:                   PropTypes.arrayOf(PropTypes.object),
    onEdit:                 PropTypes.func,
    dateFormatter:          PropTypes.object,
    timeFormatter:          PropTypes.object,
    filters:                PropTypes.objectOf(PropTypes.any),
    setFilter:              PropTypes.func.isRequired,
    countdownTimerOrStatus: PropTypes.any,
  }

  state = {
    hover: '',
  }

  toggleHover = id => () => this.setState({ hover: id })

  render() {
    const {
      data,
      onEdit,
      dateFormatter,
      timeFormatter,
      filters,
      setFilter,
      countdownTimerOrStatus,
    } = this.props;
    return (
      <CustomTable
        tableClass="table table-striped table-bordered table-hover width-100"
        data={data}
        striped
        hover
        pagination
        filters
        trClassName={row => row.pharmacy.isStock && 'bg-yellow-mint'}
        //onRowClick={this.onEdit}
        onDblRowClick={onEdit}
      >
        <HeaderColumn width="5%" dataField="id" dataAlign="center" isKey> № </HeaderColumn>
        <HeaderColumn width="10%" dataField="createdAt" dataAlign="center" dataSort dataFormat={dateFormatter}>Дата <br /> замовлення</HeaderColumn>
        <HeaderColumn width="10%" dataField="createdAt" dataAlign="center" dataSort dataFormat={timeFormatter}>Час <br /> замовлення</HeaderColumn>

        <HeaderColumn
          width="25%"
          dataField="pharmacy"
          headerAlign="center"
          // KSM-166
          dataFormat={(pharmacy, { id, isSyncProblem, noSyncInterval }) => (
            isSyncProblem
              ? <div
                className="sync-problem"
                onMouseEnter={this.toggleHover(id)}
                onMouseLeave={this.toggleHover('')}
              >
                {pharmacy.name}
                {this.state.hover === id && <div className="sync-problem--hover">
                  <div>{'Немає зв\'язку'}</div>
                  {/* <br /> */}
                  <div>{noSyncInterval ? `${(noSyncInterval / 60 / 60).toFixed()} хв` : '' }</div>
                </div>}
              </div>
              : pharmacy.name
          )}
          className={(cell, { id, isSyncProblem }) => (isSyncProblem ?
            classNames('font-default', this.state.hover === id ? 'sync-problem-bg-opacity' : 'bg-red-pink')
            : ''
          )}
          filters={
            (data.length > 0 || filters.filters['pharmacy.name'].value) &&
              <input
                type="text"
                className="form-control" placeholder="Фільтр"
                value={filters.filters['pharmacy.name'].value}
                onChange={setFilter(['filters', 'pharmacy.name'], true)}
              />

          }
        > Аптека(и) </HeaderColumn>

        <HeaderColumn width="10%" dataField="totalPrice" headerAlign="center" dataSort >Сума</HeaderColumn>
        <HeaderColumn width="10%" dataField="client" headerAlign="center" dataSort dataFormat={c => c.name} > Клієнт ПІБ</HeaderColumn>
        <HeaderColumn width="10%" dataField="client" dataAlign="center" dataSort columnTitle dataFormat={c => c.tel} > Конт.тел</HeaderColumn>
        <HeaderColumn width="10%" dataField="client" dataAlign="center" dataSort columnTitle dataFormat={c => c.email} > E-mail</HeaderColumn>
        <HeaderColumn width="10%" dataField="operator" dataAlign="center" dataSort columnTitle > Оператор</HeaderColumn>
        {/* <HeaderColumn width="10%" dataField="status" dataAlign="center" dataSort >Статус</HeaderColumn> */}
        <HeaderColumn width="10%" dataField="status" dataAlign="center" dataSort
          dataFormat={countdownTimerOrStatus}
          // filters={
          //   <Select
          //     name="form-field-name"
          //     placeholder="Фільтр по бренду"
          //     // value={{value: 0, label: '22222222' }}
          //     value={filterManufacturers.filters.name.value}
          //     options={filterManufacturers.filters.name.value ? manufacturersOptions : [{ label: 'Введіть назву бренду', disabled: true }]}
          //     onInputChange={this.setFilterManufacturers(['filters', 'name'], true)}
          //     onChange={this.setFilterSelect(['filters', 'status'])}
          //     // onOpen={this.loadManufacturers}
          //     noResultsText="Нічого не знайдено"
          //     clearValueText="Стерти"
          //     isLoading={isLoadingManufacturer}
          //     clearable
          //     onInputKeyDown={this.setFilterAfterESC(['filters', 'manufacturer'])}
          //     style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0 }}
          //   />
          // }

          // filters={
          //
          // }
        >Статус</HeaderColumn>
        <HeaderColumn width="10%" dataField="internetGround" dataAlign="center" dataSort dataFormat={c => c && c.name} columnTitle > Джерело</HeaderColumn>
        <HeaderColumn width="100px" dataFormat={c => c && c.substring(0, 10)} dataField="clientComment" headerAlign="center" dataSort columnTitle > Коментар <br /> клієнта </HeaderColumn>
        <HeaderColumn width="100px" dataFormat={c => c && c.substring(0, 10)} dataField="operatorComment" headerAlign="center" dataSort columnTitle > Коментар <br /> оператора </HeaderColumn>

      </CustomTable>
    );
  }
}
export default OrderTable;
