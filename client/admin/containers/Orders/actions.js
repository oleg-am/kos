import * as types from './constants';

import { createParams } from '../../helpers/api';
import { ORDERS_ARR } from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const resetFilterTel = () => ({
  type: types.RESET_FILTER_TEL,
});

export const setFilterStatuses = (value, ordersStatus) => ({
  type: types.SET_FILTER_STATUSES, value, ordersStatus,
});

export const edit = nomenclature => ({
  type: types.EDIT,
  nomenclature,
});

export const changeShownOrdersStatus = status => ({
  type: types.CHANGE_SHOWN_ORDERS_STATUS,
  status,
});

export const load = (filters, ordersStatus) => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/orders${createParams(filters)}`),
  schema:  ORDERS_ARR,
  ordersStatus,
});

export const takeOrder = () => ({
  types: [
    types.LOAD_TAKE_ORDER_REQUEST,
    types.LOAD_TAKE_ORDER_SUCCESS,
    types.LOAD_TAKE_ORDER_FAILURE,
  ],
  promise: api => api.post('/api/admin/take_order'),
});

export const takeOrderInfo = () => ({
  types: [
    types.LOAD_TAKE_ORDER_INFO_REQUEST,
    types.LOAD_TAKE_ORDER_INFO_SUCCESS,
    types.LOAD_TAKE_ORDER_INFO_FAILURE,
  ],
  promise: api => api.post('/api/admin/take_order?info'),
});
