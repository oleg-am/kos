const name = 'orders';

export const LOAD_REQUEST = `${name}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${name}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${name}/LOAD_FAILURE`;

export const LOAD_TAKE_ORDER_REQUEST = `${name}/LOAD_TAKE_ORDER_REQUEST`;
export const LOAD_TAKE_ORDER_SUCCESS = `${name}/LOAD_TAKE_ORDER_SUCCESS`;
export const LOAD_TAKE_ORDER_FAILURE = `${name}/LOAD_TAKE_ORDER_FAILURE`;

export const LOAD_TAKE_ORDER_INFO_REQUEST = `${name}/LOAD_TAKE_ORDER_INFO_REQUEST`;
export const LOAD_TAKE_ORDER_INFO_SUCCESS = `${name}/LOAD_TAKE_ORDER_INFO_SUCCESS`;
export const LOAD_TAKE_ORDER_INFO_FAILURE = `${name}/LOAD_TAKE_ORDER_INFO_FAILURE`;

export const SET_FILTER = `${name}/SET_FILTER`;
export const RESET_FILTER_TEL = `${name}/RESET_FILTER_TEL`;
export const SET_FILTER_STATUSES = `${name}/SET_FILTER_STATUSES`;
export const EDIT = `${name}/EDIT`;

export const CHANGE_SHOWN_ORDERS_STATUS = `${name}/CHANGE_SHOWN_ORDERS_STATUS`;

export default name;
