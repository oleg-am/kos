import faker from 'faker';
import moment from '../../helpers/moment';

const orderDefaultShema = {
  id: '', //order
  date: '',
  time: '',
  operator: '',
  source: '',
  comment: '',
  status: {
    id: '',
    name: '',
  },
  client: {},
  goods: [],
  pharmacy: {
    id: '',
    name: '',
  }

}

export const statusObj = [
  {id: 10, name: 'Створено', color: 'label-green-turquoise'}, // (зелений)
  {id: 11, name: 'Отримано', color:  'label-green-turquoise'}, // (зелений)
  {id: 12, name: 'Обробляється Оператором', color:  'label-grey-steel'}, //(сірий)
  {id: 13, name: 'Опрацьовано Оператором', color:  'label-grey-steel'}, // (сірий)
  {id: 14, name: 'Отримано для обробки в аптеці', color:  'label-grey-steel'},// (сірий)
  {id: 15, name: 'Отримано для обробки на складі', color:  'label-grey-steel'},// (сірий)
  {id: 16, name: 'Обробляється в аптеці', color:  'label-grey-steel'},// (сірий)
  {id: 17, name: 'Відправлений на редагування', color:  'label-warning'},// (жовтий)
  {id: 18, name: 'Обробляється Call-центром після відправки на редагування', color:  'label-grey-steel'},// (сірий)
  {id: 19, name: 'Опрацьовано Call-центром після відправки на редагування', color:  'label-grey-steel'},// (сірий)
  {id: 20, name: 'Опрацьовано на складі  (Вн-розхід)', color:  'label-grey-steel'},// (сірий)
  {id: 21, name: 'Опрацьовано в аптеці  (Резерв)', color:  'label-grey-steel'},// (сірий)
  {id: 22, name: 'Продаж', color:  'label-grey-steel'},// (сірий)
  {id: 23, name: 'Відмова', color:  'label-grey-steel'},// (сірий)
]

export const editableStatusies = [10, 11, 17];

export const getShemaOrder = () => {
  let date = new Date();
  return {
    id: 10000, //faker.random.number(),
    // id: faker.random.uuid(),
    date: date,//faker.date.future(),
    time: date,//faker.date.past(),
    pharmacy: -1,
    sum: 0,
    fullName: '',
    phone: '',
    email: '',
    operator: 'Оператор 1',
    status: 10,
    statusColor: statusObj[0].color,
    source: '',
    comment: '',
    goods: [],

  }
}
export const getRandomOrders = (count, startDate, endDate) => {

  let endStartDate = new Date(endDate.format('YYYY-MM-DD'))
  let arr = [];
  let date1 = new Date(endStartDate);
  Array.from({ length: endDate.diff(startDate, 'days')+1 }, (v1, k1) => k1).forEach((_, i1)=>{
    let date = new Date();
    date.setDate(date1.getDate() - 1 * i1);//(count - i)
    Array.from({ length: count }, (v2, k2) => k2).forEach((_, i2) => {
      let time = new Date();
      time.setMinutes(time.getMinutes() - 10 * i2);//(count - i)
      const goods = getRandomGoods(faker.finance.amount(1, 5), i1*10+i2);
      const sum = getTotalSum(goods).toFixed(2);
      const status = faker.random.arrayElement(statusObj);
      arr.push( {
        id: 1000+i1*100 +i2*10, //faker.random.number(),
        // id: faker.random.uuid(),
        date,//faker.date.future(),
        time,//faker.date.past(),
        pharmacy: {
          id: 100+i1*10+i2,
          name: faker.company.companyName(),
        },
        sum,
        fullName: `${faker.name.firstName()} ${faker.name.lastName()}`,
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
        operator: `${faker.name.firstName()} ${faker.name.lastName()}`,
        status: status.id,
        statusColor: status.color,
        source: faker.internet.url(),
        comment: faker.lorem.sentence(5, 100),
        goods,
        // goodsTotal: getGoodsTotal(goods),
      })
    }
  )

  })
  return arr;
}
export const getRandomGoods = (count, indx = 0) => {

  return Array.from({ length: count }, (v, k) => indx*10000+9000+k).map((id) => {
    const quantity = +faker.finance.amount(1, 20);
    const pharmacyStock = faker.finance.amount(0, 10);
    const storageStock = faker.finance.amount(0, 10);

    // const countPharmacy = getcountPharmacy(quantity, pharmacyStock);
    // const countStorage = getCountStorage(quantity, storageStock, countPharmacy);
    // const deficit = getDeficit(countPharmacy, countStorage, quantity);

    return {
      // id: faker.random.number(),
      id,
      name: faker.commerce.productName(),
      reservePrice: faker.finance.amount(20, 300),
      quantity,
      // countPharmacy,
      // countStorage,
        // deficit,
      // price: faker.finance.amount(100, 1000),
      pharmacyStock,
      storageStock,
    }
  }
)}


export const getcountPharmacy = (quantity, pharmacyStock) => {
  const result=(+quantity <= +pharmacyStock  ? +quantity : +pharmacyStock)
  return result;
};
export const getCountStorage = (quantity, storageStock, countPharmacy) => {
  if (quantity == countPharmacy) {
    return 0;
  } else {
    const targetCount = +quantity - +countPharmacy;
    const result = +targetCount <= +storageStock ? +targetCount : +storageStock;
    return result > 0 ? result : 0;
  }
}
export const getDeficit = (countPharmacy, countStorage, quantity) => {
    const result = countPharmacy + countStorage - quantity;
    return result;
    // return result > 0 ? 0 : result;
  }
export const getPrice = (reservePrice, quantity) => {
    const result = reservePrice * quantity;
    return result.toFixed(2) || 0;
    // return result > 0 ? 0 : result;
  }
  export const getTotalSum =(goods) => (
    goods.reduce((sum, next) => (
      sum + (next.reservePrice * next.quantity)
    ), 0))
