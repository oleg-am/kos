import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

import { Link, browserHistory } from 'react-router';
// import { push } from 'react-router-redux';

import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
import Select from 'react-select';

import _debounce from 'lodash/debounce';
import { DateField, Calendar } from 'react-date-picker';

import 'csshake/dist/csshake.min.css';

import moment from '../../helpers/moment';
import * as actions from './actions';
import selectors from './selectors';
import OrderTable from './Components/OrderTable';

// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';
import Paginate from '../../components/Paginate';

// import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import LimitSelect from '../../components/LimitSelect';
import CountdownTimer from '../../components/CountdownTimer';

// import IconCheck from '../../elements/IconCheck';
// import IconStatus from '../../elements/IconStatus';
// import LinkSearch from '../../elements/LinkSearch';

import {
  // priceFormatter,
  dateFormatter,
  timeFormatter,
  statusFormatter,
} from '../../components/Formatters';

const countdownTimerOrStatus = (status, row) => (
  row.pauseTimeLeft
    ? <CountdownTimer seconds={row.pauseTimeLeft} />
    : statusFormatter(status)
);
const currentDate = moment();
const searchButtonStyle = {
  // boxShadow:               'none',
  borderTopLeftRadius:     '0px',
  borderTopRightRadius:    '4px',
  borderBottomLeftRadius:  '0px',
  borderBottomRightRadius: '4px',
  // height:                  '34px',
  borderLeft:              '0px',
  width:                   'inherit',
};

const placeholderSeachInput = {
  'client.tel': 'Введiть телефон',
};

class Orders extends Component {
  static propTypes = {
    isAvailableOrders:        PropTypes.bool,
    ordersStatus:             PropTypes.oneOf(['active', 'ended']),
    statuses:                 PropTypes.array.isRequired,
    errors:                   PropTypes.arrayOf(PropTypes.object),
    orders:                   PropTypes.arrayOf(PropTypes.object),
    // activeOrders:           PropTypes.arrayOf(PropTypes.object),
    // endedOrders:            PropTypes.arrayOf(PropTypes.object),
    paging:                   PropTypes.objectOf(PropTypes.any).isRequired,
    filters:                  PropTypes.objectOf(PropTypes.any).isRequired,
    sAutoReloadInterval:      PropTypes.number,
    ordersAutoReloadEnable:   PropTypes.bool.isRequired,
    ordersAutoReloadInterval: PropTypes.string.isRequired,
    isEditableRoleAccess:     PropTypes.bool,
    load:                     PropTypes.func.isRequired,
    // loadStatuses:             PropTypes.func,
    resetFilterTel:           PropTypes.func,
    setFilter:                PropTypes.func.isRequired,
    setFilterStatuses:        PropTypes.func.isRequired,
    takeOrder:                PropTypes.func.isRequired,
    takeOrderInfo:            PropTypes.func.isRequired,
    statusesNames:            PropTypes.any.isRequired,
    changeShownOrdersStatus:  PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const {
      load,
      filters,
      ordersStatus,
      takeOrderInfo,
      ordersAutoReloadEnable,
      ordersAutoReloadInterval,
    } = this.props;

    this.debounceLoad = _debounce(load, 400);
    this.setIntervalRefreshOrders = this.setIntervalRefresh(this.refresOrders);
    this.setIntervalRefreshTakeOrderInfo = this.setIntervalRefresh(takeOrderInfo);

    if (ordersAutoReloadEnable) {
      takeOrderInfo();

      this.idAutoReloadIntervalRefreshOrders =
        this.setIntervalRefreshOrders(ordersAutoReloadInterval);
      this.idAutoReloadIntervalRefreshTakeOrderInfo =
        this.setIntervalRefreshTakeOrderInfo(ordersAutoReloadInterval);
    }
    load(filters, ordersStatus);
  }

  state = {
    searchOption: '',
    isOpenSearch: false,
  };

  componentWillReceiveProps(next) {
    const { load, filters, ordersAutoReloadEnable, ordersAutoReloadInterval } = this.props;
    const {
      isFiltersDebounce,
      ordersStatus,
      filters: nextFilters,
      ordersAutoReloadEnable: nextOrdersAutoReloadEnable,
      ordersAutoReloadInterval: nextOrdersAutoReloadInterval,
      takeOrderInfo,
    } = next;

    if (!ordersAutoReloadEnable && nextOrdersAutoReloadEnable) {
      takeOrderInfo();
    }

    if (nextOrdersAutoReloadEnable && ordersAutoReloadInterval !== nextOrdersAutoReloadInterval) {
      this.idAutoReloadIntervalRefreshOrders =
        this.setIntervalRefreshOrders(ordersAutoReloadInterval);
      this.idAutoReloadIntervalRefreshTakeOrderInfo =
        this.setIntervalRefreshTakeOrderInfo(ordersAutoReloadInterval);
    } else if (ordersAutoReloadEnable && !nextOrdersAutoReloadEnable) {
      clearInterval(this.idAutoReloadIntervalRefreshOrders);
      clearInterval(this.idAutoReloadIntervalRefreshTakeOrderInfo);
    }

    if (filters !== nextFilters) {
      const newfilters = filters.page === nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters;

      isFiltersDebounce
      ? this.debounceLoad(newfilters, ordersStatus)
      : load(newfilters, ordersStatus);
    }
  }

  componentWillUnmount() {
    this.idAutoReloadIntervalRefreshOrders &&
      clearInterval(this.idAutoReloadIntervalRefreshOrders);
    this.idAutoReloadIntervalRefreshTakeOrderInfo &&
      clearInterval(this.idAutoReloadIntervalRefreshTakeOrderInfo);
  }

  onEdit = (order) => {
    browserHistory.push(`/admin/orders/${order.id || ''}`);
  }
  setFilterStatuses = (arr) => {
    const value = arr.map(item => item.value).join(',');
    this.props.setFilterStatuses(value, this.props.ordersStatus);
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }

  setFilterAdvance = (key, isFiltersDebounce) => (e) => {
    let val = '';
    if (e.target) {
      if (e.target.value && e.target.type !== 'checkbox') {
        val = e.target.value;
      } else if (e.target.type === 'checkbox') {
        val = !e.target.checked;
      }
    } else {
      val = moment(e, 'DD.MM.YYYY').format(`YYYY-MM-DD ${key[2] === 'from' ? 'HH:mm:ss' : '23:59:59'}`);
    }
    this.props.setFilter(key, val, isFiltersDebounce);
  }

  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  setIntervalRefresh = action => time => (
    setInterval(() => {
      action();
    }, time * 1000)
  )
  refresOrders = () => {
    const { load, filters, ordersStatus } = this.props;
    load(filters, ordersStatus);
  }
  handleLinkToOrder = () => {
    this.props.isAvailableOrders && this.props.takeOrder();
  }
  changeShownOrdersStatus = status => () => {
    this.props.changeShownOrdersStatus(status);
  }
  fieldSearch = () => {
    const { isOpenSearch } = this.state;
    const { resetFilterTel } = this.props;
    isOpenSearch && resetFilterTel();
    this.setState({
      isOpenSearch: !isOpenSearch,
    });
  }
  searchOption = (e) => {
    const { resetFilterTel, filters } = this.props;
    this.setState({ searchOption: e.target.value });
    filters.filters['client.tel'].value && resetFilterTel();
  };
  render() {
    const {
      ordersStatus,
      errors,
      orders,
      paging,
      filters,
      statuses,
      statusesNames,
      filterDateFrom,
      isEditableRoleAccess,
      ordersAutoReloadEnable,
      isAvailableOrders,
    } = this.props;
    const { searchOption, isOpenSearch } = this.state;

    // console.log({ 'this.props': this.props });

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          components={{
            title: 'Замовлення',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Замовлення',
              },
            ],
          }}
        />

        <Portlet
          actionsLeft={[
            <div key={1}>
              <div key={2} className="input-group">
                <LimitSelect
                  key="actionsLeft_1"
                  optionsValue={[10, 15, 20]}
                  value={filters.limit}
                  onChange={this.setFilter('limit')}
                />
                {/*
                  Закоментил так как фильтр нужен только по 1 полю. (10,02,2017)
                {isOpenSearch &&
                  <select
                    key="actionsLeft_3"
                    value={searchOption}
                    style={{ width: 'inherit', borderLeft: '0px' }}
                    onChange={this.searchOption}
                    className="form-control"
                  >
                    <option key={'default'} disabled value="" >Оберiть фiльтр</option>
                    <option key={1} value="client.tel" >Телефон</option>
                    </select>
                } */}
                { isOpenSearch &&
                  // searchOption.length > 0 &&
                    <input
                      key="actionsLeft_2"
                      style={{ width: 'inherit', borderLeft: '0px' }}
                      type="text"
                    // value={filters.filters[searchOption].value} (10,02,2017)
                      value={filters.filters['client.tel'].value}
                    //  onChange={this.setFilter(['filters', searchOption], true)} (10,02,2017)
                      onChange={this.setFilter(['filters', 'client.tel'], true)}
                      className="form-control"
                    //  placeholder={placeholderSeachInput[searchOption]} (10,02,2017)
                      placeholder={placeholderSeachInput['client.tel']}
                    />
                }
                <button
                  key={3}
                  style={searchButtonStyle}
                  onClick={this.fieldSearch}
                  type="button"
                  className="form-control btn btn-default"
                  title={isOpenSearch ? 'Закрити пошук' : 'Вiдкрити пошук'}
                ><i className={isOpenSearch ? 'fa fa-times' : 'fa fa-search'} aria-hidden="true" />
                </button>
                <div
                  style={{
                    marginLeft: '5px',
                    float: 'left',
                  }}
                >
                  <span> Перiод вiдображення: </span>
                  <DateField
                    dateFormat="DD.MM.YYYY"
                    maxDate={currentDate}
                    forceValidDate
                    updateOnDateClick
                    collapseOnDateClick

                    defaultValue={filterDateFrom}
                    showClock={false}
                  >
                    <Calendar
                      navigation
                      locale="uk"
                      forceValidDate
                      highlightWeekends
                      highlightToday
                      weekNumbers
                      weekStartDay={1}
                      footer={false}
                      onChange={this.setFilterAdvance(['filters', 'updatedAt', 'from'])}
                    />
                  </DateField>
                  <span> - </span>
                  <DateField
                    dateFormat="DD.MM.YYYY"
                    maxDate={currentDate}
                    minDate={filterDateFrom}
                    forceValidDate
                    updateOnDateClick
                    collapseOnDateClick
                    defaultValue={currentDate}
                    showClock={false}
                  ><Calendar
                    navigation
                    locale="uk"
                    forceValidDate
                    highlightWeekends
                    highlightToday
                    weekNumbers
                    weekStartDay={1}
                    footer={false}
                    onChange={this.setFilterAdvance(['filters', 'updatedAt', 'to'])}
                  />
                  </DateField>
                  <label
                    htmlFor="isShowMyOrders"
                    className="mt-checkbox mt-checkbox-outline "
                    style={{ paddingTop: '7px', margin: '0 0 0 20px' }}
                  >
                    Показувати свої замовлення
                    <input
                      id="isShowMyOrders"
                      type="checkbox"
                      checked={!filters.all_orders}
                      onChange={this.setFilterAdvance('all_orders')}
                    />
                    <span className="checkbox" />
                  </label>
                </div>
              </div>
            </div>,
          ]}

          actionsRight={[
            isEditableRoleAccess &&
              <Link
                key="actionsRight_1"
                disabled={!isAvailableOrders}
                onClick={this.handleLinkToOrder}
                className={classNames('btn btn-sm green-meadow', {
                  'shake-slow shake-constant shake-constant--hover':
                    ordersAutoReloadEnable && isAvailableOrders,
                })}
                style={{ marginRight: '10px' }}
              >Взяти замовлення
              </Link>,
            isEditableRoleAccess &&
              <Link
                key="actionsRight_2"
                className="btn btn-sm green-meadow"
                style={{ marginRight: '10px' }}
                to={{
                  state: {
                    isReset: true,
                  },
                  pathname: '/admin/orders/create',
                }}
              >
                Створити замовлення
              </Link>,
            <Link key="actionsRight_3" onClick={this.refresOrders} className="btn btn-sm blue-sharp">
              <i className="fa fa-refresh" title="Оновити" />
            </Link>,
          ]}
        >
          <div className="portlet-body" style={{ marginTop: '10px' }}>
            <div className="tabbable-custom ">
              <ul className="nav nav-tabs">
                {['active', 'ended'].map(status =>
                  <li
                    key={`ordersStatus_${status}`}
                    className={classNames({ active: ordersStatus === status })}
                    onClick={this.changeShownOrdersStatus(status)}
                  >
                    <a data-toggle="tab" aria-expanded="false">
                      {
                        (status === 'active' && 'Активні') ||
                        (status === 'ended' && 'Завершені')
                      }
                    </a>
                  </li>,
                )}
              </ul>
              <div className="tab-content">
                <div className="tab-pane active" id="active">
                  <div key="actionsLeft_4">
                    <Select
                      placeholder="Фільтр по статусу"
                      noResultsText="Нічого не знайдено"
                      multi
                      onChange={this.setFilterStatuses}
                      options={statuses}
                      value={statusesNames}
                    />
                  </div>

                  <OrderTable
                    data={errors.length ? [] : orders}
                    onEdit={this.onEdit}
                    dateFormatter={dateFormatter}
                    timeFormatter={timeFormatter}
                    filters={filters}
                    setFilter={this.setFilter}
                    countdownTimerOrStatus={countdownTimerOrStatus}
                  />
                </div>

              </div>
            </div>
          </div>
        </Portlet>

        {!errors.length && !!orders.length &&
          <Paginate
            pageNum={paging.totalPages}
            clickCallback={this.setFilterPage}
            forceSelected={paging.page - 1}
          />
        }
      </div>
    );
  }
}

export default connect(selectors, actions)(Orders);
