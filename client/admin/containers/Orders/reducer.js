import { combineReducers } from 'redux';
import * as types from './constants';
import moment from '../../helpers/moment';
import { setFiltersValue } from '../../helpers/setObjValue';

const nowDateStart = moment().format('YYYY-MM-DD 00:00:00');
const nowDateEnd = moment().format('YYYY-MM-DD 23:59:59');

const ACTIVE_STATUSES = '1,2,3,4,5,6,7,8,9,10,12,13';
const ENDED_STATUSES = '11,14,15';

const initialState = {
  isAvailableOrders: true,
  filters: {
    all_orders: true,
    page:  1,
    limit: 10,
    with:  {
      client: {
        value: '*',
      },
      pharmacy: {
        value: 'isStock',
      },
      status: {
        value: '*',
      },
    },
    order_by: {
      updatedAt: {
        value: 'DESC',
      },
    },
    filters: {
      updatedAt: {
        multiple: true,
        from: { // eslint-disable-line key-spacing
          comparison: 'gte',
          value:      nowDateStart,
        },
        to: {
          comparison: 'lte',
          value:      nowDateEnd,
        },
      },
      'pharmacy.name': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      'client.name': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      status: {
        comparison: 'in',
        value:      '',
      },
      'client.tel': {
        comparison: 'like',
        value:      '',
      },
      // connected: {
      //   value: '',
      // },
      // active: {
      //   value: '',
      // },
    },
  },
  ordersStatus: 'active', // ['active', 'ended']
  ordersStatusData: {
    active: {
      entities: {
        operator: {},
        orders:   {},
        pharmacy: {},
        status:   {},
      },
      paging: {},
      ids:    [],
      status: {
        comparison: 'in',
        value:      '',
      },
    },
    ended: {
      entities: {
        operator: {},
        orders:   {},
        pharmacy: {},
        status:   {},
      },
      paging: {},
      ids:    [],
      status: {
        comparison: 'in',
        value:      '',
      },
    },
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const isAvailableOrders = (state = initialState.isAvailableOrders, action) => {
  switch (action.type) {
    case types.LOAD_TAKE_ORDER_INFO_SUCCESS:
      return action.data.isAvailableOrders;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.RESET_FILTER_TEL:
      return {
        ...state,
        filters: {
          ...state.filters,
          'client.tel': initialState.filters.filters['client.tel'],
        },
      };
    default:
      return state;
  }
};

const paging = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.paging;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const entities = (state = initialState.entities, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return {
        ...state,
        ...action.entities,
      };
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data;
    default:
      return state;
  }
};

const ordersStatus = (state = initialState.ordersStatus, action) => {
  switch (action.type) {
    case types.CHANGE_SHOWN_ORDERS_STATUS:
      return action.status;
    default:
      return state;
  }
};

const ordersStatusData = (state = initialState.ordersStatusData, action) => {
  const { ordersStatus } = action;
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return {
        ...state,
        [ordersStatus]: {
          ...state[ordersStatus],
          entities: entities(state[ordersStatus].entities, action),
          ids:      ids(state[ordersStatus].ids, action),
          paging:   paging(state[ordersStatus].paging, action),
        },
      };
    case types.SET_FILTER_STATUSES:
      return {
        ...state,
        [ordersStatus]: {
          ...state[ordersStatus],
          status: {
            ...state[ordersStatus].status,
            value: action.value || '',
          },
        },
      };
    default:
      return state;
  }
};

export default combineReducers({
  isAvailableOrders,
  ordersStatus,
  ordersStatusData,
  errors, // []
  filters, // {}
  isFiltersDebounce, // false
  // paging, // {}
  // entities, // {}
  // ids, // []
});
