import { takeEvery } from 'redux-saga';
// import { put } from 'redux-saga/effects';

import { browserHistory } from 'react-router';

import { LOAD_TAKE_ORDER_SUCCESS } from './constants';
// import * as actions from './actions';

function* goToOrdersItem(action) {
  yield [
    browserHistory.push(`/admin/orders/${action.data.id}`),
  ];
}

function* rootSagas() {
  yield [
    takeEvery(LOAD_TAKE_ORDER_SUCCESS, goToOrdersItem),
  ];
}

export default rootSagas;
