import { Schema, arrayOf } from 'normalizr';

const order = new Schema('orders');
const operator = new Schema('operator', { idAttribute: 'name' });
// const pharmacy = new Schema('pharmacy', { idAttribute: 'name' });
// const status = new Schema('status', { idAttribute: 'name' });

order.define({
  operator,
  // pharmacy,
  // status,
  // phones: arrayOf(phones, { schemaAttribute: 'phoneType' }),
});

export const ORDERS_ARR = { data: arrayOf(order) };

export const statusies = [
  { id: 10, name: 'Створено', color: 'label-green-turquoise' }, // (зелений)
  { id: 11, name: 'Отримано', color: 'label-green-turquoise' }, // (зелений)
  { id: 12, name: 'Обробляється Оператором', color: 'label-grey-steel' }, // (сірий)
  { id: 13, name: 'Опрацьовано Оператором', color: 'label-grey-steel' }, // (сірий)
  { id: 14, name: 'Отримано для обробки в аптеці', color: 'label-grey-steel' }, // (сірий)
  { id: 15, name: 'Отримано для обробки на складі', color: 'label-grey-steel' }, // (сірий)
  { id: 16, name: 'Обробляється в аптеці', color: 'label-grey-steel' }, // (сірий)
  { id: 17, name: 'Відправлений на редагування', color: 'label-warning' }, // (жовтий)
  { id: 18, name: 'Обробляється Call-центром після відправки на редагування', color: 'label-grey-steel' }, // (сірий)
  { id: 19, name: 'Опрацьовано Call-центром після відправки на редагування', color: 'label-grey-steel' }, // (сірий)
  { id: 20, name: 'Опрацьовано на складі  (Вн-розхід)', color: 'label-grey-steel' }, // (сірий)
  { id: 21, name: 'Опрацьовано в аптеці  (Резерв)', color: 'label-grey-steel' }, // (сірий)
  { id: 22, name: 'Продаж', color: 'label-grey-steel' }, // (сірий)
  { id: 23, name: 'Відмова', color: 'label-grey-steel' }, // (сірий)
];
