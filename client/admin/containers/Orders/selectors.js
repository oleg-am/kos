import { createSelector, createStructuredSelector } from 'reselect';

import _omit from 'lodash/omit';
import _pick from 'lodash/pick';

import moment from '../../helpers/moment';
import REDUCER from './constants';

const APP = 'app';

const ACTIVE_STATUSES     = '1,2,3,4,5,6,7,8,9,10,12,13';
const ENDED_STATUSES      = '11,14,15';

const ENDED_STATUSES_NAME = ['check', 'canceled', 'cancel_after_edit'];

const ordersAutoReloadInterval = state => state[APP].configs.ordersAutoReloadInterval;
const ordersAutoReloadEnable   = state => state[APP].configs.ordersAutoReloadEnable;
const allStatuses              = state => state[APP].statuses;

const filtersState   = state => state[REDUCER].filters;


const ordersStatus     = state => state[REDUCER].ordersStatus; // ['active', 'ended']
const ordersStatusData = state => state[REDUCER].ordersStatusData;

const entities  = createSelector(
  ordersStatusData,
  ordersStatus,
  (item, status) => item[status].entities,
);
const ordersIds = createSelector(
  ordersStatusData,
  ordersStatus,
  (item, status) => item[status].ids,
);
const filterStatusState = createSelector(
  ordersStatusData,
  ordersStatus,
  (item, status) => item[status].status,
);

const filterStatus = createSelector(
  filterStatusState,
  ordersStatus,
  (statusFilter, status) => ({
    ...statusFilter,
    value: statusFilter.value || ({ active: ACTIVE_STATUSES, ended: ENDED_STATUSES })[status],
  }),
);

const orderUpdatedAt  = createSelector(filtersState, item => item.filters.updatedAt);

const filters = createSelector(
  filtersState,
  filterStatus,
  (filtersState_, filterStatus_) => ({
    ...filtersState_,
    filters: {
      ...filtersState_.filters,
      status: { ...filterStatus_ },
    },
  }),
);

const orders = createSelector(entities, items => items.orders);

const activeStatuses = createSelector(allStatuses, items => _omit(items, ENDED_STATUSES_NAME));
const endedStatuses = createSelector(allStatuses, items => _pick(items, ENDED_STATUSES_NAME));
const statuses = createSelector(
  ordersStatus,
  activeStatuses,
  endedStatuses,
  (status, active, ended) => Object.values(({ active, ended })[status]).map(
    ({ statusName, id }) => ({ label: statusName, value: id }),
  ),
);
export default createStructuredSelector({
  ordersStatus,
  statuses,
  filters,
  isAvailableOrders: state => state[REDUCER].isAvailableOrders,
  errors:            state => state[REDUCER].errors,
  isFiltersDebounce: state => state[REDUCER].isFiltersDebounce,

  paging: createSelector(ordersStatusData, ordersStatus, (item, status) => item[status].paging),

  filterDateFrom: createSelector(orderUpdatedAt, items => moment(items.from.value)),

  ordersAutoReloadInterval: createSelector(ordersAutoReloadInterval, item => +item.value),
  ordersAutoReloadEnable:   createSelector(ordersAutoReloadEnable, item => item.value === 'true'),

  orders: createSelector(orders, ordersIds, (items, ids) => ids.map(id => items[id])),

  statusesNames: createSelector(
    statuses,
    filterStatusState,
    (items, { value: statusesIds }) => items.filter(item =>
      (statusesIds ? statusesIds.split(',') : [])
        .includes(item.value.toString()),
    ),
  ),
});
