import React from 'react';
// import { Link } from 'react-router';
import classNames from 'classnames';

const getbadge = (className, value) => (
  <span className={`pull-right badge badge-${className}`}>{className === 'warning' ? '+' : ''}{value}</span>
);

const getDeficit = (value) => {
  if (!value && value != 0) {
    return '';
  } else if (value == 0) {
    return getbadge('success', value);
  } else if (value > 0) {
    return getbadge('warning', value);
  } else if (value < 0) {
    return getbadge('danger', value);
  }
};

const getEditableCell = (editable, step, data) => ({
  max,
  value,
  deficitValue,
  isRenderDeficit,
  onChange,
}) => {
  return (
    data.name === 'Итого' || !editable
    ? <span style={{ display: 'block', marginLeft: '10px' }}>{value} {isRenderDeficit && getDeficit(+deficitValue)}</span>
    : (<div>
      <div style={{ padding: 0, marginBottom: 0, width: '65%' }} className="pull-left form-group form-md-line-input">
        <input
          min="0"
          max={max}
          step={step}
          className="form-control"
          style={{ height: 'auto', padding: '2px 8px 2px 12px' }}
          type="number"
          value={value}
          onChange={onChange}
        />
        <label />
      </div>
      {getDeficit(+deficitValue)}
    </div>)
  );
};

const RowGoods = ({
  data,
  isStockOrder,
  className,
  style,
  editable,
  onChangeQuantity,
  onChangeQuantityPharmacy,
  onChangeQuantityStock,
  onDelete,
  onLoadRemainders,
}) => {

  let { pharmacyRemainders, stockRemainders } = data;
  const { realQuantity, quantity, is_divisible, sub_quantity } = data;

  const step = (is_divisible && sub_quantity && 1 / sub_quantity) || 1;

  const renderEditableCell = getEditableCell(editable, step, data);

  if (realQuantity || realQuantity === 0) {
    pharmacyRemainders = isStockOrder ? pharmacyRemainders : realQuantity;
    stockRemainders = isStockOrder ? realQuantity : stockRemainders;
  }

  return (
    <tr className={classNames(className, { 'bg-default bg-font-default danger': data.isInfo })} style={style}>
      {/* <td>
          <label className="mt-checkbox">
              <input type="checkbox" />
              <span></span>
          </label>
      </td> */}

      <td style={{ textAlign: 'left' }} title={data.code1C}>{data.name}</td>
      <td>{data.price}</td>
      {/* <td>{data.quantity}</td> */}
      <td style={data.isInfo ? { textAlign: 'left', paddingLeft: '20px' } : {}}>
        {data.isInfo
            ? data.quantity
            : renderEditableCell({
              max:             pharmacyRemainders + stockRemainders,
              value:           data.quantity,
              deficitValue:    data.quantityDeficit,
              isRenderDeficit: false,
              onChange:        onChangeQuantity,
            })
        }
      </td>
      {editable && <td>
        {data.isInfo
          ? data.quantityPharmacy
          : renderEditableCell({
            max:             Math.min(pharmacyRemainders, quantity),
            value:           data.quantityPharmacy,
            deficitValue:    data.pharmacyDeficit,
            isRenderDeficit: false,
            onChange:        onChangeQuantityPharmacy,
          })
        }
      </td>}
      {editable && <td>
        {data.isInfo
          ? data.quantityStock
          : renderEditableCell({
            max:             Math.min(stockRemainders, quantity),
            value:           data.quantityStock,
            deficitValue:    data.stockDeficit,
            isRenderDeficit: false,
            onChange:        onChangeQuantityStock,
          })
        }
      </td>}
      <td>{data.sum}</td>
      {editable && data.name !== 'Итого' && <td>{data.pharmacyRemainders}</td>}
      {editable && data.name !== 'Итого' && <td>{data.stockRemainders}</td>}
      {editable && data.name !== 'Итого' &&
        <td
          title={data.isInfo ? '' : 'Видалити'}
          style={data.isInfo ? {} : { verticalAlign: 'middle', cursor: 'pointer' }}
          onClick={data.isInfo ? null : onDelete}
        >
          {!data.isInfo && <i className="fa fa-times pull-right font-default font-hover-red" />}
        </td>
      }
      {editable && data.name === 'Итого' &&
        <td colSpan="3">
          <button style={{ padding: '4px 10px' }} className="btn blue-sharp" onClick={onLoadRemainders} title="Оновити залишки">
            <i className="fa fa-refresh" /> Залишки
          </button>
        </td>
      }

      {/* <td>
        {data.name !== 'Итого' &&
          <Link className="btn btn-sm green-meadow" >
            <i style={{ verticalAlign: '-3px' }} className="hover fa fa-search fa-lg" />
          </Link>
        }
      </td> */}
      {/* <td>
          <label className="mt-checkbox">
              <input type="checkbox" />
              <span></span>
          </label>
      </td>  */}
    </tr>
  );
};

export default RowGoods;
