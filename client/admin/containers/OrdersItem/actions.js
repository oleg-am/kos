import * as types from './constants';
// import { createParams } from '../../helpers/api';

import { ORDER_OBJ, REMAINDERS_ARR } from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});

export const changePharmacy = (pharmacy = {}) => ({
  type: types.CHANGE_PHARMACY,
  pharmacy,
});

export const changeQuantity = (key, value, changedField) => ({
  type: types.CHANGE_QUANTITY,
  key,
  value,
  changedField,
});

export const changeShowAllPharmacies = isShowAllPharmacies => ({
  type: types.CHANGE_PARAM_SHOW_ALL_PHARMACIES,
  isShowAllPharmacies,
});

export const selectItems = (items = {}) => ({
  type: types.SELECT_ITEMS,
  items,
});

export const addItems = (items = {}) => ({
  type: types.ADD_ITEMS,
  items,
});

export const deleteItems = id => ({
  type: types.DELETE_ITEMS,
  id,
});

export const goToOrders = () => ({
  type: types.GO_TO_ORDERS,
});

export const reset = () => ({
  type: types.RESET,
});

export const changePauseTime = pauseTime => ({
  type: types.CHANGE_PAUSE_TIME,
  pauseTime,
});

export const changeStatus = changedStatusId => ({
  type: types.CHANGE_STATUS,
  changedStatusId,
});

export const pauseInputOn = data => ({
  type: types.PAUSE_INPUT_ON,
  data,
});

export const pauseInputOff = data => ({
  type: types.PAUSE_INPUT_OFF,
  data,
});

export const setCancelReason = cancelReason => ({
  type: types.SET_CANCEL_REASON,
  cancelReason,
});

export const openModal = () => ({
  type:        types.OPEN_MODAL,
  isOpenModal: true,
});

export const closeModal = () => ({
  type:        types.CLOSE_MODAL,
  isOpenModal: false,
});

export const getValidateErrors = (errors, orderId) => ({
  type: types.GET_VALIDATE_ERRORS,
  errors,
  orderId,
});

export const load = id => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/orders/${id}?with[*]=*`),
  schema:  ORDER_OBJ,

  // schemaOptions: options,
});

export const loadPharmacies = () => ({
  types: [
    types.LOAD_PHARMACIES_REQUEST,
    types.LOAD_PHARMACIES_SUCCESS,
    types.LOAD_PHARMACIES_FAILURE,
  ],
  promise: api => api.get(
    '/api/admin/pharmacies?nolimit&fields=id,name,isStock,connected,active,isSellPoint&order_by[name]=ASC&with[deliverySchedules]=*',
  ),
  // schema:  ORDER_OBJ,
});

export const loadRemainders = (pharmacyId, nomenclature = []) => ({
  types: [
    types.LOAD_REMAINDERS_REQUEST,
    types.LOAD_REMAINDERS_SUCCESS,
    types.LOAD_REMAINDERS_FAILURE,
  ],
  promise: api => api.get(
    `/api/admin/pharmacies/${pharmacyId}/remainders?filters[nomenclature]=in|${
      nomenclature.length === 1 ? nomenclature[0] : nomenclature.join(',')
    }&include_stock`,
  ),
  schema: REMAINDERS_ARR,
  // schemaOptions: remaindersOptions,
});

export const create = data => ({
  types:   [types.CREATE_REQUEST, types.CREATE_SUCCESS, types.CREATE_FAILURE],
  promise: api => api.post('/api/admin/orders', data),
});

export const edit = (id, data) => ({
  types:   [types.EDIT_REQUEST, types.EDIT_SUCCESS, types.EDIT_FAILURE],
  promise: api => api.put(`/api/admin/orders/${id}`, data),
});

export const cancel = (id, cancelReason) => ({
  types:   [types.CANCEL_REQUEST, types.CANCEL_SUCCESS, types.CANCEL_FAILURE],
  promise: api => api.put(`/api/admin/orders/${id}/cancel`, { cancelReason }),
});

export const pause = (id, pauseTime) => ({
  types:   [types.PAUSE_REQUEST, types.PAUSE_SUCCESS, types.PAUSE_FAILURE],
  promise: api => api.put(`/api/admin/orders/${id}/pause`, { minutes: pauseTime }),
});

export const cancelPause = id => ({
  types:   [types.CANCEL_PAUSE_REQUEST, types.CANCEL_PAUSE_SUCCESS, types.CANCEL_PAUSE_FAILURE],
  promise: api => api.put(`/api/admin/orders/${id}/pause`, { minutes: 0 }),
});

export const saveStatus = (id, data) => ({
  types:   [types.SAVE_STATUS_REQUEST, types.SAVE_STATUS_SUCCESS, types.SAVE_STATUS_FAILURE],
  promise: api => api.put(`/api/admin/orders/${id}/status`, data),
});
