const name = 'ordersItem';

export const LOAD_REQUEST = `${name}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${name}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${name}/LOAD_FAILURE`;

export const LOAD_REMAINDERS_REQUEST = `${name}/LOAD_REMAINDERS_REQUEST`;
export const LOAD_REMAINDERS_SUCCESS = `${name}/LOAD_REMAINDERS_SUCCESS`;
export const LOAD_REMAINDERS_FAILURE = `${name}/LOAD_REMAINDERS_FAILURE`;

export const LOAD_PHARMACIES_REQUEST = `${name}/LOAD_PHARMACIES_REQUEST`;
export const LOAD_PHARMACIES_SUCCESS = `${name}/LOAD_PHARMACIES_SUCCESS`;
export const LOAD_PHARMACIES_FAILURE = `${name}/LOAD_PHARMACIES_FAILURE`;

export const CREATE_REQUEST = `${name}/CREATE_REQUEST`;
export const CREATE_SUCCESS = `${name}/CREATE_SUCCESS`;
export const CREATE_FAILURE = `${name}/CREATE_FAILURE`;

export const EDIT_REQUEST = `${name}/EDIT_REQUEST`;
export const EDIT_SUCCESS = `${name}/EDIT_SUCCESS`;
export const EDIT_FAILURE = `${name}/EDIT_FAILURE`;

export const CANCEL_REQUEST = `${name}/CANCEL_REQUEST`;
export const CANCEL_SUCCESS = `${name}/CANCEL_SUCCESS`;
export const CANCEL_FAILURE = `${name}/CANCEL_FAILURE`;

export const PAUSE_REQUEST = `${name}/PAUSE_REQUEST`;
export const PAUSE_SUCCESS = `${name}/PAUSE_SUCCESS`;
export const PAUSE_FAILURE = `${name}/PAUSE_FAILURE`;

export const CANCEL_PAUSE_REQUEST = `${name}/CANCEL_PAUSE_REQUEST`;
export const CANCEL_PAUSE_SUCCESS = `${name}/CANCEL_PAUSE_SUCCESS`;
export const CANCEL_PAUSE_FAILURE = `${name}/CANCEL_PAUSE_FAILURE`;

export const SAVE_STATUS_REQUEST = `${name}/SAVE_STATUS_REQUEST`;
export const SAVE_STATUS_SUCCESS = `${name}/SAVE_STATUS_SUCCESS`;
export const SAVE_STATUS_FAILURE = `${name}/SAVE_STATUS_FAILURE`;

export const SELECT_ITEMS = `${name}/SELECT_ITEMS`;
export const ADD_ITEMS = `${name}/ADD_ITEMS`;
export const DELETE_ITEMS = `${name}/DELETE_ITEMS`;

export const GO_TO_ORDERS = `${name}/GO_TO_ORDERS`;
export const GET_VALIDATE_ERRORS = `${name}/GET_VALIDATE_ERRORS`;

export const SET_FILTER   = `${name}/SET_FILTER`;
export const CHANGE_PARAM = `${name}/CHANGE_PARAM`;
export const CHANGE_QUANTITY = `${name}/CHANGE_QUANTITY`;
export const CHANGE_STATUS = `${name}/CHANGE_STATUS`;

export const RESET = `${name}/RESET`;

export const PAUSE_INPUT_ON = `${name}/PAUSE_INPUT_ON`;
export const PAUSE_INPUT_OFF = `${name}/PAUSE_INPUT_OFF`;
export const CHANGE_PAUSE_TIME = `${name}/CHANGE_PAUSE_TIME`;

export const SET_CANCEL_REASON = `${name}/SET_CANCEL_REASON`;

export const OPEN_MODAL = `${name}/OPEN_MODAL`;
export const CLOSE_MODAL = `${name}/CLOSE_MODAL`;

export const CHANGE_PARAM_SHOW_ALL_PHARMACIES = `${name}/CHANGE_PARAM_SHOW_ALL_PHARMACIES`;

export default name;
