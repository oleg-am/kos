import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';
import { Link } from 'react-router';

import classNames from 'classnames';
import { Button, Row, Col } from 'react-bootstrap';
// import Select from 'react-select';
import InputElement from 'react-input-mask';

// import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';
import { postValidator, putValidator } from './schemas';

// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import ConfirmationGroup from '../../components/ConfirmationGroup';

import {
  ControlLabel,
  FormControl,
  FormGroup,
  // InputText,
  // Select,
  // Textarea,
} from '../../components/Metronic/Form';

import {
  // priceFormatter,
  dateFormatter,
  timeFormatter,
  // dateAndTimeFormatter,
  statusFormatter,
  // extraDataObjFormatter,
  // formTextFormatter,
} from '../../components/Formatters';

import RowGoods from './Components/RowGoods';

const CheckBoxShowAllPharmacies = ({ checked, onChange }) =>
  <label
    htmlFor="isShowAllPharmacies"
    className="mt-checkbox mt-checkbox-outline "
    style={{ marginBottom: 0, fontSize: '18px', margin: '0 0 0 20px' }}
  >
    Показувати всі аптеки
    <input
      id="isShowAllPharmacies"
      type="checkbox"
      checked={checked}
      onChange={onChange}
    />
    <span className="checkbox-white" />
  </label>;

const getRequiredMark = () => (
  <span style={{ color: 'red' }} >*</span>
);

const renderFieldGroup = ({ controlLabelMd = 4, colMd = 8, label, isStatic, isRequired, error, ...props }) => (
  <FormGroup error={error} style={{ margin: '0 -15px 5px', ...(isStatic && { height: '44.5px' }) }}>
    <ControlLabel md={controlLabelMd}>{label} { isRequired && getRequiredMark()}</ControlLabel>
    <Col md={colMd}>
      <FormControl {...{ ...props, isStatic, error }} />
    </Col>
  </FormGroup>
);

const renderFieldGroupTelMask = ({ controlLabelMd = 4, colMd = 8, label, value, isStatic, isRequired, ...props }) => (
  <FormGroup style={{ margin: '0 -15px 5px' }}>
    <ControlLabel md={controlLabelMd}>{label} { isRequired && getRequiredMark()}</ControlLabel>
    <Col md={colMd}>
      {isStatic
        ? <div className="form-control form-control-static"> {value} </div>
        : <InputElement
          className="form-control"
          // alwaysShowMask
          mask="38 (099) 999-99-99"
          // defaultValue={value}
          value={value}
          // maskChar='-'
          {...props}
        />
      }
      <div className="form-control-focus" />
      {/* <FormControl {...props} /> */}
    </Col>
  </FormGroup>
  );

class OrdersItem extends React.Component {
  static propTypes = {
    pharmacyError:    PropTypes.string,
    isSavingStatus:   PropTypes.bool,
    isEditableStatus: PropTypes.bool,
    isChangedStatus:  PropTypes.bool,

    editableStatusesForChiefOperator: PropTypes.arrayOf(PropTypes.shape({
      id:         PropTypes.oneOf(PropTypes.string, PropTypes.number),
      statusName: PropTypes.string,
    })),
    errors:          PropTypes.arrayOf(PropTypes.object),
    pharmacies:      PropTypes.arrayOf(PropTypes.object),
    nomenclatureEnt: PropTypes.objectOf(PropTypes.object),
    nomenclature:    PropTypes.arrayOf(PropTypes.object),
    order:           PropTypes.shape({
      id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]).isRequired,
    }),
    location: PropTypes.objectOf(PropTypes.any).isRequired,
    params:   PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
    isShowAllPharmacies: PropTypes.bool,

    isStockOrder:    PropTypes.bool,
    isQuantityStock: PropTypes.bool,
    editable:        PropTypes.bool,
    isPause:         PropTypes.bool,
    load:            PropTypes.func.isRequired,
    loadPharmacies:  PropTypes.func.isRequired,
    loadRemainders:  PropTypes.func.isRequired,
    saveStatus:      PropTypes.func.isRequired,
    setFilter:       PropTypes.func.isRequired,
    changeParam:     PropTypes.func.isRequired,
    changeQuantity:  PropTypes.func.isRequired,
    create:          PropTypes.func.isRequired,
    edit:            PropTypes.func.isRequired,
    selectItems:     PropTypes.func.isRequired,
    deleteItems:     PropTypes.func.isRequired,
    reset:           PropTypes.func.isRequired,
    cancel:          PropTypes.func.isRequired,
    pauseInputOn:    PropTypes.func.isRequired,
    pauseInputOff:   PropTypes.func.isRequired,
    // pause:           PropTypes.func.isRequired,
    changeStatus:    PropTypes.func.isRequired,
    changePauseTime: PropTypes.func.isRequired,
    setCancelReason: PropTypes.func.isRequired,
    pauseParam:      PropTypes.shape({
      isSetPauseTime: PropTypes.bool.isRequired,
      pauseTime:      PropTypes.number.isRequired,
    }),
    pause:       PropTypes.func.isRequired,
    cancelPause: PropTypes.func.isRequired,
    cancelParam: PropTypes.shape({
      cancelReason: PropTypes.string.isRequired,
      isOpenModal:  PropTypes.bool.isRequired,
    }),
    openModal:  PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,

    changeShowAllPharmacies: PropTypes.func.isRequired,
    getValidateErrors:       PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const { loadPharmacies } = props;

    // this.debounceLoad = _debounce(load, 200);

    loadPharmacies();
  }
  componentWillMount() {
    const {
      params: { id },
      location,
      load,
      changeParam,
      reset,
      order,
    } = this.props;

    const { isReset } = location.state || {};

    this.createItem = id === 'create';
    isReset && reset();

    this.createItem
      ? changeParam(['status', 'name'], 'creation')
      : order.id !== +id && load(id);
    // !this.createItem && order.id !== +id && load(id);
  }

  componentWillUnmount() {
    const NOT_RESET_URL = '/nomenclature-selection-in-pharmacy/';
    !~window.location.href.search(NOT_RESET_URL) && this.props.reset();
  }

  onChangeParam = (key, value) => (e) => {
    this.props.changeParam(key, value || e.value || e.target.value);
  }

  onChangeQuantity = (key, value) => (e) => {
    this.props.changeQuantity(key, value || +e.target.value, key[1]);
  }

  onSave = () => {
    const { order, nomenclature, create, edit, params: { id }, getValidateErrors } = this.props;
    const { firstName, tel } = order.client;
    let data = {
      ...order,
      firstName,
      pharmacy: order.sellInPharmacy,
      tel:      tel.replace(/([^0-9])/g, ''), // return only numbers
      items:    nomenclature.map(item => ({
        ...item,
        quantity:      item.quantityPharmacy ? item.quantityPharmacy : 0,
        stockQuantity: item.quantityStock ? item.quantityStock : 0,
      })),
    };
    const filterDataItems = data.items.filter(item => (
      item.quantity > 0 || item.stockQuantity > 0
    ));

    data = {
      ...data,
      items: filterDataItems,
    };

    const validData = this.createItem
      ? postValidator.validate(data)
      : putValidator.validate(data);
    // console.log("validData", validData);
    if (validData) {
      this.createItem
        ? create(validData)
        : edit(id, validData);
    } else {
      getValidateErrors(
        this.createItem ? postValidator.getErrors() : putValidator.getErrors(),
        id,
      );
    }
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  deleteItems = id => () => {
    this.props.deleteItems(id);
  }
  selectItems = (nomenclature) => {
    this.props.selectItems(nomenclature);
  }
  checkPharmacy = ({ pharmacy, sellInPharmacy }) => (e) => {
    if (pharmacy === -1 && sellInPharmacy === -1) {
      alert('Виберіть будь ласка аптеку');
      e.preventDefault();
    }
    this.selectItems(
      Object.values(this.props.nomenclatureEnt),
    );
  }
  changeShowAllPharmacies = (e) => {
    this.props.changeShowAllPharmacies(e.target.checked);
  }
  changeStatus = (e) => {
    this.props.changeStatus(e.target.value);
  }
  pauseInputOn =() => {
    this.props.pauseInputOn();
  }
  pauseInputOff =() => {
    this.props.pauseInputOff();
  }
  submitPause = () => {
    const pauseTime = this.props.pauseParam.pauseTime;
    const id = this.props.order.id;
    this.props.pauseInputOff();
    this.props.pause(id, pauseTime);
  }
  cancelPause =() => {
    const id = this.props.order.id;
    this.props.cancelPause(id);
  }
  setPause = (e) => {
    const pauseTime = +e.target.value;
    this.props.changePauseTime(pauseTime);
  }
  setCancelReason = (e) => {
    const reason = e.target.value;
    this.props.setCancelReason(reason);
  }
  reset = () => {
    this.props.reset();
  }
  cancel = () => {
    const { params: { id }, cancel, cancelParam } = this.props;
    cancel(id, cancelParam.cancelReason);
  }
  closeModal = () => {
    // e && e.preventDefault();
    this.props.closeModal();
  }
  openModal = () => {
    this.props.openModal();
  }
  loadRemainders = (sellInPharmacyId, nomenclatureIds) => () => {
    this.props.loadRemainders(sellInPharmacyId, nomenclatureIds);
  }
  saveStatus = () => {
    const { params: { id }, order: { status } } = this.props;
    this.props.saveStatus(id, { status: status.id });
  }
  selectStatusFormatter = (status, allStatuses) =>
    <ConfirmationGroup
      onSave={this.saveStatus}
      isEdited={this.props.isChangedStatus}
      isSaving={this.props.isSavingStatus}
    >
      <select value={status.id} className="form-control" onChange={this.changeStatus}>
        {allStatuses.map(({ id, statusName }) =>
          <option key={`option${id}`} value={id}> {statusName} </option>,
        )}
      </select>
    </ConfirmationGroup>
  statusFormatter = (isEditable, allStatuses) => (status = {}) => (
    isEditable
      ? this.selectStatusFormatter(status, allStatuses)
      : statusFormatter(status)
  )

  render() {
    const {
      params: { id },
      order,
      editable,
      errors,
      pharmacyError,
      pharmacies,
      deliverySchedules,
      nomenclature,
      nomenclatureIds,
      nomenclatureTotal,
      isStockOrder,
      isQuantityStock,
      pauseParam,
      isPause,
      cancelParam,
      isShowAllPharmacies,
      isEditableStatus,
      editableStatusesForChiefOperator,
    } = this.props;
    // console.log('props: ', this.props);
    const isStock = isStockOrder || isQuantityStock;

    return (
      <div className="page-content" style={{ paddingTop: '20px' }} >
        <Modal show={cancelParam.isOpenModal} onHide={this.closeModal}>
          <Header closeButton>
            <Title
              className={classNames('text-center caption-subject bold', 'font-yellow-lemon')}
            >
              Введіть причину відміни
            </Title>
          </Header>
          <Body>
            <div className="form-group form-md-line-input">
              <textarea
                className="form-control"
                value={cancelParam.cancelReason}
                onChange={this.setCancelReason}
              />
              <div className="form-control-focus" />
            </div>
          </Body>
          <Footer>

               {/* <Button
                 onClick={this.cancel}
                 className="green-meadow pull-left"
                 disabled={!cancelParam.cancelReason}
               >
                  Підтвердити
                </Button> */}
            {cancelParam.cancelReason && <Link
              className="btn btn-sm green-meadow pull-left"
              to="/admin/orders"
              onClick={this.cancel}
            >
              <i className="fa fa-check" />  Підтвердити
            </Link>}
             <Button
               onClick={this.closeModal}
               >
              Закрити
            </Button>
          </Footer>

        </Modal>
        <Breadcrumbs
          title=""
          components={{
            title: `${this.createItem ? 'Створення замовлення' : 'Інформація про замовлення'}`,
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Замовлення',
                to:   '/admin/orders',
              }, {
                name: this.createItem ? 'Створення замовлення' : `Замовлення №${id}`,
              },
            ],
          }}
        />
        {/* dataFormat={statusFormatter} formatExtraData={statuses} */}
        <CustomTable
          tableHeaderClass="bg-blue bg-font-blue"
          trClassName="bold"
          tableClass="table table-striped table-bordered table-hover"
          data={[order]}
          striped
          hover
        >
          <HeaderColumn dataField="id" width="5%" dataAlign="center" isKey >№ Замовлення</HeaderColumn>
          <HeaderColumn dataField="createdAt" width="10%" dataAlign="center" dataFormat={dateFormatter}>Дата замовлення</HeaderColumn>
          <HeaderColumn dataField="createdAt" width="10%" dataAlign="center" dataFormat={timeFormatter}>Час замовлення</HeaderColumn>
          <HeaderColumn dataField="operator" width="10%" dataAlign="center" dataFormat={cell => cell.name} >Оператор</HeaderColumn>
          <HeaderColumn
            dataField="status"
            width="20%"
            dataAlign="center"
            dataFormat={this.statusFormatter(isEditableStatus, editableStatusesForChiefOperator)}
          >
            Статус
          </HeaderColumn>
          <HeaderColumn dataField="internetGround" width="10%" dataAlign="center" dataFormat={c => c && c.name}>Джерело</HeaderColumn>
        </CustomTable>

        <Row>
          <Col md={6} sm={6}>
            <Portlet
              className={`box ${isStock ? 'yellow-mint' : 'blue-dark'}`}
              title={[
                <i key="title1" className="fa fa-cogs" />,
                <span key="title2">Аптека</span>,
                <CheckBoxShowAllPharmacies
                  key="title3"
                  checked={isShowAllPharmacies}
                  onChange={this.changeShowAllPharmacies}
                />,
              ]}
            >
              <form role="form" className="form-horizontal">
                {renderFieldGroup({
                  isStatic:!editable,
                  label:"Аптека:",
                  component: 'reactSelect',
                  matchProp: 'label',
                  className: 'Select-option-in-order',
                  styles: { border: 0 },
                  error: pharmacyError,
                  value: order.sellInPharmacy,
                  options: pharmacies,
                  field: "name",
                  placeholder: "Оберіть аптеку",
                  onChange: this.onChangeParam('sellInPharmacy'),
                })}
                {renderFieldGroup({
                  isStatic: true,
                  label:"Дата та час доставки:",
                  type:"text",
                  value: deliverySchedules,
                })}
                {renderFieldGroup({
                  isStatic:!editable,
                  label:"Коментар оператора:",
                  placeholder:"Введіть коментар оператора:",
                  component:"textarea",
                  value: order.operatorComment,
                  onChange: this.onChangeParam('operatorComment'),
                })}

                {/* <FieldGroup key="111"
                  isStatic={!editable}
                  label="Аптека:"
                  component="select"
                  value={order.pharmacy}
                  options={pharmacies}
                  field="name"
                  placeholder="Оберіть аптеку"
                  onChange={this.onChangeParam('pharmacy')}
                />
                <FieldGroup key="1112"
                  isStatic
                  label="Дата та час доставки:"
                  value={dateFormatter(order.date) + ' ' + timeFormatter(order.time)}
                />
                <FieldGroup key="1141"
                  isStatic={!editable}
                  label="Коментар оператора:"
                  type="text"
                  value={order.comment}
                  onChange={this.onChangeParam('comment')}
                /> */}
              </form>
            </Portlet>
          </Col>
          <Col md={6} sm={6}>
            <div className="portlet blue-hoki box">
                <div className="portlet-title">
                    <div className="caption">
                        <i className="fa fa-cogs"></i>Клієнт </div>
                    {/* <div className="actions">
                        <a href="javascript:;" className="btn btn-default btn-sm">
                            <i className="fa fa-pencil"></i> Edit </a>
                    </div> */}
                </div>
                <div className="portlet-body">
                  <form role="form" className="form-horizontal">
                    {renderFieldGroup({
                      isRequired: true,
                      controlLabelMd: 3,
                      colMd: 9,
                      isStatic:!editable,
                      label:"ПІБ:",
                      placeholder:"Введіть ПІБ:",
                      type:"text",
                      value: order.client.firstName,
                      onChange: this.onChangeParam(['client', 'firstName']),
                    })}
                    {renderFieldGroupTelMask({
                      isRequired: true,
                      controlLabelMd: 3,
                      colMd: 9,
                      isStatic:!editable,
                      label:"Телефон:",
                      placeholder:"Введіть телефон:",
                      type:"text",
                      value: order.client.tel,
                      title: 'Номер телефона повинен складатись з 12 цифр та починатись на 380',
                      onChange: this.onChangeParam(['client', 'tel']),
                    })}
                    {/* {renderFieldGroup({
                      controlLabelMd: 2,
                      colMd: 9,
                      isStatic:!editable,
                      label:"Телефон:",
                      type:"text",
                      value: order.client.tel,
                      title: 'Номер телефона повинен складатись з 12 цифр та починатись на 380',
                      onChange: this.onChangeParam(['client', 'tel']),
                    })} */}
                    {/* {renderFieldGroup({
                      controlLabelMd: 2,
                      colMd: 9,
                      isStatic:!editable,
                      label:"E-mail:",
                      type:"text",
                      value: order.client.email,
                      onChange: this.onChangeParam(['client', 'email']),
                    })} */}
                    {renderFieldGroup({
                      controlLabelMd: 3,
                      colMd: 9,
                      isStatic: !this.createItem || !editable,
                      label:"Коментар клієнта:",
                      placeholder:"Введіть коментар клієнта:",
                      component:"textarea",
                      value: order.clientComment,
                      onChange: this.onChangeParam('clientComment'),
                    })}
                    {/*<FieldGroup key="111fs"
                      isStatic={!editable}
                      label="E-mail:"
                      type="text"
                      value={order.email}
                      onChange={this.onChangeParam('email')}
                    /> */}
                  </form>
                </div>
            </div>
          </Col>
        </Row>

        <Portlet
          actionsLeft={[
            editable &&
              <Link
                key="actionsRight_1"
                className="btn btn-sm blue-madison"
                disabled={order.pharmacy === -1 && order.sellInPharmacy === -1}
                to={`/admin/orders/${id}/nomenclature-selection-in-pharmacy/${order.sellInPharmacy}`}
                onClick={this.checkPharmacy(order)}
                  style={{ marginRight: '10px' }}
              >
                + Додати товари
              </Link>,
            editable && !isPause && !this.createItem &&
              <button
                key="actionsRight_2"
                className="btn btn-sm yellow-mint"
                onClick={(this.pauseInputOn)}
              >
                На паузу
              </button>,
            editable && isPause &&
            <input
              type="number"
              min="0"
              max="60"
              step="5"
              key="actionsRight_3"
              className=" btn yellow-mint"
              style={{ marginRight: '10px',width:'65px' }}
              onChange={this.setPause}
            />,
            editable && isPause &&
              <button
                key="actionsRight_4"
                className="btn btn-sm green-meadow"
                style={{ marginRight: '10px' }}
                onClick={(this.submitPause)}
                disabled={pauseParam.pauseTime === 0}
              >
                <i className="fa fa-check" /> Підтвердити
            </button>,
            editable && isPause &&
              <button
                key="actionsRight_5"
                className="btn btn-sm red"
                onClick={(this.pauseInputOff)}
              >
                <i className="fa fa-close" />Скасувати
            </button>,
            !editable && !isPause && !!order.pauseTimeLeft &&
            <button
              key="actionsRight_6"
              className="btn btn-sm yellow-mint"
              onClick={this.cancelPause}
            >
              <i className="fa fa-close" />Зняти з паузи
          </button>,
            order.cancelReason && <span style={{ color: 'red' }} >
        Причина відмови: { order.cancelReason}
            </span>,
          ]}

          actionsRight={[
            editable && !!nomenclature.length && <button
              key="actionsRight_2"
              className="btn green-meadow"
              style={{ marginRight: '10px' }}
              onClick={this.onSave}
            >
              <i className="fa fa-check" /> {this.createItem ? 'Створити' : 'Зберегти'}
            </button>,
            // <Link
            //   key="actionsRight_3"
            //   className={`btn btn-sm ${editable && !this.createItem ? 'red' : 'default'}`}
            //   to="/admin/orders"
            //   onClick={editable && !this.createItem ? this.cancel : this.reset}
            // >
            //   <i className="fa fa-times" /> {editable ? 'Відмінити' : 'Закрити'}
            // </Link>,
            <Link
              key="actionsRight_3"
              style={{ marginRight: '10px' }}
              className={`btn btn-sm ${editable && !this.createItem ? 'red' : 'default'}`}
              to={editable && !this.createItem ? ' ' : "/admin/orders" }
              onClick={editable && !this.createItem ? this.openModal : this.reset}
            >
              <i className="fa fa-times" /> {editable ? 'Відмінити' : 'Закрити'}
            </Link>,
          ]}
        >

          {!!nomenclature.length && <div
            className={`table-scrollable td-right th-middle ${editable ? 'th-center' : 'th-right'}`}
          >
            <table className="table table-hover table-bordered">
              <colgroup>
                <col width="40%" />
                <col width="12%" />
                <col width="12%" />
                <col width="12%" />
                <col width="12%" />
                <col width="12%" />
                <col width="5%%" />

              </colgroup>
              <thead className="bg-blue bg-font-blue">
                <tr>
                  <th
                    rowSpan="2"
                    style={editable ? {} : { textAlign: 'center' }}
                  > Найменування </th>
                  <th rowSpan="2"> Ціна </th>
                  <th colSpan={`${editable ? 3 : 1}`}> Кількість </th>
                  {/* <th> Кількість Аптека </th> */}
                  {/* <th> Кількість Склад </th> */}
                  <th rowSpan="2"> Вартість </th>
                  {editable && <th colSpan="2"> Залишок </th>}
                  {/* <th> Залишок на складі </th> */}
                  {editable && <th rowSpan="2" />}
                  {/* <th rowSpan="2"> Підбір аналога </th> */}
                </tr>
                <tr>
                  {/* <th> Найменування </th> */}
                  {/* <th> Ціна </th> */}
                  {editable && <th> Замовлення </th>}
                  {editable && <th> Аптека </th>}
                  {editable && <th> Склад </th>}
                  {/* <th> Вартість </th> */}
                  {editable && <th> Аптека </th>}
                  {editable && <th> Склад </th>}
                  {/* <th> Підбір аналога </th> */}
                </tr>
              </thead>
              <tbody>
                {/* {console.log('nomenclature', nomenclature)} */}
                {nomenclature.map(item => (
                  <RowGoods
                    key={`good${item.id}`}
                    editable={editable}
                    data={item}
                    isStockOrder={isStockOrder}
                    onChangeQuantity={this.onChangeQuantity([item.id, 'quantity'])}
                    onChangeQuantityPharmacy={this.onChangeQuantity([item.id, 'quantityPharmacy'])}
                    onChangeQuantityStock={this.onChangeQuantity([item.id, 'quantityStock'])}

                    onDelete={this.deleteItems(item.id)}
                  />
                ))}
                <RowGoods
                  key="goods_total"
                  onLoadRemainders={this.loadRemainders(order.sellInPharmacy, nomenclatureIds)}
                  style={{ fontWeight: 'bold' }}
                  className={'success'}
                  data={nomenclatureTotal}
                  editable={editable}
                />
              </tbody>
            </table>
          </div>
        }

        </Portlet>

      </div>
    );
  }
}

export default connect(selectors, actions)(OrdersItem);
