export const getQuantityPharmacy = (quantity, pharmacyRemainders) => {
  const result = (+quantity <= +pharmacyRemainders ? +quantity : +pharmacyRemainders);
  return result;
};

export const getQuanityStock = (quantity, stockRemainders, quantityPharmacy) => {
  if (quantity == quantityPharmacy) {
    return 0;
  } else {
    const targetCount = +quantity - +quantityPharmacy;
    const result = +targetCount <= +stockRemainders ? +targetCount : +stockRemainders;
    return result > 0 ? result : 0;
  }
};

export const getDeficit = (quantityPharmacy, quantityStock, quantity) => {
  const result = +(quantityPharmacy + quantityStock).toFixed(6) - quantity;
  return result;
  // return result > 0 ? 0 : result;
};

export const getSum = (reservePrice, quantity) => {
  const result = reservePrice * quantity;
  return +result.toFixed(2) || 0;
  // return result > 0 ? 0 : result;
};

export const getTotalSum = goods => (
  goods.reduce((sum, next) => (
    sum + (next.reservePrice * next.quantity)
  ), 0)
);

export const getNomenclatureTotal = (
  goods,
  SUM_KEYS = [
    // 'price',
    'quantity',
    'sum',
    'pharmacyRemainders',
    'stockRemainders',
    'quantityPharmacy',
    'quantityStock',
  ],
  TO_FIXED_KEYS = [
    'price',
    'sum',
  ],
) => !console.log('goods get total: ', goods) && (
  Object.keys(goods[0] || {}).reduce((obj, key) => {
    if (SUM_KEYS.includes(key)) {
      const total = goods.reduce((sum, next) => (sum + (+next[key] || 0)), 0);
      obj[key] = TO_FIXED_KEYS.includes(key) ? (total.toFixed(2) || 0) : total;
      return obj;
    }
    obj[key] = key === 'name' ? 'Итого' : '';
    return obj;
  }, {})
);
