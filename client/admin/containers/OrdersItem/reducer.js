import { combineReducers } from 'redux';
import _omit from 'lodash/omit';

import * as types from './constants';

import { setValue } from '../../helpers/setObjValue';
import { getNomenclatureIfStock, getNomenclatureWithDeficit } from './utils/reducer';

const initialState = {
  isShowAllPharmacies: true,
  pharmacies: [],
  entities: {
    nomenclature:            {},
    nomenclatureWithDeficit: {},
    pharmacy:                {},
  },
  order: {
    client: {
      id:         '',
      firstName:  '',
      middleName: '',
      lastName:   '',
      email:      '',
      tel:        '',
      createdAt:  '',
    },
    operator: {
      name: '',
    },
    status: {
      id:         '',
      name:       '',
      statusName: '',
    },
    id:              'Новий',
    clientComment:   '',
    operatorComment: '',
    pharmacy:        -1,
    sellInPharmacy:  -1,
    nomenclature:    [],
    paused:          false,

    // items:[{
    //   'id':'_integer_',
    //   'pharmacy':'_integer_',
    //   'quantity':'_float_',
    //   'price':'_float_',
    // }],
  },
  originalSellInPharmacy: -1,

  isChangedQuanityField: false,
  isChangedStatus:       false, // Головний оператор може змінювати статуси

  pauseParam: {
    isSetPauseTime: false,
    pauseTime:      0,
  },
  cancelParam: {
    cancelReason: '',
    isOpenModal:  false,
  },
};

const isShowAllPharmacies = (state = initialState.isShowAllPharmacies, action) => {
  switch (action.type) {
    case types.CHANGE_PARAM_SHOW_ALL_PHARMACIES:
      return action.isShowAllPharmacies;
    default:
      return state;
  }
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const pauseParam = (state = initialState.pauseParam, action) => {
  switch (action.type) {
    case types.PAUSE_INPUT_ON:
      return { ...state, isSetPauseTime: true, pauseTime: initialState.pauseParam.pauseTime };
    case types.PAUSE_INPUT_OFF:
      return { ...state, isSetPauseTime: false };
    case types.CHANGE_PAUSE_TIME:
      return { ...state, pauseTime: action.pauseTime };
    case types.RESET:
      return initialState.pauseParam;
    default:
      return state;
  }
};
const cancelParam = (state = initialState.cancelParam, action) => {
  switch (action.type) {
    case types.SET_CANCEL_REASON:
      return { ...state, cancelReason: action.cancelReason };
    case types.CLOSE_MODAL:
      return initialState.cancelParam;
    case types.OPEN_MODAL:
      return {
        ...state,
        isOpenModal: action.isOpenModal,
      };
    case types.RESET:
      return initialState.cancelParam;
    default:
      return state;
  }
};

const originalSellInPharmacy = (state = initialState.originalSellInPharmacy, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.sellInPharmacy;
    default:
      return state;
  }
};

const order = (state = initialState.order, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return { ...state, ...action.result };
    case types.CHANGE_PARAM:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
    case types.CHANGE_STATUS:
      return {
        ...state,
        ...setValue(state, ['status', 'id'], action.changedStatusId),
      };
    case types.SAVE_STATUS_SUCCESS:
      return { ...state, status: action.data.status };
    case types.CANCEL_PAUSE_SUCCESS:
      return { ...state, paused: false };
    case types.PAUSE_SUCCESS:
      return { ...state, paused: true };
    case types.RESET:
      return initialState.order;
    default:
      return state;
  }
};

const pharmacies = (state = initialState.pharmacies, action) => {
  switch (action.type) {
    case types.LOAD_PHARMACIES_SUCCESS:
      // delete Суматра ООО and stock
      return action.data.filter(({ isSellPoint }) => isSellPoint);
    // case types.RESET:
    //   return initialState.pharmacies;
    default:
      return state;
  }
};

const entities = (state = initialState.entities, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS: {
      const { pharmacy, sellInPharmacy, cancelReason } = action.result;

      if (cancelReason) {
        return { ...state, ...action.entities };
      }

      const { nomenclature, nomenclatureWithDeficit } =
        getNomenclatureWithDeficit(action.entities.nomenclature, action.result.status);

      const newState = { ...state, ...action.entities, nomenclature, nomenclatureWithDeficit };
      if (pharmacy !== sellInPharmacy) {
        return {
          ...newState,
          nomenclature: getNomenclatureIfStock(nomenclature),
        };
      }
      return newState;
    }
    case types.ADD_ITEMS: {
      // console.log("action.items", action.items);
      const nomenclatureRemainders = action.items.reduce((obj, item) => {
        const id = item.nomenclature.id;
        // console.log("obj", obj);
        // console.log("itemAdd", item);
        const stateNomenclature = state.nomenclature[id] || {};
        const price = stateNomenclature.price
          ? stateNomenclature.price
          : item.sell_price;
        obj[id] = {
        // ...item,
          ...stateNomenclature,
          id,
          price,
          sub_quantity:       item.sub_quantity,
          is_divisible:       item.is_divisible,
          name:               item.nomenclature.name,
          quantity:           item.nomenclature.quantity || 0,
          pharmacyRemainders: (item.pharmacyRemainders || item.pharmacyRemainders === 0)
            ? item.pharmacyRemainders
            : item.quantity,
          stockRemainders: item.stockRemainders,
        };
        return obj;
      }, {});
      return {
        ...state,
        nomenclature: {
          ...state.nomenclature,
          ...nomenclatureRemainders,
        },
      };
    }
    case types.DELETE_ITEMS: {
      const { nomenclature, nomenclatureWithDeficit } = state;
      return {
        ...state,
        nomenclature:            _omit(nomenclature, action.id),
        nomenclatureWithDeficit: _omit(nomenclatureWithDeficit, action.id),
      };
    }
    case types.LOAD_REMAINDERS_FAILURE:
    case types.LOAD_REMAINDERS_SUCCESS: {
      const nomenclature = { ...state.nomenclature };
      const remainders = action.entities ? action.entities.remainders : {};

      Object.keys(nomenclature).forEach((id) => {
        const { quantity = 0, stock } = remainders[id] || {};
        nomenclature[id] = {
          ...nomenclature[id],
          pharmacyRemainders: quantity,
          stockRemainders:    stock ? stock.quantity : 0,
        };
      });
      return { ...state, nomenclature };
    }
    case types.CHANGE_QUANTITY: {
      const { key: [itemId, param], value, changedField }  = action;
      return {
        ...state,
        nomenclature: {
          ...state.nomenclature,
          [itemId]: {
            ...state.nomenclature[itemId],
            changedField,
            [param]: value,
          },
        },
      };
    }
    case types.RESET:
      return initialState.entities;
    default:
      return state;
  }
};

const isChangedStatus = (state = initialState.isChangedStatus, action) => {
  switch (action.type) {
    case types.CHANGE_STATUS:
      return true;
    case types.SAVE_STATUS_SUCCESS:
      return false;
    default:
      return state;
  }
};

const isChangedQuanityField = (state = initialState.isChangedQuanityField, action) => {
  switch (action.type) {
    case types.CHANGE_QUANTITY: {
      return action.changedField === 'quantity' ? true : state;
    }
    default:
      return state;
  }
};

const isSavingStatus = (state = false, action) => {
  switch (action.type) {
    case types.SAVE_STATUS_REQUEST:
      return true;
    case types.SAVE_STATUS_SUCCESS:
    case types.SAVE_STATUS_FAILURE:
      return false;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

export default combineReducers({
  isSavingStatus,
  cancelParam,
  pauseParam,
  errors, // []
  order, // {}
  originalSellInPharmacy,
  entities,
  pharmacies,
  isFiltersDebounce, // false
  isShowAllPharmacies,
  isChangedStatus,
  isChangedQuanityField,
});
