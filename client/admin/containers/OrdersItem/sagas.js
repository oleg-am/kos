import { takeEvery } from 'redux-saga';
import { put, select } from 'redux-saga/effects';

import { browserHistory } from 'react-router';

import REDUCER, {
  LOAD_SUCCESS,
  CREATE_SUCCESS,
  EDIT_SUCCESS,
  CANCEL_SUCCESS,
  CHANGE_PARAM,
  GET_VALIDATE_ERRORS,
} from './constants';

import { ADD_ITEMS } from '../NomenclatureSelectionInPharmacy/constants';
import * as actions from './actions';


[ADD_ITEMS].forEach((actionType) => {
  if (!actionType) {
    throw new Error(`Action type "${actionType}" is not define`);
  }
});
const getNomenclatureIds = entities => (
  entities
    && entities.nomenclature
    && Object.keys(entities.nomenclature)
);

function* loadRemaindersAfterLoad({ result, entities }) {
  const pharmacyId = result && result.sellInPharmacy;
  const nomenclatureIds = getNomenclatureIds(entities);
  if (pharmacyId && nomenclatureIds && Object.keys(nomenclatureIds).length) {
    yield put(actions.loadRemainders(pharmacyId, nomenclatureIds));
  }
}

function* loadRemaindersAfterChangeParam({ key, value }) {
  const pharmacyId = key === 'sellInPharmacy' && value;
  if (pharmacyId) {
    const state = yield select();
    const entities = state[REDUCER].entities;
    const nomenclatureIds = getNomenclatureIds(entities);

    if (nomenclatureIds && Object.keys(nomenclatureIds).length) {
      yield put(actions.loadRemainders(pharmacyId, nomenclatureIds));
    }
  }
}

function* addItems({ items }) {
  const itemsArr = Object.values(items);
  if (itemsArr.length) {
    yield put(actions.addItems(itemsArr));
  }
}

// [KSM-181] Відміняємо замовлення, якщо товар на редагуванні і кількість 0
function* cancelOrder({ errors, orderId }) {
  const CANSEL_REASON = 'Відмова після редагування';

  if (errors.items === 'CANNOT_BE_EMPTY' && Object.keys(errors).length === 1) {
    yield put(actions.cancel(orderId, CANSEL_REASON));
  }
}

function* resetAndGoToOrders() {
  yield [
    put(actions.reset()),
    put(actions.goToOrders()),
    browserHistory.push('/admin/orders'),
  ];
}

function* rootSagas() {
  yield [
    takeEvery(LOAD_SUCCESS, loadRemaindersAfterLoad),
    takeEvery(CHANGE_PARAM, loadRemaindersAfterChangeParam),
    takeEvery(ADD_ITEMS, addItems),
    takeEvery([CREATE_SUCCESS, EDIT_SUCCESS, CANCEL_SUCCESS], resetAndGoToOrders),
    takeEvery(GET_VALIDATE_ERRORS, cancelOrder),
  ];
}

export default rootSagas;
