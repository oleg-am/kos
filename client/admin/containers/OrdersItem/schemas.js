import { Schema, arrayOf } from 'normalizr';
import LIVR from 'livr';

LIVR.Validator.defaultAutoTrim(true);

const pharmacy = new Schema('pharmacy');
const sellInPharmacy = new Schema('sellInPharmacy');
const nomenclature = new Schema('nomenclature');
// const status = new Schema('status', { idAttribute: 'name' });

const getNomenclatureId = item => item.nomenclature.id;
const remainders = new Schema('remainders', { idAttribute: getNomenclatureId });

const order = {
  pharmacy,
  sellInPharmacy,
  // status,
  nomenclature: arrayOf(nomenclature),
  // orderParts: arrayOf(orderParts, { schemaAttribute: 'name' }),
};

export const ORDER_OBJ = order;
export const REMAINDERS_ARR = { data: arrayOf(remainders) };

const orderSchema = {
  pharmacy:        ['required', 'positive_integer'],
  firstName:       'string',
  middleName:      'string',
  lastName:        'string',
  email:           'string',
  tel:             'string',
  clientComment:   'string',
  operatorComment: 'string',
  items:           ['not_empty_list', { list_of_objects: {
    id:            ['required', 'positive_integer'],
    quantity:      ['required', { min_number: 0 }],
    stockQuantity: ['required', { min_number: 0 }],
    price:         ['required', 'positive_decimal'],
  } }],
};

const idSchema = {
  id: ['required', 'positive_integer'],
};

export const postValidator = new LIVR.Validator(orderSchema);
export const putValidator = new LIVR.Validator({ ...orderSchema, ...idSchema });

export const statusies = [
  'creation', // custom status

  'created',
  'in_process_by_operator',
  'processed_by_operator',
  'got_in_pharmacy',
  'got_in_stock',
  'in_process_by_pharmacy',
  'in_process_by_stock',
  'returned_to_edit',
  'editing_by_operator',
  'processed_after_edit',
  'cancel_after_edit',
  'processed_by_pharmacy',
  'processed_by_stock',
  'check',
  'canceled',
];

export const EDITABLE_STATUSES = [
  'creation',
  'in_process_by_operator',
  'editing_by_operator',
];

export const NOT_EDITABLE_STATUSES = [
  'processed_after_edit',
  'got_in_pharmacy',
  'got_in_stock',
  'processed_by_stock',
  'processed_by_pharmacy',
  'in_process_by_stock',
  'in_process_by_pharmacy',
];

// KSM-220
export const EDITABLE_STATUSES_FOR_CHIEF_OPERATOR = [
  {
    from: ['processed_by_operator', 'processed_after_edit'],
    to:   ['cancel_after_edit'],
  },
  {
    from: ['got_in_pharmacy', 'got_in_stock'],
    to:   [
      'in_process_by_pharmacy',
      'in_process_by_stock',
      'processed_by_pharmacy',
      'processed_by_stock',
      'cancel_after_edit',
    ],
  },
  {
    from: ['in_process_by_pharmacy', 'in_process_by_stock'],
    to:   ['processed_by_pharmacy', 'processed_by_stock', 'cancel_after_edit'],
  },
  {
    from: ['processed_by_pharmacy', 'processed_by_stock'],
    to:   ['cancel_after_edit'],
  },
];
