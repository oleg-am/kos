import { createSelector, createStructuredSelector } from 'reselect';

import transformMinusValueToZero from '../../helpers/transformMinusValueToZero';

import REDUCER from './constants';
import { EDITABLE_STATUSES, EDITABLE_STATUSES_FOR_CHIEF_OPERATOR } from './schemas';
import {
  getQuantityPharmacy,
  getQuanityStock,
  getDeficit,
  getSum,
  getNomenclatureTotal,
  // getTotalSum,
} from './model';

import moment from '../../helpers/moment';

const APP = 'app';

const convertToTwoSimbols = value => (value < 10 ? `0${value}` : value);

const daysOfTheWeek = {
  1: 'Пн',
  2: 'Вт',
  3: 'Ср',
  4: 'Чт',
  5: 'Пт',
  6: 'Сб',
  7: 'Нд',
};

// Знаходимо останній об'єкт в історії зміни товару в замовленні'
const getLastItemHistory = (orderNomenclatureHistory = [], itemId) => (
  [...orderNomenclatureHistory].reverse().find(itemHistory =>
    itemId === itemHistory.nomenclature.id,
));
// Знаходимо останню кількість замовленого товару в історії змін'
const getLastItemHistoryQuantity = (lastItemHistory = {}, isOrderEditable) =>
  +(lastItemHistory[isOrderEditable ? 'toQuantity' : 'fromQuantity']);

const paramsId             = (_, props) => props.params.id;
const isEditableRoleAccess = (_, props) => props.isEditableRoleAccess;

const chiefOperator = state => state[APP].user.role.chiefOperator;
const allStatuses   = state => state[APP].statuses;

const nomenclatureWithDeficit = state => state[REDUCER].entities.nomenclatureWithDeficit;

const pharmacy            = state => state[REDUCER].entities.pharmacy;
const pharmacies          = state => state[REDUCER].pharmacies;

const isFiltersDebounce   = state => state[REDUCER].isFiltersDebounce;
const errors              = state => state[REDUCER].errors;
const pauseParam          = state => state[REDUCER].pauseParam;
const cancelParam         = state => state[REDUCER].cancelParam;
const isShowAllPharmacies = state => state[REDUCER].isShowAllPharmacies;
const isChangedStatus     = state => state[REDUCER].isChangedStatus;
const isSavingStatus      = state => state[REDUCER].isSavingStatus;

const isChangedQuanityField = state => state[REDUCER].isChangedQuanityField;

const originalSellInPharmacy = state => state[REDUCER].originalSellInPharmacy;
const sellInPharmacy         = state => state[REDUCER].order.sellInPharmacy;

const nomenclatureEnt = createSelector(state => state[REDUCER].entities.nomenclature, item => item);
const order           = createSelector(state => state[REDUCER].order, item => item);
const orderStatusName = createSelector(order, item => item.status.name);

const isStockOrder = createSelector(
  pharmacy,
  item => (
    (Object.values(item)[0] || {}).isStock
  ));

// Показує, чи змінювали аптеку в замовленні
// Використовується для замовлень, які прийшли на редагування з аптеки і є realQuantity
const isConsiderRealQuantity = createSelector(
  [originalSellInPharmacy, sellInPharmacy, isStockOrder],
  (originalSellInPharmacy_, sellInPharmacy_, isStockOrder_) =>
    isStockOrder_ || (originalSellInPharmacy_ === sellInPharmacy_),
);

const editableStatusesForChiefOperator = createSelector(
  [allStatuses, orderStatusName],
  (allStatuses_, orderStatusName_) =>
    EDITABLE_STATUSES_FOR_CHIEF_OPERATOR
      // шукаємо редагуємі статуси
      .filter(item => item.from.includes(orderStatusName_))
      // створюємо масив для селекта виду:
      // [ 'поточний статус замовлення', 'статуси, на які можна змінити']
      .reduce((arr, statuses) => arr.concat(orderStatusName_, statuses.to), [])
      .map(status => allStatuses_[status] || {}),
);

const isEditableStatus = createSelector(
  [chiefOperator, editableStatusesForChiefOperator],
  (chiefOperator_, editableStatusesForChiefOperator_) =>
    chiefOperator_ && !!editableStatusesForChiefOperator_.length,
);

const editable = createSelector(
  [order, paramsId, isEditableRoleAccess],
  (item, id, isEditableRoleAccess_) => (
    !item.paused &&
    isEditableRoleAccess_ &&
    ((id === 'create') || EDITABLE_STATUSES.includes(item.status.name))
  ),
);
const isPause = createSelector(
  pauseParam,
  pauseP => pauseP.isSetPauseTime,
);

// Якщо товар був раніше замовлений, то потрібно змінити склад.
// Розрахунок:
// 1) Новий склад = кількість попереднього замовлення + склад
// 2) Якщо попередній результат мінусовий - то повертаємо 0, інакше - результат

// Розрахунок відбувається на залишку центрального складу або на аптеці, в залежності
// від замовлення на склад або аптеку
const nomenclatureWithUpdateRemainders = createSelector(
  nomenclatureEnt,
  order,
  editable,
  isStockOrder,
  (items, order_, isOrderEditable, isStockOrder_) => Object.keys(items).reduce((obj, key) => {
    const item = items[key];
    const lastItemHistory = getLastItemHistory(order_.nomenclatureHistory, item.id);

    // Якщо товар був замовлений і замовлення не з татусом in_process_by_operator
    // in_process_by_operator - присваюється, коли приходить з майданчиків
    if (lastItemHistory && order_.status.name !== 'in_process_by_operator') {
      const lastItemHistoryQuantity =
        getLastItemHistoryQuantity(lastItemHistory, isOrderEditable);

      const stockRemainders = isStockOrder_
      ? lastItemHistoryQuantity + item.stockRemainders
      : item.stockRemainders;

      const pharmacyRemainders = !isStockOrder_
      ? lastItemHistoryQuantity + item.pharmacyRemainders
      : item.pharmacyRemainders;

      obj[key] = {
        ...item,
        stockRemainders:    stockRemainders < 0 ? 0 : stockRemainders,
        pharmacyRemainders: pharmacyRemainders < 0 ? 0 : pharmacyRemainders,
      };
    } else {
      obj[key] = item; // if added new product
    }

    return obj;
  }, {}),
);

// Динамічний розрахунок інформаційних дефіцитів
const nomenclatureWithoutInfoItem = createSelector(
  nomenclatureWithUpdateRemainders,
  isStockOrder,
  editable,
  isConsiderRealQuantity,
  isChangedQuanityField,
  (items, isStockOrder_, editable_, isConsiderRealQuantity_, isChangedQuanityField_) =>
    Object.keys(items).reduce((obj, key) => {
      const item = items[key];

      let quantity           = +item.quantity;
      let pharmacyRemainders = +item.pharmacyRemainders;
      let stockRemainders    = +item.stockRemainders;

      const sum = getSum(+item.price, quantity);

      if (!editable_) {
        obj[key] = { ...item, sum };
      } else {
        let quantityPharmacy = 0;
        let quantityStock = 0;

        if (isConsiderRealQuantity_ && (item.realQuantity || item.realQuantity === 0)) {
          if (isStockOrder_) {
            stockRemainders = item.realQuantity;
          } else {
            pharmacyRemainders = item.realQuantity;
          }
          // Якщо повернули на редагування і в номенклатури є поле *realQuantity*,
          // то в кількість замовлення потрібно підставити *realQuantity*.
          // Якщо відразу зменшують кількості по аптеці або складу (quantity > quantitySum),
          // то закальна кількість = *realQuantity*
          const quantitySum = (item.quantityStock || 0) + (item.quantityPharmacy || 0);
          if (
            (item.changedField === undefined || quantity > quantitySum) &&
            // Якщо потім користувач змінює дане поле, то умова не діє.
            !isChangedQuanityField_
          ) {
            quantity = item.realQuantity;
          }
        }

        if (isStockOrder_) {
          quantityStock = item.changedField === 'quantityStock'
            ? item.quantityStock
            // : getQuantityPharmacy(quantity, stockRemainders);
            : transformMinusValueToZero(
              getQuantityPharmacy(quantity - (item.quantityPharmacy || 0), stockRemainders),
            );

          quantityPharmacy = item.changedField === 'quantityPharmacy'
            ? item.quantityPharmacy
            : getQuanityStock(quantity, pharmacyRemainders, +quantityStock);
        } else {
          quantityPharmacy = item.changedField === 'quantityPharmacy'
            ? item.quantityPharmacy
            : transformMinusValueToZero(
              getQuantityPharmacy(quantity - (item.quantityStock || 0), pharmacyRemainders),
            );

          quantityStock = item.changedField === 'quantityStock'
            ? item.quantityStock
            : getQuanityStock(quantity, stockRemainders, +quantityPharmacy);
        }

        obj[key] = {
          ...item,
          quantity,
          quantityPharmacy: transformMinusValueToZero(+quantityPharmacy.toFixed(6)),
          quantityStock:    transformMinusValueToZero(+quantityStock.toFixed(6)),
          sum,
          quantityDeficit:  +getDeficit(+quantityPharmacy, +quantityStock, quantity).toFixed(6),
          pharmacyDeficit:  +(pharmacyRemainders - +quantityPharmacy).toFixed(6),
          stockDeficit:     +(stockRemainders - +quantityStock).toFixed(6),
        };
      }

      return obj;
    }, {}),
);

const isQuantityStock = createSelector(
  nomenclatureWithoutInfoItem,
  items => Object.values(items).some(item => item.quantityStock > 0),
);

// Розрахунок сум по стовпчиках
const nomenclatureTotal = createSelector(
  nomenclatureWithoutInfoItem,
  (items) => {
    const total = getNomenclatureTotal(Object.values(items));
    return {
      ...total,
      quantityDeficit: getDeficit(+total.quantityPharmacy, +total.quantityStock, +total.quantity),
      pharmacyDeficit: +total.pharmacyRemainders - +total.quantityPharmacy,
      stockDeficit:    +total.stockRemainders - +total.quantityStock,
    };
  },
);

// В товарі, який повернувся на редагування по причині менших залишків ніж в замовленні,
// Пасивний(червона строка): змінюємо кількість на останню в історії замовлення
// Активний(біла строка): зінюємо на кількість наявного товару
const nomenclature = createSelector(
  nomenclatureWithoutInfoItem,
  nomenclatureWithDeficit,
  order,
  editable,
  isStockOrder,
  isConsiderRealQuantity,
  (items, itemsDeficit, order_, isOrderEditable, isStockOrder_, isConsiderRealQuantity_) =>
    Object.keys(items).reduce((arr, key) => {
      const item = {
        ...items[key],
        // Робимо *realQuantity* пусто, щоб не спрацьовували ліміти на збільшення кількості товару
        // Вичисляєтья в OrdersItem/Components/RowGoods
        realQuantity: isConsiderRealQuantity_ ? items[key].realQuantity : '',
      };
      const itemDeficit = itemsDeficit[key];

      if (itemDeficit) {
        const lastItemHistory = getLastItemHistory(order_.nomenclatureHistory, itemDeficit.id);
        const quantityForItemWithDeficit =
          getLastItemHistoryQuantity(lastItemHistory, isOrderEditable);

        if (quantityForItemWithDeficit) {
          arr.push({
            ...itemDeficit,
            id:       `deficit_${itemDeficit.id}`,
            quantity: quantityForItemWithDeficit,
            isInfo:   true,
            sum:      isOrderEditable && isConsiderRealQuantity_ ? 'В наявності:' : '',

            [isStockOrder_ ? 'stockRemainders' : 'pharmacyRemainders']:
              isConsiderRealQuantity_ ? itemDeficit.realQuantity : '',
          });
        }
      }

      if (!isOrderEditable) {
      // количество в строке заказаного товара (белая строка)
        arr.push({
          ...item,
          quantity: (itemDeficit && itemDeficit.quantity) || item.quantity,
        });
      } else {
        arr.push(item);
      }
      return arr;
    }, []),
);

const deliverySchedules = createSelector(
  pharmacies,
  order,
  isStockOrder,
  isQuantityStock,
  () => moment().format('d'),
  () => moment().format('H'),
  () => moment().format('m'),
  (items, { sellInPharmacy }, isStockOrder_, isQuantityStock_, currentDay, currentHour, currentMinute) => {
    if (isStockOrder_ || isQuantityStock_) {
      const deliverySchedules =
      (items.find(item => item.id == sellInPharmacy) || {}).deliverySchedules;
      if (!deliverySchedules) {
        return 'Графік доставки не встановлений!';
      }
      let deliveryTime = deliverySchedules.find(d => (
        d.day == currentDay &&
        (currentHour < d.hour || (currentHour == d.hour && currentMinute <= d.minute))
      ));
      if (!deliveryTime) {
        deliveryTime = deliverySchedules.find(d => d.day > currentDay);
      }
      if (!deliveryTime) {
        deliveryTime = deliverySchedules[0];
      }

      return deliveryTime
        ? `${daysOfTheWeek[deliveryTime.deliveryDay]} ${deliveryTime.deliveryHour} год ${convertToTwoSimbols(deliveryTime.deliveryMinute)} хв`
        : 'Графік доставки не встановлений!';
    }
    return '';
  },
);

export default createStructuredSelector({
  isSavingStatus,
  isEditableStatus,
  isChangedStatus,
  editableStatusesForChiefOperator,
  isShowAllPharmacies,
  pauseParam,
  isFiltersDebounce,
  errors,
  order,
  nomenclatureEnt,
  nomenclature,
  nomenclatureTotal,
  editable,
  isStockOrder,
  isQuantityStock,
  isPause,
  cancelParam,
  deliverySchedules,
  nomenclatureIds: createSelector(
    nomenclatureEnt,
    items => Object.keys(items),
  ),
  pharmacies: createSelector(
    pharmacies, isShowAllPharmacies,
    (items, isShowAllPharmacies_) => (isShowAllPharmacies_
      ? items
      : items.filter(({ active, connected }) => active && connected)
    ),
  ),
  // Якщо з аптекою немає звязку або вона неактивна, то виводимо повідомлення
  pharmacyError: createSelector(
    [pharmacies, sellInPharmacy],
    (items, sellInPharmacy_) => {
      if (items.length && sellInPharmacy_ !== -1) {
        const { active, connected } = items.find(item => item.id === sellInPharmacy_) || {};
        const msg = [
          ...(!active && 'не активна'),
          ...(!connected && 'немає зв\'язку'),
        ];
        return msg.join(msg.length > 1 ? ', ' : ' ');
      }
      return '';
    },
  ),
});
