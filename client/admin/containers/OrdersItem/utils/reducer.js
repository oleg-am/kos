import { EDITABLE_STATUSES } from '../schemas';
import transformMinusValueToZero from '../../../helpers/transformMinusValueToZero';

export const getNomenclatureIfStock = nomenclature => (
  Object.keys(nomenclature).reduce((obj, key) => {
    obj[key] = {
      ...nomenclature[key],
      quantityPharmacy: 0,
      quantityStock:    nomenclature[key].quantity,

    };
    return obj;
  }, {})
);

export const getNomenclatureWithDeficit = (nomenclature, status) => (
  Object.keys(nomenclature).reduce((obj, key) => {
    const item = nomenclature[key];
    const { quantity, deficit } = item;

    if (deficit) {
      obj.nomenclatureWithDeficit[key] = item;
      obj.nomenclature[key] = {
        ...item,
        quantity: transformMinusValueToZero(
          EDITABLE_STATUSES.includes(status) ? quantity - deficit : quantity
        ),
      };
    } else {
      obj.nomenclature[key] = item;
    }

    return obj;
  }, { nomenclature: {}, nomenclatureWithDeficit: {} })
);
