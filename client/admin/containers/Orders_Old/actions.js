import * as types from './constants';
import { ORDERS_ARRAY } from './schemas.js';

import { normalize } from 'normalizr';

export const loadData = (data) => (
  {type: types.LOAD_DATA, data: normalize(data, ORDERS_ARRAY)}
)

export const addGoods = (orderId, data) => {
  return {
    type: types.ADD_GOODS,
    orderId,
    data,
  }
}
// export const changeGoods = (orderId, data) => {
//   return {
//     type: types.CHANGE_GOODS,
//     orderId,
//     data,
//   }
// }
export const changeCount = (orderId, productId, stock, count) => {
  return {
    type: types.CHANGE_COUNT,
    orderId,
    productId,
    stock,
    count,
  }
}
export const changeOrderParam = (orderId, param, value) => ({
    type: types.CHANGE_ORDER_PARAM,
    orderId,
    param,
    value,
  })

export const changeStatus = (orderId, status, statusColor) => ({
  type: types.CHANGE_STATUS, orderId, status, statusColor
})
export const addOrder = (id, order) => ({
  type: types.ADD_ORDER, id, order
})
export const setFilter = (filter, value) => ({
  type: types.SET_FILTER, filter, value
})
