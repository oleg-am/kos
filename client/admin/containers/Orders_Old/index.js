import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Link, browserHistory } from 'react-router'
import { push } from 'react-router-redux';

import ReactPaginate from 'react-paginate';
import DatePicker from 'react-datepicker';

// import * as dictionaryActions from '../actions/nomenclatureActions';
// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';
// import Table from '../../components/Table/index';
// import Row from '../../components/common/Row';
// import CategoryTree from '../components/dictionaries/nomenclature/Category-tree/CategoryTree';
// import Archive from '../components/dictionaries/nomenclature/Archive/Archive';

import { getRandomOrders, editableStatusies, getShemaOrder, statusObj } from './data';
import { loadData, addOrder, setFilter } from './actions';
import { getOrder, fromArrayToObj } from './selectors';

// console.log(getRandomOrders(2));

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import {
  priceFormatter,
  dateFormatter,
  timeFormatter,
  statusFormatter,
  extraDataObjFormatter
} from '../../components/Formatters';

class Orders extends React.Component {

  constructor(props, context) {
    super(props, context);
    const {loadData, getRandomOrders, startDate, endDate} = this.props;

    this.getNewPage = this.getNewPage.bind(this);
    if (!props.orders.length) {
      loadData(getRandomOrders(10, startDate, endDate));
    }
  }
  componentWillReceiveProps(nextProps) {
    console.log('nextProps: ', nextProps);
    const { loadData, getRandomOrders, startDate, endDate } = this.props;
    // console.group('componentwillreceiveprops')
    //   console.log('startDate: ', startDate)
    //   console.log('nextProps.startDate: ', nextProps.startDate)
    //   console.log('diff: ', startDate==nextProps.startDate);
    //   console.log('endDate: ', endDate)
    //   console.log('nextProps.endDate: ', nextProps.endDate)
    //   console.log('diff: ', endDate==nextProps.endDate);
    // console.groupEnd()
    if(startDate!=nextProps.startDate || endDate!=nextProps.endDate) {
      loadData(getRandomOrders(10, nextProps.startDate, nextProps.endDate));
    }
  }
  getNewPage(data) {
    let page = data.selected + 1;
    this.props.actions.loadNomenclature(page);
  }
  handleLinkToOrder = (id) => () => {
    browserHistory.push(`/admin/orders/${id || ''}`);
  }
  handleLoadData() {
    const { loadData, getRandomOrders, startDate, endDate } = this.props;
    loadData(getRandomOrders(10, startDate, endDate));
  }
  handleAddOrder() {
    this.props.addOrder(this.props.nextOrderId, this.props.getShemaOrder());
    browserHistory.push(`/admin/orders/${this.props.nextOrderId || ''}`);
  }
  handleSetFilter(filter, value) {
    console.log(filter, value);
    this.props.setFilter(filter, value);
  }
  onRowSelect = (row, isSelected) => {
    console.log(row);
    console.log("selected: " + isSelected)
    browserHistory.push(`/admin/orders/${row.id || ''}`);
  }

  render() {
    const { orders, statuses, orderIdForEditing, newOrderId, pharmacies, startDate, endDate } = this.props;

var selectRowProp = {
  mode: "checkbox",
  clickToSelect: true,
  // bgColor: "rgb(238, 193, 213)",
  // onSelect: this.onRowSelect,
  hideSelectColumn: true
};
const tableOptions = {
  onRowClick(row) {
    console.log('onRowClick', row);
    // console.log("selected: " + isSelected)
    browserHistory.push(`/admin/orders/${row.id || ''}`);
  }
}
    return (
      <div className="page-content" style={{paddingTop: '20px'}}>
        <Breadcrumbs title="" components={{
          title: 'Замовлення',
          links: [
            {
              name: 'Меню',
              to: `/admin/reports`
            }, {
              name: 'Замовлення'
            }
          ]
        }}/>
        {/* <PortletBody> */}

        <Portlet

          actionsLeft={[
            <span style={{marginRight: '10px'}} key="1">Період відображення:</span>,
            <DatePicker
              key="2"
              selected={startDate}
              selectsStart
              startDate={startDate}
              endDate={endDate}
              maxDate={endDate}
              todayButton={'Сьогодні'}
              onChange={this.handleSetFilter.bind(this, 'startDate')}
            />,
            <DatePicker
              key="3"
              selected={endDate}
              selectsEnd
              startDate={startDate}
              endDate={endDate}
              maxDate={endDate}
              todayButton={'Сьогодні'}
              onChange={this.handleSetFilter.bind(this, 'endDate')}
            />

          ]}
          actionsRight={[
            <Link key="1" onClick={this.handleLinkToOrder(orderIdForEditing)} className="btn btn-sm green-meadow">
              Взяти замовлення
            </Link>,
            <Link key="2" to="/admin/orders/create" className="btn btn-sm green-meadow">
              Створити замовлення
            </Link>,
            <Link key="3" onClick={this.handleLoadData.bind(this)} className="btn btn-sm blue-sharp">
              <i className="fa fa-refresh"></i>
              Оновити дані
            </Link>,
          ]}
        >

          <BootstrapTable options={tableOptions} data={orders} striped={true} hover={true} height={`${window.innerHeight-315}`} search={true}>
            <TableHeaderColumn width="50" dataField="id" isKey={true} dataAlign="center" dataSort={true}>№</TableHeaderColumn>
            <TableHeaderColumn width="90" dataField="date" dataAlign="center" dataSort={true} dataFormat={dateFormatter}>Дата замовлення</TableHeaderColumn>
            <TableHeaderColumn width="90" dataField="time" dataAlign="center" dataSort={true} dataFormat={timeFormatter}>Час замовлення</TableHeaderColumn>
            <TableHeaderColumn width="100" dataField="pharmacy" dataAlign="center"  dataFormat={extraDataObjFormatter('name', null)} formatExtraData={pharmacies} dataSort={true}>Аптека(и)</TableHeaderColumn>
            <TableHeaderColumn width="90" dataField="sum" dataAlign="center"  dataSort={true}>Сума</TableHeaderColumn>
            <TableHeaderColumn width="100" dataField="fullName" dataAlign="center"  dataSort={true} columnTitle={true}>Клієнт ПІБ</TableHeaderColumn>
            <TableHeaderColumn width="100" dataField="phone" dataAlign="center"  dataSort={true} columnTitle={true}>Конт.тел</TableHeaderColumn>
            <TableHeaderColumn width="100" dataField="email" dataAlign="center"  dataSort={true} columnTitle={true}>E-mail</TableHeaderColumn>
            <TableHeaderColumn width="100" dataField="operator" dataAlign="center"  dataSort={true} columnTitle={true}>Оператор</TableHeaderColumn>
            <TableHeaderColumn width="100" dataField="status"  dataAlign="center" dataSort={true} dataFormat={statusFormatter} formatExtraData={statuses} filter={{type: "SelectFilter", options: statuses}}>Статус</TableHeaderColumn>
            <TableHeaderColumn width="100" dataField="source"  dataAlign="center" dataSort={true} columnTitle={true}>Джерело</TableHeaderColumn>
            <TableHeaderColumn width="100" dataField="comment" dataAlign="center"  dataSort={true} columnTitle={true}>Коментар</TableHeaderColumn>
          </BootstrapTable>

        </Portlet>

        {/* <Portlet components={{
          title: '',
          actionsLeft: [
            <span style={{marginRight: '10px'}} key="1">Період відображення:</span>,
            <DatePicker
              key="2"
              selected={startDate}
              selectsStart    startDate={startDate}
              endDate={endDate}
              onChange={this.handleSetFilter.bind(this, 'startDate')}
            />,
            <DatePicker
              key="3"
              selected={endDate}
              selectsEnd    startDate={startDate}
              endDate={endDate}
              onChange={this.handleSetFilter.bind(this, 'endDate')}
            />

          ],
          actionsRight: [
            <Link key="1" onClick={this.handleLinkToOrder(orderIdForEditing)} className="btn btn-sm green-meadow">
              Взяти замовлення
            </Link>,
            <Link key="2" onClick={this.handleAddOrder.bind(this)} className="btn btn-sm green-meadow">
              Створити замовлення
            </Link>,
            <Link key="3" onClick={this.handleLoadData.bind(this)} className="btn btn-sm blue-sharp">
              <i className="fa fa-refresh"></i>
              Оновити дані
            </Link>,
          ]
        }}> */}
          {/* <table className="table">
            <colgroup>
              <col width="15%" />
              <col width="40%" />
              <col width="20%" />
              <col width="25%" />
            </colgroup>
            <tbody>
            <tr>
              <td></td>
              <td>
                <input type="text" className="form-control input-md input-inline" placeholder="Фильтр по названию"/>
              </td>
              <td>
                <input type="text" className="form-control input-md input-inline" placeholder="Фильтр по производителю"/>
              </td>
              <td></td>
            </tr>
            </tbody>
          </table> */}

          {/* <Table>
            {orders.map(item =>
              <Row
                statusClass={item.statusColor}
                key={item.id}
                data={item}
                pharmacies={pharmacies}
                click={this.handleLinkToOrder(item.id)}/>
            )}
          </Table>
        </Portlet> */}

        {/* <div className="center">
								<ReactPaginate previousLabel={"Предыдущая"}
											   nextLabel={"Следующая"}
											   breakLabel={<a href="">...</a>}
											   breakClassName={"break-me"}
											  //  pageNum={this.props.pageNum}
									   		   marginPagesDisplayed={2}
											  //  forceSelected={this.props.paging.page - 1}
											   pageRangeDisplayed={5}
											   clickCallback={this.getNewPage}
											   containerClassName={"pagination"}
											   subContainerClassName={"pages pagination"}
											   activeClassName={"active"} />
							</div> */}
        {/* </PortletBody> */}
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  console.log('state: ', state);
  const { orders: ordersObj, goods: goodsObj, pharmacy: pharmacies} = state.ordersOld.entities;
  const { startDate, endDate} = state.ordersOld.filters;

  const result = state.ordersOld.result;
  // const pharmacies = Object.keys(pharmaciesObj).map(pharmacyId => {
  //   return pharmaciesObj[pharmacyId];
  // })

  const orders = result.map(val => {
    // console.log('ordersObj[val]:',ordersObj[val]);
    // console.log('state.ordersOld.pharmacy:',state.ordersOld.entities.pharmacy);
    // console.log('ordersObj[val].pharmacy:',ordersObj[val].pharmacy);
    // console.log('state.ordersOld.entities.pharmacy[ordersObj[val].pharmacy]name:',state.ordersOld.entities.pharmacy[ordersObj[val].pharmacy].name);

    return {
      ...ordersObj[val],
      // pharmacy: (state.ordersOld.entities.pharmacy[ordersObj[val].pharmacy].name || '')
    }
  })
  console.log(orders);
  const orderIdForEditing = getOrder(ordersObj, result, editableStatusies);
  return {
    orders,
    startDate,
    endDate,
    pharmacies,
    orderIdForEditing,
    editable: false,
    newOrderId: state.newOrderId,
    nextOrderId: state.ordersOld.result[state.ordersOld.result.length - 1] + 1,
    // pageNum: state.ordersOld.data.length/5,
    // paging: state.nomenclature.paging || {},
  };
}

function mapDispatchToProps(dispatch) {
  return {
    // actions: bindActionCreators(dictionaryActions, dispatch),
    loadData: bindActionCreators(loadData, dispatch),
    addOrder: bindActionCreators(addOrder, dispatch),
    setFilter: bindActionCreators(setFilter, dispatch),
    getRandomOrders,
    getOrder,
    getShemaOrder,
    statuses: fromArrayToObj(statusObj, 'name'),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
