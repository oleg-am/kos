// import * as types from '../actions/actionTypes';
import * as types from './constants';
import { getRandomOrders } from './data';
import { fromArrayToObj } from './selectors';

import faker from 'faker';
import moment from '../../helpers/moment';

const getOrdersId = (data) => (
  data.map(obj => obj.id)
)

let dateNow = moment(moment().format('YYYY-MM-DD'));
// dateNow = moment(dateNow.format('YYYY.MM.DD'));
// dateNow = moment(dateNow.format('YYYY..DD.MM'));

const initialState = {
  entities: {
    orders: {},
    goods: {},
    pharmacy: {},
  },
  result: [],
  newOrderId: '',
  filters: {
    startDate: dateNow,
    endDate: dateNow
  }
  // ordersId: []
}

export default function ordersReducer(state = initialState, action) {
    switch (action.type) {
      case types.LOAD_DATA:
          return {...state, ...action.data};
          // ordersId: getOrdersId(action.data)
      case types.SET_FILTER:
          return {
            ...state,
            filters: setFilter(state.filters, action),
          };
      case types.ADD_GOODS:
          return {...state,
            entities: {
              ...state.entities,
              goods: {
                ...state.entities.goods,
                ...fromArrayToObj(action.data),
              },
              orders: {
                ...state.entities.orders,
                [action.orderId]: {
                  ...state.entities.orders[action.orderId],
                  goods: [
                    ...state.entities.orders[action.orderId].goods,
                    ...action.data.map(val => val.id),
                  ],
                },
              },
            },
          };
      // case types.CHANGE_GOODS:
      //     return {...state,
      //       entities: {
      //         ...state.entities,
      //         goods: {
      //           ...state.entities.goods,
      //           ...fromArrayToObj(action.data),
      //         },
      //       },
      //     };
      case types.CHANGE_STATUS:
          return {...state,
            entities: {
              ...state.entities,
              orders: {
                ...state.entities.orders,
                [action.orderId]: {
                  ...state.entities.orders[action.orderId],
                  status: action.status,
                  statusColor: action.statusColor,
                },
              },
            },
          };
      case types.CHANGE_ORDER_PARAM:
          return {...state,
            entities: {
              ...state.entities,
              orders: {
                ...state.entities.orders,
                [action.orderId]: {
                  ...state.entities.orders[action.orderId],
                  [action.param]: action.value,
                },
              },
            },
          };
      case types.ADD_ORDER:
        const order = {
          ...action.order,
          id: action.id,
        }
          return {...state,
            newOrderId: action.id,
            result: [
              ...state.result,
              action.id
            ],
            entities: {
              ...state.entities,
              orders: {
                ...state.entities.orders,
                [action.id]: order,
              },
            },
          };
      case types.CHANGE_COUNT:
      const goods = {
        ...state.entities.goods,
        [action.productId]: {
          ...state.entities.goods[action.productId],
          [action.stock]: action.count,
          customStockChange: ~(['countPharmacy','countStorage'].indexOf(action.stock)) && action.stock

        }
      };
        return {
          ...state,
          entities: {
            ...state.entities,
            goods,
            orders: {
              ...state.entities.orders,
              [action.orderId]: {
                ...state.entities.orders[action.orderId],
                sum: state.entities.orders[action.orderId].goods.reduce((sum, next) => {
                  return sum + (goods[next].quantity*goods[next].reservePrice)
                }, 0),
              },
            },

          },
        };

        default:
          return state;
    }
}

const setFilter = (state, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        [action.filter]: action.value,
      }
      break;
    default:

  }
}

function updateGoods(array, index, action) {
    return array.map( (item, i) => {
        if(i != index) {
            // This isn't the item we care about - keep it as-is
            return item;
        }
        // Otherwise, this is the one we want - return an updated value
        return {
            ...item,
            goods: item.goods.concat(action.data)
        };
    });
}
function updateObjectInArray(array, index, action) {
    return array.map( (item, i) => {
        if(i != index) {
            // This isn't the item we care about - keep it as-is
            return item;
        }
        // Otherwise, this is the one we want - return an updated value
        return {
            ...item,
            goods: updateGoods2(item.goods, action)
        };
    });
}
function updateGoods2(array, action) {
    return array.map( (item, i) => {
        if(item.id != action.productId) {
            // This isn't the item we care about - keep it as-is
            return item;
        }
        // Otherwise, this is the one we want - return an updated value
        return {
            ...item,
            [action.stock]: action.count,
        };
    });
}


function addGoods(obj, items) {
  let newObj = {...obj};
  return {
    ...obj,
    goods: newObj.goods.concat(items)
  }
}
