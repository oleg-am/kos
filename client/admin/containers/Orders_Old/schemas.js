import { Schema, arrayOf } from 'normalizr';

const ORDER = new Schema('orders', { idAttribute: 'id' });
const PRODUCT = new Schema('goods', { idAttribute: 'id' });
const PHARMACY = new Schema('pharmacy', { idAttribute: 'id' });

ORDER.define({
    // author: postAuthorSchema,
    goods: arrayOf(PRODUCT),
    pharmacy: PHARMACY
});

export const ORDERS_ARRAY = arrayOf(ORDER);
