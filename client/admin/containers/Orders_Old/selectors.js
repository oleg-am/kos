export const getOrderById = (data, id) => {
  const order = data.filter(order => {
    return order.id == id;
  })
  return order[0];
}

export const getOrder = (ordersObj, result, statusies) => {
  let order;
  result.some(val => {
    if (statusies.indexOf(ordersObj[val].status) != -1) {
      order = val;
      return true;
    }
  })
  return order;
}

export const fromArrayToObj = (data, field) => {
  return data.reduce((obj, data, i) => {
    obj = obj || {};
    obj[(data.id || i)] = (data[field] || data);
    return obj
  }, 0 );
}
