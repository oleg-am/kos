import * as types from './constants';

import { createParams } from '../../helpers/api';
import { PHARMACY_ARR } from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const edit = nomenclature => ({
  type: types.EDIT,
  nomenclature,
});

export const loadPharmacies = filters => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/pharmacies${createParams(filters)}`),
  schema:  PHARMACY_ARR,
});
