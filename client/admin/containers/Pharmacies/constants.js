const name = 'pharmacies';

export const LOAD_REQUEST = `${name}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${name}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${name}/LOAD_FAILURE`;

export const SET_FILTER = `${name}/SET_FILTER`;
export const EDIT = `${name}/EDIT`;

export default name;
