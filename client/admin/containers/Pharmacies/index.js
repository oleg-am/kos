import React from 'react';
import { connect } from 'react-redux';

import { Link, browserHistory } from 'react-router';
// import { push } from 'react-router-redux';

// import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
// import Select from 'react-select';

import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';

// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';
import Paginate from '../../components/Paginate';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import LimitSelect from '../../components/LimitSelect';

import IconCheck from '../../elements/IconCheck';
import IconStatus from '../../elements/IconStatus';
import LinkSearch from '../../elements/LinkSearch';

class Pharmacies extends React.Component {

  constructor(props, context) {
    super(props, context);
    const { loadPharmacies, filters } = this.props;

    this.debounceLoadPharmacies = _debounce(loadPharmacies, 400);

    loadPharmacies(filters);
  }

  componentWillReceiveProps(next) {
    const { isFiltersDebounce, filters: nextFilters } = next;
    const { loadPharmacies, filters } = this.props;

    if (filters != nextFilters) {

      const newfilters = filters.page == nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters

      isFiltersDebounce
      ? this.debounceLoadPharmacies(newfilters)
      : loadPharmacies(newfilters);
    }
  }
  remainders = pharmacies => {
    pharmacies.id && browserHistory.push(`/admin/pharmacies/${pharmacies.id}/remainders`);
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  refreshData = () => {
    this.props.loadPharmacies(this.props.filters);
  }
  render() {
    const { errors, pharmacies, paging, filters } = this.props;

    const check = cell => (
      <IconCheck isValue={!!cell} />
    );

    const selectTrueFalse = (onChange, value, values = [true, false]) => (
      <select value={value} onChange={onChange} className="form-control">
        <option key="All" value="">Всі</option>
        <option key="True" value={values[0]}>Активний</option>
        <option key="False" value={values[1]}>Не активний</option>
      </select>
    );

    const dataFormatPhones = phoneType => (cell = []) => (
      <ul style={{ listStyle: 'none', padding: 0, whiteSpace: 'normal' }}>
        {cell.filter(item => item.phoneType === phoneType).map(item => (
          <li key={item.id}> {item.number} </li>
        ))}
      </ul>
    );

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          components={{
            title: 'Всі аптеки',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Аптеки',
              },
            ],
          }}
        />

        <Portlet
          actionsLeft={[
            <LimitSelect key="actionsLeft_1" optionsValue={[10, 15, 20]} value={filters.limit} onChange={this.setFilter('limit')} />,
          ]}

          actionsRight={[
            <Link key="actionsRight_2" to="/admin/pharmacies/map" className="btn btn-sm green-meadow" style={{ marginRight: '10px' }} title="Перейти на мапу">
              <i className="fa fa-map-marker" />
              На мапі
            </Link>,
            <Link key="actionsRight_3" onClick={this.refreshData} className="btn btn-sm blue-sharp">
              <i className="fa fa-refresh" title="Оновити" />
            </Link>,
          ]}
        >

          <CustomTable
            tableClass="table table-striped table-bordered table-hover"
            data={errors.length ? [] : pharmacies}
            striped
            hover
            pagination
            filters
            onDblRowClick={this.remainders}
          >
            <HeaderColumn dataField="id" width="5%" dataAlign="center" isKey> № </HeaderColumn>
            <HeaderColumn dataField="name" headerAlign="center" width="25%" filters={<input type="text" value={filters.filters.name.value} onChange={this.setFilter(['filters', 'name'], true)} className="form-control" placeholder="Введіть найменування" />}> Найменування </HeaderColumn>
            <HeaderColumn dataField="address" headerAlign="center" width="25%" filters={<input type="text" value={filters.filters.address.value} onChange={this.setFilter(['filters', 'address'], true)} className="form-control" placeholder="Введіть адресу" />}> Адреса </HeaderColumn>
            <HeaderColumn dataField="phones" dataAlign="center" filters="Міський" dataFormat={dataFormatPhones('city')}> Телефон </HeaderColumn>
            <HeaderColumn dataField="phones" dataAlign="center" filters="Внутрішній" dataFormat={dataFormatPhones('inner')}> Телефон </HeaderColumn>
            <HeaderColumn dataField="deliverySchedules" dataAlign="center" dataFormat={check} filters={selectTrueFalse(this.setFilter(['filters', 'deliverySchedules']), filters.filters.deliverySchedules.value, ['not null', 'null'])} > Графік доставки </HeaderColumn>
            <HeaderColumn dataField="connected" dataAlign="center" dataFormat={check} filters={selectTrueFalse(this.setFilter(['filters', 'connected']), filters.filters.connected.value)} > Зв'язок </HeaderColumn>
            <HeaderColumn
              dataField="active" dataAlign="center" filters={selectTrueFalse(this.setFilter(['filters', 'active']), filters.filters.active.value)}
              dataFormat={cell => (
                <IconStatus isValue={!!cell} />
              )}
            > Статус </HeaderColumn>
            <HeaderColumn
              dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle"
              dataFormat={id => (
                <LinkSearch to={`/admin/pharmacies/${id}/remainders`} title="Залишки" />
              )}
            > Залишки </HeaderColumn>
            <HeaderColumn
              dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle"
              dataFormat={id => (
                <Link to={`/admin/pharmacies/${id}`} className="btn btn-icon-only yellow" title="Редагувати">
                  <i className="fa fa-edit" />
                </Link>
              )}
            />

          </CustomTable>

        </Portlet>

        {!errors.length && !!pharmacies.length &&
          <Paginate
            pageNum={paging.totalPages}
            clickCallback={this.setFilterPage}
            forceSelected={paging.page - 1}
          />
        }
      </div>
    );
  }
}

export default connect(selectors, actions)(Pharmacies);
