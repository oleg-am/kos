import { combineReducers } from 'redux';
import * as types from './constants';

import { setFiltersValue } from '../../helpers/setObjValue';

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};
const pharmaciesEntities = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities.pharmacies;
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data;
    default:
      return state;
  }
};

const paging = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.paging;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const filtersInit = {
  page:  1,
  limit: 10,
  with: {
    '*': {
      value: '*',
    },
  },
  order_by: {
    id: {
      value: 'ASC',
    },
  },
  filters: {
    name: {
      comparison: 'like',
      value:      '',
      modifier:   'lower',
    },
    address: {
      comparison: 'like',
      value:      '',
      modifier:   'lower',
    },
    deliverySchedules: {
      value: '',
    },
    connected: {
      value: '',
    },
    active: {
      value: '',
    },
  },
};

const filters = (state = filtersInit, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    default:
      return state;
  }
};

const activePharmacies = (state = {}, action) => {
  switch (action.type) {
    case types.EDIT:
      return action.pharmacies;
    default:
      return state;
  }
};

export default combineReducers({
  errors, // []
  pharmaciesEntities, // {}
  ids, // []
  paging, // {}
  activePharmacies, // {}
  filters, // {}
  isFiltersDebounce, // false
});
