import { Schema, arrayOf } from 'normalizr';

const pharmacy = new Schema('pharmacies');
const network = new Schema('network', { idAttribute: 'name' });

// const inner  = new Schema('inner');
// const city   = new Schema('city');
// const mobile = new Schema('mobile');

// const phones = {
//   inner,
//   city,
//   mobile,
// };

pharmacy.define({
  network,
  // phones: arrayOf(phones, { schemaAttribute: 'phoneType' }),

});

export const PHARMACY_ARR = { data: arrayOf(pharmacy) };
