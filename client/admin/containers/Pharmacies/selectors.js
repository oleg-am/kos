import { createSelector, createStructuredSelector } from 'reselect';

import REDUCER from './constants';

const pharmaciesEntities = state => state[REDUCER].pharmaciesEntities;
// const pharmaciesIds      = state => state[REDUCER].ids;

const filtersState  = state => state[REDUCER].filters;

const paging            = state => state[REDUCER].paging;
const isFiltersDebounce = state => state[REDUCER].isFiltersDebounce;
const errors            = state => state[REDUCER].errors;

const pharmacies = createSelector(
  pharmaciesEntities,
  items => Object.values(items),
);

export const filters = createSelector(filtersState, items => items);

export default createStructuredSelector({
  pharmacies,
  filters,
  paging,
  isFiltersDebounce,
  errors,
});
