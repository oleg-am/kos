import * as types from './constants';

export const loadData = (data) => (
  {type: types.LOAD_DATA, data}
)
export const toogleSelectedItems = (checked, id) => (
  {type: types.TOOGLE_CHECKED_ITEMS, checked, id}
)
