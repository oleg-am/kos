import faker from 'faker';

export const getRandomPharmacies = (count) => (
  Array.from({ length: count }, (v, k) => k).map((_, i) => ({
    // id: faker.random.number(),
    id: 1000+i, //faker.random.number()
    name: faker.company.companyName(),
    reservePrice: faker.finance.amount(10, 50),
    pharmacyStock: faker.finance.amount(0, 10),
    // storageStock: faker.finance.amount(10, 50),

  })
))
