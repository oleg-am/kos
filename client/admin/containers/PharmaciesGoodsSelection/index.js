import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ReactPaginate from 'react-paginate';

import { browserHistory, Link } from 'react-router'

import RowPharmaciesGoodsSelection from '../../components/common/RowPharmaciesGoodsSelection';

// import { addGoods } from '../../reducers/ordersReducer';

import { getRandomPharmacies } from './data';
import * as actions from './actions';
import { getSelectedGoods } from './selectors';

import { addGoods } from '../Orders/actions';
import { getOrderById } from '../Orders/selectors';

class PharmaciesGoodsSelection extends React.Component {

    constructor(props) {
        super(props);
        const { loadData, getRandomPharmacies, toogleSelectedItems } = this.props;
        this.getNewPage = this.getNewPage.bind(this);
        console.log('props: ', props);
        loadData(getRandomPharmacies(10));
    }

    getNewPage(data) {
        let page = data.selected + 1;
        this.props.actions.loadNomenclature(page);
    }
    handlerClick(id) {
      console.log('hi' + id);
      browserHistory.push(`/admin/orders/${id}`);
    }
    handlerCheck(id, e) {
      this.props.toogleSelectedItems(e.target.checked, id);
    }
    handlerAddGoods(id, selectedGoods) {
      this.props.addGoods(id, selectedGoods);
    }

    render() {
      console.log('this.props:', this.props);
      const { productName, routeParams, selectedGoods } = this.props;

      return (
			<div className="page-content" style={{paddingTop: '20px'}}>
            <div className="breadcrumbs">
              <h1> Подбор товара <span style={{color: '#ed6b75'}}>"{productName}"</span> в других аптеках</h1>
                <ol className="breadcrumb">
                  <li>
                    <Link className="" to={`/admin/orders`}>
                      Меню
                    </Link>
                  </li>
                  <li>
                    <Link className="" to={`/admin/orders`}>
                      Заказы
                    </Link>
                  </li>
                  <li>
                    <Link className="" to={`/admin/orders/${routeParams.id}`}>
                      № {routeParams.id}
                    </Link>
                  </li>
                    <li className="active">Подбор товара в других аптеках</li>
                </ol>
              </div>

							<table className="table">
								<colgroup>
									<col width="15%" />
									<col width="40%" />
									<col width="20%" />
									<col width="25%" />
								</colgroup>
								<tbody>
                <tr>
									<td></td>
									<td>
										<input type="text" className="form-control input-md input-inline" placeholder="Фильтр по названию"/>
									</td>
									<td>
										<input type="text" className="form-control input-md input-inline" placeholder="Фильтр по производителю"/>
									</td>
									<td></td>
								</tr>
                </tbody>
							</table>
							<div className="table-scrollable">
								<table className="table table-hover">
									<colgroup>
										<col width="5%" />
										<col width="45" />
										<col width="25%" />
										<col width="25%" />

									</colgroup>
									<thead>
										<tr>
											<th>  </th>
											<th> Аптека </th>
											<th> Остаток в аптеке </th>
											<th> Цена резерва </th>
											{/* <th name="publish-head"> Публикация </th> */}
										</tr>
									</thead>
									<tbody>
									{this.props.goodsSelectionInPharmacies.map((item) => {
										return <RowPharmaciesGoodsSelection key={item.id} data={item} checkHandler={this.handlerCheck.bind(this, item.id)} click={this.handlerClick.bind(item.id)} />
									})}
									</tbody>
								</table>
							</div>
							<div className="center">
								<ReactPaginate previousLabel={"Предыдущая"}
											   nextLabel={"Следующая"}
											   breakLabel={<a href="">...</a>}
											   breakClassName={"break-me"}
											  //  pageNum={this.props.pageNum}
									   		   marginPagesDisplayed={2}
											  //  forceSelected={this.props.paging.page - 1}
											   pageRangeDisplayed={5}
											   clickCallback={this.getNewPage}
											   containerClassName={"pagination"}
											   subContainerClassName={"pages pagination"}
											   activeClassName={"active"} />
							</div>
              <Link onClick={this.handlerAddGoods.bind(this, routeParams.id, selectedGoods)} className="btn btn-sm green-meadow">
                + Добавить в заказ
              </Link>
              <Link className="pull-right btn btn-sm red btn-outline" to={`/admin/orders/${routeParams.id}`}>
                <i className="fa fa-close"></i> Закрыть
              </Link>

						</div>
        );
    }
}


function mapStateToProps(state, ownProps) {
  const products = state.goodsSelection.data;
  console.log('products: ', products);
  const product = products.filter(item => item.id == ownProps.routeParams.productId);
  console.log('product: ', product);
  const productName = state.entities.goods[ownProps.routeParams.productId].name;

    return {
        productName,
        goodsSelectionInPharmacies: state.goodsSelectionInPharmacies.data,
        selectedGoods: getSelectedGoods(state.goodsSelectionInPharmacies.data, state.goodsSelectionInPharmacies.selected),
        // pageNum: state.goodsSelectionInPharmacies.data.length/5,
        // paging: state.nomenclature.paging || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        // actions: bindActionCreators(dictionaryActions, dispatch),
        addGoods: bindActionCreators(addGoods, dispatch),
        ...bindActionCreators(actions, dispatch),
        getRandomPharmacies,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PharmaciesGoodsSelection);
