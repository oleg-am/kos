// import * as types from '../actions/actionTypes';
import * as types from './constants';

const getRezult = (data) => (
  data.map(obj => obj.id)
)

const initialState = {
  data: [],
  selected:[],
}

export default function goodsSelectionInPharmacies(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_DATA:
      return {...state, data: action.data, result: getRezult(action.data)};
    case types.TOOGLE_CHECKED_ITEMS:
      const {id, checked } = action;
      if (!checked) {
        let selected = state.selected.slice();
        selected.splice(selected.indexOf(id), 1);
        return {...state, selected };
      }
      return {...state, selected: [...state.selected, action.id]};
    default:
      return state;
  }
}

export const getOrderById = (data, id) => {
  const order = data.filter(order => {
    return order.id == id;
  })
  return order[0];
}

// export const getGoodsByName = (goods, name) => {
//   return goods.filter(val => (name.indexOf))
// }
