import { normalize } from 'normalizr';

import axios from 'axios';
import _omit from 'lodash/omit';
import _union from 'lodash/union';

import * as types from './constants';
import { createParams } from '../../helpers/api';

import {
  PHARMACY_ITEM_OBJ,
  NETWORKS_ARR,
  PHONES_ARR,
  DELIVERY_SCHEDULES_ARR,
  // pharmacyValidator,
  // deleteValidator,
  pharmacyPhonesPostValidator,
  pharmacyPhonesPutValidator,
  postValidatorDeliverySchedules,
  putValidatorDeliverySchedules,
} from './schemas';

export const setFilterCities = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER_CITIES,
  key,
  value,
  isFiltersDebounce,
});

export const setFilterRegions = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER_REGIONS,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});

export const placeChanged = ({ lat, lng }) => ({
  type: types.PLACE_CHANGED, lat, lng,
});

export const changeParamDeliverySchedules = (key, value) => ({
  type: types.CHANGE_PARAM_DELIVERY_SCHEDULES,
  key,
  value,
});

export const closeMessage = () => ({
  type: types.CLOSE_MESSAGE,
});

export const edit = nomenclature => ({
  type: types.EDIT,
  nomenclature,
});

export const addPhone = () => ({
  type: types.ADD_PHONE,
});

export const deletePhone = id => ({
  type: types.DELETE_PHONE, id,
});

export const addDeliverySchedules = day => ({
  type: types.ADD_DELIVERY_SCHEDULES, day,
});

export const deleteDeliverySchedules = id => ({
  type: types.DELETE_DELIVERY_SCHEDULES, id,
});

export const reset = () => ({
  type: types.RESET,
});

export const loadPharmaciesItem = id => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/pharmacies/${id}?with[*]=*&with[networkPhones]`),
  schema:  PHARMACY_ITEM_OBJ,
});

export const loadCities = filters => ({
  types:   [types.LOAD_REQUEST_CITIES, types.LOAD_SUCCESS_CITIES, types.LOAD_FAILURE_CITIES],
  promise: api => api.get(`/api/admin/cities${createParams(filters)}`),
});

export const loadRegions = filters => ({
  types:   [types.LOAD_REQUEST_REGIONS, types.LOAD_SUCCESS_REGIONS, types.LOAD_FAILURE_REGIONS],
  promise: api => api.get(`/api/admin/regions${createParams(filters)}`),

  isDismissMessage: true,
});

export const loadNetworks = () => ({
  types:   [types.LOAD_REQUEST_NETWORKS, types.LOAD_SUCCESS_NETWORKS, types.LOAD_FAILURE_NETWORKS],
  promise: api => api.get('/api/admin/networks?with[phones]=*'),
  schema:  NETWORKS_ARR,

  isDismissMessage: true,
});

export const savePharmacy = (id, data) => ({
  types:   [types.SAVE_PHARMACY_REQUEST, types.SAVE_PHARMACY_SUCCESS, types.SAVE_PHARMACY_FAILURE],
  promise: api => api.put(`/api/admin/pharmacies/${id}`, data),
});

// export const savePharmacy = (id, data) => (dispatch) => {
//   dispatch({
//     type: types.SAVE_PHARMACY_REQUEST
//   })
//   axios
//   .put(`/api/admin/pharmacies/${id}`, data)
//   .then((res) => {
//     console.log('res: ', res);
//     // console.log('data: ', res.data);
//     // console.log('normalize: ', normalize(res.data, PHARMACY_ITEM_OBJ));
//     return dispatch({
//       type: types.SAVE_PHARMACY_SUCCESS,
//       errors: [],
//       res: res,
//       statusText: res.statusText
//     })
//   }).catch((error) => {
//
//     console.error(error);
//     return dispatch({
//       type: types.SAVE_PHARMACY_FAILURE,
//       errors: error.errors,
//     })
//   })
// }

export const saveImg = (id, data) => (dispatch) => {
  dispatch({
    type: types.SAVE_IMG_REQUEST
  })
  console.log('axios data: ', data);
  axios
  .post(`/api/admin/pharmacies/${id}/image`, data)
  .then((res) => {
    console.log('res: ', res);
    return dispatch({
      type: types.SAVE_IMG_SUCCESS,
      errors: [],
      res,
      // statusText: res.statusTextpharmacyValidator
    })
  }).catch((error) => {

    console.error(error);
    return dispatch({
      type: types.SAVE_IMG_FAILURE,
      errors: error.response.data.errors
    })
  })
}

export const savePhones = (pharmacyId, { phones, editedPhones, deletedPhones }) => (dispatch) => {
  dispatch({
    type: types.SAVE_PHONES_REQUEST
  })

  const validate = (validator) => (item) => {
    console.log('phones map item: ', item);
    const validData = validator.validate({...item, pharmacy: pharmacyId});
    if (validData) {
      console.log('validData: ', validData);
      return validData;
    } else {
      console.log('errors', validator.getErrors());
    }
  }

  const phonesDelete = deletedPhones.filter(id => isFinite(id));

  const phonesPut = Object.values(_omit(phones, deletedPhones))
    .filter(({ id, number }) => editedPhones.includes(id) && isFinite(id) && number.length)
    .map(validate(pharmacyPhonesPutValidator))
  console.log('savePhones phonesPut: ', phonesPut);

  const phonesPost = Object.values(_omit(phones, deletedPhones))
    .filter(({ id, number }) => editedPhones.includes(id) && !isFinite(id) && number.length)
    .map(validate(pharmacyPhonesPostValidator))
  console.log('savePhones phonesPost: ', phonesPost);

  const axiosMap = (axiosMethod) => (phone) => axiosMethod(phone);

  const axiosDelete = (id) => axios.delete(`/api/admin/pharmacy_phones/${id}`);
  const axiosPut    = (phone) => axios.put(`/api/admin/pharmacy_phones/${phone.id}`, phone);
  const axiosPost   = (phone) => axios.post(`/api/admin/pharmacy_phones`, phone);

  axios.all([
    ...phonesDelete.map(axiosMap(axiosDelete)),
    ...phonesPut.map(axiosMap(axiosPut)),
    ...phonesPost.map(axiosMap(axiosPost))
  ]).then((res) => {
    console.log('res: ', res);
    return dispatch({
      type: types.SAVE_PHONES_SUCCESS,
      errors: [],
      res: res,
      ids: _union(editedPhones, deletedPhones),
      ...normalize(res.map(item => item.data), PHONES_ARR),
    })
  }).catch((error) => {

    console.error(error);
    return dispatch({
      type: types.SAVE_PHONES_FAILURE,
      errors: error.response.data.errors
    })
  })
}

export const saveDeliverySchedules = ({ id, items, editedItems, deletedItems }) => (dispatch) => {
  dispatch({
    type: types.SAVE_DELIVERY_SCHEDULES_REQUEST
  })
  let errors = {};

  const validate = (validator) => (item) => {
    console.log('items map item: ', item);
    const validData = validator.validate({...item, pharmacy: id});
    if (validData) {
      console.log('validData: ', validData);
      return validData;
    } else {
      errors = { ...errors, ...validator.getErrors() }
    }
  }
  console.log('saveItems items: ', items);
  console.log('saveItems editedItems: ', editedItems);
  console.log('saveItems deletedItems: ', deletedItems);

  const itemsDelete = deletedItems.filter(id => isFinite(id));
  console.log('saveItems itemsDelete: ', itemsDelete);
  const itemsPut = Object.values(_omit(items, deletedItems))
    .filter(({ id }) => editedItems.includes(id) && isFinite(id))
    .map(validate(putValidatorDeliverySchedules))
  console.log('saveItems itemsPut: ', itemsPut);

  const itemsPost = Object.values(_omit(items, deletedItems))
    .filter(({ id }) => editedItems.includes(id) && !isFinite(id))
    .map(validate(postValidatorDeliverySchedules))
  console.log('saveItems itemsPost: ', itemsPost);

  if (Object.values(errors).length) {
    return dispatch({
      type: types.SAVE_DELIVERY_SCHEDULES_FAILURE,
      errors
    })
  }

  const axiosMap = (axiosMethod) => (item) => axiosMethod(item);

  const axiosDelete = (id) => axios.delete(`/api/admin/delivery_schedules/${id}`);
  const axiosPut    = (item) => axios.put(`/api/admin/delivery_schedules/${item.id}`, item);
  const axiosPost   = (item) => axios.post(`/api/admin/delivery_schedules`, item);

  axios.all([
    ...itemsDelete.map(axiosMap(axiosDelete)),
    ...itemsPut.map(axiosMap(axiosPut)),
    ...itemsPost.map(axiosMap(axiosPost))
  ]).then((res) => {
    console.log('res: ', res);
    return dispatch({
      type: types.SAVE_DELIVERY_SCHEDULES_SUCCESS,
      errors: [],
      res: res,
      ids: _union(editedItems, deletedItems),
      ...normalize(res.map(item => item.data), DELIVERY_SCHEDULES_ARR),
    })
  }).catch((error) => {

    console.error(error);
    return dispatch({
      type: types.SAVE_DELIVERY_SCHEDULES_FAILURE,
      errors: error.response.data.errors
    })
  })
}
