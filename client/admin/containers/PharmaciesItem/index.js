import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

import { Link, browserHistory } from 'react-router';

import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';
import Select from 'react-select';
import Dropzone from 'react-dropzone';

import InputElement from 'react-input-mask';
import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';
import { pharmacyValidator, daysOfTheWeek } from './schemas';

import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

// import LimitSelect from '../../components/LimitSelect';

import SearchBoxMap from '../../components/GoogleMap';

import LinkClose from '../../elements/LinkClose';

const PlusIcon = () => <i className="fa fa-plus pull-right font-default font-hover-green-jungle" />

class PharmaciesItem extends Component {
  static propTypes = {
    isEditableRoleAccess: PropTypes.bool,

    location: PropTypes.objectOf(PropTypes.any).isRequired,
    params:   PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
    errors:             PropTypes.arrayOf(PropTypes.object),
    filterCities:       PropTypes.objectOf(PropTypes.any).isRequired,
    filterRegions:      PropTypes.objectOf(PropTypes.any).isRequired,
    nomenclature:       PropTypes.objectOf(PropTypes.any),
    paging:             PropTypes.objectOf(PropTypes.any),
    loadPharmaciesItem: PropTypes.func.isRequired,
    loadCities:         PropTypes.func.isRequired,
    loadRegions:        PropTypes.func.isRequired,
    loadNetworks:       PropTypes.func.isRequired,
    // setFilter:          PropTypes.func.isRequired,
    changeParam:        PropTypes.func.isRequired,

    changeParamDeliverySchedules: PropTypes.func.isRequired,

    setFilterCities:         PropTypes.func.isRequired,
    addPhone:                PropTypes.func.isRequired,
    deletePhone:             PropTypes.func.isRequired,
    savePhones:              PropTypes.func.isRequired,
    addDeliverySchedules:    PropTypes.func.isRequired,
    deleteDeliverySchedules: PropTypes.func.isRequired,
    saveDeliverySchedules:   PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);

    const {
      params: { id },
      loadPharmaciesItem,
      loadCities,
      loadRegions,
      loadNetworks,
      filterCities,
    } = this.props;

    this.debounceLoadCities = _debounce(loadCities, 300);
    this.debounceLoadRegions = _debounce(loadRegions, 300);
    loadPharmaciesItem(id);
    loadCities(filterCities);
    loadNetworks();
  }

  state = {
    files: [],
  }

  componentWillReceiveProps(next) {
    const {
      filterCities,
      filterRegions,
      loadCities,
      loadRegions,
      pharmaciesItem,
    } = this.props;
    const {
      filterCities: nextFilterCities,
      filterRegions: nextFilterRegions,
      pharmaciesItem: nextPharmaciesItem,
      isFiltersDebounce,
      // errorsValidation,
    } = next;

    if (pharmaciesItem.city !== nextPharmaciesItem.city) {
      loadRegions(nextFilterRegions);
    } else if (filterRegions !== nextFilterRegions) {
      isFiltersDebounce
       ? this.debounceLoadRegions(nextFilterRegions)
       : loadRegions(nextFilterRegions);
    }

    if (filterCities !== nextFilterCities) {
      isFiltersDebounce
       ? this.debounceLoadCities(nextFilterCities)
       : loadCities(nextFilterCities);
    }
  }
  componentWillUnmount() {
    this.reset();
  }
  onChangeParam = (key, value) => (e) => {
    this.props.changeParam(key, value || e.target.value);
  }
  onChangeParamDelivery = (path = [], keys = []) => (e) => {
    if (!keys.length) {
      this.props.changeParamDeliverySchedules(path, e.target.value);
    } else {
      const result = e.target.value.match(/([0-9]{2}|_[0-9]{1}|[0-9]{1}_|__) : ([0-9]{2}|_[0-9]{1}|[0-9]{1}_|__)/i);
      this.props.changeParamDeliverySchedules([...path, keys[0]], result[1]);
      this.props.changeParamDeliverySchedules([...path, keys[1]], result[2]);
    }
  }
  onChangeParamSelect = key => (obj) => {
    this.props.changeParam(key, obj ? obj.value : '');
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }

  setFilterCities = (key, isFiltersDebounce) => (value = '') => {
    if (value != '' || (value == '' && (this.props.filterCities.filters.name.value != ''))) {
      this.props.setFilterCities(key, value, isFiltersDebounce);
    }
  }
  setFilterRegions = (key, isFiltersDebounce) => (value = '') => {
    if (value != '' || (value == '' && (this.props.filterRegions.filters.name.value != ''))) {
      this.props.setFilterRegions(key, value, isFiltersDebounce);
    }
  }
  addPhone = () => {
    this.props.addPhone();
  }
  deletePhone = (id) => (e) => {
    this.props.deletePhone(id);
  }
  savePhones = () => {
    const { params: { id }, pharmacyPhones, editedPhones, deletedPhones } = this.props;
    this.props.savePhones(id, { phones: pharmacyPhones, editedPhones, deletedPhones });
  }

  addDeliverySchedules = (day) => (e) => {
    this.props.addDeliverySchedules(day);
  }
  deleteDeliverySchedules = (id) => (e) => {
    this.props.deleteDeliverySchedules(id);
  }
  saveDeliverySchedules = () => {
    const {
      params: { id },
      deliverySchedules,
      editedDeliverySchedules,
      deletedDeliverySchedules,
    } = this.props;

    const items = {};
    Object.values(deliverySchedules).forEach(item => {
      Object.keys(item).forEach(key => {
        items[key] = item[key];
      });
    });

    this.props.saveDeliverySchedules({
      id,
      items,
      editedItems: editedDeliverySchedules,
      deletedItems: deletedDeliverySchedules,
    });
  }
  // savePhones = () => {
  //   const { params: { id }, pharmacyPhones, editedPhones, deletedPhones } = this.props;
  //   this.props.savePhones(id, { phones: pharmacyPhones, editedPhones, deletedPhones });
  // }
  //
  onSubmit = () => {
    const { pharmaciesItem, savePharmacy, params: { id } } = this.props;

    const validData = pharmacyValidator.validate({
      ...pharmaciesItem,
    });

    if (validData) {
      savePharmacy(id, validData);
    } else {
      console.log('errors', pharmacyValidator.getErrors());
    }
  }
  reset = () => {
    this.props.reset();
  }
  onEdit = (pharmacy) => (e) => {
    e.preventDefault();
    browserHistory.push(`/admin/pharmacies/${pharmacy.id || ''}`);
  }
  onDrop = (files) => {
    this.setState({files});
  }
  saveImg = () => {
    let data = new FormData();
    data.append('file', this.state.files[0]);
    this.props.saveImg(this.props.params.id, data)
  }
  onOpenClick = () => {
    this.dropzone.open();
  }
  onPlacesChanged = (latLng) => {
    this.props.placeChanged(latLng);
  }
  render() {
    const {
      isEditableRoleAccess,
      params: { id },
      location,
      imgUrl,
      errors,
      pharmaciesItem,
      networks,
      networkPhones,
      citiesOptions,
      regionsOptions,
      filterRegions,
      pharmacyPhones,
      isSavePhones,
      deliverySchedules,
      isSaveDeliverySchedules
    } = this.props;

    const { returnTo } = location.state || {};

    const {
      network: networkId,
    } = pharmaciesItem;

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs title="" components={{
          title: <span style={{textTransform: 'none'}}>{pharmaciesItem.nameIn1C}</span>,
          links: [
            {
              name: 'Меню',
              to:   '/admin/reports',
            }, {
              name: 'Всі аптеки',
              to:   '/admin/pharmacies',
            }, {
              name: 'Картка аптеки',
            },
          ] }}
        />
        <Portlet
          title={
            <div>
              <i className="icon-pointer" />
              <span className="caption-subject"> {pharmaciesItem.addressIn1C} </span>
            </div>
          }
          actionsRight={[
            <Link
              key="actionsRight_2"
              style={{ marginRight: '10px' }}
              className="btn btn-sm blue"
              to={{
                state: {
                  returnTo: location.pathname,
                },
                pathname: `/admin/pharmacies/${id}/remainders`,
              }}
            >
              <i className="fa fa-search" /> Залишки
            </Link>,
            isEditableRoleAccess && <button
              key="actionsRight_3"
              className="btn green-meadow"
              style={{ marginRight: '10px' }}
              onClick={this.onSubmit}
            >
              <i className="fa fa-check" /> Зберегти
            </button>,
            // <Link key="actionsRight_4" className="btn btn-sm default" to="/admin/pharmacies">
            //   <i className="fa fa-times" /> Закрити
            // </Link>,
            <LinkClose key="actionsRight_5" to={returnTo || '/admin/pharmacies'} />,
          ]}
        >

        <Row>
          <Col md={4}>
            <div className="form-group">
              <label>Назва аптеки</label>
              <input
                type="text"
                className="form-control"
                placeholder=""
                value={pharmaciesItem.name}
                readOnly={!isEditableRoleAccess}
                onChange={this.onChangeParam('name')}
              />
            </div>
            <div className="form-group">
              <label>Місто</label>
              <Select
                name="city"
                disabled={!isEditableRoleAccess}
                placeholder="Введіть місто"
                value={pharmaciesItem.city}
                options={citiesOptions}
                onInputChange={this.setFilterCities(['filters', 'name'], true)}
                onChange={this.onChangeParamSelect('city')}
                // onOpen={this.loadManufacturers}
                noResultsText="Нічого не знайдено"
                clearValueText="Стерти"
                clearable
                // isLoading={isLoadingManufacturer}
                // onCloseResetsInput={false}
                onClose={this.setFilterCities(['filters', 'name'], true)}
              />
            </div>
            <div className="form-group">
              <label>Район</label>
              <Select
                name="regions"
                disabled={!isEditableRoleAccess}
                placeholder="Введіть назву"
                value={pharmaciesItem.region}
                options={regionsOptions}
                onInputChange={this.setFilterRegions(['filters', 'name'], true)}
                onChange={this.onChangeParamSelect('region')}
                // onOpen={this.loadManufacturers}
                noResultsText="Нічого не знайдено"
                clearValueText="Стерти"
                clearable
                // isLoading={isLoadingManufacturer}
                // onCloseResetsInput={false}
                onClose={this.setFilterRegions(['filters', 'name'], true)}
              />

            </div>
            <div className="form-group">
              <label>Адреса</label>
              <input
                type="text"
                className="form-control"
                readOnly={!isEditableRoleAccess}
                placeholder="Введіть адрес"
                value={pharmaciesItem.address}
                onChange={this.onChangeParam('address')}
              />
            </div>
            <div className="form-group">
              <label>Адреса</label>
              <div style={{ height: '400px' }}>
                <SearchBoxMap
                  lat={pharmaciesItem.lat}
                  lng={pharmaciesItem.lng}
                  onPlacesChanged={this.onPlacesChanged}
                />
              </div>
            </div>

          </Col>
          <Col md={3}>
            {/* <div className="form-group">
              <label>Код ЄДРПОУ</label>
              <input value={pharmaciesItem.egrpou} type="text" className="form-control" placeholder="" readOnly />
            </div> */}
            <div className="form-group">
              <label>Тип</label>
              <input value={pharmaciesItem.isStock ? 'Склад' : 'Аптека'} type="text" className="form-control" placeholder="" readOnly />
            </div>
            <div className="form-group">
              <label>Мережа</label>
              <select value={networkId} onChange={this.onChangeParam('network')} className="form-control" disabled={!isEditableRoleAccess}>
                {!networkId &&
                  <option disabled key="-1" value="-1"> Виберіть мережу </option>
                }
                {Object.values(networks).map(item => (
                  <option key={item.id} value={item.id}> {item.name} </option>
                ))}
              </select>
            </div>
            {networkPhones.map(({ id, ownerName, number, phoneType }) => (
              <div key={id} className="form-group">
                <label> {ownerName} </label>
                <div className="input-icon left">
                  <i
                    title={phoneType}
                    className={classNames(
                      'fa',
                      {'fa-phone': phoneType == 'city'},
                      {'fa-mobile-phone': phoneType == 'mobile'},
                      {'fa-phone-square': phoneType == 'inner'}
                    )}>
                  </i>
                  <input value={`${number} - ${phoneType}`} type="text" className="form-control" placeholder="" readOnly />
                </div>
              </div>
            ))}
            <div className="form-group">
              <label>Телефони аптеки</label>
              {Object.values(pharmacyPhones).map(({ id, number, ownerName, phoneType }) => (
                <div key={id} title={ownerName} className={classNames({ 'input-group': isEditableRoleAccess })}>
                  <div className="input-icon">
                    <i
                      style={{ zIndex: 4 }}
                      title={phoneType}
                      className={classNames(
                        'fa', 'font-green',
                        {'fa-phone': phoneType == 'city'},
                        {'fa-mobile-phone': phoneType == 'mobile'},
                        {'fa-phone-square': phoneType == 'inner'}
                      )}>
                    </i>
                    <input
                      type="text"
                      readOnly={!isEditableRoleAccess}
                      className="form-control"
                      placeholder="Введіть телефон"
                      value={number}
                      onChange={this.onChangeParam(['entities', 'phones', id, 'number'])}
                    />
                  </div>

                  {isEditableRoleAccess &&
                    <div style={{ paddingBottom: '5px' }} className="input-group-btn">
                      <button style={{ padding: '8px 7px 7px' }} type="button" className="btn yellow dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i className="fa fa-cogs" /> <i className="fa fa-angle-down" />
                      </button>
                      <ul className="dropdown-menu pull-right">
                        <li>
                          <a onClick={this.onChangeParam(['entities', 'phones', id, 'phoneType'], 'city')} href="javascript:;">
                            <i className="fa fa-phone" /> Міський
                          </a>
                        </li>
                        <li>
                          <a onClick={this.onChangeParam(['entities', 'phones', id, 'phoneType'], 'mobile')} href="javascript:;">
                            <i className="fa fa-mobile-phone" /> Мобільний
                          </a>
                        </li>
                        <li>
                          <a onClick={this.onChangeParam(['entities', 'phones', id, 'phoneType'], 'inner')} href="javascript:;">
                            <i className="fa fa-phone-square" /> Внутрішній
                          </a>
                        </li>
                        <li className="divider">  </li>
                        <li>
                          <a onClick={this.deletePhone(id)} href="javascript:;"><i className="fa fa-trash-o font-red" /> Видалити </a>
                        </li>
                      </ul>
                    </div>
                  }
                </div>
              ))}
            </div>
            {isEditableRoleAccess &&
              <div className="form-group">
                <button onClick={this.addPhone} style={{ marginRight: '10px' }} className="btn green" title="Додати номер">
                  <i className="fa fa-plus" /><span />
                  <i className="fa fa-phone" />
                </button>
                {isSavePhones &&
                  <button className="btn green-meadow" onClick={this.savePhones}>
                    <i className="fa fa-check" /> Зберегти
                  </button>
                }
              </div>
            }
            <div className="form-group">
              <div style={{ display: 'initial' }} className={`fileinput fileinput-${this.state.files.length ? 'new' : 'exist'}`} data-provides="fileinput">
                <div
                  className="fileinput-preview thumbnail"
                  style={{ width: '100%', height: '200px', lineHeight: '200px' }}
                >
                  {!this.state.files.length &&
                    <img height="200px" src={pharmaciesItem.image.url_main && `${imgUrl}${pharmaciesItem.image.url_main}`} />
                  }
                  {this.state.files.map((file, i) =>
                    <img key={`file_${i}`} height="200px" src={file.preview} />
                  )}
                </div>
                {isEditableRoleAccess &&
                  <div className="form-group">
                    <span style={{ marginRight: '5px' }} className="btn green btn-file" onClick={this.onOpenClick}>
                    {this.state.files.length ? 'Змінити файл' : 'Виберіть файл'}
                    </span>
                    {!!this.state.files.length &&
                      <button className="btn green-meadow" onClick={this.saveImg}>
                        <i className="fa fa-check" /> Зберегти
                      </button>
                    }
                  </div>
                }
                {!!this.state.files.length && (
                  <div>
                    <span className="fileinput-filename">
                      {this.state.files[0].name} <i title="Відмінити" className="fa fa-times close" />
                    </span>
                  </div>
                )}
              </div>
            </div>
            <div className="form-group">
              <Dropzone
                style={{ display: 'none' }}
                ref={(node) => { this.dropzone = node; }}
                multiple={false}
                accept="image/*"
                onDrop={this.onDrop}
              >
                {/* <div>Try dropping some files here, or click to select files to upload.</div> */}
              </Dropzone>
            </div>
          </Col>
          <Col md={5}>
            <div className="form-group">
              <label>Коментар</label>
              <textarea
                rows="5"
                className="form-control"
                placeholder="Введіть коментар"
                readOnly={!isEditableRoleAccess}
                value={pharmaciesItem.description}
                onChange={this.onChangeParam('description')}
              />
            </div>
            <div className="form-group">
              <label>Графік роботи</label>
              <table className="table table-bordered tr-center td-center">
                <colgroup>
                  <col width="33%" />
                  <col width="33%" />
                  <col width="33%" />
                </colgroup>
                <thead>
                  <tr>
                    <th> Пн-Пт </th>
                    <th> Сб </th>
                    <th> Нд </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div style={{ padding: 0, margin: '0 0 15px' }} className="form-group form-md-line-input">
                        <input
                          type="text"
                          className="form-control"
                          style={{ minWidth: '50%', textAlign: 'center' }}
                          placeholder="Введіть час"
                          readOnly={!isEditableRoleAccess}
                          value={pharmaciesItem.workOnWeekDays}
                          onChange={this.onChangeParam('workOnWeekDays')}
                        />
                        <div className="form-control-focus" />
                      </div>
                    </td>
                    <td>
                      <div style={{ padding: 0, margin: '0 0 15px' }} className="form-group form-md-line-input">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Введіть час"
                          style={{ textAlign: 'center' }}
                          readOnly={!isEditableRoleAccess}
                          value={pharmaciesItem.workOnSaturday}
                          onChange={this.onChangeParam('workOnSaturday')}
                        />
                        <div className="form-control-focus" />
                      </div>
                    </td>
                    <td>
                      <div style={{ padding: 0, margin: '0 0 15px' }} className="form-group form-md-line-input">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Введіть час"
                          style={{ textAlign: 'center' }}
                          value={pharmaciesItem.workOnSunday}
                          readOnly={!isEditableRoleAccess}
                          onChange={this.onChangeParam('workOnSunday')}
                        />
                        <div className="form-control-focus" />
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="form-group deliverySchedules">
              <div>
                <label>Графік доставки товарів зі складу до аптеки</label>
                <Link className="pull-right deliverySchedules custom-collapse" key="tools_1" title="Розгорнути/Згорнути" />
              </div>
              <table className="deliverySchedules table table-bordered tr-center td-center">
                <colgroup>
                  <col width="25%" />
                  <col width="25%" />
                  <col width="25%" />
                  <col width="25%" />
                </colgroup>
                <thead>
                  <tr>
                    <th colSpan="2"> Замовлення </th>
                    <th colSpan="2"> Доставка </th>
                    {isEditableRoleAccess && <th />}
                  </tr>
                </thead>
                <tbody>
                  {Object.keys(deliverySchedules).map((day) => {
                    const currentDay = Object.values(deliverySchedules[day]);
                    if (!currentDay.length) {
                      return (
                        <tr key={'delivery_' + day}>
                          <td {...(isEditableRoleAccess ? { onClick: this.addDeliverySchedules(day), title: 'Додати час' } : {})}>
                            {daysOfTheWeek[day]}
                            {isEditableRoleAccess && <PlusIcon /> }
                          </td>
                          <td style={{ cursor: 'auto' }} colSpan="4">Не встановлено</td>
                        </tr>
                      );
                    }
                    return (
                      currentDay.map((hours, i) => (
                        <tr key={hours.id}>
                          {i === 0 &&
                            <td {...(isEditableRoleAccess ? { onClick: this.addDeliverySchedules(day), title: 'Додати час' } : {})} rowSpan={currentDay.length}>
                              {daysOfTheWeek[day]}
                              {isEditableRoleAccess && <PlusIcon /> }
                            </td>
                          }
                          <td className="input">
                            <div style={{ padding: 0, margin: 0 }} className="form-group form-md-line-input">
                              <InputElement
                                readOnly={!isEditableRoleAccess}
                                className="form-control"
                                style={{ minWidth: '50%', textAlign: 'center' }}
                                alwaysShowMask
                                mask="до 99 : 99"
                                defaultValue={
                                  `до ${hours.hour < 10 ? `0${hours.hour}` : hours.hour} :
                                  ${hours.minute < 10 ? `0${hours.minute}` : hours.minute}`
                                }
                                onChange={this.onChangeParamDelivery([day, hours.id], ['hour', 'minute'])}
                              />
                              <div className="form-control-focus" />
                            </div>
                          </td>
                          <td className="input">
                            <div style={{ padding: 0, margin: 0 }} className="form-group form-md-line-input">
                              <select
                                className="form-control"
                                style={{ minWidth: '50%', textAlign: 'center' }}
                                disabled={!isEditableRoleAccess}
                                value={hours.deliveryDay}
                                onChange={this.onChangeParamDelivery([day, hours.id, 'deliveryDay'])}
                              >
                                {Object.keys(daysOfTheWeek).map(key => (
                                  <option key={key} value={key}>{daysOfTheWeek[key]}</option>
                                ))}
                              </select>
                              <div className="form-control-focus" />
                            </div>
                          </td>
                          <td className="input">
                            <div style={{ padding: 0, margin: 0 }} className="form-group form-md-line-input">
                              <InputElement
                                readOnly={!isEditableRoleAccess}
                                className="form-control"
                                style={{ minWidth: '50%', textAlign: 'center' }}
                                alwaysShowMask
                                mask="99 : 99"
                                defaultValue={
                                  `${hours.deliveryHour < 10 ? `0${hours.deliveryHour}` : hours.deliveryHour} :
                                  ${hours.deliveryMinute < 10 ? `0${hours.deliveryMinute}` : hours.deliveryMinute}`
                                }
                                onChange={this.onChangeParamDelivery([day, hours.id], ['deliveryHour', 'deliveryMinute'])}
                              />
                              <div className="form-control-focus" />
                            </div>
                          </td>
                          {isEditableRoleAccess &&
                            <td onClick={this.deleteDeliverySchedules(hours.id)} title="Видалити">
                              <i className="fa fa-times pull-right font-default font-hover-red" />
                            </td>
                          }
                        </tr>
                      ))
                    )
                  })}
                </tbody>
              </table>
            </div>
            <div className="form-group">
              {isSaveDeliverySchedules &&
                <button className="btn green-meadow pull-right" onClick={this.saveDeliverySchedules}>
                  <i className="fa fa-check" /> Зберегти
                </button>
              }
            </div>

          </Col>
        </Row>
        </Portlet>

      </div>
    );
  }
}

export default connect(selectors, actions)(PharmaciesItem);
