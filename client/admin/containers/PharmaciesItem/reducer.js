import { combineReducers } from 'redux';

import _uniqueId from 'lodash/uniqueId';
import _omit from 'lodash/omit';
import _pick from 'lodash/pick';
import _union from 'lodash/union';
import _merge from 'lodash/merge';

import * as types from './constants';

import { mapObj } from '../../helpers/mapObj';

import { setValue, setFiltersValue } from '../../helpers/setObjValue';

const initialState = {
  filterRegionsInit: {
    // page: 1,
    limit:   20,
    fields:  'id,name',
    filters: {
      name: {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      city: {
        value: '',
      },
    },
  },
  savePharmacyInit: {
    showSuccessMessage: false,
  },
  pharmaciesItemInit: {
    name:  '',
    lat:   '',
    lng:   '',
    image: {
      url_main: '',
    },
    phones: [],
    network: '',
    entities: {
      city: {},
      phones: {
        cid0: {
          id:        'cid0',
          number:    '',
          phoneType: 'mobile',
        },
      },
      region: {},
      network: {},
    },
  },
  deliverySchedulesInit: {
    1: {},
    2: {},
    3: {},
    4: {},
    5: {},
    6: {},
    7: {},
  },
  filterCitiesInit: {
    limit: 20,
    // with: {
    //   '*': {
    //     value: '*',
    //   }
    // },
    filters: {
      name: {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
    },
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const errorsValidation = (state = {}, action) => {
  switch (action.type) {
    case types.SAVE_DELIVERY_SCHEDULES_FAILURE:
      return action.errors;
    case types.RESET:
    case types.CLOSE_MESSAGE:
      return {};
    default:
      return state;
  }
};

// const imgUrl = (state = '', action) => {
//   switch (action.type) {
//     case types.LOAD_SUCCESS_IMG_URL:
//     case types.LOAD_FAILURE_IMG_URL:
//       return action.data;
//     default:
//       return state;
//   }
// };

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER_CITIES:
    case types.SET_FILTER_REGIONS:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const isSuccessMessage = (state = false, action) => {
  switch (action.type) {
    case types.SAVE_PHARMACY_SUCCESS:
    case types.SAVE_IMG_SUCCESS:
    case types.SAVE_PHONES_SUCCESS:
    case types.SAVE_DELIVERY_SCHEDULES_SUCCESS:
      return true;
    case types.CLOSE_MESSAGE:
      return false;
    default:
      return state;
  }
};

const isErrorMessage = (state = false, action) => {
  switch (action.type) {
    case types.SAVE_PHARMACY_FAILURE:
    case types.SAVE_IMG_FAILURE:
    case types.SAVE_PHONES_FAILURE:
    case types.SAVE_DELIVERY_SCHEDULES_FAILURE:
      return true;
    case types.CLOSE_MESSAGE:
      return false;
    default:
      return state;
  }
};

const isSavePhones = (state = false, action) => {
  switch (action.type) {
    case types.CHANGE_PARAM: {
      if (Array.isArray(action.key) &&
        action.key.includes('entities') &&
        action.key.includes('phones')
      ) {
        return true;
      }
      return state;
    }
    case types.DELETE_PHONE:
      return true;
    case types.SAVE_PHONES_SUCCESS:
    case types.RESET:
      return false;
    default:
      return state;
  }
};

const editedPhones = (state = [], action) => {
  switch (action.type) {
    case types.CHANGE_PARAM: {
      const id = Array.isArray(action.key) ? action.key[2] : '';
      if (id &&
        action.key.includes('phones') &&
        action.key.includes('entities') &&
        !state.includes(id)
      ) {
        return [...state, id];
      }
      return state;
    }
    case types.SAVE_PHONES_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const deletedPhones = (state = [], action) => {
  switch (action.type) {
    case types.DELETE_PHONE:
      return [...state, action.id];
    case types.SAVE_PHONES_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const savePharmacy = (state = initialState.savePharmacyInit, action) => {
  switch (action.type) {
    case types.SAVE_PHARMACY_SUCCESS:
      return { showSuccessMessage: true };
    default:
      return state;
  }
};

const pharmaciesItem = (state = initialState.pharmaciesItemInit, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return {
        ...state,
        ...action.result,
        entities: {
          ...state.entities,
          ..._omit(action.entities, [1, 2, 3, 4, 5, 6, 7]),
          deliverySchedules: {
            ...state.entities.deliverySchedules,
            ..._pick(action.entities, [1, 2, 3, 4, 5, 6, 7]),
          },
        },
      };
    case types.CHANGE_PARAM:
      return {
        ...state,
        region: action.key === 'city' ? '' : state.region,
        ...setValue(state, action.key, action.value),
      };
    case types.PLACE_CHANGED:
      return { ...state, lat: action.lat, lng: action.lng };
    case types.ADD_PHONE: {
      const id = _uniqueId('cid');
      return {
        ...state,
        entities: {
          ...state.entities,
          phones: {
            ...state.entities.phones,
            [id]: {
              id,
              number:    '',
              phoneType: 'mobile',
            },
          },
        },
      };
    }
    case types.SAVE_PHONES_SUCCESS: {
      const { phones } = action.entities;
      return {
        ...state,
        entities: {
          ...state.entities,
          phones: { ..._omit(state.entities.phones, action.ids), ...phones },
        },
      };
    }
    case types.RESET:
      return initialState.pharmaciesItemInit;
    default:
      return state;
  }
};

const deliverySchedules = (state = initialState.deliverySchedulesInit, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return {
        ...state,
        ..._pick(action.entities, [1, 2, 3, 4, 5, 6, 7]),
      };
    case types.CHANGE_PARAM_DELIVERY_SCHEDULES:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
    case types.ADD_DELIVERY_SCHEDULES: {
      const id = _uniqueId('cid');
      return {
        ...state,
        [action.day]: {
          ...state[action.day],
          [id]: {
            id,
            day: action.day,
            deliveryDay: action.day,
            deliveryHour: 0,
            deliveryMinute: 0,
            hour: 0,
            minute: 0,
          }
        }
      };
    }
    case types.SAVE_DELIVERY_SCHEDULES_SUCCESS:
      return {
        ..._merge(
          mapObj(state, key => _omit(key, action.ids)),
          action.entities,
        ),
      };
    case types.RESET:
      return initialState.deliverySchedulesInit;
    default:
      return state;
  }
};

const isSaveDeliverySchedules  = (state = false, action) => {
  switch (action.type) {
    case types.CHANGE_PARAM_DELIVERY_SCHEDULES:
    case types.DELETE_DELIVERY_SCHEDULES:
      return true;
    case types.SAVE_DELIVERY_SCHEDULES_SUCCESS:
    case types.RESET:
      return false;
    default:
      return state;
  }
};

const editedDeliverySchedules = (state = [], action) => {
  switch (action.type) {
    case types.CHANGE_PARAM_DELIVERY_SCHEDULES: {
      const id = Array.isArray(action.key) ? action.key[1] : '';
      if (id) {
        return _union([...state, id]);
      } else {
        return state;
      }
    }
    case types.SAVE_DELIVERY_SCHEDULES_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const deletedDeliverySchedules = (state = [], action) => {
  switch (action.type) {
    case types.DELETE_DELIVERY_SCHEDULES:
      return [...state, action.id];
    case types.SAVE_DELIVERY_SCHEDULES_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const cities = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS_CITIES:
      return action.data;
    default:
      return state;
  }
};

const regions = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS_REGIONS:
    case types.LOAD_FAILURE_REGIONS:
      return action.data || [];
    // case types.RESET:
    //   return [];
    default:
      return state;
  }
};

const networks = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS_NETWORKS:
      return action.entities.networks;
    // case types.RESET:
    //   return [];
    default:
      return state;
  }
};

const filterCities = (state = initialState.filterCitiesInit, action) => {
  switch (action.type) {
    case types.SET_FILTER_CITIES:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.RESET:
      return initialState.filterCitiesInit;
    default:
      return state;
  }
};

const filterRegions = (state = initialState.filterRegionsInit, action) => {
  switch (action.type) {
    case types.SET_FILTER_REGIONS:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.CHANGE_PARAM:
      if (action.key === 'city') {
        return {
          ...state,
          ...setFiltersValue(state, ['filters', 'city'], action.value),
        };
      }
      return state;
    case types.LOAD_SUCCESS: {
      const city = action.result && action.result.city;
      if (city) {
        return {
          ...state,
          ...setFiltersValue(state, ['filters', 'city'], city),
        };
      }
      return state;
    }
    case types.RESET:
      return initialState.filterRegionsInit;
    default:
      return state;
  }
};

export default combineReducers({
  // imgUrl,
  isSuccessMessage,
  isErrorMessage,
  isSavePhones,
  errors, // []
  errorsValidation, // {}
  pharmaciesItem, // {}
  deliverySchedules: combineReducers({
    isSave:  isSaveDeliverySchedules,
    items:   deliverySchedules,
    edited:  editedDeliverySchedules,
    deleted: deletedDeliverySchedules,
  }), // {}
  cities, // {}
  regions, // {}
  networks, // {}
  isFiltersDebounce, // false
  filterCities,
  filterRegions,
  editedPhones,
  deletedPhones,
});
