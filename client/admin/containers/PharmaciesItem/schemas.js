import { Schema, arrayOf } from 'normalizr';

import LIVR from 'livr';

LIVR.Validator.defaultAutoTrim(true);

const phones = new Schema('phones');
const city = new Schema('city');
const region = new Schema('region');
const network = new Schema('network');

const day1 = new Schema('1');
const day2 = new Schema('2');
const day3 = new Schema('3');
const day4 = new Schema('4');
const day5 = new Schema('5');
const day6 = new Schema('6');
const day7 = new Schema('7');

const deliverySchedules = {
  1: day1,
  2: day2,
  3: day3,
  4: day4,
  5: day5,
  6: day6,
  7: day7,
};

const pharmaciesItem = {
  city,
  region,
  network,
  deliverySchedules: arrayOf(deliverySchedules, { schemaAttribute: 'day' }),
  phones: arrayOf(phones),
};

const networks = new Schema('networks');
// const phones = new Schema('phones', { idAttribute: 'ownerName' });
//
// networks.define({
//   phones: arrayOf(phones)
// });

export const PHARMACY_ITEM_OBJ = pharmaciesItem;
export const NETWORKS_ARR = { data: arrayOf(networks) };
export const PHONES_ARR = arrayOf(phones);
export const DELIVERY_SCHEDULES_ARR = arrayOf(deliverySchedules, { schemaAttribute: 'day' });

const pharmacySchema = {
  name:           'string',
  city:           'positive_integer',
  region:         'positive_integer',
  network:        'positive_integer',
  address:        'string',
  description:    'string',
  workOnWeekDays: 'string',
  workOnSunday:   'string',
  workOnSaturday: 'string',
  lat:            'decimal',
  lng:            'decimal',
};

const idSchema = {
  id: ['required', 'positive_integer'],
};

const pharmacyPhoneSchema = {
  pharmacy:       [ 'required', 'positive_integer' ],
  phoneType:      { one_of : ['city', 'mobile', 'inner'] },
  number:         { max_length : 12 },
  ownerName:      'string',
};

const deliverySchedulesSchema = {
  pharmacy:       [ 'required', 'positive_integer' ],
  day:            [ 'required', 'positive_integer', { number_between: [1, 7] } ],
  hour:           [ 'required', 'integer',          { number_between: [0, 23] } ],
  minute:         [ 'required', 'integer',          { number_between: [0, 59] } ],
  deliveryDay:    [ 'required', 'positive_integer', { number_between: [1, 7] } ],
  deliveryHour:   [ 'required', 'integer',          { number_between: [0, 23] } ],
  deliveryMinute: [ 'required', 'integer',          { number_between: [0, 59] } ],
};

export const pharmacyValidator = new LIVR.Validator(pharmacySchema);

export const deleteValidator = new LIVR.Validator(idSchema);

export const pharmacyPhonesPostValidator = new LIVR.Validator(pharmacyPhoneSchema);
export const pharmacyPhonesPutValidator = new LIVR.Validator({
    ...idSchema,
    ...pharmacyPhoneSchema,
});

export const postValidatorDeliverySchedules = new LIVR.Validator(deliverySchedulesSchema);
export const putValidatorDeliverySchedules = new LIVR.Validator({
    ...idSchema,
    ...deliverySchedulesSchema,
});

export const daysOfTheWeek = {
  1: 'Пн',
  2: 'Вт',
  3: 'Ср',
  4: 'Чт',
  5: 'Пт',
  6: 'Сб',
  7: 'Нд',
};
