import { createSelector, createStructuredSelector } from 'reselect';
import _omit from 'lodash/omit';
// import _pick from 'lodash/pick';

import { mapObj } from '../../helpers/mapObj';

const REDUCER = 'pharmaciesItem';
const APP = 'app';

// const getFrom = reducer => param => state => state[reducer][param];
// const getFromReducer = getFrom(REDUCER);
// const getFromApp     = getFrom(APP);

const imgUrl            = state => state[APP].imgUrl;

const pharmaciesItem    = state => state[REDUCER].pharmaciesItem;
const entities          = state => state[REDUCER].pharmaciesItem.entities;
const cities            = state => state[REDUCER].cities;
const regions           = state => state[REDUCER].regions;
const isFiltersDebounce = state => state[REDUCER].isFiltersDebounce;
const errors            = state => state[REDUCER].errors;
const errorsValidation  = state => state[REDUCER].errorsValidation;
const networks          = state => state[REDUCER].networks;

const editedPhones      = state => state[REDUCER].editedPhones;
const deletedPhones     = state => state[REDUCER].deletedPhones;

const isSuccessMessage = state => state[REDUCER].isSuccessMessage;
const isErrorMessage   = state => state[REDUCER].isErrorMessage;
const isSavePhones     = state => state[REDUCER].isSavePhones;

const deliverySchedulesitems   = state => state[REDUCER].deliverySchedules.items;
const isSaveDeliverySchedules  = state => state[REDUCER].deliverySchedules.isSave;
const editedDeliverySchedules  = state => state[REDUCER].deliverySchedules.edited;
const deletedDeliverySchedules = state => state[REDUCER].deliverySchedules.deleted;

const filterCities  = createSelector(state => state[REDUCER].filterCities, items => items);
const filterRegions = createSelector(state => state[REDUCER].filterRegions, items => items);
const telObj = createSelector(state => state[REDUCER].pharmaciesItem.telObj, items => items);

const networkPhones = createSelector(
  pharmaciesItem,
  networks,
  ({ network: id }, networks) =>
    (id && Object.keys(networks).length) ? networks[id].phones : []
);

const pharmacyPhones = createSelector(
  // pharmaciesItem,
  deletedPhones,
  entities,
  (deletedPhones_, { phones }) =>
    deletedPhones_.length ? _omit(phones, deletedPhones_) : phones
);

const deliverySchedules = createSelector(
  deliverySchedulesitems,
  deletedDeliverySchedules,
  (items, deleted) =>
    deleted.length ? mapObj(items, key => _omit(key, deleted)) : items
);

const createSelectOptions = items => items.map(item => ({
  value: item.id,
  label: item.name,
}));

const citiesOptions  = createSelector(cities, createSelectOptions);
const regionsOptions = createSelector(regions, createSelectOptions);

export default createStructuredSelector({
  imgUrl,
  pharmaciesItem,
  networks,
  networkPhones,
  citiesOptions,
  regionsOptions,
  isFiltersDebounce,
  errors,
  errorsValidation,
  filterCities,
  filterRegions,
  telObj,
  isSuccessMessage,
  isErrorMessage,
  isSavePhones,
  pharmacyPhones,
  editedPhones,
  deletedPhones,
  deliverySchedules,
  editedDeliverySchedules,
  deletedDeliverySchedules,
  isSaveDeliverySchedules,
  // cityName,
  // regionName
});
