import { normalize } from 'normalizr';

import axios from 'axios';

import * as types from './constants';
import { createParams } from '../../helpers/api';

import {
  PHARMACIES_ARR,
} from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const setFilterCities = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER_CITIES,
  key,
  value,
  isFiltersDebounce,
});

export const setFilterRegions = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER_REGIONS,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});

export const selectPharmacy = id => ({
  type: types.SELECT_PHARMACY,
  id,
});

export const placeChanged = ({ lat, lng }) => ({
  type: types.PLACE_CHANGED, lat, lng,
});

export const changeParamDeliverySchedules = (key, value) => ({
  type: types.CHANGE_PARAM_DELIVERY_SCHEDULES,
  key,
  value,
});

export const closeMessage = () => ({
  type: types.CLOSE_MESSAGE,
});

export const reset = () => ({
  type: types.RESET,
});

export const load = filters => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/pharmacies${createParams(filters)}`),
  schema:  PHARMACIES_ARR,
});

export const loadCities = filters => ({
  types:   [types.LOAD_REQUEST_CITIES, types.LOAD_SUCCESS_CITIES, types.LOAD_FAILURE_CITIES],
  promise: api => api.get(`/api/admin/cities${createParams(filters)}`),
});

export const loadRegions = filters => ({
  types:   [types.LOAD_REQUEST_REGIONS, types.LOAD_SUCCESS_REGIONS, types.LOAD_FAILURE_REGIONS],
  promise: api => api.get(`/api/admin/regions${createParams(filters)}`),

  isDismissMessage: true,
});

// export const loadCities = filters => dispatch => (
//   // dispatch({
//   //   type: types.LOAD_REQUEST_MANUFACTURES,
//   //   filters
//   // })
//
//   axios
//     .get(`/api/admin/cities${createParams(filters)}`)
//     .then((res) => {
//       console.log('data: ', res.data);
//
//     	return dispatch({
//     		type: types.LOAD_SUCCESS_CITIES,
//         data: res.data.data
//         // errors: [],
//     		// ...normalize(res.data, MANUFACTURER_ARR)
//     	})
//     	// return dispatch({
//     	// 	type: types.LOAD_SUCCESS_MANUFACTURES,
//       //   errors: [],
//     	// 	...normalize(res.data, MANUFACTURER_ARR)
//     	// })
//     }).catch((error) => {
//
//       console.error(error);
//       // return dispatch({
//     	// 	type: types.LOAD_FAILURE_MANUFACTURES,
//     	// 	errors: error.response.data.errors
//     	// })
//     })
// )
//
// export const loadRegions = (filters) => (dispatch) => {
//   // dispatch({
//   //   type: types.LOAD_REQUEST_MANUFACTURES,
//   //   filters
//   // })
//
//   axios
// 		.get(`/api/admin/regions${createParams(filters)}`)
// 		.then((res) => {
//       console.log('data: ', res.data);
//
// 			return dispatch({
// 				type: types.LOAD_SUCCESS_REGIONS,
//         data: res.data.data || []
//         // errors: [],
// 				// ...normalize(res.data, MANUFACTURER_ARR)
// 			})
// 			// return dispatch({
// 			// 	type: types.LOAD_SUCCESS_MANUFACTURES,
//       //   errors: [],
// 			// 	...normalize(res.data, MANUFACTURER_ARR)
// 			// })
// 		}).catch((error) => {
//
//       console.error(error);
//       return dispatch({
// 				type: types.LOAD_FAILURE_REGIONS,
//         data: [],
// 				// errors: error.response.data.errors
// 			})
//     })
//   }
