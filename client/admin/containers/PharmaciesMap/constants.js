const module = 'pharmaciesMap';

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const LOAD_REQUEST_CITIES = `${module}/LOAD_REQUEST_CITIES`;
export const LOAD_SUCCESS_CITIES = `${module}/LOAD_SUCCESS_CITIES`;
export const LOAD_FAILURE_CITIES = `${module}/LOAD_FAILURE_CITIES`;

export const LOAD_REQUEST_REGIONS = `${module}/LOAD_REQUEST_REGIONS`;
export const LOAD_SUCCESS_REGIONS = `${module}/LOAD_SUCCESS_REGIONS`;
export const LOAD_FAILURE_REGIONS = `${module}/LOAD_FAILURE_REGIONS`;

export const CLOSE_MESSAGE = `${module}/CLOSE_MESSAGE`;

export const SET_FILTER = `${module}/SET_FILTER`;
export const SET_FILTER_CITIES = `${module}/SET_FILTER_CITIES`;
export const SET_FILTER_REGIONS = `${module}/SET_FILTER_REGIONS`;

export const SELECT_PHARMACY = `${module}/SELECT_PHARMACY`;
export const CHANGE_PARAM = `${module}/CHANGE_PARAM`;
export const PLACE_CHANGED = `${module}/PLACE_CHANGED`;
export const EDIT = `${module}/EDIT`;

export const RESET = `${module}/RESET`;

export default module;
