import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

import { Link, browserHistory } from 'react-router';

import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';
import Select from 'react-select';

import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';

import Breadcrumbs from '../../components/Breadcrumbs/index';
// import Portlet from '../../components/Portlet/index';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';

import MultiMarkersMap from '../../components/GoogleMap/MultiMarkersMap';

import './style.css';

const HEIGHT = 700;

class PharmaciesMap extends Component {
  static propTypes = {
    selectedPharmacy: PropTypes.number,
    // errors:        PropTypes.arrayOf(PropTypes.object),
    // selectedItems: PropTypes.objectOf(PropTypes.object),
    // filters:       PropTypes.objectOf(PropTypes.any).isRequired,
    location:         PropTypes.objectOf(PropTypes.any).isRequired,
    // params:        PropTypes.shape({
    //   id:         PropTypes.string.isRequired,
    //   pharmacyId: PropTypes.string.isRequired,
    // }),
    load:             PropTypes.func.isRequired,
    loadRegions:      PropTypes.func.isRequired,
    loadCities:       PropTypes.func.isRequired,
    setFilter:        PropTypes.func.isRequired,
    changeParam:      PropTypes.func.isRequired,
    setFilterCities:  PropTypes.func.isRequired,
    setFilterRegions: PropTypes.func.isRequired,
    selectPharmacy:   PropTypes.func.isRequired,
    placeChanged:     PropTypes.func.isRequired,
    reset:            PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);

    const { load, loadCities, filters, filterCities } = this.props;

    this.debounceLoadPharmacies = _debounce(load, 250);
    load(filters);
    loadCities(filterCities);
  }

  componentWillReceiveProps(next) {
    const {
      load,
      loadRegions,
      loadCities,
      filters,
      filterRegions,
      filterCities,
      selectedPharmacy,
    } = this.props;
    const {
      isFiltersDebounce,
      filters: nextFilters,
      filterRegions: nextFilterRegions,
      filterCities: nextFilterCities,
      selectedPharmacy: nextSelectedPharmacy,
    } = next;

    if (filters !== nextFilters) {
      isFiltersDebounce
      ? this.debounceLoadPharmacies(nextFilters)
      : load(nextFilters);
    }

    if (((filters.filters.city !== nextFilters.filters.city) && nextFilters.filters.city !== '') // ||
      //  (filterRegions !== nextFilterRegions)
       ) {
      loadRegions(nextFilterRegions);
    }

    if (filterCities != nextFilterCities) {
      loadCities(nextFilterCities);
    }

    // Коли натискаємо на аптеці на карті, то список прокручується до відповідної аптеки
    console.log({ selectedPharmacy, nextSelectedPharmacy });
    if (nextSelectedPharmacy && (selectedPharmacy !== nextSelectedPharmacy)) {
      this[`pharmacy${nextSelectedPharmacy}`].scrollIntoView(false);
    }
  }

  // componentWillUnmount() {
  //   this.reset();
  // }
  onChangeParamSelect = key => (obj) => {
    this.props.changeParam(key, obj ? obj.value : '');
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }

  setFilterCities = (key, isFiltersDebounce) => (value = '') => {
    if (value != '' || (value == '' && (this.props.filterCities.filters.name.value != ''))) {
      this.props.setFilterCities(key, value, isFiltersDebounce);
    }
  }
  setFilterRegions = (key, isFiltersDebounce) => (value = '') => {
    if (value != '' || (value == '' && (this.props.filterRegions.filters.name.value != ''))) {
      this.props.setFilterRegions(key, value, isFiltersDebounce);
    }
  }
  reset = () => {
    this.props.reset();
  }
  onEdit = id => (e) => {
    e.preventDefault();
    browserHistory.push(`/admin/pharmacies/${id || ''}`);
  }
  selectPharmacy = ({ id }) => {
    this.props.selectPharmacy(id);
  }
  onPlacesChanged = (latLng) => {
    this.props.placeChanged(latLng);
  }
  // param: fieldValue, row, rowIdx, colIdx
  selectedPharmacyFormat = id => (fieldValue, row) => {
    return classNames(
      'vertical-middle',
      { 'bg-green-meadow bg-font-green-meadow': id === row.id },
      { 'bg-default bg-font-default': !(row.lat && row.lng) },
    )
  };

  render() {
    const {
      location,
      errors,
      pharmacies,
      citiesOptions,
      regionsOptions,
      filters,
      filterRegions,
      markers,
      selectedPharmacy,
      mapCenter,
    } = this.props;
    console.log('this.props', this.props);
    const dataFormatEdit = id => (
      <Link
        to={{
          state: {
            returnTo: location.pathname,
          },
          pathname: `/admin/pharmacies/${id || ''}`,
        }}
      >
        <i
          title="Редагувати"
          className="fa fa-lg fa-pencil font-yellow"
          style={{ cursor: 'pointer' }}
        />
      </Link>
    );

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          title=""
          components={{
            title: 'Всі аптеки',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Всі аптеки',
                to:   '/admin/pharmacies',
              }, {
                name: 'Мапа',
              },
            ],
          }}
        />
        <Row>
          <Col md={3} sm={6}>
            <div className="form-group">
              <label htmlFor="name">Найменування</label>
              <input
                id="name"
                type="text"
                className="form-control"
                placeholder="Введіть найменування"
                value={filters.filters.name.value}
                onChange={this.setFilter(['filters', 'name'], true)}
              />
            </div>
          </Col>
          <Col md={3} sm={6}>
            <div className="form-group">
              <label htmlFor="address">Адреса</label>
              <input
                id="address"
                type="text"
                className="form-control"
                placeholder="Введіть адресу"
                value={filters.filters.address.value}
                onChange={this.setFilter(['filters', 'address'], true)}
              />
            </div>
          </Col>
          <Col md={3} sm={6}>
            <div className="form-group">
              <label htmlFor="city">Місто</label>
              <Select
                id="city"
                name="city"
                placeholder="Введіть місто"
                value={filters.filters.city}
                options={citiesOptions}
                onInputChange={this.setFilterCities(['filters', 'name'], true)}
                onChange={this.onChangeParamSelect('city')}
                // onOpen={this.loadManufacturers}
                noResultsText="Нічого не знайдено"
                clearValueText="Стерти"
                clearable
                // isLoading={isLoadingManufacturer}
                // onCloseResetsInput={false}
                onClose={this.setFilterCities(['filters', 'name'], true)}
              />
            </div>
          </Col>
          <Col md={3} sm={6}>
            <div className="form-group">
              <label htmlFor="region">Район</label>
              <Select
                id="region"
                name="regions"
                placeholder="Введіть район"
                // value={pharmacies.region}
                value={filters.filters.region}
                options={regionsOptions}
                onInputChange={this.setFilterRegions(['filters', 'name'], true)}
                onChange={this.onChangeParamSelect('region')}
                // onOpen={this.loadManufacturers}
                noResultsText="Нічого не знайдено"
                clearValueText="Стерти"
                clearable
                // isLoading={isLoadingManufacturer}
                // onCloseResetsInput={false}
                onClose={this.setFilterRegions(['filters', 'name'], true)}
              />
            </div>
          </Col>
        </Row>

        <Row>
          <Col md={4} sm={5}>
            <CustomTable
              tableContainerClass="margin-0"
              tableClass="table table-striped table-bordered table-hover"
              data={errors.length ? [] : pharmacies}
              striped
              hover
              tableStyle={{ overflowY: 'auto' }}
              height={HEIGHT}
              onRowClick={this.selectPharmacy}
              selectRow={selectedPharmacy}
              selectedRowClassName={'bg-green-meadow bg-font-green-meadow'}
            >
              <HeaderColumn
                dataField="name"
                width="95%"
                columnClassName={(cell, row) =>
                  !console.log('cell', cell) &&
                  !console.log('row', row) &&
                  classNames(
                    'vertical-middle',
                    { 'bg-green-meadow bg-font-green-meadow': selectedPharmacy === row.id },
                    { 'bg-default bg-font-default': !(row.lat && row.lng) },
                  )
                }
                dataFormat={(cell, row) =>
                  <span ref={(node) => { this[`pharmacy${row.id}`] = node; }}> {cell} </span>
                }
              >
                Найменування
              </HeaderColumn>
              <HeaderColumn
                dataField="id"
                width="5%"
                isKey
                dataAlign="center"
                columnClassName={this.selectedPharmacyFormat(selectedPharmacy)}
                dataFormat={dataFormatEdit}
              />
            </CustomTable>
          </Col>
          <Col md={8} sm={7}>
            <div style={{ height: `${HEIGHT}px` }}>
              <MultiMarkersMap
                markers={markers}
                onMarkerClick={this.selectPharmacy}
                selectedMarker={selectedPharmacy}
                onPlacesChanged={this.onPlacesChanged}
                center={mapCenter}
              />
            </div>
          </Col>
        </Row>

      </div>
    );
  }
}

export default connect(selectors, actions)(PharmaciesMap);
