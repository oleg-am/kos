import { combineReducers } from 'redux';
import * as types from './constants';

import { setValue, setFiltersValue } from '../../helpers/setObjValue';

const initialState = {
  entities: {
    pharmacies: {},
  },
  mapCenter: {
    lat: 50.4501,
    lng: 30.5234,
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const imgUrl = (state = '', action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS_IMG_URL:
    case types.LOAD_FAILURE_IMG_URL:
      return action.data;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_CITIES:
    case types.SET_FILTER_REGIONS:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const entities = (state = initialState.entities, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities;
    case types.LOAD_FAILURE:
      return {
        ...state,
        pharmacies: {},
      };
    default:
      return state;
  }
};

const selectedPharmacy = (state = null, action) => {
  switch (action.type) {
    case types.SELECT_PHARMACY:
      return state === action.id ? null : action.id;
    default:
      return state;
  }
};

const mapCenter = (state = initialState.mapCenter, action) => {
  switch (action.type) {
    case types.SELECT_PHARMACY:
      return state === action.id ? null : action.id;
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data;
    default:
      return state;
  }
};

const filtersInit = {
  nolimit: true,

  with: {
    '*': {
      value: '*',
    },
  },
  order_by: {
    id: {
      value: 'ASC',
    },
  },
  filters: {
    name: {
      comparison: 'like',
      value:      '',
      modifier:   'lower',
    },
    address: {
      comparison: 'like',
      value:      '',
      modifier:   'lower',
    },
    city:   '',
    region: '',
  },
};

const filters = (state = filtersInit, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.CHANGE_PARAM:
      if (['city', 'region'].includes(action.key)) {
        return {
          ...state,
          ...setValue(state, ['filters', action.key], action.value),
        };
      }
      return state;
    default:
      return state;
  }
};

const cities = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS_CITIES:
      return action.data;
    default:
      return state;
  }
};

const regions = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS_REGIONS:
      return action.data;
    case types.LOAD_FAILURE_REGIONS:
    case types.RESET:
      return [];
    case types.CHANGE_PARAM:
      if (action.key === 'city') {
        if (action.value === '') {
          return [];
        }
      }
      return state;
    default:
      return state;
  }
};

const filterCitiesInit = {
  limit: 20,
  // with: {
  //   '*': {
  //     value: '*',
  //   }
  // },
  filters: {
    name: {
      comparison: 'like',
      value:      '',
      modifier:   'lower',
    },
  },
};

const filterCities = (state = filterCitiesInit, action) => {
  switch (action.type) {
    case types.SET_FILTER_CITIES:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.RESET:
      return filterCitiesInit;
    default:
      return state;
  }
};

const filterRegionsInit = {
  // page: 1,
  limit: 20,
  fields: 'id,name',
  // with: {
  //   'city': {
  //     value: '',
  //   }
  // },
  filters: {
    name: {
      comparison: 'like',
      value:      '',
      modifier:   'lower',
    },
    city: '',
  },
};

const filterRegions = (state = filterRegionsInit, action) => {
  switch (action.type) {
    case types.SET_FILTER_REGIONS:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.CHANGE_PARAM:
      if (action.key === 'city') {
        return {
          ...state,
          ...setValue(state, ['filters', 'city'], action.value),
        };
      }
      return state;
    case types.RESET:
      return filterRegionsInit;
    default:
      return state;
  }
};

export default combineReducers({
  imgUrl,
  errors, // []
  entities,
  ids,
  filters, // {}
  cities, // {}
  regions, // {}
  isFiltersDebounce, // false
  filterCities,
  filterRegions,
  selectedPharmacy,
  mapCenter,
});
