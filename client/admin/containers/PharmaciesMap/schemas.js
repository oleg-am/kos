import { Schema, arrayOf } from 'normalizr';

const pharmacies = new Schema('pharmacies');
const phones = new Schema('phones');
const city = new Schema('city');
const region = new Schema('region');
const network = new Schema('network');

pharmacies.define({
  city,
  region,
  network,
  // deliverySchedules: arrayOf(deliverySchedules, { schemaAttribute: 'day' }),
  phones: arrayOf(phones),
});

const networks = new Schema('networks');

export const PHARMACIES_ARR = { data: arrayOf(pharmacies) };
export const NETWORKS_ARR = arrayOf(networks);
export const PHONES_ARR = arrayOf(phones);
