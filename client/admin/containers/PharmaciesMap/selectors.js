import { createSelector, createStructuredSelector } from 'reselect';

import MODULE from './constants';


const pharmaciesEntities = state => state[MODULE].entities.pharmacies;

const selectedPharmacy   = state => state[MODULE].selectedPharmacy;

const cities            = state => state[MODULE].cities;
const regions           = state => state[MODULE].regions;
const isFiltersDebounce = state => state[MODULE].isFiltersDebounce;
const errors            = state => state[MODULE].errors;
const errorsValidation  = state => state[MODULE].errorsValidation;

const isSuccessMessage = state => state[MODULE].isSuccessMessage;
const isErrorMessage   = state => state[MODULE].isErrorMessage;
const isSavePhones     = state => state[MODULE].isSavePhones;

const pharmacies = createSelector(pharmaciesEntities, items => Object.values(items));

const filters  = createSelector(state => state[MODULE].filters, items => items);
const filterCities  = createSelector(state => state[MODULE].filterCities, items => items);
const filterRegions = createSelector(state => state[MODULE].filterRegions, items => items);

const createSelectOptions = items => items.map(item => ({
  value: item.id,
  label: item.name,
}));

const citiesOptions  = createSelector(cities, createSelectOptions);
const regionsOptions = createSelector(regions, createSelectOptions);

const markers = createSelector(
  pharmaciesEntities,
  items => Object.values(items)
    .filter(({ lat, lng }) => lat && lng)
    .map(item => ({
      position: {
        lat: item.lat,
        lng: item.lng,
      },
      info: item,
      key:  `${item.lat}${item.lng}`,

      defaultAnimation: 2,
    })),
);

const mapCenter = createSelector(
  pharmaciesEntities,
  selectedPharmacy,
  (items, id) => {
    if (id && items[id] && items[id].lat && items[id].lng) {
      return {
        lat: items[id].lat,
        lng: items[id].lng,
      };
    }
    return null;
  },
);


export default createStructuredSelector({
  pharmacies,
  selectedPharmacy,
  citiesOptions,
  regionsOptions,
  isFiltersDebounce,
  errors,
  errorsValidation,
  filters,
  filterCities,
  filterRegions,
  isSuccessMessage,
  isErrorMessage,
  isSavePhones,
  markers,
  mapCenter,
});
