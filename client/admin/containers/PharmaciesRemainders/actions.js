import * as types from './constants';

import { createParams } from '../../helpers/api';
import REMAINDERS_ARR from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});

export const edit = nomenclature => ({
  type: types.EDIT,
  nomenclature,
});

export const reset = () => ({
  type: types.RESET,
});

export const loadRemainders = (id, filters) => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/pharmacies/${id}/remainders${createParams(filters)}`),
  schema:  REMAINDERS_ARR,
});

export const loadPharmacy = id => ({
  types:   [types.LOAD_PHARMACY_REQUEST, types.LOAD_PHARMACY_SUCCESS, types.LOAD_PHARMACY_FAILURE],
  promise: api => api.get(`/api/admin/pharmacies/${id}?fields=name`),
  // schema:  REMAINDERS_ARR,
});
