const module = 'PHARMACIES_REMAINDERS';

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const LOAD_PHARMACY_REQUEST = `${module}/LOAD_PHARMACY_REQUEST`;
export const LOAD_PHARMACY_SUCCESS = `${module}/LOAD_PHARMACY_SUCCESS`;
export const LOAD_PHARMACY_FAILURE = `${module}/LOAD_PHARMACY_FAILURE`;

export const SET_FILTER = `${module}/SET_FILTER`;
export const CHANGE_PARAM = `${module}/CHANGE_PARAM`;

export const EDIT = `${module}/EDIT`;

export const RESET = `${module}/RESET`;
