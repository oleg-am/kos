import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

// import { Link, browserHistory } from 'react-router'

// import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
// import Select from 'react-select';
import HighlightWords from 'react-highlight-words';

import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';

import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import LimitSelect from '../../components/LimitSelect';
import Paginate from '../../components/Paginate';

import { dateFormatter } from '../../components/Formatters';
import FilterSearch from '../../components/FilterSearch';
import LinkClose from '../../elements/LinkClose';

// import LinkEdit from '../../elements/LinkEdit';

class PharmaciesRemainders extends React.Component {

  static propTypes = {
    location: PropTypes.objectOf(PropTypes.any).isRequired,
    errors:  PropTypes.arrayOf(PropTypes.object),
    filters: PropTypes.objectOf(PropTypes.any).isRequired,
    params:  PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
    pharmacy:       PropTypes.objectOf(PropTypes.any),
    remainders:     PropTypes.arrayOf(PropTypes.object).isRequired,
    paging:         PropTypes.objectOf(PropTypes.any),
    setFilter:      PropTypes.func.isRequired,
    changeParam:    PropTypes.func.isRequired,
    reset:          PropTypes.func.isRequired,
    loadRemainders: PropTypes.func.isRequired,
    loadPharmacy:   PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const { params: { id }, loadRemainders, filters, loadPharmacy } = this.props;

    this.debounceLoadRemainders = _debounce(loadRemainders, 200);

    loadRemainders(id, filters);
    loadPharmacy(id);
  }

  componentWillReceiveProps(next) {
    const { isFiltersDebounce, filters: nextFilters } = next;
    const { loadRemainders, filters, params: { id } } = this.props;

    if (filters !== nextFilters) {
      const newfilters = filters.page === nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters;

      isFiltersDebounce
      ? this.debounceLoadRemainders(id, newfilters)
      : loadRemainders(id, newfilters);
    }
  }

  componentWillUnmount() {
    this.reset();
  }

  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterAfterESC = key => (e) => {
    e.keyCode === 27 && this.props.setFilter(key, '');
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  reset = () => {
    this.props.reset();
  }
  toggleComparison = key => (value) => {
    this.props.changeParam(key, value);
  }
  refreshData = () => {
    this.props.loadRemainders(this.props.params.id, this.props.filters);
  }
  render() {
    const { errors, remainders, paging, filters, params: { id }, location, pharmacy } = this.props;

    const { returnTo } = location.state || {};

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          components={{
            title: <span> Залишки <span style={{ color: '#ed6b75' }}>{pharmacy.name}</span></span>,
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Всі аптеки',
                to:   '/admin/pharmacies',
              }, {
                name: 'Аптека',
                to:   `/admin/pharmacies/${id}`,
              }, {
                name: 'Залишки',
              },
            ],
          }}
        />
        {/* <PortletBody> */}

        <Portlet
          actionsLeft={[
            <LimitSelect key="actionsLeft_1" optionsValue={[10, 15, 20]} value={filters.limit} onChange={this.setFilter('limit')} />,
              // // <input
              //   type="text"
              //   style={{width: 'inherit', display: 'inline-block'}}
              //   className="form-control"
              //   placeholder="Фільтер найменуванню"
              // />
          ]}

          actionsRight={[
            <LinkClose key="actionsRight_5" to={returnTo || '/admin/pharmacies'} />,
            // <Link style={{marginRight: '5px'}} key="actionsRight_3" onClick={this.refreshData} className="btn btn-sm blue-sharp">
            //   <i className="fa fa-refresh" />
            //   {/* Оновити дані */}
            // </Link>,
            // <Link key="actionsRight_4" className="btn btn-sm default" onClick={this.reset} to={`/admin/pharmacies/${id}`}>
            //   <i className="fa fa-times" />
            // </Link>,

          ]}
        >

          <CustomTable
            tableClass="table table-striped table-bordered table-hover"
            data={errors.length ? [] : remainders}
            striped
            hover
            pagination
            filters
          >
            <HeaderColumn highLightWords={[filters.filters['nomenclature.name'].value]} dataField="id" width="5%" dataAlign="center" isKey> № </HeaderColumn>
            <HeaderColumn
              dataField="nomenclature"
              width="40%"
              dataFormat={(cell, { nomenclature }) => (
                <HighlightWords
                  searchWords={[filters.filters['nomenclature.name'].value]}
                  textToHighlight={nomenclature.name}
                />
              )}
              filters={
                <FilterSearch
                  value={filters.filters['nomenclature.name'].value}
                  onChangeValue={this.setFilter(['filters', 'nomenclature.name'], true)}
                  onKeyUpValue={this.setFilterAfterESC(['filters', 'nomenclature.name'])}
                  comparison={filters.filters['nomenclature.name'].comparison}
                  onChangeCheckbox={this.toggleComparison(['filters', 'nomenclature.name', 'comparison'])}
                />
              }
            > Найменування </HeaderColumn>
            <HeaderColumn dataField="quantity"> Кількість </HeaderColumn>
            <HeaderColumn dataField="price" > Ціна Аптеки</HeaderColumn>
            <HeaderColumn dataField="sell_price" > Ціна Резерву</HeaderColumn>
            <HeaderColumn dataField="stockPrice" > Ціна Cкладу </HeaderColumn>
            {/* <HeaderColumn dataField="purchasePrice"> Ціна закупки </HeaderColumn> */}
            <HeaderColumn dataField="expiredAt" dataFormat={dateFormatter} > Термін придатності </HeaderColumn>
            {/* <HeaderColumn
              dataField="id" width="5%" dataAlign="center" columnClassName="vertical-middle"
              dataFormat={(cell, { nomenclature }) => (
                <LinkEdit to={`/admin/nomenclature/${nomenclature.id}`} />
              )}
            /> */}

          </CustomTable>

        </Portlet>

        {!errors.length && !!remainders.length &&
          <Paginate
            pageNum={paging.totalPages}
            clickCallback={this.setFilterPage}
            forceSelected={paging.page - 1}
          />
        }
      </div>
    );
  }
}

export default connect(selectors, actions)(PharmaciesRemainders);
