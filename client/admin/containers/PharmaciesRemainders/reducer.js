import { combineReducers } from 'redux';
import * as types from './constants';

import { setFiltersValue, setValue } from '../../helpers/setObjValue';

const initialState = {
  pharmacy: {
    name: '',
  },
  filters: {
    page:  1,
    limit: 10,

    with: {
      '*': {
        value: '*',
      },
    },
    order_by: {
      id: {
        value: 'ASC',
      },
    },
    filters: {
      'nomenclature.name': {
        comparison: 'begins', // 'like'
        value:      '',
        modifier:   'lower',
      },
    },
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const pharmacy = (state = initialState.pharmacy, action) => {
  switch (action.type) {
    case types.LOAD_PHARMACY_SUCCESS:
      return action.data;
    case types.RESET:
      return initialState.pharmacy;
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data;
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const remaindersEntities = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities.remainders || {};
    default:
      return state;
  }
};

const stockEntities = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities.stock || {};
    default:
      return state;
  }
};

const paging = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.paging;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.CHANGE_PARAM:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
    case types.RESET:
      return { ...initialState.filters };
    default:
      return state;
  }
};

export default combineReducers({
  errors, // []
  pharmacy,
  ids, // []
  paging, // {}
  filters, // {}
  isFiltersDebounce, // false
  remaindersEntities, // {}
  stockEntities,
});
