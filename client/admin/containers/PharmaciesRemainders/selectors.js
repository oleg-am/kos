import { createSelector, createStructuredSelector } from 'reselect';

const REDUCER = 'pharmaciesRemainders';

const pharmacy           = state => state[REDUCER].pharmacy;

const remaindersIds      = state => state[REDUCER].ids;
const remaindersEntities = state => state[REDUCER].remaindersEntities;
const stockEntities      = state => state[REDUCER].stockEntities;

const filtersState       = state => state[REDUCER].filters;

const paging             = state => state[REDUCER].paging;
const isFiltersDebounce  = state => state[REDUCER].isFiltersDebounce;
const errors             = state => state[REDUCER].errors;

const remainders = createSelector(
  remaindersIds,
  remaindersEntities,
  stockEntities,
  (ids, items, stockEnt) => ids.map((id) => {
    const stock = stockEnt[items[id].stock];
    return {
      ...items[id],
      stockPrice: stock ? stock.price : '',
    };
  }),
);

export const filters = createSelector(filtersState, items => items);

export default createStructuredSelector({
  pharmacy,
  paging,
  isFiltersDebounce,
  errors,
  remainders,
  filters,
});
