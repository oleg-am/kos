import * as types from './constants';
// import { createParams } from '../../helpers/api';

import { PRICE_PARAMS_ARR } from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});

export const saveValidatorErrors = errors => ({
  type: types.SAVE_VALIDATE_ERRORS,
  errors,
});

export const load = () => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get('/api/admin/configs'),
  schema:  PRICE_PARAMS_ARR,
});

export const save = (param, data) => ({
  types:   [types.SAVE_REQUEST, types.SAVE_SUCCESS, types.SAVE_FAILURE],
  promise: api => api.put(`/api/admin/configs/${param}`, data),
  param,
});
