import React, { PropTypes } from 'react';
import classNames from 'classnames';

import Icon from '../../../elements/Icon';

const propTypes = {
  param:    PropTypes.string.isRequired,
  value:    PropTypes.string.isRequired,
  label:    PropTypes.string.isRequired,
  helpText: PropTypes.string,
  error:    PropTypes.string,
  isSaving: PropTypes.bool,
  isEdited: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onSave:   PropTypes.func.isRequired,
};

const defaultProps = {
  param:    '',
  value:    '',
  label:    '',
  helpText: '',
  error:    '',
  isSaving: false,
  isEdited: false,
  onChange: null,
  onSave:   null,
};

const InputGroup = ({
  param,
  value,
  label,
  helpText,
  error,
  isSaving,
  isEdited,
  onChange,
  onSave,
  ...rest
}) => (
  <div className={classNames('form-group', { 'has-error': error })}>
    <label htmlFor={param} className="col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label"> {label} </label>
    <div
      className="input-group col-xs-5 col-sm-5 col-md-6 col-lg-6"
      style={{ ...(isEdited ? {} : { paddingRight: '51px' }) }}
    >
      <input id={param} value={value} onChange={onChange} {...rest} className="form-control" />
      <span className="input-group-btn">
        {isEdited &&
          <button
            type="button"
            title="Зберегти"
            className="btn bg-blue bg-font-blue"
            disabled={isSaving}
            onClick={onSave}
          >
            {isSaving
              ? <i className="fa fa-spinner fa-pulse fa-lg fa-fw" />
              : <Icon icon="check" size="lg" collor="default" />
            }
          </button>
        }
      </span>
      {helpText && <span className="help-block"> {helpText} </span>}
    </div>
  </div>
);

InputGroup.propTypes = propTypes;
InputGroup.defaultProps = defaultProps;

export default InputGroup;
