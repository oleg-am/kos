const module = 'prices';

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const SAVE_REQUEST = `${module}/SAVE_REQUEST`;
export const SAVE_SUCCESS = `${module}/SAVE_SUCCESS`;
export const SAVE_FAILURE = `${module}/SAVE_FAILURE`;

export const SET_FILTER   = `${module}/SET_FILTER`;

export const SAVE_VALIDATE_ERRORS   = `${module}/SAVE_VALIDATE_ERRORS`;

export const CHANGE_PARAM = `${module}/CHANGE_PARAM`;

export default module;
