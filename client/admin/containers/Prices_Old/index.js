import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

// import { Link } from 'react-router';

import { Row, Col } from 'react-bootstrap';

// import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';
import { validator } from './schemas';

import InputGroup from './components/InputGroup';

import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

class Prices extends React.Component {
  static propTypes = {
    isSaving:            PropTypes.objectOf(PropTypes.bool).isRequired,
    isEdited:            PropTypes.objectOf(PropTypes.bool).isRequired,
    // errors:              PropTypes.arrayOf(PropTypes.object),
    priceParams:         PropTypes.objectOf(PropTypes.object).isRequired,
    load:                PropTypes.func.isRequired,
    setFilter:           PropTypes.func.isRequired,
    changeParam:         PropTypes.func.isRequired,
    save:                PropTypes.func.isRequired,
    saveValidatorErrors: PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const { load } = this.props;

    // this.debounceLoad = _debounce(load, 200);

    load();
  }

  onChangeParam = (key, value) => (e) => {
    this.props.changeParam(key, value || e.target.value);
  }

  onSave = (param, value) => (e) => {
    const { save, saveValidatorErrors } = this.props;

    const paramValidator = validator[param];
    const validData = paramValidator.validate({ [param]: value });

    validData
      ? save(param, { value: validData[param] })
      : saveValidatorErrors(paramValidator.getErrors());

    e.preventDefault();
  }

  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  render() {
    const {
      priceParams: {
        basicDiscount,
        ordersAutoReloadInterval,
        ordersAutoReloadEnable,
      },
      isSaving,
      isEdited,
    } = this.props;

    return (
      <div style={{ paddingTop: '40px' }} >
        <Breadcrumbs
          title=""
          components={{
            title: 'Налаштування цін',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Налаштування цін',
              },
            ],
          }}
        />

        <Portlet
          actionsRight={[
            // <button style={{ marginRight: '10px' }} className="btn green-meadow"
              // onClick={this.onSave}
              // >
            //   <i className="fa fa-check" /> Зберегти
            // </button>,
            // <Link key="actionsRight_3" className="btn btn-sm default" to="/admin/settings">
            //   <i className="fa fa-times" /> Закрити
            // </Link>,

          ]}
        >
          <form role="form" className="form-horizontal"> {/* style={{ marginTop: '24px' }} */}
            <Row style={{ marginTop: '15px' }}>
              <Col xs={10} sm={10} md={5}>
                <InputGroup
                  param="basicDiscount"
                  type="number"
                  min="0"
                  max="100"
                  step="0.1"
                  label="Налаштування знижки, %:"
                  placeholder="Введіть знижку"
                  title="Введіть знижку"
                  value={basicDiscount.value}
                  onChange={this.onChangeParam(['basicDiscount', 'value'])}
                  onSave={this.onSave('basicDiscount', basicDiscount.value)}
                  isSaving={isSaving.basicDiscount}
                  isEdited={isEdited.basicDiscount}
                  // helpText="Максимум 12 символів, приклад: 380671234567"
                  // error={validateErors.tel}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={10} sm={10} md={5}>
                <InputGroup
                  param="ordersAutoReloadInterval"
                  type="number"
                  min="0"
                  // max="100"
                  // step="0.1"
                  label="Налаштування автооновлення, сек:"
                  placeholder="Введіть інтервал"
                  title="Введіть інтервал"
                  value={ordersAutoReloadInterval.value}
                  onChange={this.onChangeParam(['ordersAutoReloadInterval', 'value'])}
                  onSave={this.onSave('ordersAutoReloadInterval', ordersAutoReloadInterval.value)}
                  isSaving={isSaving.ordersAutoReloadInterval}
                  isEdited={isEdited.ordersAutoReloadInterval}
                  // helpText="Максимум 12 символів, приклад: 380671234567"
                  // error={validateErors.tel}
                />
              </Col>
              <Col xs={2} sm={2} md={1}>
                {ordersAutoReloadEnable.value === 'true' ?
                  <button onClick={this.onSave('ordersAutoReloadEnable', 'false')} className="btn btn-icon-only green-meadow" title="Виключити">
                    <i className="fa fa-toggle-off" />
                  </button>
                  : <button onClick={this.onSave('ordersAutoReloadEnable', 'true')} className="btn btn-icon-only red" title="Активувати">
                    <i className="fa fa-toggle-on" />
                  </button>
                }
              </Col>
            </Row>
          </form>

        </Portlet>

      </div>
    );
  }
}

export default connect(selectors, actions)(Prices);
