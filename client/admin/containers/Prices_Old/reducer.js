import { combineReducers } from 'redux';
import * as types from './constants';

import { setValue } from '../../helpers/setObjValue';

const initialState = {
  validateErors: {},
  isSaving: {
    basicDiscount: false,
  },
  isEdited: {
    basicDiscount: false,
  },
  priceParams: {
    basicDiscount: {
      value: '0',
    },
    ordersAutoReloadInterval: {
      value: '60',
    },
    ordersAutoReloadEnable: {
      value: 'true',
    },
    pharmacyInactivityInterval: {},
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const isSaving = (state = initialState.isSaving, action) => {
  switch (action.type) {
    case types.SAVE_REQUEST:
      return {
        ...state,
        [action.param]: true,
      };
    case types.SAVE_SUCCESS:
    case types.SAVE_FAILURE:
      return {
        ...state,
        [action.param]: false,
      };
    default:
      return state;
  }
};

const isEdited = (state = initialState.isEdited, action) => {
  switch (action.type) {
    case types.CHANGE_PARAM:
      return {
        ...state,
        [action.key[0]]: true,
      };
    case types.SAVE_SUCCESS:
      return {
        ...state,
        [action.param]: false,
      };
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const priceParams = (state = initialState.priceParams, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return {
        ...state,
        ...action.entities.priceParams,
      };
    case types.CHANGE_PARAM:
      return {
        ...state,
        ...setValue(state, action.key, action.value),
      };
    case types.SAVE_SUCCESS: {
      if (action.param === 'ordersAutoReloadEnable') {
        return {
          ...state,
          ...setValue(state, ['ordersAutoReloadEnable', 'value'], action.data.value),
        };
      }
      return state;
    }
    default:
      return state;
  }
};

// const filters = (state = initialState.filters, action) => {
//   switch (action.type) {
//     case types.SET_FILTER:
//       return {
//         ...state,
//         ...setFiltersValue(state, action.key, action.value),
//       };
//     default:
//       return state;
//   }
// };

export default combineReducers({
  isSaving,
  isEdited,
  errors, // []
  isFiltersDebounce, // false
  priceParams,
});
