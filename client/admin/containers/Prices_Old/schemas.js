import { Schema, arrayOf } from 'normalizr';
import LIVR from 'livr';

LIVR.Validator.defaultAutoTrim(true);

const priceParams = new Schema('priceParams', { idAttribute: 'param' });

const schemas = {
  basicDiscount:            ['string', { number_between: [0, 100] }],
  ordersAutoReloadInterval: ['string', { min_number: 0 }],
  ordersAutoReloadEnable:   { one_of: ['true', 'false'] },
  // basicDiscount: ['integer', { number_between: [0, 100] }],
};
const createIndividualValidators = schema =>
  Object.keys(schema).reduce((obj, key) => {
    obj[key] = new LIVR.Validator({ [key]: schema[key] });
    return obj;
  }, {});

export const PRICE_PARAMS_ARR = { data: arrayOf(priceParams) };

export const validator = createIndividualValidators(schemas);
