import { createSelector, createStructuredSelector } from 'reselect';

import REDUCER from './constants';

const priceParams          = state => state[REDUCER].priceParams;

// const filtersState      = state => state[REDUCER].filters;

const isSaving          = state => state[REDUCER].isSaving;
const isEdited          = state => state[REDUCER].isEdited;
const isFiltersDebounce = state => state[REDUCER].isFiltersDebounce;
const errors            = state => state[REDUCER].errors;

// const userRoles = createSelector(
//   userRolesEntities,
//   userRolesIds,
//   (items, ids) => ids.map(id => items[id]),
// );

// const filters = createSelector(filtersState, items => items);

export default createStructuredSelector({
  // filters,
  isSaving,
  isEdited,
  isFiltersDebounce,
  errors,
  priceParams,
});
