import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Bar } from 'react-chartjs-2';
import { startDefault, endDefault } from './utils/defaultDate';
// import { Link, browserHistory } from 'react-router'
import Select from 'react-select';
import DatePicker from 'react-datepicker';
import _debounce from 'lodash/debounce';
import * as actions from './actions';
import selectors from './selectors';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';
import { CustomTable, HeaderColumn } from '../../components/CustomTable';
const dateFormat = require('dateformat');
const moment = require('moment');

const graphModeOptions = [
    { label: 'Скасовані замовлення', value: 0  },
    { label: 'Виконані замовлення', value: 1 },
    { label: 'Усі замовлення', value: 2 },
];

class ReportOrderByPharmacy extends React.Component {
  static propTypes = {
    reports:        PropTypes.arrayOf(PropTypes.object),
    load:           PropTypes.func.isRequired,
    loadPharmacies: PropTypes.func.isRequired,
    filters:        PropTypes.objectOf(PropTypes.any).isRequired,
    setFilter:      PropTypes.func.isRequired,
    pharmacies:     PropTypes.arrayOf(PropTypes.object),
  }
  constructor(props, context) {
    super(props, context);
    const { load, filters, loadPharmacies } = this.props;

    this.debounceLoadPharmacies = _debounce(loadPharmacies, 400);
    load(filters);
    loadPharmacies(filters);
    this.state = {
      startDate:         null,
      endDate:           null,
      start:             null,
      end:               null,
      graphMode:         false,
      currentGraphValue: graphModeOptions[1],
    }
  }
  componentWillReceiveProps(next) {
    const { isFiltersDebounce, filters: nextFilters, load } = next;
    const { loadPharmacies, filters } = this.props;

    if (filters !== nextFilters) {
      isFiltersDebounce
      ?
      this.debounceLoadPharmacies(nextFilters)
      : loadPharmacies(nextFilters);
      load(nextFilters);
    }
  }


  setFilterSelect = (key, isFiltersDebounce) => (pharmacy) => {
    this.props.setFilter(key, pharmacy ? pharmacy.value : '', isFiltersDebounce);
  }
  setFilterSearchPharmacy = (key, isFiltersDebounce) => (value) => {
    this.props.setFilter(key, value ? value : '', isFiltersDebounce);
  }
  handleChangeStart = (date) => {
    dateFormat.masks.dateForFilter = 'yyyy-mm-dd';
    const start = `${dateFormat(new Date(date), 'dateForFilter')} 00:00:00`;
    if (this.state.endDate) {
      const a = new Date(date);
      const b = new Date(this.state.endDate);
      if (+a > +b) { return; }
    }
    this.setState({ startDate: date, start });
    console.log('start', start);
  };
  handleChangeEnd = (date) => {
    dateFormat.masks.dateForFilter = 'yyyy-mm-dd';
    const end = `${dateFormat(new Date(date), 'dateForFilter')} 23:59:59`;
    const a = new Date(date);
    const b = new Date(this.state.startDate);
    if (+a < +b) { return;}
    this.setState({ endDate: date, end });
    console.log('end', end);
  };
  filterDate = (key, isFiltersDebounce) => () => {
    const { start, end } = this.state;
    this.props.setFilter(key[0], start ? start : '', isFiltersDebounce);
    this.props.setFilter(key[1], end ? end : '', isFiltersDebounce);
  }
  clearDate= (key, isFiltersDebounce) => () => {
    this.setState({ endDate: null, end: null, startDate: null, start: null });
    this.props.setFilter(key[0], startDefault, isFiltersDebounce);
    this.props.setFilter(key[1], endDefault, isFiltersDebounce);
  }
  toggleGraph= () => {
    this.setState({ graphMode: !this.state.graphMode })
  }
  changeGraphMode = (value) =>{

    this.setState({ currentGraphValue: graphModeOptions[value.value] })
  }
  render() {
    const { reports, pharmacies, filters } = this.props;
    const { startDate, endDate, graphMode, currentGraphValue } = this.state;

    const statusValue = ['canceled', 'check', 'count'][currentGraphValue.value];
    const dataSecondGraph = reports.map(each => each[statusValue]);

    const dataFirstGraph = statusValue !== 'canceled' ? reports.map(each => each.totalCost) : [];

    const data    = {
      labels:   reports.map(each => each.date),
      datasets: [
        {
          label:                     'Сума замовлень,грн',
          type:                      'line',
          data:                      dataFirstGraph,
          fill:                      false,
          borderColor:               '#EC932F',
          backgroundColor:           '#EC932F',
          pointBorderColor:          '#EC932F',
          pointBackgroundColor:      '#EC932F',
          pointHoverBackgroundColor: '#EC932F',
          pointHoverBorderColor:     '#EC932F',
          yAxisID:                   'y-axis-2'
        },
        {
          type:                 'bar',
          label:                'Кількість замовлень,шт',
          data:                 dataSecondGraph,
          fill:                 false,
          backgroundColor:      '#71B37C',
          borderColor:          '#71B37C',
          hoverBackgroundColor: '#71B37C',
          hoverBorderColor:     '#71B37C',
          yAxisID:              'y-axis-1'
        }
      ]
    };

    const options = {
      responsive: true,
      tooltips:   {
        mode: 'label'
      },
      elements: {
        line: {
          fill: false
        }
      },
      scales: {
        xAxes: [
          {
            display:   true,
            gridLines: {
              display: false
            },
            labels: {
              show: true
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            },
            type:      'linear',
            display:   true,
            position:  'left',
            id:        'y-axis-1',
            gridLines: {
              display: false
            },
            labels: {
              show: true
            }
          },
          { ticks: {
            beginAtZero: true
          },
            type:      'linear',
            display:   true,
            position:  'right',
            id:        'y-axis-2',
            gridLines: {
              display: false
            },
            labels: {
              show: true
            }
          }
        ]
      }
    };

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          title=""
          components={{
            title: 'Звіти',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports'
              }, {
                name: ''
              }
            ],
          }}
        />
        <Portlet
          actionsLeft={[
            <Select
              key="actionsLeft_1"
              style={{ minWidth: '220px' }}
              name="form-field-name"
              noResultsText="Не найдено результатов"
              placeholder="Вибір аптеки"
              value={filters.pharmacy || ""}
              options={pharmacies}
              onChange={this.setFilterSelect(['pharmacy'], true)}
              onInputChange={this.setFilterSearchPharmacy(['filters', 'name'], true)}
              clearable

            />,


          ]}
          actionsRight={[


            <div
              key="actionsLeft_1"
            >
              {
                <button
                  className={graphMode ? 'btn red' : 'btn green-meadow'}
                  onClick={this.toggleGraph}
                >
                  {graphMode ? 'У вигляді таблиці' : 'У вигляді графіка'}
                </button>
            }
              <span style={{ margin: 'auto 10px' }}>Оберіть дату від</span>
              <DatePicker
                placeholderText="Початкова дата"
                dateFormat="DD/MM/YYYY"
                selected={startDate}
                startDate={startDate}
                endDate={endDate}
                onChange={this.handleChangeStart}
              /> <span style={{ margin: 'auto 5px' }}>до</span>
              <DatePicker
                placeholderText="Кінцева дата"
                dateFormat="DD/MM/YYYY"
                selected={endDate}
                startDate={startDate}
                endDate={endDate}
                onChange={this.handleChangeEnd}
              />
              <button
                disabled={!(endDate && startDate) }
                style={{ margin: 'auto 10px' }}
                className="btn bg-blue bg-font-blue "
                onClick={this.filterDate(['startDate', 'endDate'])}
              >
                <i className="fa fa-check default fa-lg" />
              </button>
              <button
                className="btn bg-red bg-font-red "
                onClick={this.clearDate(['startDate', 'endDate'])}
              >
                <i className="fa fa-close " />
              </button>

            </div>,
          ]}
        >

        </Portlet>
        {!graphMode &&
        <CustomTable
          tableClass="table table-striped table-bordered table-hover width-100"
          data={reports || []}
          striped
          hover
          pagination
          filters
          //trClassName={row => row.pharmacy.isStock && 'bg-yellow-mint'}
            // onRowClick={this.onEdit}
        >
          <HeaderColumn width="10%" dataField="date" dataAlign="center" dataSort dataFormat={date => date} isKey>Дата <br /> замовлення</HeaderColumn>
          <HeaderColumn width="10%" dataField="count" dataAlign="center" dataSort dataFormat={count => count+'шт'}>Кількість</HeaderColumn>
          <HeaderColumn width="10%" dataField="totalCost" dataAlign="center" dataSort dataFormat={totalCost => totalCost+'грн'}>Сума замовлень</HeaderColumn>

        </CustomTable>
      }
        <Portlet
          actionsLeft={[
          <Select
            clearable={false}
            key="Left_1"
            style={{ minWidth: '220px' }}
            value={currentGraphValue}
            options={graphModeOptions}
            onChange={this.changeGraphMode}  />,

          ]}
          />

          { graphMode &&
            <Bar
              data={data}
              options={options}
            />
      }
        </div>

    );
  }
}

export default connect(selectors, actions)(ReportOrderByPharmacy);
