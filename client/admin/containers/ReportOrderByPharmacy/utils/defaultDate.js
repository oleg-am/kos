const moment = require('moment');
// const t = new Date();
// export const end = `${t.getFullYear() + "-" + (t.getMonth()<9 ? '0'+
// (t.getMonth() + 1): t.getMonth() + 1) + "-" +
// new Date(t.getFullYear(),t.getMonth()+1,0).getDate()}`;
// export const start = `${t.getFullYear()}-${(t.getMonth()<9 ? '0'+
// (t.getMonth() + 1): t.getMonth() + 1)}-01`;

export const startDefault = moment().subtract(30, 'days').format('YYYY-MM-DD 00:00:00');
export const endDefault = moment().format('YYYY-MM-DD 23:59:59');
