const name = 'reports';

export const LOAD_REQUEST = `${name}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${name}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${name}/LOAD_FAILURE`;

export const LOAD_PHARMACIES_REQUEST = `${name}/LOAD_REQUEST`;
export const LOAD_PHARMACIES_SUCCESS = `${name}/LOAD_PHARMACIES_SUCCESS`;
export const LOAD_PHARMACIES_FAILURE = `${name}/LOAD_PHARMACIES_FAILURE`;

export const SET_FILTER = `${name}/SET_FILTER`;
export const EDIT = `${name}/EDIT`;

export default name;
