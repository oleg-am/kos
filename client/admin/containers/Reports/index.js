import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { startDefault, endDefault } from './utils/defaultDate';
import { Link, browserHistory } from 'react-router'
// import { push } from 'react-router-redux';

// import classNames from 'classnames';
// import { Row, Col } from 'react-bootstrap';
import Select from 'react-select';
// import Dropzone from 'react-dropzone';

import DatePicker from 'react-datepicker';
import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';
// import { pharmacyValidator } from './schemas.js';

// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
// import LimitSelect from '../../components/LimitSelect';

// import {
//   // priceFormatter,
//   dateFormatter,
//   timeFormatter,
//   statusFormatter,
// } from '../../components/Formatters';
const dateFormat = require('dateformat');
const moment = require('moment');

class Reports extends React.Component {
  static propTypes = {

  }
  constructor(props, context) {
    super(props, context);
  }

  render() {

    return (
      <div className="page-content" style={{ paddingTop: '20px' }}>
        <Breadcrumbs
          title=""
          components={{
            title: 'Звіти',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports'
              }, {
                name: 'Робочий стіл',
                to:   '/admin/'
              }
            ],
          }}
        />

        <CustomTable
          tableClass="table table-striped table-bordered table-hover width-100"
          data={[{ id: 1, reportName: 'Звіт за вказаною датою' }]}
          striped
          hover
          pagination
          filters
          // trClassName={row => row.pharmacy.isStock && 'bg-yellow-mint'}
          // onRowClick={this.onEdit}
        >
          <HeaderColumn width="5%" dataField="id" dataAlign="center"  dataFormat={id => id} iskey>Назва звіту</HeaderColumn>
          <HeaderColumn width="65%" dataField="reportName" dataAlign="center">Переглянути</HeaderColumn>
          <HeaderColumn width="30%" dataField="olol" dataAlign="center" dataFormat={() =>
            <Link to="/admin/reports/orderbypharmacy" >Детальний перегляд </Link>}
          >
          </HeaderColumn>


        </CustomTable>

      </div>
    );
  }
}

export default connect(selectors, actions)(Reports);
