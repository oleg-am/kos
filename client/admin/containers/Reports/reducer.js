import { combineReducers } from 'redux';
import * as types from './constants';
import { setFiltersValue } from '../../helpers/setObjValue';

import { startDefault, endDefault } from './utils/defaultDate';

const initialState = {
  filters: {
    startDate: `${startDefault}`,
    endDate:   `${endDefault}`,
    pharmacy:  '',
    // with: {
    //   '*': {
    //     value: '*',
    //   },
    //},
    // order_by: {
    //   createdAt: {
    //     value: 'DESC',
    //   },
    //},
    filters:   {
      'name': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },

    },
  },
  reports: [],
  pharmacies:[],
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    default:
      return state;
  }
};

const reports = (state = initialState.reports, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.data;
    case types.LOAD_FAILURE:
      return initialState.reports;
    default:
      return state;
  }
};
const pharmacies = (state = initialState.pharmacies, action) => {
  switch (action.type) {
    case types.LOAD_PHARMACIES_SUCCESS:
      return action.data;
    default:
      return state;
  }
};

export default combineReducers({
  filters, // {}
  reports, // []
  pharmacies,
});
