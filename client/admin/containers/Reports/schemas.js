import { Schema, arrayOf } from 'normalizr';

import LIVR from 'livr';
LIVR.Validator.defaultAutoTrim(true);

const phones = new Schema('phones');
const city = new Schema('city');
const region = new Schema('region');
const network = new Schema('network');

const pharmaciesItem = {
  city,
  region,
  network,
  phones: arrayOf(phones),
};

const networks = new Schema('networks');
// const phones = new Schema('phones', { idAttribute: 'ownerName' });
//
// networks.define({
//   phones: arrayOf(phones)
// });

export const PHARMACY_ITEM_OBJ = pharmaciesItem;
export const NETWORKS_ARR = arrayOf(networks);
export const PHONES_ARR = arrayOf(phones);

const pharmacySchema = {
  name:           'string',
  city:           'positive_integer',
  region:         'positive_integer',
  network:        'positive_integer',
  address:        'string',
  description:    'string',
  workOnWeekDays: 'string',
  workOnSunday:   'string',
  workOnSaturday: 'string',
}
const pharmacyPhoneIdSchema = {
  id: [ 'required', 'positive_integer' ],
}
const pharmacyPhoneSchema = {
  pharmacy:       [ 'required', 'positive_integer' ],
  phoneType:      { one_of : ['city', 'mobile', 'inner'] },
  number:         { max_length : 12 },
  ownerName:      'string',
}

export const pharmacyValidator = new LIVR.Validator(pharmacySchema);

export const pharmacyPhonesDeleteValidator = new LIVR.Validator(pharmacyPhoneIdSchema);
export const pharmacyPhonesPostValidator = new LIVR.Validator(pharmacyPhoneSchema);
export const pharmacyPhonesPutValidator = new LIVR.Validator({
    ...pharmacyPhoneIdSchema,
    ...pharmacyPhoneSchema,
});
