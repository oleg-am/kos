import * as types from './constants';
// import { createParams } from '../../helpers/api';

import { USER_ROLES_ARR, options } from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const changeParam = (key, value) => ({
  type: types.CHANGE_PARAM,
  key,
  value,
});

export const goToUserRoles = () => ({
  type: types.GO_TO_USER_ROLES,
});

export const reset = () => ({
  type: types.RESET,
});

export const load = id => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/user_roles/${id}`),
  schema:  USER_ROLES_ARR,

  schemaOptions: options,
});

export const create = data => ({
  types:   [types.CREATE_REQUEST, types.CREATE_SUCCESS, types.CREATE_FAILURE],
  promise: api => api.post('/api/admin/user_roles', data),
});

export const edit = (id, data) => ({
  types:   [types.EDIT_REQUEST, types.EDIT_SUCCESS, types.EDIT_FAILURE],
  promise: api => api.put(`/api/admin/user_roles/${id}`, data),
});
