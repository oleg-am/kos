const module = 'USER_ROLE_ITEM';

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const CREATE_REQUEST = `${module}/CREATE_REQUEST`;
export const CREATE_SUCCESS = `${module}/CREATE_SUCCESS`;
export const CREATE_FAILURE = `${module}/CREATE_FAILURE`;

export const EDIT_REQUEST = `${module}/EDIT_REQUEST`;
export const EDIT_SUCCESS = `${module}/EDIT_SUCCESS`;
export const EDIT_FAILURE = `${module}/EDIT_FAILURE`;

export const GO_TO_USER_ROLES = `${module}/GO_TO_USER_ROLES`;

export const SET_FILTER   = `${module}/SET_FILTER`;
export const CHANGE_PARAM = `${module}/CHANGE_PARAM`;

export const RESET = `${module}/RESET`;
