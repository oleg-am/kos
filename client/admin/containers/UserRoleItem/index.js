import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router';

// import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';
// import Select from 'react-select';

// import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';
import { ROLE_ACCESS, postValidator, putValidator } from './schemas';


// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';

class UserRoleItem extends React.Component {
  static propTypes = {
    isEditableRoleAccess: PropTypes.bool,

    errors:   PropTypes.array.isRequired,
    userRole: PropTypes.object.isRequired,
    params:   PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
    load:        PropTypes.func.isRequired,
    setFilter:   PropTypes.func.isRequired,
    changeParam: PropTypes.func.isRequired,
    create:      PropTypes.func.isRequired,
    edit:        PropTypes.func.isRequired,
    reset:       PropTypes.func.isRequired,
  }

  constructor(props, context) {
    super(props, context);
    const { params: { id }, load } = this.props;

    // this.debounceLoad = _debounce(load, 200);

    this.createItem = id === 'create';

    !this.createItem && load(id);
  }
  componentWillUnmount() {
    this.reset();
  }
  onChangeParam = (key, value) => (e) => {
    this.props.changeParam(key, value || e.target.value);
  }

  onSave = () => {
    const { userRole, create, edit, params: { id } } = this.props;

    const validData = this.createItem
      ? postValidator.validate({ ...userRole })
      : putValidator.validate({ ...userRole });

    if (validData) {
      this.createItem
        ? create(validData)
        : edit(id, validData);
    } else {
      console.log('errors', postValidator.getErrors(), putValidator.getErrors());
    }
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  reset = () => {
    this.props.reset();
  }
  render() {
    const { params: { id }, errors, userRole, isEditableRoleAccess } = this.props;

    const selectRole = (param, isEditable) => cell => (
      <div style={{ padding: 0, margin: '0 0 15px' }} className="form-group form-md-line-input">
        <select value={cell} onChange={this.onChangeParam(param)} className="form-control" disabled={!isEditable}>
          {Object.keys(ROLE_ACCESS).map(key => (
            <option key={`role_${key}`} value={key}>{ROLE_ACCESS[key]}</option>
          ))}
        </select>
        <div className="form-control-focus" />
      </div>
    );

    const renderColumnSelect = isEditable => (key, title, isKey) => (
      <HeaderColumn
        iskey={!!isKey}
        dataField={key}
        width={`${100 / 8}%`}
        dataFormat={selectRole(key, isEditable)}
        dataAlign="center"
        columnClassName="vertical-middle"
      >
        {title}
      </HeaderColumn>
    );

    const renderColumnSelectWithCheckEditable = renderColumnSelect(isEditableRoleAccess);
    // const renderColumnText = (key, title) => (
    //   <HeaderColumn
    //     width="25%"
    //     dataField={key}
    //     dataFormat={cell => (
    //       <div style={{ padding: 0, margin: '0 0 15px' }} className="form-group form-md-line-input">
    //         <input onChange={this.onChangeParam(key)} value={cell} type="text" className="form-control" placeholder="Введіть назву ролі" />
    //         <div className="form-control-focus" />
    //       </div>
    //     )}
    //     dataAlign="center"
    //     columnClassName="vertical-middle"
    //   >
    //     {title}
    //   </HeaderColumn>
    // );

    return (
      <div style={{ paddingTop: '40px' }} >
        <Breadcrumbs
          title=""
          components={{
            title: `${id === 'create' ? 'Створення ролі' : 'Редагування ролі'}`,
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Всі ролі',
                to:   '/admin/settings/user-roles',
              }, {
                name: `${id === 'create' ? 'Створення ролі' : 'Редагування ролі'}`,
              },
            ],
          }}
        />

        <Portlet
          actionsLeft={[
            <div key="actionLepft_1" style={{ padding: 0, margin: '0 0 15px', width: '300px', float: 'left', marginRight: '10px' }} className="form-group form-md-line-input">
              <input
                className="form-control"
                style={{ fontWeight: 'bold', fontSize: '16px', color: 'red' }}
                placeholder="Введіть назву ролі"
                readOnly={!isEditableRoleAccess}
                value={userRole.name} type="text"
                onChange={this.onChangeParam('name')}
              />
              <div className="form-control-focus" />
            </div>,
            <div key="actionLepft_2" style={{ float: 'left' }}>
              <span > Суперюзер </span>
              <button
                disabled={!isEditableRoleAccess}
                onClick={this.onChangeParam('chiefOperator', !userRole.chiefOperator)}
                className={`btn btn-icon-only ${userRole.chiefOperator ? 'green-meadow' : 'red'}`}
                title={userRole.chiefOperator ? 'Заблокувати' : 'Розблокувати'}
              >
                <i className={`fa fa-${userRole.chiefOperator ? 'user' : 'user-times'}`} aria-hidden="true" />
              </button>
            </div>,
          ]}

          actionsRight={[
            isEditableRoleAccess && <button
              key="actionsRight_2"
              style={{ marginRight: '10px' }}
              className="btn green-meadow"
              onClick={this.onSave}
            >
              <i className="fa fa-check" /> {this.createItem ? 'Створити' : 'Зберегти'}
            </button>,
            <Link key="actionsRight_3" className="btn btn-sm default" to="/admin/settings/user-roles">
              <i className="fa fa-times" /> Закрити
            </Link>,

          ]}
        >

          <CustomTable
            tableClass="table table-bordered"
            data={errors.length ? [] : [userRole]}
          >
            {/* {renderColumnText('name', 'Найменування')} */}
            {renderColumnSelectWithCheckEditable('dashboard', 'Роб. стіл', 'isKey')}
            {renderColumnSelectWithCheckEditable('nomenclature', 'Номен-ра')}
            {renderColumnSelectWithCheckEditable('pharmacies', 'Аптеки')}
            {renderColumnSelectWithCheckEditable('clients', 'Клієнти')}
            {renderColumnSelectWithCheckEditable('userManagement', 'Користувачі')}
            {renderColumnSelectWithCheckEditable('config', 'Налашт.')}
            {renderColumnSelectWithCheckEditable('orders', 'Замовл.')}
            {renderColumnSelectWithCheckEditable('reports', 'Звіти')}
          </CustomTable>

          <Row style={{ marginTop: '15px' }}>
            <Col md={3}>
              <div className="form-group">
                <label htmlFor="legend">Легенда</label>
                <ul id="legend" style={{ listStyleType: 'none' }}>
                  <li> П - перегляд </li>
                  <li> Р - редагування </li>
                  <li> Н - вкладка не доступна </li>
                  <li> Х - немає доступу до даних </li>
                </ul>
              </div>
            </Col>
            <Col md={9}>
              <div className="form-group">
                <label htmlFor="description" >Опис ролі</label>
                <textarea
                  id="description"
                  onChange={this.onChangeParam('description')}
                  value={userRole.description}
                  className="form-control"
                  rows="4"
                  readOnly={!isEditableRoleAccess}
                  placeholder="Введіть описання ролі"
                />
              </div>
            </Col>
          </Row>

        </Portlet>

      </div>
    );
  }
}

export default connect(selectors, actions)(UserRoleItem);
