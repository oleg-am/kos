import { combineReducers } from 'redux';
import * as types from './constants';

const initialState = {
  userRole: {
    id:             'create',
    name:           '',
    chiefOperator:  false,
    dashboard:      'N',
    nomenclature:   'N',
    pharmacies:     'N',
    clients:        'N',
    userManagement: 'N',
    config:         'N',
    orders:         'N',
    reports:        'N',
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
    case types.RESET:
      return [];
    default:
      return state;
  }
};

const userRole = (state = initialState.userRole, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.data;
    case types.CHANGE_PARAM:
      return {
        ...state,
        [action.key]: action.value,
      };
    case types.RESET:
      return initialState.userRole;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

export default combineReducers({
  errors, // []
  userRole, // {}
  isFiltersDebounce, // false
});
