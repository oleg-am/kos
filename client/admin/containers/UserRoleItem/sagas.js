import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

import { browserHistory } from 'react-router';

import { CREATE_SUCCESS, EDIT_SUCCESS } from './constants';
import * as actions from './actions';

function* goToUserRoles() {
  yield put(actions.goToUserRoles());
  yield browserHistory.push('/admin/settings/user-roles');
}

function* rootSagas() {
  yield takeEvery([CREATE_SUCCESS, EDIT_SUCCESS], goToUserRoles);
}

export default rootSagas;
