// import { Schema, arrayOf } from 'normalizr';
import LIVR from 'livr';
import CONFIG from '../UserRoles/schemas';

LIVR.Validator.defaultAutoTrim(true);

const accessRule = { one_of: ['W', 'V', 'R', 'N'] };

const userRoleSchema = {
  name:           ['required', 'string'],
  description:    'string',
  chiefOperator:  { one_of: [true, false] },
  dashboard:      accessRule,
  nomenclature:   accessRule,
  pharmacies:     accessRule,
  clients:        accessRule,
  userManagement: accessRule,
  config:         accessRule,
  orders:         accessRule,
  reports:        accessRule,
};

const idSchema = {
  id: ['required', 'positive_integer'],
};

// const userRoles = new Schema('userRoles');

// export const USER_ROLES_ARR = { data: arrayOf(userRoles) };
export const ROLE_ACCESS = CONFIG.access;

export const postValidator = new LIVR.Validator(userRoleSchema);
export const putValidator = new LIVR.Validator({ ...userRoleSchema, ...idSchema });
