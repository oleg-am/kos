import { createStructuredSelector } from 'reselect';

const REDUCER = 'userRoleItem';

const userRole          = state => state[REDUCER].userRole;

const isFiltersDebounce = state => state[REDUCER].isFiltersDebounce;
const errors            = state => state[REDUCER].errors;

export default createStructuredSelector({
  isFiltersDebounce,
  errors,
  userRole,
});
