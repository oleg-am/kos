import * as types from './constants';
import { createParams } from '../../helpers/api';

import { USER_ROLES_ARR, options } from './schemas';

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});

export const load = filters => ({
  types:   [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise: api => api.get(`/api/admin/user_roles${createParams(filters)}`),
  schema:  USER_ROLES_ARR,

  schemaOptions: options,
});

export const deleteItem = id => ({
  types:   [types.DELETE_REQUEST, types.DELETE_SUCCESS, types.DELETE_FAILURE],
  promise: api => api.delete(`/api/admin/user_roles/${id}`),
  id,
});

export const openModal = row => ({
  type: types.OPEN_MODAL,
  row,
});

export const closeModal = () => ({
  type: types.CLOSE_MODAL,
});
