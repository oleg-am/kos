const module = 'USER_ROLES';

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const DELETE_REQUEST = `${module}/DELETE_REQUEST`;
export const DELETE_SUCCESS = `${module}/DELETE_SUCCESS`;
export const DELETE_FAILURE = `${module}/DELETE_FAILURE`;

export const SET_FILTER   = `${module}/SET_FILTER`;

export const OPEN_MODAL  = `${module}/OPEN_MODAL`;
export const CLOSE_MODAL = `${module}/CLOSE_MODAL`;
