import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { Link, browserHistory } from 'react-router';
// import { push } from 'react-router-redux';

// import classNames from 'classnames';
import { Button } from 'react-bootstrap';
// import Select from 'react-select';

import _debounce from 'lodash/debounce';

import * as actions from './actions';
import selectors from './selectors';

import Paginate from '../../components/Paginate';
// import PortletBody from '../../components/PortletBody';
import Breadcrumbs from '../../components/Breadcrumbs/index';
import Portlet from '../../components/Portlet/index';
import Modal, { Header, Title, Footer } from 'react-bootstrap/lib/Modal';

import { CustomTable, HeaderColumn } from '../../components/CustomTable';
import LimitSelect from '../../components/LimitSelect';

class UserRoles extends React.Component {
  static propTypes = {
    isEditableRoleAccess: PropTypes.bool,

    errors:      PropTypes.array.isRequired,
    userRoles:   PropTypes.arrayOf(PropTypes.object).isRequired,
    // params: PropTypes.shape({
    //   id: PropTypes.string.isRequired,
    // }),
    filters:     PropTypes.object.isRequired,
    paging:      PropTypes.object.isRequired,
    currentRole: PropTypes.object.isRequired,
    load:        PropTypes.func.isRequired,
    setFilter:   PropTypes.func.isRequired,
    deleteItem:  PropTypes.func.isRequired,
    closeModal:  PropTypes.func.isRequired,
    openModal:   PropTypes.func.isRequired,
    isOpenModal: PropTypes.bool,
  }

  constructor(props, context) {
    super(props, context);
    const { load, filters } = this.props;

    this.debounceLoad = _debounce(load, 200);

    load(filters);
  }

  componentWillReceiveProps(next) {
    const { isFiltersDebounce, filters: nextFilters } = next;
    const { load, filters } = this.props;

    if (filters !== nextFilters) {
      const newfilters = filters.page === nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters;

      isFiltersDebounce
      ? this.debounceLoad(newfilters)
      : load(newfilters);
    }
  }

  onEdit = item => (e) => {
    e.preventDefault();
    browserHistory.push(`/admin/settings/user-roles/${item.id || ''}`);
  }
  onDelete = id => (e) => {
    const { closeModal, deleteItem } = this.props;
    e.preventDefault();
    deleteItem(id);
    closeModal();
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    this.props.setFilter(key, e.target.value, isFiltersDebounce);
  }
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  refreshData = () => {
    this.props.load(this.props.filters);
  }
  openModal = row => (e) => {
    const { openModal } = this.props;
    e && e.preventDefault();
    openModal(row);
  }
  render() {
    const {
      errors,
      userRoles,
      paging,
      filters,
      isOpenModal,
      openModal,
      closeModal,
      currentRole,
      isEditableRoleAccess,
    } = this.props;

    return (
      <div style={{ paddingTop: '40px' }} >
        <Breadcrumbs
          title=""
          components={{
            title: 'Всі ролі',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              }, {
                name: 'Ролі',
              },
            ],
          }}
        />
        {/* <PortletBody> */}

        <Portlet
          actionsLeft={[
            <LimitSelect key="actionsLeft_1" optionsValue={[10, 15, 20]} value={filters.limit} onChange={this.setFilter('limit')} />,
          ]}

          actionsRight={[
            isEditableRoleAccess && <Link
              key="2"
              to="/admin/settings/user-roles/create"
              className="btn btn-sm green-meadow"
              title="Додати роль"
            >
              <i className="fa fa-plus" />
            </Link>,
            <Link key="3" onClick={this.refreshData} style={{ marginLeft: '10px' }} className="btn btn-sm blue-sharp">
              <i className="fa fa-refresh" />
            </Link>,
          ]}
        >

          <CustomTable
            tableHeaderClass="bg-blue bg-font-blue"
            tableClass="table table-striped table-hover"
            trClassName="vertical-middle"
            data={errors.length ? [] : userRoles}
            striped
            hover
            pagination
            filters={false}
          >
            <HeaderColumn dataField="id" width="5%" dataAlign="center" isKey> № </HeaderColumn>
            {/* <HeaderColumn dataField="name" width="25%" filters={<input type="text" value={filters.filters.name.value} onChange={this.setFilter(['filters', 'name'], true)} className="form-control" placeholder="Введіть найменування" />}> Найменування </HeaderColumn> */}
            <HeaderColumn dataField="name" width="25%" > Найменування </HeaderColumn>
            <HeaderColumn dataField="dashboard"> Раб. стіл </HeaderColumn>
            <HeaderColumn dataField="nomenclature"> Номен-ра </HeaderColumn>
            <HeaderColumn dataField="pharmacies"> Аптеки </HeaderColumn>
            <HeaderColumn dataField="clients"> Клієнти </HeaderColumn>
            <HeaderColumn dataField="userManagement"> Користувачі </HeaderColumn>
            <HeaderColumn dataField="config"> Налашт. </HeaderColumn>
            <HeaderColumn dataField="orders"> Замовл. </HeaderColumn>
            <HeaderColumn dataField="reports"> Звіти </HeaderColumn>
            {/* <HeaderColumn dataField="connected" dataAlign="center" dataFormat={check} > Зв'язок </HeaderColumn> */}
            {/* <HeaderColumn
              dataField="active" dataAlign="center"
              filters={selectTrueFalse(this.setFilter(['filters', 'active']))}
              dataFormat={(cell, row) => (
                <span className={`badge badge-empty ${cell ? 'badge-success' : 'badge-danger'}`}></span>
              )}
            >
              Статус
            </HeaderColumn> */}
            <HeaderColumn
              dataField="edit" width="10%" dataAlign="center" columnClassName="vertical-middle"
              dataFormat={(cell, row) => (
                <btnGroup style={{ whiteSpace: 'nowrap' }}>
                  <a onClick={this.onEdit(row)} title="Редагувати" href="" className="btn btn-icon-only yellow">
                    <i className="fa fa-edit" />
                  </a>
                  <a
                    disabled={!isEditableRoleAccess}
                    onClick={isEditableRoleAccess && this.openModal(row)}
                    title="Видалити"
                    href=""
                    className="btn btn-icon-only red"
                  >
                    <i className="fa fa-times" />
                  </a>
                </btnGroup>
              )}
            ><i className="fa fa-cogs" aria-hidden="true" /></HeaderColumn>

          </CustomTable>

          <div>
            <ul style={{ listStyleType: 'none' }}>
              <li> П - перегляд </li>
              <li> Р - редагування </li>
              <li> Н - вкладка не доступна </li>
              <li> Х - немає доступу до даних </li>
            </ul>
          </div>

        </Portlet>

        {!errors.length && !!userRoles.length &&
          <Paginate
            pageNum={paging.totalPages}
            clickCallback={this.setFilterPage}
            forceSelected={paging.page - 1}
          />
        }

        <Modal show={isOpenModal} onHide={closeModal}>
          <Header closeButton>
            <Title>
              {`Ви впевненi, що бажаєте видалити роль "${currentRole.name}"`}
            </Title>
          </Header>
          <Footer>
            <Button
              onClick={this.onDelete(currentRole.id)}
              className="green-meadow pull-left"
            >
              Підтвердити
            </Button>
            <Button onClick={closeModal} >Закрити</Button>
          </Footer>
        </Modal>
      </div>
    );
  }
}

export default connect(selectors, actions)(UserRoles);
