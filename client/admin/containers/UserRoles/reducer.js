import { combineReducers } from 'redux';

import _omit from 'lodash/omit';

import * as types from './constants';

import { setFiltersValue } from '../../helpers/setObjValue';

const initialState = {
  entities: {
    userRoles: {},
  },
  currentRole: {
    name: '',
    id:   '',
  },
  isOpenModal: false,
  filters:     {
    page:  1,
    limit: 10,

    // with: {
    //   '*': {
    //     value: '*',
    //   }
    // },
    order_by: {
      id: {
        value: 'ASC',
      },
    },
    // filters: {
    //   name: {
    //     comparison: 'like',
    //     value: '',
    //     modifier: 'lower',
    //   },
    //   address: {
    //     comparison: 'like',
    //     value: '',
    //     modifier: 'lower',
    //   },
    // }
  },
};

const errors = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_FAILURE:
      return action.errors;
    case types.LOAD_SUCCESS:
      return [];
    default:
      return state;
  }
};

const isOpenModal = (state = initialState.isOpenModal, action) => {
  switch (action.type) {
    case types.OPEN_MODAL:
      return true;
    case types.CLOSE_MODAL:
      return false;
    default:
      return state;
  }
};

const currentRole = (state = initialState.currentRole, action) => {
  switch (action.type) {
    case types.OPEN_MODAL:
      return action.row;
    case types.CLOSE_MODAL:
      return {};
    default:
      return state;
  }
};

const entities = (state = initialState.entities, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.entities;
    case types.DELETE_SUCCESS:
      return {
        ...state,
        userRoles: _omit(state.userRoles, action.id),
      };
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.data;
    case types.DELETE_SUCCESS:
      return state.filter(id => id !== action.id);
    default:
      return state;
  }
};

const paging = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return action.result.paging;
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
    case types.SET_FILTER_MANUFACTURES:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    default:
      return state;
  }
};

export default combineReducers({
  errors, // []
  entities, // {}
  currentRole, // {}
  ids, // []
  isOpenModal, // false
  paging, // {}
  filters, // {}
  isFiltersDebounce, // false
});
