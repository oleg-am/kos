import { Schema, arrayOf } from 'normalizr';


const CONFIG = {
  access: {
    V: 'П', // view and read
    W: 'Р', // write
    R: 'Н', // read
    N: 'X', // no access
  },
  keys: [
    'dashboard',
    'nomenclature',
    'pharmacies',
    'clients',
    'userManagement',
    'config',
    'orders',
    'reports',
  ],
};

const userRoles = new Schema('userRoles');

// /* eslint no-param-reassign: ["error", { "props": false }]*/

export const options = {
  // params: obj, key, val, originalInput, schema
  assignEntity: (obj, key, val) => {
    let newValue;

    if (CONFIG.keys.includes(key)) {
      newValue = CONFIG.access[val];

      if (newValue) {
        obj[key] = newValue;
      }
    } else {
      obj[key] = val;
    }
  },
};

export const USER_ROLES_ARR = { data: arrayOf(userRoles) };
export default CONFIG;
