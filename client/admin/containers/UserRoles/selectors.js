import { createSelector, createStructuredSelector } from 'reselect';

const REDUCER = 'userRoles';

const userRolesEntities = state => state[REDUCER].entities.userRoles;
const userRolesIds      = state => state[REDUCER].ids;

const filtersState      = state => state[REDUCER].filters;

const isOpenModal            = state => state[REDUCER].isOpenModal;
const paging            = state => state[REDUCER].paging;
const isFiltersDebounce = state => state[REDUCER].isFiltersDebounce;
const errors            = state => state[REDUCER].errors;
const currentRole            = state => state[REDUCER].currentRole;

const userRoles = createSelector(
  userRolesEntities,
  userRolesIds,
  (items, ids) => ids.map(id => items[id]),
);

const filters = createSelector(filtersState, items => items);

export default createStructuredSelector({
  filters,
  isOpenModal,
  paging,
  isFiltersDebounce,
  errors,
  currentRole,
  userRoles,
});
