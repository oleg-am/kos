import * as types from './constants';
import { createParams } from '../../helpers/api';
// import { USERS_ARRAY, validator } from './schemas.js';
// import { normalize } from 'normalizr';

export const saveData = data => ({
  type: types.SAVE_DATA,
  data,
});

export const load = filters => ({
  types:      [types.LOAD_REQUEST, types.LOAD_SUCCESS, types.LOAD_FAILURE],
  promise:    api => api.get(`/api/admin/users${createParams(filters)}`),
  userAction: '',
});

export const loadRoles = () => ({
  types:   [types.LOAD_ROLES_REQUEST, types.LOAD_ROLES_SUCCESS, types.LOAD_ROLES_FAILURE],
  promise: api => api.get('/api/admin/user_roles'),
});

export const addUser = user => ({
  types:       [types.ADD_USER_REQUEST, types.ADD_USER_SUCCESS, types.ADD_USER_FAILURE],
  promise:     api => api.post('/api/admin/users', user),
  isFormValid: true,
  formErrors:  {},
  isOpenModal: false,
  userAction:  'reqUserSuccess',
});

export const editUser = user => ({
  type:         types.EDIT_USER,
  activeUser:   user,
  userAction:   'edit',
  isOpenModal:  true,
  formErrors:   {},
  isChangePass: false,
});

export const saveFormErrors = formErrors => ({
  type:        types.SAVE_FORM_ERRORS,
  isFormValid: false,
  formErrors,
});

export const deleteUser = user => ({
  type:       types.DELETE_USER,
  userAction: 'del',
  activeUser: user,
});

export const updateUser = (user, id) => ({
  types:       [types.UPDATE_USER_REQUEST, types.UPDATE_USER_SUCCESS, types.UPDATE_USER_FAILURE],
  promise:     api => api.put(`/api/admin/users/${id}`, user),
  isFormValid: true,
  formErrors:  {},
  isOpenModal: false,
  userAction:  'reqUserSuccess',
});

export const openModal = () => ({
  type:        types.OPEN_MODAL,
  isOpenModal: true,
});

export const changeActiveUserParam = (param, value) => ({
  type: types.CHANGE_ACTIVE_USER_PARAM,
  param,
  value,
});

export const closeForm = () => ({
  type:         types.CLOSE_FORM,
  isOpenModal:  false,
  formErrors:   {},
  isChangePass: false,
});

export const setChangePass = isChangePass => ({
  type: types.SET_CHANGE_PASS,
  isChangePass,
});
export const resetFilter = () => ({
  type: types.RESET_FILTER,
});

export const setFilter = (key, value, isFiltersDebounce = false) => ({
  type: types.SET_FILTER,
  key,
  value,
  isFiltersDebounce,
});
