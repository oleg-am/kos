import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import classNames from 'classnames';
import { DateField, Calendar } from 'react-date-picker';
import _debounce from 'lodash/debounce';
import { Button, Row, Col } from 'react-bootstrap';
import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';
import HighlightWords from 'react-highlight-words';
import * as actions from './actions';
import selectors from './selectors';
import { validator, validatorPass } from './schemas';
import Breadcrumbs from '../../components/Breadcrumbs';
import Portlet from '../../components/Portlet/index';
import InputGroup from './components/InputGroup';
import Paginate from '../../components/Paginate';
import LimitSelect from '../../components/LimitSelect';
import moment from '../../helpers/moment';

import {
  CustomTable,
  HeaderColumn,
} from '../../components/CustomTable';

import {
  // priceFormatter,
  dateFormatter,
  // timeFormatter,
  // dateAndTimeFormatter,
  // statusFormatter,
  // extraDataObjFormatter,
  // formTextFormatter
} from '../../components/Formatters';

// import {
//   ControlLabel,
//   FormControl,
//   FormGroup,
//   InputText,
//   Select,
//   Textarea,
// } from '../../components/Metronic/Form';

const dateNow = moment(moment().format('YYYY-MM-DD'));

class Users extends Component {
  static propTypes = {
    isEditableRoleAccess:  PropTypes.bool,
    load:                  PropTypes.func.isRequired,
    loadRoles:             PropTypes.func.isRequired,
    resetFilter:           PropTypes.func.isRequired,
    updateUser:            PropTypes.func.isRequired,
    deleteUser:            PropTypes.func.isRequired,
    editUser:              PropTypes.func.isRequired,
    setFilter:             PropTypes.func.isRequired,
    openModal:             PropTypes.func.isRequired,
    closeForm:             PropTypes.func.isRequired,
    addUser:               PropTypes.func.isRequired,
    setChangePass:         PropTypes.func.isRequired,
    saveFormErrors:        PropTypes.func.isRequired,
    changeActiveUserParam: PropTypes.func.isRequired,
    users:                 PropTypes.array.isRequired,
    roles:                 PropTypes.array.isRequired,
    filters:               PropTypes.object,
    paging:                PropTypes.object,
    formErrors:            PropTypes.object,
    activeUser:            PropTypes.object,
    userAction:            PropTypes.string,
    isChangePass:          PropTypes.bool,
    isOpenModal:           PropTypes.bool,
    isEditableRoleAccess:  PropTypes.bool,
  }

  constructor(props) {
    super(props);
    const { load, loadRoles, filters } = this.props;
    this.debounceLoad = _debounce(load, 300);
    load(filters);
    loadRoles();
  }

  state = {
    searchOption: '',
    isOpenSearch: false,
  };

  componentWillReceiveProps = (next) => {
    const { load, filters } = this.props;
    const { isFiltersDebounce, filters: nextFilters } = next;
    next.userAction === 'reqUserSuccess' && load(next.filters);
    if (filters !== nextFilters) {
      const newfilters = filters.page === nextFilters.page
      ? { ...nextFilters, page: 1 }
      : nextFilters;

      isFiltersDebounce
      ? this.debounceLoad(newfilters)
      : load(newfilters);
    }
  }

  onRowClick = (user) => {
    this.props.editUser(user);
  }
  onChangedBirthDate = param => (value) => {
    const changedDate = moment(value).format('YYYY-MM-DD');
    this.props.changeActiveUserParam(param, changedDate);
  }

  onChangeUserParam = param => (e) => {
    this.props.changeActiveUserParam(param, e.target.value);
  }
  getFullName = (user = {}) => (
    user.middleName ?
      `${user.lastName} ${user.firstName} ${user.middleName}`
    :
      `${user.lastName} ${user.firstName}`
  )
  setFilterPage = ({ selected }) => {
    this.props.setFilter('page', selected + 1);
  }
  setFilter = (key, isFiltersDebounce) => (e) => {
    let val = '';
    if (e.target) {
      val = e.target.value;
    } else {
      val = moment(e).format('YYYY-MM-DD');
    }
    this.props.setFilter(key, val, isFiltersDebounce);
  };

  setChangePass = isChangePass => () => {
    this.props.setChangePass(isChangePass);
  }
  searchOption = (e) => {
    this.setState({ searchOption: e.target.value });
    this.props.resetFilter();
  };

  fieldSearch = () => {
    const { isOpenSearch } = this.state;
    isOpenSearch && this.props.resetFilter();
    this.setState({
      isOpenSearch: !isOpenSearch,
    });
  }
  closeForm = (e) => {
    e && e.preventDefault();
    this.props.closeForm();
  }
  openModal = (param, user) => () => {
    if (param === 'del') {
      this.props.deleteUser(user);
    }
    this.props.openModal();
  }

  saveUser = user => () => {
    const { userAction, isChangePass } = this.props;
    let editedUser = {};
    user = {
      ...user,
      tel: user.tel.replace(/([^0-9])/g, '')
    }
    const isUserValid = validator.validate(user);
    const isPassValid = validatorPass.validate(user);
    if (userAction === 'create') {
      if (isUserValid && isPassValid) {
        this.props.addUser(user);
      } else {
        this.props.saveFormErrors({
          ...validator.getErrors(),
          ...validatorPass.getErrors(),
        });
      }
    } else if (userAction === 'edit') {
      if (isUserValid) {
        editedUser = {
          firstName:  user.firstName,
          lastName:   user.lastName,
          middleName: user.middleName,
          tel:        user.tel.replace(/([^0-9])/g, ''),
          email:      user.email,
          birthDate:  user.birthDate,
          position:   user.position,
          userRole:   user.userRole,
        };
      } else {
        this.props.saveFormErrors(validator.getErrors());
        return;
      }
      if (isChangePass) {
        if (isUserValid && isPassValid) {
          editedUser = {
            ...editedUser,
            password:  user.password,
            password2: user.password2,
          };
        } else {
          this.props.saveFormErrors(validatorPass.getErrors());
          return;
        }
      }
      this.props.updateUser(editedUser, user.id);
    }
  }
  deleteUserInModal = user => () => {
    const disabledUser = {
      enabled:   !user.enabled,
      firstName: user.firstName,
      position:  user.position,
    };
    this.props.updateUser(disabledUser, user.id);
    this.closeForm();
  }

  render() {
    const {
      users,
      roles,
      activeUser,
      userAction,
      formErrors,
      isOpenModal,
      isChangePass,
      paging,
      filters,
      resetFilter,
      isEditableRoleAccess,
     } = this.props;

       console.log("this.props", this.props);
    const { searchOption, isOpenSearch } = this.state;

    const searchButtonStyle = {
      boxShadow:               'none',
      borderTopLeftRadius:     '0px',
      borderTopRightRadius:    '4px',
      borderBottomLeftRadius:  '0px',
      borderBottomRightRadius: '4px',
      height:                  '34px',
      borderLeft:              '0px',
    };

    const placeholderSeachInput = {
      position: 'Введiть посаду',
      tel:      'Введiть телефон',
      email:    'Введiть email',
      pib:      {
        firstName:  'Iм\'я',
        lastName:   'Прiзвище',
        middleName: 'По-батьковi',
      },
    };
    const getRequiredMark = () => (
      <span style={{ color: 'red' }} >*</span>
    );

    return (
      <div style={{ paddingTop: '40px' }}>
        <Breadcrumbs
          title="" components={{
            title: 'Користувачі',
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              },
              {
                name: 'Користувачі',
              },
            ],
          }}
        />

        <Portlet
          key="users"

          actionsLeft={[
            <div key={1}>
              <div key={2} className="input-group">
                <LimitSelect key="actionsLeft_1" optionsValue={[10, 15, 20]} value={filters.limit} onChange={this.setFilter('limit')} />
                {isOpenSearch &&
                  <select key="actionsLeft_3" value={searchOption} style={{ width: 'inherit', borderLeft: '0px' }} onChange={this.searchOption} className="form-control">
                    <option key={'default'} disabled value="" >Оберiть фiльтр</option>
                    <option key={1} value="pib" >ПІБ</option>
                    <option key={2} value="position" >Посада</option>
                    <option key={3} value="tel" >Телефон</option>
                    <option key={4} value="email" >E-mail</option>
                    <option key={5} value="birthDate" >День народження</option>
                    <option key={6} value="enabled" >Статус</option>
                  </select>
                }
                {isOpenSearch &&
                  (searchOption === 'enabled' ?
                    <select
                      key="select_2"
                      value={filters.filters.enabled.value}
                      style={{ width: 'inherit', borderLeft: '0px' }}
                      onChange={this.setFilter(['filters', 'enabled'], true)}
                      className="form-control"
                    > <option key={'default'} disabled value="" >Оберiть статус</option>
                      <option key={1} value="true" >Розблоковано</option>
                      <option key={2} value="false" >Заблоковано</option>
                    </select>
                  :
                  searchOption === 'pib' ?
                    <pibSearch>
                      <input
                        key="1"
                        style={{ borderRadius: '0px', width: '120px', borderLeft: '0px' }}
                        type="text"
                        value={filters.filters['userInfo.lastName'].value}
                        onChange={this.setFilter(['filters', 'userInfo.lastName'], true)}
                        className="form-control"
                        placeholder={placeholderSeachInput[searchOption].lastName}
                      />
                      <input
                        key="2"
                        style={{ borderRadius: '0px', width: '120px', borderLeft: '0px' }}
                        type="text"
                        value={filters.filters['userInfo.firstName'].value}
                        onChange={this.setFilter(['filters', 'userInfo.firstName'], true)}
                        className="form-control"
                        placeholder={placeholderSeachInput[searchOption].firstName}
                      />
                      <input
                        key="3"
                        style={{ borderRadius: '0px', width: '120px', borderLeft: '0px' }}
                        type="text"
                        value={filters.filters['userInfo.middleName'].value}
                        onChange={this.setFilter(['filters', 'userInfo.middleName'], true)}
                        className="form-control"
                        placeholder={placeholderSeachInput[searchOption].middleName}
                      />
                    </pibSearch>
                  :
                  searchOption === 'birthDate' &&
                    <DateField
                      dateFormat="YYYY-MM-DD"
                      forceValidDate
                      updateOnDateClick
                      collapseOnDateClick
                      defaultValue={filters.filters['userInfo.birthDate'].value.length > 0 ? filters.filters['userInfo.birthDate'].value : moment()}
                      showClock={false}
                      style={{ width: 'inherit', borderLeft: '0px' }}
                    ><Calendar
                      navigation
                      locale="uk"
                      forceValidDate
                      highlightWeekends
                      highlightToday
                      weekNumbers
                      weekStartDay={1}
                      footer={false}
                      onChange={this.setFilter(['filters', 'userInfo.birthDate'], true)}
                    />
                    </DateField>
                  )}

                { isOpenSearch &&
                  searchOption.length > 0 &&
                  searchOption !== 'pib' &&
                  searchOption !== 'enabled' &&
                  searchOption !== 'birthDate' &&
                    <input
                      key="actionsLeft_2"
                      style={{ width: 'inherit', borderLeft: '0px' }}
                      type="text"
                      value={filters.filters['userInfo.' + searchOption].value}
                      onChange={this.setFilter(['filters', 'userInfo.' + searchOption], true)}
                      className="form-control"
                      placeholder={placeholderSeachInput[searchOption]}
                    />
                }
                {!isOpenSearch ?
                  <button
                    key={3}
                    style={searchButtonStyle}
                    onClick={this.fieldSearch}
                    type="button"
                    className="btn btn-default"
                    title="Вiдкрити пошук"
                  ><i className="fa fa-search" aria-hidden="true" />
                  </button>
                  :
                  <button
                    key={4}
                    style={searchButtonStyle}
                    onClick={this.fieldSearch}
                    type="button"
                    className="btn btn-default"
                    title="Закрити пошук"
                  ><i className="fa fa-times" aria-hidden="true" />
                  </button>
                }
              </div>
            </div>,
          ]}
          actionsRight={[
            isEditableRoleAccess && <Link
              key="1"
              to={'/admin/settings/users/create'}
              className="btn btn-sm green-meadow"
            >
              Створити користувача
            </Link>,
          ]}
        >
          <CustomTable
            tableHeaderClass="bg-blue bg-font-blue"
            tableClass="table table-striped  table-hover"
            trClassName="vertical-middle"
            data={users}
            striped
            hover
            columnClassName="vertical-middle"
            // onRowClick={this.onRowClick}
          >
            <HeaderColumn
              dataField="id"
              width="5%"
              dataSort
              columnClassName="vertical-middle"
              isKey
            >№</HeaderColumn>
            <HeaderColumn
              dataField="firstName"
              width="20%"
              dataSort
              columnClassName="vertical-middle"
              dataFormat={(cell, row) => (
                this.getFullName(row)
              )}
            >ПІБ</HeaderColumn>
            <HeaderColumn
              dataField="position"
              width="10%"
              dataSort
              columnClassName="vertical-middle"
            >Посада</HeaderColumn>
            <HeaderColumn
              dataField="tel"
              width="15%"
              dataSort
              columnClassName="vertical-middle"
              dataFormat={cell => (
                <HighlightWords
                  searchWords={[filters.filters['userInfo.tel'].value]}
                  textToHighlight={cell ? cell : ''}
                />
              )}
            >Телефон</HeaderColumn>
            <HeaderColumn
              dataField="email"
              width="20%"
              dataSort
              columnClassName="vertical-middle"
            >E-mail</HeaderColumn>
            <HeaderColumn
              dataField="birthDate"
              width="20%"
              dataSort
              columnClassName="vertical-middle"
              dataFormat={dateFormatter}
            >День народження</HeaderColumn>
            <HeaderColumn
              dataField="id"
              width="5%"
              dataAlign="center"
              dataSort
              columnClassName="vertical-middle"
              dataFormat={(cell, row) => (
                <btnGroup style={{ whiteSpace: 'nowrap' }}>
                  <Link key="1" to={`/admin/settings/users/${cell}`} className="btn btn-icon-only yellow" title="Редагувати">
                    <i className="fa fa-edit" aria-hidden="true" />
                  </Link>
                  {row.enabled ?
                    <button disabled={!isEditableRoleAccess} onClick={this.openModal('del', row)} className="btn btn-icon-only green-meadow" title="Заблокувати">
                      <i className="fa fa-toggle-off" aria-hidden="true" />
                    </button>
                  : <button disabled={!isEditableRoleAccess} onClick={this.openModal('del', row)} className="btn btn-icon-only red" title="Розблокувати">
                    <i className="fa fa-toggle-on" aria-hidden="true" />
                  </button>
                  }
                </btnGroup>
              )}
            ><i className="fa fa-cogs" aria-hidden="true" />
            </HeaderColumn>
          </CustomTable>

        </Portlet>
        {paging &&
        <Paginate
          pageNum={paging.totalPages}
          clickCallback={this.setFilterPage}
          forceSelected={paging.page - 1}
        />
        }
        <Modal show={isOpenModal} onHide={this.closeForm}>
          <Header closeButton>
            <Title
              className={classNames('text-center caption-subject bold',
              { 'font-yellow-lemon': userAction === 'edit' },
              { 'font-green-haze': userAction === 'create' })}
            >
              { userAction === 'del' && (activeUser.enabled ? 'Ви підтверджуєте блокування користувача?' : 'Ви підтверджуєте розблокування користувача?') }
              { userAction === 'edit' && 'Редагування користувача' }
              { userAction === 'create' && 'Створення користувача' }
            </Title>
          </Header>
          <Body>
            { userAction === 'del' && <p className="text-danger lead text-center"> {this.getFullName(activeUser)} </p>}
            { (userAction === 'create' || userAction === 'edit') &&
            <Portlet
              key="editUser"
              className="light"
              bodyClassName="form"
              isTitle={false}
            >
              <form key="mainForm" role="form" className="form-horizontal">
                <div className="form-body">
                  <InputGroup
                    isError={formErrors.lastName}
                    label="Прізвище"
                    value={activeUser.lastName}
                    onChange={this.onChangeUserParam('lastName')}
                    placeholder="Введіть прізвище"
                  />
                  <InputGroup
                    isError={formErrors.firstName}
                    label="Ім'я"
                    value={activeUser.firstName}
                    onChange={this.onChangeUserParam('firstName')}
                    placeholder="Введіть ім'я"
                  />
                  <InputGroup
                    isError={formErrors.middleName}
                    label="По батькові"
                    isRequired={false}
                    value={activeUser.middleName}
                    onChange={this.onChangeUserParam('middleName')}
                    placeholder="Введіть по батькові"
                  />
                  <InputGroup
                    isError={formErrors.tel}
                    label="Телефон"
                    component="phone"
                    value={activeUser.tel}
                    onChange={this.onChangeUserParam('tel')}
                    placeholder="Введіть номер телефону"
                  />
                  <InputGroup
                    isError={formErrors.email}
                    label="E-mail"
                    value={activeUser.email}
                    onChange={this.onChangeUserParam('email')}
                    placeholder="Введіть e-mail"
                    type="email"
                  />
                  <InputGroup
                    isError={formErrors.birthDate}
                    label="День народження"
                    isRequired={false}
                    nowDate={dateNow}
                    selectedDate={activeUser.birthDate ? moment(activeUser.birthDate) : moment()}
                    onChange={this.onChangedBirthDate('birthDate')}
                    component="DatePicker"
                  />
                </div>
              </form>

              <form key="middleForm" role="form">
                <div className="form-body">
                  <Row>
                    <Col md={5} mdOffset={1}>
                      <div className={classNames('form-group form-md-line-input', { 'has-error': formErrors.position }, { 'has-info': !formErrors.position })}>
                        <input value={activeUser.position} onChange={this.onChangeUserParam('position')} type="text" className="form-control" id="form_control_1" placeholder="Введіть посаду" />
                        <label htmlFor="form_control_1">Посада {getRequiredMark()}</label>
                        { formErrors.position && <span className={classNames({ 'has-error': formErrors.position })} style={{ color: 'red' }} > Поле "Посада" не може бути пустим.</span> }
                      </div>
                    </Col>
                    <Col md={5}>
                      <div className={classNames('form-group form-md-line-input', { 'has-error': formErrors.userRole }, { 'has-success': !formErrors.userRole })} >
                        <select value={activeUser.userRole} className="form-control" onChange={this.onChangeUserParam('userRole')} id="form_control_1">
                          <option disabled key={'default'} value="" >Виберiть роль {getRequiredMark()}</option>
                          {roles.map(item => (
                            <option key={item.id} value={item.id} >{item.name}</option>
                          ))}
                        </select>
                        <label htmlFor="form_control_1">Роль</label>
                        { formErrors.userRole && <span className={classNames({ 'has-error': formErrors.userRole })} style={{ color: 'red' }} > Поле "Роль" обов'язкове до вибору.</span> }
                      </div>
                    </Col>
                  </Row>

                  {!isChangePass && userAction === 'edit' &&
                  <Row>
                    <Col md={5} mdOffset={1}>
                      <button
                        type="button"
                        className="btn yellow-mint"
                        onClick={this.setChangePass(true)}
                      >
                        Скинути пароль
                      </button>
                    </Col>
                  </Row>}

                  {(isChangePass || userAction === 'create') &&
                    <Row>
                      <Col md={userAction === 'edit' ? 4 : 5} mdOffset={1}>
                        <div className={classNames('form-group form-md-line-input', { 'has-error': formErrors.password }, { 'has-warning': !formErrors.password })}>
                          <input value={activeUser.password} onChange={this.onChangeUserParam('password')} type="password" className="form-control" id="form_control_1" placeholder="Введіть пароль" />
                          <label htmlFor="form_control_1">Пароль {getRequiredMark()}</label>
                          { formErrors.password &&
                            <span
                              className={classNames({ 'has-error': formErrors.password })}
                              style={{ color: 'red' }}
                            > {formErrors.password === 'TOO_SHORT' && 'Повинно бути не менше 5 символiв.'}
                              {formErrors.password === 'REQUIRED' && 'Поле "Пароль" не може бути пустим.'}
                            </span>
                          }

                        </div>
                      </Col>
                      <Col md={userAction === 'edit' ? 4 : 5}>
                        <div className={classNames('form-group form-md-line-input', { 'has-error': formErrors.password2 }, { 'has-warning': !formErrors.password2 })}>
                          <input value={activeUser.password2} onChange={this.onChangeUserParam('password2')} type="password" className="form-control" id="form_control_1" placeholder="Повторіть пароль" />
                          <label htmlFor="form_control_1">Підтвердження {getRequiredMark()}</label>
                          { formErrors.password2 &&
                            <span
                              className={classNames({ 'has-error': formErrors.password2 })}
                              style={{ color: 'red' }}
                            > {formErrors.password2 === 'FIELDS_NOT_EQUAL' && 'Паролi не спiвпадають'}
                              {formErrors.password2 === 'REQUIRED' && 'Поле "Підтвердження" не може бути пустим.'}
                            </span>
                          }
                        </div>
                      </Col>
                      {userAction === 'edit' &&
                      <Col md={2}>
                        <Button className="" onClick={this.setChangePass(false)}>Відмінити</Button>
                      </Col>}
                    </Row>
                  }

                </div>
              </form>
            </Portlet>
            }
          </Body>
          <Footer>
            { userAction === 'del' && <Button className="green-meadow pull-left" onClick={this.deleteUserInModal(activeUser)}>Так</Button>}
            { userAction === 'del' && <Button className="" onClick={this.closeForm}>Ні</Button> }

            { (userAction === 'edit' || userAction === 'create') &&
              <Button onClick={this.saveUser(activeUser)} className="green-meadow pull-left">Підтвердити</Button> }

            { (userAction === 'edit' || userAction === 'create') &&
              <Button onClick={this.closeForm} className="">Закрити</Button> }

          </Footer>
        </Modal>

      </div>
    );
  }
}

export default connect(selectors, actions)(Users);
