import { createSelector, createStructuredSelector } from 'reselect';

import _omit from 'lodash/omit';

const REDUCER = 'users';

const usersData    = state => state[REDUCER].users.entities.users;
const activeUser   = state => state[REDUCER].activeUser.activeUser;
const roles        = state => state[REDUCER].users.roles;
const paging       = state => state[REDUCER].paging;
const filters      = state => state[REDUCER].filters;
const userAction   = state => state[REDUCER].userAction.userAction;
const isFormValid  = state => state[REDUCER].isFormValid.isFormValid;
const formErrors   = state => state[REDUCER].formErrors.formErrors;
const isChangePass = state => state[REDUCER].isChangePass.isChangePass;
const isOpenModal  = state => state[REDUCER].isOpenModal.isOpenModal;
const isFiltersDebounce = state => state[REDUCER].isFiltersDebounce;

const userInfo = {
  avatar:    '',
  birthDate: '',
  email:     '',
  firstName: '',
  lastName:  '',
  position:  '',
  tel:       '',
};

const users = createSelector(
  usersData,
  items => items.map(item => (
    !item.userInfo
      ? {
        ...item,
        ...userInfo,
        userRole: '-1',
      }
      : {
        ...item,
        ...item.userInfo,
        userRole: item.userInfo.userRole ? item.userInfo.userRole.id : '',
      }
  )));


export default createStructuredSelector({
  users,
  activeUser,
  roles,
  paging,
  filters,
  userAction,
  isFormValid,
  formErrors,
  isChangePass,
  isOpenModal,
  isFiltersDebounce,
});
