import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { DateField, Calendar } from 'react-date-picker';
import InputElement from 'react-input-mask';

const getRequiredMark = () => (
  <span style={{ color: 'red' }} >*</span>
);

const propTypes = {
  value:        PropTypes.string,
  label:        PropTypes.string.isRequired,
  isError:      PropTypes.oneOfType(PropTypes.bool, PropTypes.string),
  isRequired:   PropTypes.bool,
  disabled:     PropTypes.bool,
  onChange:     PropTypes.func.isRequired,
  placeholder:  PropTypes.string,
  component:    PropTypes.string,
  type:         PropTypes.string,
  nowDate:      PropTypes.object,
  selectedDate: PropTypes.object,

};

const defaultProps = {
  value:        '',
  label:        '',
  isError:      false,
  disabled:     false,
  isRequired:   true,
  onChange:     null,
  placeholder:  '',
  component:    '',
  type:         'text',
  nowDate:      {},
  selectedDate: {},

};

const InputGroup = ({
  value,
  label,
  isError,
  onChange,
  disabled,
  placeholder,
  isRequired,
  component,
  type,
  nowDate,
  selectedDate,


}) => (
  <div className={classNames('form-group form-md-line-input', { 'has-error': isError })} >
    <label className="col-md-3 control-label" htmlFor="form_control_1">{label} { isRequired && getRequiredMark()}</label>
    <div className="col-md-9">
      {!component &&
        <comp>
          <input
            value={value}
            onChange={onChange}
            type={type}
            disabled={disabled}
            className="form-control"
            id="form_control_1"
            placeholder={placeholder}
          />
          { isError &&
            <span
              className={classNames({ 'has-error': isError })}
              style={{ color: 'red' }}
            > {isError === 'WRONG_EMAIL' ?
                `Поле "${label}" не вiдповiдає формату example@domain.com`
              : `Поле "${label}" не може бути пустим.`
              }
            </span>
          }
        </comp>

      }
      {component === 'DatePicker' &&
        <DateField
          dateFormat="YYYY-MM-DD"
          forceValidDate
          updateOnDateClick
          collapseOnDateClick
          disabled={disabled}
          defaultValue={selectedDate}
          showClock={false}
        ><Calendar
          navigation
          locale="uk"
          forceValidDate
          highlightWeekends
          highlightToday
          weekNumbers
          weekStartDay={1}
          footer={false}
          maxDate={nowDate}
          onChange={onChange}
        />
        </DateField>
      }
      {component === 'phone' &&
        <comp>
          <InputElement
            className="form-control"
            placeholder={placeholder}
            mask="38 (099) 999-99-99"
            value={value}
            disabled={disabled}
            onChange={onChange}
          />
          { isError &&
            <span
              className={classNames({ 'has-error': isError })}
              style={{ color: 'red' }}
            > {isError === 'TOO_SHORT' ?
                `Поле "${label}" повинне мати 12 цифр.`
              : `Поле "${label}" не може бути пустим.`
              }
            </span>
          }
        </comp>
      }
      <div className="form-control-focus" />
    </div>
  </div>
);

InputGroup.propTypes = propTypes;
InputGroup.defaultProps = defaultProps;

export default InputGroup;
