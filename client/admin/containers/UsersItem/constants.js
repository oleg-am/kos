const module = `USER_ITEM`;

export const SHOW_FORM                = `${module}/SHOW_FORM`;
export const EDIT_USER                = `${module}/EDIT_USER`;
export const CLOSE_FORM               = `${module}/CLOSE_FORM`;
export const CREATE_USER              = `${module}/CREATE_USER`;
export const SET_CHANGE_PASS          = `${module}/SET_CHANGE_PASS`;
export const SAVE_FORM_ERRORS         = `${module}/SAVE_FORM_ERRORS`;
export const DELETE_USER_BY_ID        = `${module}/DELETE_USER_BY_ID`;
export const CHANGE_ACTIVE_USER_PARAM = `${module}/CHANGE_ACTIVE_USER_PARAM`;

export const LOAD_REQUEST = `${module}/LOAD_REQUEST`;
export const LOAD_SUCCESS = `${module}/LOAD_SUCCESS`;
export const LOAD_FAILURE = `${module}/LOAD_FAILURE`;

export const OPEN_MODAL  = `${module}/OPEN_MODAL`;
export const DELETE_USER = `${module}/DELETE_USER`;

export const LOAD_ROLES_REQUEST = `${module}/LOAD_ROLES_REQUEST`;
export const LOAD_ROLES_SUCCESS = `${module}/LOAD_ROLES_SUCCESS`;
export const LOAD_ROLES_FAILURE = `${module}/LOAD_ROLES_FAILURE`;

export const ADD_USER_REQUEST = `${module}/ADD_USER_REQUEST`;
export const ADD_USER_SUCCESS = `${module}/ADD_USER_SUCCESS`;
export const ADD_USER_FAILURE = `${module}/ADD_USER_FAILURE`;

export const UPDATE_USER_REQUEST = `${module}/UPDATE_USER_REQUEST`;
export const UPDATE_USER_SUCCESS = `${module}/UPDATE_USER_SUCCESS`;
export const UPDATE_USER_FAILURE = `${module}/UPDATE_USER_FAILURE`;

export const SET_FILTER   = `${module}/SET_FILTER`;
export const RESET_FILTER = `${module}/RESET_FILTER`;
