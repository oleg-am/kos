import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import classNames from 'classnames';
import { DateField, Calendar } from 'react-date-picker';
import _debounce from 'lodash/debounce';
import { Button, Row, Col } from 'react-bootstrap';
import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';
import HighlightWords from 'react-highlight-words';
import * as actions from './actions';
import selectors from './selectors';
import { validator, validatorPass } from './schemas';
import Breadcrumbs from '../../components/Breadcrumbs';
import Portlet from '../../components/Portlet/index';
import InputGroup from './components/InputGroup';
import Paginate from '../../components/Paginate';
import LimitSelect from '../../components/LimitSelect';
import moment from '../../helpers/moment';

// import {
//   CustomTable,
//   HeaderColumn,
// } from '../../components/CustomTable';

// import {
//   dateFormatter,
// } from '../../components/Formatters';

const { shape, string, bool, object, func, array } = PropTypes;

const getRequiredMark = () => (
  <span style={{ color: 'red' }} >*</span>
);

const dateNow = moment(moment().format('YYYY-MM-DD'));

class UsersItem extends Component {
  static propTypes = {
    location: shape({
      pathname: string,
    }),
    isEditableRoleAccess:  bool,
    params:                object,
    activeUser:            object,
    formErrors:            object,
    addUser:               func.isRequired,
    updateUser:            func.isRequired,
    changeActiveUserParam: func.isRequired,
    loadRoles:             func.isRequired,
    load:                  func.isRequired,
    createUser:            func.isRequired,
    setChangePass:         func.isRequired,
    isChangePass:          bool,
    isEditableRoleAccess:  bool,
    roles:                 array.isRequired,
    saveFormErrors:        func.isRequired,

  }

  constructor(props) {
    super(props);
    console.log(props);
    const { load, loadRoles, params, location: { pathname } } = this.props;

    this.isMe = ['/admin/me', '/admin/me/'].includes(pathname);
    if (this.isMe) {
      load();
    } else if (params.id !== 'create') {
      load(params.id);
    }
    loadRoles();
  }

  componentWillMount() {
    const { createUser, params } = this.props;
    params.id === 'create' && createUser();
  }

  onChangeUserParam = param => (e) => {
    this.props.changeActiveUserParam(param, e.target.value);
  }

  onChangedBirthDate = param => (value) => {
    const changedDate = moment(value).format('YYYY-MM-DD');
    this.props.changeActiveUserParam(param, changedDate);
  }

  setChangePass = isChangePass => () => {
    this.props.setChangePass(isChangePass);
  }

  saveUser = user => () => {
    const { params, isChangePass } = this.props;
    let editedUser = {};
    user = {
      ...user,
      tel: user.tel.replace(/([^0-9])/g, '')
    }
    const isUserValid = validator.validate(user);
    const isPassValid = validatorPass.validate(user);
    if (params.id === 'create') {
      if (isUserValid && isPassValid) {
        this.props.addUser(user);
      } else {
        this.props.saveFormErrors({
          ...validator.getErrors(),
          ...validatorPass.getErrors(),
        });
      }
    } else {
      if (isUserValid) {
        editedUser = {
          firstName:  user.firstName,
          lastName:   user.lastName,
          middleName: user.middleName,
          tel:        user.tel.replace(/([^0-9])/g, ''),
          email:      user.email,
          birthDate:  user.birthDate,
          position:   user.position,
          userRole:   user.userRole,
        };
      } else {
        this.props.saveFormErrors(validator.getErrors());
        return;
      }
      if (isChangePass) {
        if (isUserValid && isPassValid) {
          editedUser = {
            ...editedUser,
            password:  user.password,
            password2: user.password2,
          };
        } else {
          this.props.saveFormErrors(validatorPass.getErrors());
          return;
        }
      }
      this.props.updateUser(editedUser, user.id);
    }
  }

  render() {
    const {
      params,
      activeUser,
      formErrors,
      roles,
      isChangePass,
      isEditableRoleAccess,
    } = this.props;

    return (
      <div style={{ paddingTop: '40px' }}>
        <Breadcrumbs
          title="" components={{
            title: this.isMe
              ? 'Мій профіль'
              : `${params.id === 'create' ? 'Створення' : 'Редагування'} користувача`,
            links: [
              {
                name: 'Меню',
                to:   '/admin/reports',
              },
              {
                name: 'Користувачі',
              },
            ],
          }}
        />

        <Portlet
          key="users"
          actionsRight={[
            <comp key={'actionBtn'}>
              {
                isEditableRoleAccess &&
                <Link key="1" onClick={this.saveUser(activeUser)} className="btn btn-sm green-meadow" style={{ marginRight: '5px' }}>
                  Зберегти
                </Link>
              }
              <Link key="2" to={this.isMe ? '/admin' : '/admin/settings/users'} className="btn btn-sm btn-default">
                Закрити
              </Link>
            </comp>,
          ]}
        >
          <form key="mainForm" role="form" className="form-horizontal">
            <div className="form-body">
              <InputGroup
                isError={formErrors.lastName}
                label="Прізвище"
                disabled={!isEditableRoleAccess}
                value={activeUser.lastName}
                onChange={this.onChangeUserParam('lastName')}
                placeholder="Введіть прізвище"
              />
              <InputGroup
                isError={formErrors.firstName}
                label="Ім'я"
                disabled={!isEditableRoleAccess}
                value={activeUser.firstName}
                onChange={this.onChangeUserParam('firstName')}
                placeholder="Введіть ім'я"
              />
              <InputGroup
                isError={formErrors.middleName}
                label="По батькові"
                disabled={!isEditableRoleAccess}
                isRequired={false}
                value={activeUser.middleName}
                onChange={this.onChangeUserParam('middleName')}
                placeholder="Введіть по батькові"
              />
              <InputGroup
                isError={formErrors.tel}
                label="Телефон"
                disabled={!isEditableRoleAccess}
                component="phone"
                value={activeUser.tel}
                onChange={this.onChangeUserParam('tel')}
                placeholder="Введіть номер телефону"
              />
              <InputGroup
                isError={formErrors.email}
                label="E-mail"
                disabled={!isEditableRoleAccess}
                value={activeUser.email}
                onChange={this.onChangeUserParam('email')}
                placeholder="Введіть e-mail"
                type="email"
              />
              <InputGroup
                isError={formErrors.birthDate}
                label="День народження"
                disabled={!isEditableRoleAccess}
                isRequired={false}
                nowDate={dateNow}
                selectedDate={activeUser.birthDate}
                onChange={this.onChangedBirthDate('birthDate')}
                component="DatePicker"
              />
            </div>
          </form>

          <form key="middleForm" role="form">
            <div className="form-body">
              <Row>
                <Col md={5} mdOffset={1}>
                  <div className={classNames('form-group form-md-line-input', { 'has-error': formErrors.position }, { 'has-info': !formErrors.position })}>
                    <input
                      value={activeUser.position}
                      onChange={this.onChangeUserParam('position')}
                      type="text"
                      disabled={!isEditableRoleAccess}
                      className="form-control"
                      id="form_control_1"
                      placeholder="Введіть посаду"
                    />
                    <label htmlFor="form_control_1">Посада {getRequiredMark()}</label>
                    { formErrors.position && <span className={classNames({ 'has-error': formErrors.position })} style={{ color: 'red' }} > Поле "Посада" не може бути пустим.</span> }
                  </div>
                </Col>
                <Col md={5}>
                  <div className={classNames('form-group form-md-line-input', { 'has-error': formErrors.userRole }, { 'has-success': !formErrors.userRole })} >
                    <select
                      disabled={!isEditableRoleAccess}
                      value={activeUser.userRole}
                      className="form-control"
                      onChange={this.onChangeUserParam('userRole')}
                      id="form_control_1"
                    >
                      <option disabled key={'default'} value="" >Виберiть роль {getRequiredMark()}</option>
                      {roles.map(item => (
                        <option key={item.id} value={item.id} >{item.name}</option>
                      ))}
                    </select>
                    <label htmlFor="form_control_1">Роль</label>
                    { formErrors.userRole && <span className={classNames({ 'has-error': formErrors.userRole })} style={{ color: 'red' }} > Поле "Роль" обов'язкове до вибору.</span> }
                  </div>
                </Col>
              </Row>

              {!isChangePass && params.id !== 'create' &&
              <Row>
                <Col md={5} mdOffset={1}>
                  <button
                    type="button"
                    disabled={!isEditableRoleAccess}
                    className="btn yellow-mint"
                    onClick={this.setChangePass(true)}
                  >
                    Скинути пароль
                  </button>
                </Col>
              </Row>}

              {(isChangePass || params.id === 'create') &&
                <Row>
                  <Col md={params.id !== 'create' ? 4 : 5} mdOffset={1}>
                    <div className={classNames('form-group form-md-line-input', { 'has-error': formErrors.password }, { 'has-warning': !formErrors.password })}>
                      <input
                        value={activeUser.password}
                        onChange={this.onChangeUserParam('password')}
                        type="password"
                        className="form-control"
                        id="form_control_1"
                        placeholder="Введіть пароль"
                      />
                      <label htmlFor="form_control_1">Пароль {getRequiredMark()}</label>
                      { formErrors.password &&
                        <span
                          className={classNames({ 'has-error': formErrors.password })}
                          style={{ color: 'red' }}
                        > {formErrors.password === 'TOO_SHORT' && 'Повинно бути не менше 5 символiв.'}
                          {formErrors.password === 'REQUIRED' && 'Поле "Пароль" не може бути пустим.'}
                        </span>
                      }

                    </div>
                  </Col>
                  <Col md={params.id !== 'create' ? 4 : 5}>
                    <div className={classNames('form-group form-md-line-input', { 'has-error': formErrors.password2 }, { 'has-warning': !formErrors.password2 })}>
                      <input
                        value={activeUser.password2}
                        onChange={this.onChangeUserParam('password2')}
                        type="password"
                        className="form-control"
                        id="form_control_1"
                        placeholder="Повторіть пароль"
                      />
                      <label htmlFor="form_control_1">Підтвердження {getRequiredMark()}</label>
                      { formErrors.password2 &&
                        <span
                          className={classNames({ 'has-error': formErrors.password2 })}
                          style={{ color: 'red' }}
                        > {formErrors.password2 === 'FIELDS_NOT_EQUAL' && 'Паролi не спiвпадають'}
                          {formErrors.password2 === 'REQUIRED' && 'Поле "Підтвердження" не може бути пустим.'}
                        </span>
                      }
                    </div>
                  </Col>
                  {params.id !== 'create' &&
                  <Col md={2}>
                    <Button className="" onClick={this.setChangePass(false)}>Відмінити</Button>
                  </Col>}
                </Row>
              }

            </div>
          </form>
        </Portlet>

      </div>
    );
  }
}

export default connect(selectors, actions)(UsersItem);
