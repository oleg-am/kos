import { combineReducers } from 'redux';
import * as types from './constants';
import { setFiltersValue, resetFiltersValue } from '../../helpers/setObjValue';
import moment from '../../helpers/moment';

const nowDate = moment();

const initialState = {
  users: {
    entities: {
      users: [],
    },
    roles:      [],
    result:     [],
    activeUser: {
      enabled:    true,
      firstName:  '',
      lastName:   '',
      middleName: '',
      tel:        '',
      email:      '',
      position:   '',
      birthDate:  nowDate,
      password:   '',
      password2:  '',
      userRole:   '-1',
    },
    userAction:   '',
    isOpenModal:  false,
    isChangePass: false,
    formErrors:   {},
    isFormValid:  false,
  },
  filters: {
    page:  1,
    limit: 10,
    order_by: {
      id: {
        value: 'ASC',
      },
    },
    filters: {
      enabled: {
        value: '',
      },
      'userInfo.firstName': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      'userInfo.lastName': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      'userInfo.middleName': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      'userInfo.position': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      'userInfo.tel': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      'userInfo.email': {
        comparison: 'like',
        value:      '',
        modifier:   'lower',
      },
      'userInfo.birthDate': {
        comparison: 'eq',
        value:      '',
        modifier:   'lower',
      },
    },
  },
};

const userAction = (state = initialState.users.userAction, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
    // case types.CREATE_USER:
    case types.EDIT_USER:
    case types.DELETE_USER:
    case types.ADD_USER_SUCCESS:
    case types.UPDATE_USER_SUCCESS:
      return {
        ...state,
        userAction: action.userAction,
      };
    case types.CLOSE_FORM:
      return {
        userAction: '',
      };
    default:
      return state;
  }
};

const activeUser = (state = initialState.users.activeUser, action) => {
  switch (action.type) {
    case types.DELETE_USER:
    case types.EDIT_USER:
      return {
        ...state,
        ...action.activeUser,
      };
    case types.LOAD_SUCCESS: {
      const data = action.data.userInfo || action.data;
      return {
        ...action.data,
        ...data,
        birthDate: data.birthDate ? moment(data.birthDate) : nowDate,
        userRole:  data.userRole ? data.userRole.id : '-1',
      };
    }
    case types.CLOSE_FORM:
    case types.CREATE_USER:
      return initialState.users.activeUser;
    case types.CHANGE_ACTIVE_USER_PARAM:
      return {
        ...state,
        [action.param]: action.value,
      };
    default:
      return state;
  }
};

const isFormValid = (state = initialState.users.isFormValid, action) => {
  switch (action.type) {
    case types.ADD_USER_SUCCESS:
    case types.SAVE_FORM_ERRORS:
    case types.UPDATE_USER_SUCCESS:
      return {
        ...state,
        isFormValid: action.isFormValid,
      };
    default:
      return state;
  }
};

const formErrors = (state = initialState.users.formErrors, action) => {
  switch (action.type) {
    case types.ADD_USER_SUCCESS:
    // case types.CREATE_USER:
    case types.EDIT_USER:
    case types.SAVE_FORM_ERRORS:
    case types.UPDATE_USER_SUCCESS:
    case types.CLOSE_FORM:
      return action.formErrors;
    default:
      return state;
  }
};

const isChangePass = (state = initialState.users.isChangePass, action) => {
  switch (action.type) {
    // case types.CREATE_USER:
    case types.EDIT_USER:
    case types.CLOSE_FORM:
    case types.SET_CHANGE_PASS:
      return {
        ...state,
        isChangePass: action.isChangePass,
      };
    default:
      return state;
  }
};
//
const users = (state = initialState.users, action) => {
  switch (action.type) {
    // case types.LOAD_SUCCESS:
    //   return {
    //     ...state,
    //     entities: {
    //       users: action.data,
    //     },
    //   };
    case types.LOAD_ROLES_SUCCESS:
      return {
        ...state,
        roles: action.data,
      };
    // case types.LOAD_FAILURE:
    //   return {
    //     ...state,
    //     entities: {
    //       users: [],
    //     },
    //   };
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return {
        ...state,
        ...setFiltersValue(state, action.key, action.value),
      };
    case types.RESET_FILTER:
      return {
        ...state,
        filters: initialState.filters.filters,
      };
    default:
      return state;
  }
};

const isFiltersDebounce = (state = false, action) => {
  switch (action.type) {
    case types.SET_FILTER:
      return action.isFiltersDebounce;
    default:
      return state;
  }
};

// const paging = (state = {}, action) => {
//   switch (action.type) {
//     case types.LOAD_SUCCESS:
//       return action.paging;
//     case types.LOAD_FAILURE:
//       return null;
//     default:
//       return state;
//   }
// };

export default combineReducers({
  users,
  filters,
  // paging,
  // isOpenModal,
  userAction,
  activeUser,
  isFormValid,
  formErrors,
  isChangePass,
  isFiltersDebounce,

});
