import { takeEvery } from 'redux-saga';
 import { put } from 'redux-saga/effects';

import { browserHistory } from 'react-router';

import { ADD_USER_SUCCESS } from './constants';
import * as actions from './actions';

function* goToUsers() {
  yield put(actions.closeForm());
  yield browserHistory.push('/admin/settings/users');
}

function* rootSagas() {
  yield takeEvery([ADD_USER_SUCCESS], goToUsers);
}

export default rootSagas;
