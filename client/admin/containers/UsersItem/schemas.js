import { Schema, arrayOf } from 'normalizr';

import LIVR from 'livr';
LIVR.Validator.defaultAutoTrim(true);

// import Joi from 'joi';
// console.log('joi:', Joi);
const USER = new Schema('users', { idAttribute: 'id' });
export const USERS_ARRAY = arrayOf(USER);

// const validateShema = Joi.object().keys({
//   firstName: Joi.string().alphanum().min(3).max(30).required(),
//   lastName: Joi.string().alphanum().min(3).max(30).required(),
//   middleName: Joi.string().alphanum().min(3).max(30).required(),
//   tel: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
//   email: Joi.string().email(),
//   position: Joi.string().alphanum().min(3).max(30).required(),
//   role: Joi.string().alphanum().min(3).max(30).required(),
//   birthyear: Joi.number().integer().min(1900).max(2013),
//   // password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
// });


export const validate = (user) => {
  // Joi.validate(user, validateShema, function (err, value) {
  //   console.log('err: ', err, 'value: ', value);
  // });  // err === null -> valid
}

export const validator = new LIVR.Validator({
    firstName: ['required', 'string'],
    lastName:  ['required', 'string'],
    tel:       ['required', { min_length: 12 }],
    email:     ['required', 'email'],
    position:  ['required', 'string'],
    userRole:  'required',
});

export const validatorPass = new LIVR.Validator({
  password: [
    'required',
    { min_length: 5 },
  ],
  password2: [
    'required',
    { equal_to_field: 'password' },
  ],
});
