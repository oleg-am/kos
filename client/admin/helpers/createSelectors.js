export const createSelectors = (selectors = {}) => (state, ownProps) => (
  // TODO: add errors
  Object.keys(selectors).reduce((obj, key) => {
    obj[key] = selectors[key](state, ownProps); // TODO: check selectors[key] == functions
    return obj;
  }, {})
)
