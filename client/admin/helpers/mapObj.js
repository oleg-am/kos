export const mapObj = (obj = {}, cb) => {
  // TODO: add errors

  if (typeof cb !== 'function') {
    throw new Error('Expected second argument to be function.');
  }

  return Object.keys(obj).reduce((newObj, key) => {
    newObj[key] = cb(obj[key]);
    return newObj;
  }, {})
}
