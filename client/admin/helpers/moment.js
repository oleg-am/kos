import moment from 'moment';
import locale_uk from "moment/locale/uk";
moment.updateLocale("uk", locale_uk);

export default moment;
