const transformMinusValueToZero = value => !!value && value < 0 ? 0 : value;

export default transformMinusValueToZero;
