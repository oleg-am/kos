import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import axios from 'axios';

import { version } from '!json!../../config/version.json';

import configureStore from './store/configureStore';
import routes from './routes';


const initialState = {
  app: {
    version: version || '',
  },
};

const store = configureStore(axios, initialState);

// const getUserInfo = () => {
//   const URL = '/api/me/check_rights';
//   return axios
//       .get(URL)
//       .then((res) => {
//         this.setState({ user: res.data });
//       }).catch((error) => {
//         console.log(error);
//       });
// }

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('main-app'),
);
