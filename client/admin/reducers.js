import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import app from './containers/App/reducer';

import orders from './containers/Orders/reducer';
import ordersOld from './containers/Orders_Old/reducer';
import ordersItem from './containers/OrdersItem/reducer';

import orderInfo from './containers/OrderInfo/reducer';

import nomenclatureSelectionInPharmacy from './containers/NomenclatureSelectionInPharmacy/reducer';
import goodsSelection from './containers/GoodsSelection/reducer';
import goodsSelectionInPharmacies from './containers/PharmaciesGoodsSelection/reducer';

import nomenclature from './containers/Nomenclature/reducer';
import nomenclatureItem from './containers/NomenclatureItem/reducer';
import nomenclatureRemainders from './containers/NomenclatureRemainders/reducer';

import pharmacies from './containers/Pharmacies/reducer';
import pharmaciesMap from './containers/PharmaciesMap/reducer';
import pharmaciesItem from './containers/PharmaciesItem/reducer';
import pharmaciesRemainders from './containers/PharmaciesRemainders/reducer';

import clients from './containers/Clients/reducer';
import clientItem from './containers/ClientItem/reducer';

import marketPlace from './containers/MarketPlace/reducer';
import marketPlaceItem from './containers/MarketPlaceItem/reducer';
import users from './containers/Users/reducer';
import usersItem from './containers/UsersItem/reducer';
import userRoles from './containers/UserRoles/reducer';
import userRoleItem from './containers/UserRoleItem/reducer';
import configGeneral from './containers/ConfigGeneral/reducer';
import reports from './containers/Reports/reducer';
import desktop from './containers/Desktop/reducer';

const rootReducer = combineReducers({
  form: formReducer,
  app,

  orders,
  ordersOld,
  ordersItem,
  nomenclatureSelectionInPharmacy,
  goodsSelection,
  goodsSelectionInPharmacies,
  orderInfo,

  nomenclature,
  nomenclatureItem,
  nomenclatureRemainders,

  pharmacies,
  pharmaciesMap,
  pharmaciesItem,
  pharmaciesRemainders,

  clients,
  clientItem,

  users,
  usersItem,
  marketPlace,
  marketPlaceItem,
  userRoles,
  userRoleItem,
  configGeneral,
  reports,
  desktop,
});

export default rootReducer;
