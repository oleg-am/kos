import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App/index';

import MainLayout from './components/MainLayout';

// HOCs:
import CheckRoleHOC from './HOC/CheckRole';

// import GoodsSelection from './containers/GoodsSelection';
// import PharmaciesGoodsSelection from './containers/PharmaciesGoodsSelection';

import Reports from './containers/Reports';

import Orders from './containers/Orders';
import OrdersItem from './containers/OrdersItem/index';

// import OrderInfo from './containers/OrderInfo/index';

import NomenclatureSelectionInPharmacy from './containers/NomenclatureSelectionInPharmacy';

import Nomenclature from './containers/Nomenclature';
import NomenclatureItem from './containers/NomenclatureItem';
import NomenclatureRemainders from './containers/NomenclatureRemainders';

import Pharmacies from './containers/Pharmacies';
import PharmaciesMap from './containers/PharmaciesMap';
import PharmaciesItem from './containers/PharmaciesItem';
import PharmaciesRemainders from './containers/PharmaciesRemainders';

import Clients from './containers/Clients';
import ClientItem from './containers/ClientItem';

import SettingsLayout from './components/SettingsLayout';
import MarketPlace from './containers/MarketPlace';
import MarketPlaceItem from './containers/MarketPlaceItem';
import Users from './containers/Users';
import UsersItem from './containers/UsersItem';
import UserRoles from './containers/UserRoles';
import UserRoleItem from './containers/UserRoleItem/';
import ConfigGeneral from './containers/ConfigGeneral';
import Desktop from './containers/Desktop';
import ReportOrderByPharmacy from './containers/ReportOrderByPharmacy';

// const CheckAccessPage = [
//   clients,
//   config,
//   dashboard,
//   description,
//   orders,
//   pharmacies,
//   reports,
//   userManagement,
// ];

const addHOC = HOC => key => param => component => (
  HOC({ [key]: param })(component)
);

const checkAccessRole = addHOC(CheckRoleHOC)('componentAccessName');

const CheckAccessDesktop                         = checkAccessRole('dashboard')(Desktop);
const CheckAccessReports                         = checkAccessRole('reports')(Reports);
const CheckAccessReportOrderByPharmacy           = checkAccessRole('reports')(ReportOrderByPharmacy);

const CheckAccessOrders                          = checkAccessRole('orders')(Orders);
const CheckAccessOrdersItem                      = checkAccessRole('orders')(OrdersItem);
const CheckAccessNomenclatureSelectionInPharmacy = checkAccessRole('orders')(NomenclatureSelectionInPharmacy);

const CheckAccessNomenclature                    = checkAccessRole('nomenclature')(Nomenclature);
const CheckAccessNomenclatureItem                = checkAccessRole('nomenclature')(NomenclatureItem);
const CheckAccessNomenclatureRemainders          = checkAccessRole('nomenclature')(NomenclatureRemainders);

const CheckAccessPharmacies                      = checkAccessRole('pharmacies')(Pharmacies);
const CheckAccessPharmaciesMap                   = checkAccessRole('pharmacies')(PharmaciesMap);
const CheckAccessPharmaciesItem                  = checkAccessRole('pharmacies')(PharmaciesItem);
const CheckAccessPharmaciesRemainders            = checkAccessRole('pharmacies')(PharmaciesRemainders);

const CheckAccessMainLayout                      = checkAccessRole('clients')(MainLayout);
const CheckAccessClients                         = checkAccessRole('clients')(Clients);
const CheckAccessClientItem                      = checkAccessRole('clients')(ClientItem);

const CheckAccessSettingsLayout                  = checkAccessRole('config')(SettingsLayout);
const CheckAccessConfigGeneral                   = checkAccessRole('config')(ConfigGeneral);
const CheckAccessMarketPlace                     = checkAccessRole('config')(MarketPlace);
const CheckAccessMarketPlaceItem                 = checkAccessRole('config')(MarketPlaceItem);
const CheckAccessUsers                           = checkAccessRole('config')(Users);
const CheckAccessUsersItem                       = checkAccessRole('config')(UsersItem);
const CheckAccessUserRoles                       = checkAccessRole('config')(UserRoles);
const CheckAccessUserRoleItem                    = checkAccessRole('config')(UserRoleItem);

// const Page = CheckRole({ componentAccessName: 'userManagement' })(Page);

export default (
  <Route path="/admin" component={App}>
    <IndexRoute component={CheckAccessDesktop} />

    <Route path="/admin/me" component={MainLayout}>
      <IndexRoute component={UsersItem} />
    </Route>
    <Route path="/admin/reports" component={CheckAccessReports} />
    <Route path="/admin/reports/orderbypharmacy" component={CheckAccessReportOrderByPharmacy} />

    <Route path="/admin/orders" component={CheckAccessOrders} />
    <Route path="/admin/orders/:id" component={CheckAccessOrdersItem} />
    <Route path="/admin/orders/:id/nomenclature-selection-in-pharmacy/:pharmacyId" component={CheckAccessNomenclatureSelectionInPharmacy} />

    <Route path="/admin/nomenclature" component={CheckAccessNomenclature} />
    <Route path="/admin/nomenclature/:id" component={CheckAccessNomenclatureItem} />
    <Route path="/admin/nomenclature/:id/remainders" component={CheckAccessNomenclatureRemainders} />

    <Route path="/admin/pharmacies" component={CheckAccessPharmacies} />
    <Route path="/admin/pharmacies/map" component={CheckAccessPharmaciesMap} />
    <Route path="/admin/pharmacies/:id" component={CheckAccessPharmaciesItem} />
    <Route path="/admin/pharmacies/:id/remainders" component={CheckAccessPharmaciesRemainders} />

    <Route path="/admin/clients" component={CheckAccessMainLayout}>
      <IndexRoute component={CheckAccessClients} />
      <Route path="/admin/clients/:id" component={CheckAccessClientItem} />
    </Route>

    <Route path="/admin/settings/general" menu="settings" component={CheckAccessSettingsLayout}>
      <IndexRoute component={CheckAccessConfigGeneral} />
      <Route path="/admin/settings/marketPlace" menu="settings" component={CheckAccessMarketPlace} />
      <Route path="/admin/settings/marketPlaceItem/:id" menu="settings" component={CheckAccessMarketPlaceItem} />
      <Route path="/admin/settings/users" menu="settings" component={CheckAccessUsers} />
      <Route path="/admin/settings/users/:id" menu="settings" component={CheckAccessUsersItem} />
      <Route path="/admin/settings/user-roles" menu="settings" component={CheckAccessUserRoles} />
      <Route path="/admin/settings/user-roles/:id" menu="settings" component={CheckAccessUserRoleItem} />
    </Route>

    <Route path="*" component={Reports} />
  </Route>
);
