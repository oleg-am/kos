import { fork } from 'redux-saga/effects';

import app from './containers/App/sagas/';
import marketPlace from './containers/MarketPlace/sagas';
import MarketPlaceItem from './containers/MarketPlaceItem/sagas';

import orders from './containers/Orders/sagas';
import ordersItem from './containers/OrdersItem/sagas';
import nomenclatureSelectionInPharmacy from './containers/NomenclatureSelectionInPharmacy/sagas';
import userRoleItem from './containers/UserRoleItem/sagas';
import userItem from './containers/UsersItem/sagas';
import clientItem from './containers/ClientItem/sagas';
import client from './containers/Clients/sagas';
import ConfigGeneral from './containers/ConfigGeneral/sagas';

function* rootSagas() {
  yield [
    fork(app),
    fork(marketPlace),
    fork(MarketPlaceItem),
    fork(ordersItem),
    fork(orders),
    fork(nomenclatureSelectionInPharmacy),
    fork(userRoleItem),
    fork(userItem),
    fork(clientItem),
    fork(client),
    fork(ConfigGeneral),
  ];
}

export default rootSagas;
