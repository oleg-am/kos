// Fixed header ----------------------------------
if ($(window).scrollTop() > 130) {
    makeFixedHeader();
}

$(window).scroll(function() {
    if ($(this).scrollTop() > 130) {
        makeFixedHeader();
    } else {
        makeSimpleHeader();
    }
});

//SELECT SCROLL PREVENT
$( document ).on('mousewheel DOMMouseScroll', '.Select-menu', function ( e ) {
    let e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;

    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
    e.preventDefault();
});

function makeFixedHeader() {
    $('.header__wrapper-fixed').addClass("header__wrapper-fixed--scrolled");
}

function makeSimpleHeader() {
    $('.header__wrapper-fixed').removeClass("header__wrapper-fixed--scrolled");
}
// Fixed header END -------------------------------


// Footer links toggle mobile ---------------------
// $('.main-footer__title').click(function(e) {
//     if ($(window).width() < 768) {
//         //$(this).next('.main-footer__link-list').slideToggle("slow");

//         //Toggle footer links and set padding-top for .content
//         $(this).next('.main-footer__link-list').slideToggle({
//             duration: "slow",
//             progress: setWrapperPadding,
//             done: setWrapperPadding
//         });
//     }
// });
// Footer links toggle mobile END -----------------


// Set padding-top for .wrapper --------------------- (footer bottom)
// var $content = $('.content-wrapper');
// var $footer = $('.main-footer');

// function setWrapperPadding() {
//     $content.css('paddingBottom', $footer.outerHeight());
// }

// window.addEventListener("resize", setWrapperPadding);

// setWrapperPadding();
// Set padding-top for .wrapper ---------------------
