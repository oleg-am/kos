// # Configuration

//  Runs in `development` mode by default
'use strict';

var path = require('path'),
    config;

config = {
    // ### Production
    // When running in the wild, use the production environment.
    production: {
        url: '',
        // #### Server
        // Can be host & port (default), or socket
        server: {
            // Host to be passed to node's `net.Server#listen()`
            host: 'localhost',
            // Port to be passed to node's `net.Server#listen()`, for iisnode set this to `process.env.PORT`
            port: '3030'
        },

        oauth: {
            client_id: '',
            client_secret: '',
            grant_type: 'password',
            url: ''
        },

        api: {
            public: '',
            admin: '',
            adminWithLocale: '',
            publicPages: '',
            hostImages: '',
            originalImage: ''
        }
    },

    // ### Development **(default)**
    development: {
        url: '',
        // #### Server
        // Can be host & port (default), or socket
        server: {
            // Host to be passed to node's `net.Server#listen()`
            host: 'localhost',
            // Port to be passed to node's `net.Server#listen()`, for iisnode set this to `process.env.PORT`
            port: '3030'
        },

        oauth: {
            client_id: '',
            client_secret: '',
            grant_type: 'password',
            url: ''
        },

        api: {
            public: '',
            publicPages: '',
            hostImages: '',
            admin: '',
            adminWithLocale: '',
            originalImage: ''
        }
    }

};

module.exports = config;
