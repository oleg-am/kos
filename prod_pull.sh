#!/usr/bin/env bash
curl -X POST --header "Content-Type:application/json" --data '{"channel":"kosmo","username":"Puller FE Prod Chicken","text":"Started production `front-end` server update to `dev_test` branch. Please, wait a minute...","icon_emoji":":chicken:"}' https://hooks.slack.com/services/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 ;
sudo git pull origin dev_test ;
curl -X POST --header "Content-Type:application/json" --data '{"channel":"kosmo","username":"Puller FE Prod Chicken","text":"Building production `front-end` server...","icon_emoji":":chicken:"}' https://hooks.slack.com/services/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 ;
sudo npm install ;
sudo npm run build ;
curl -X POST --header "Content-Type:application/json" --data '{"channel":"kosmo","username":"Puller FE Prod Chicken","text":"Restarting production `front-end` server...","icon_emoji":":chicken:"}' https://hooks.slack.com/services/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 ;
cd server/ ;
pm2 restart runner.js ;
cd .. ;
curl -X POST --header "Content-Type:application/json" --data '{"channel":"kosmo","username":"Puller FE Prod Chicken","text":"Production `front-end` server updated to `dev_test` branch","icon_emoji":":chicken:"}' https://hooks.slack.com/services/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