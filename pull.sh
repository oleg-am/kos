#!/usr/bin/env bash

branch="$1"

if [ "$branch" = "" ] ; then
    branch="dev_test"
fi

git pull origin "$branch" &&
npm install &&
npm run build &&
pm2 restart server/runner.js