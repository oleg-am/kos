#!/usr/bin/env bash
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

parse_git_username() {
    git config user.name 2> /dev/null
}

parse_git_commit_message() {
    git log -1 --pretty=%B 2> /dev/null
}

task=$1
re='^[0-9]+$'

if ! [[ $task =~ $re ]] ; then
    echo 'Input finished task ID (only number) or leave it blank: '
    read task
else
    shift;
fi

message=""

while [ "$1" != "" ]; do
    if [ "$message" = "" ]; then
        message+="${1}";
    else
        message+=" ${1}";
    fi
    shift;
done;

branch=$(parse_git_branch | sed "s/(//" | sed "s/)//" | sed "s/ //")
user=$(parse_git_username)

if [ "${message}" = "" ] ; then
    commitMessage=$(parse_git_commit_message)
    merge="Merge branch "

    if [ "${commitMessage/$merge}" = "$commitMessage" ] ; then
        echo 'Input commit message: '
        read message
        commitMessage="id: $branch comment: $message"
    fi
else
    commitMessage="id: $branch comment: $message"
fi

if [ "$task" != "" ] ; then
    commitMessage+=" yt: #KSM-$task Тестировать"
fi

username="Pusher FE Puppy"
icon=":dog:"

if [ "$branch" = "dev_test" ] ; then
    username="Pusher FE Banana"
    icon=":banana:"
fi

git add -A ;
git commit -m "$commitMessage" ;
git push origin "$branch" &&
curl -X POST --header "Content-Type:application/json" --data "{\"channel\":\"kosmo\",\"username\":\"$username\",\"text\":\"New commit pushed by \`$user\`\r\nTo \`front-end\` server repo\r\nIn \`$branch\` branch\r\nWith message: \`$commitMessage\`\",\"icon_emoji\":\"$icon\"}" https://hooks.slack.com/services/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
