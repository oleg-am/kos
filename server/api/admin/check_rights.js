var Promise = require('bluebird'),
    config = require('../../../config/config'),
    rest = require('restler'),
    errors = require('../../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

var check_rights = (req, res) => {

    return new Promise((resolve, reject) => {

            var token = req.session.token ? `?access_token=${req.session.token}` : '';

            rest.get(`${configLocal.api.public}me/check_rights${token}`).on('success', (res) => {
                resolve(res);
            }).on('fail', (err, response) => {
              console.log('check_rights server err: ', err);
                reject(errors.apiErrorHandler.handler(err, response))
            });
        }
    )
};

module.exports = check_rights;
