var Promise = require('bluebird'),
    config = require('../../../../config/config'),
    rest = require('restler'),
	  utf8 = require('utf8'),
    errors = require('../../../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

const nomenclature = (req, res) => {
  // console.log('req: ', req);
  console.log('req.query: ', req.query, !!Object.keys(req.query).length);
  console.log('req.params: ', req.params);
  console.log('req.originalUrl: ', req.originalUrl);
  return new Promise((resolve, reject) => {

    let token = req.session.token ? `access_token=${req.session.token}` : '';

    if (token) {
      token = !!Object.keys(req.query).length ? '&' + token : '?' + token;
    }

    const queryUrl = req.originalUrl.replace('/api/admin/', '');
    console.log('queryUrl: ', queryUrl);
		const query = `${configLocal.api.admin}${queryUrl}${token}`;

		// var query = `${configLocal.api.admin}nomenclature${token}${req.query.page ? "&page=" + req.query.page : "" }`;
		// for (var key in req.query) {
		// 	if (key == "page")
		// 		continue;
		// 	var value = req.query[key];
		// 	if (typeof(value) == "object") {
		// 		var keyInBrackets = Object.keys(value)[0];
		// 		query += `&${key}[${keyInBrackets}]=${utf8.encode(value[keyInBrackets])}`;
		// 	}
		// 	else
		// 		query += `&${key}=${utf8.encode(value)}`;
		// }
		console.log('query: ', query);
    rest.get(query).on('success', (res) => {
        resolve(res);
    }).on('fail', (err, response) => {
        reject(errors.apiErrorHandler.handler(err, response))
    });
  })
};

module.exports = nomenclature;
