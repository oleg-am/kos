var Promise = require('bluebird'),
    config = require('../../../../config/config'),
    rest = require('restler'),
    utf8 = require('utf8'),
    errors = require('../../../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

var nosologies = (req, res) => {

    return new Promise((resolve, reject) => {
            var token = req.session.token ? `?access_token=${req.session.token}` : '';
			var query = `${configLocal.api.admin}nosologies${token}&with[synonyms]=id,name${req.query.page ? "&page=" + req.query.page : "" }`;
			query += `${req.query.q ? "&filters[name]=like|" + utf8.encode((req.query.q).toLowerCase()) + "|lower": ""}`;
			for (var key in req.query) {
				if ((key == "page") || (key == "q"))
					continue;
				var value = req.query[key];
				if (typeof(value) == "object") {
					var keyInBrackets = Object.keys(value)[0];
					query += `&${key}[${keyInBrackets}]=${utf8.encode(value[keyInBrackets])}`;
				}
				else
					query += `&${key}=${utf8.encode(value)}`;
			}
			console.log(query);
            rest.get(query).on('success', (res) => {
                resolve(res);
            }).on('fail', (err, response) => {
                reject(errors.apiErrorHandler.handler(err, response))
            });
        }
    )
};

module.exports = nosologies;
