var Promise = require('bluebird'),
    config = require('../../../../config/config'),
    rest = require('restler'),
    errors = require('../../../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

var nosology = (req, res) => {

    return new Promise((resolve, reject) => {
            var token = req.session.token ? `?access_token=${req.session.token}` : '';
			var query = `${configLocal.api.admin}nosologies/${req.params.id}${token}&with[synonyms]=id,name`;
			console.log(query)
            rest.get(query).on('success', (res) => {
                resolve(res);
            }).on('fail', (err, response) => {
                reject(errors.apiErrorHandler.handler(err, response))
            });
        }
    )
};

module.exports = nosology;
