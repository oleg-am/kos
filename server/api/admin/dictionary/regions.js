var Promise = require('bluebird'),
    config = require('../../../../config/config'),
    rest = require('restler'),
    errors = require('../../../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

var regions = (req, res) => {

    return new Promise((resolve, reject) => {

            var token = req.session.token ? `?access_token=${req.session.token}` : '';

            rest.get(`${configLocal.api.admin}regions${token}${req.query.page ? "&page=" + req.query.page : "" }`).on('success', (res) => {
                resolve(res);
            }).on('fail', (err, response) => {
                reject(errors.apiErrorHandler.handler(err, response))
            });
        }
    )
};

module.exports = regions;
