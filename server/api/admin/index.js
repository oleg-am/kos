var nomenclature = require('./dictionary/nomenclature');
var nomenclatureItem = require('./dictionary/nomenclatureItem');
var check_rights = require('./check_rights');
var pharmacy = require('./dictionary/pharmacy');
var pharmacyItem = require('./dictionary/pharmacyItem');
var cities = require('./dictionary/cities');
var regions = require('./dictionary/regions');
var nosologies = require('./dictionary/nosologies');
var city = require('./dictionary/city');
var regionsItem = require('./dictionary/regionsItem');
var nosology = require('./dictionary/nosology');
var synonyms = require('./dictionary/synonyms');
var synonymItem = require('./dictionary/synonymItem');

module.exports = {
    nomenclature: nomenclature,
    nomenclatureItem: nomenclatureItem,
    check_rights: check_rights,
    pharmacy: pharmacy,
    pharmacyItem: pharmacyItem,
    cities: cities,
    regions: regions,
    nosologies: nosologies,
	nosology: nosology,
    city: city,
	regionsItem: regionsItem,
	synonyms: synonyms,
	synonymItem: synonymItem
};
