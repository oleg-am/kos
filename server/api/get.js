var config = require('../../config/config'),
    errors = require('../errors/index'),
    rest = require('restler');

const configLocal = process.env.NODE_ENV === 'development'
  ? config.development
  : config.production;

var get = (req, res) => {

  let token = req.session.token ? `access_token=${req.session.token}` : '';

  if (token) {
    token = !!Object.keys(req.query).length ? '&' + token : '?' + token;
  }

  const queryUrl = req.originalUrl.replace('/api/admin/', '');
  console.log('queryUrl: ', queryUrl);

  const query = `${configLocal.api.admin}${queryUrl}${token}`;
  console.log('query: ', query);

  rest.get(query).on('success', function(data, response) {
      res.send(data);
  }).on('fail', function (err, response) {
      console.log(err);
      res.status(response.statusCode).send(err);
  });

};

module.exports = get;
