var config = require('../../config/config'),
    errors = require('../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

var imgUrl = (req, res) => {
  if (configLocal.api.hostImages) {
    console.log('img-url: ', configLocal.api.hostImages);
    res.json(configLocal.api.hostImages);
  } else {
    console.log('img url missing');
  }
};

module.exports = imgUrl;
