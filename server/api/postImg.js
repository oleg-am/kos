var config = require('../../config/config'),
    errors = require('../errors/index'),
    rest = require('restler');
let multiparty = require('multiparty');

const configLocal = process.env.NODE_ENV === 'development'
  ? config.development
  : config.production;

var postImg = (req, res) => {

  let token = req.session.token ? `access_token=${req.session.token}` : '';

  if (token) {
    token = !!Object.keys(req.query).length ? '&' + token : '?' + token;
  }

  let form = new multiparty.Form();

  form.parse(req, (err, fields, files) => {

  console.log('file: ', files.file[0]);
  let { path, originalFilename, size, headers: { encoding=null, 'content-type': contentType } } = files.file[0];

  const queryUrl = req.originalUrl.replace('/api/admin/', '');
  console.log('queryUrl: ', queryUrl);

  const query = `${configLocal.api.admin}${queryUrl}${token}`;
  console.log('query: ', query);

  rest.post(query, {
    multipart: true,
    data: { file: rest.file(path, originalFilename, size, encoding, contentType) }
  }).on('success', function(data, response) {
      res.send(data);
  }).on('fail', function (err, response) {
      console.log(err);
      res.status(response.statusCode).send(err);
  });

});

  // rest.postJson(query, data).on('success', function(data, response) {
  //     res.send(data);
  // }).on('fail', function (err, response) {
  //     console.log(err);
  //     res.status(response.statusCode).send(err);
  // });

};

module.exports = postImg;
