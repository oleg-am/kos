var api = require('../../api'),
    error = require('../../errors/index');

var admin = (req, res) => {
  // return res.render('admin/index');

    if (!req.session.token) {
        return res.redirect('/oauth/login')
    }

    api.admin_api.check_rights(req, res)
        .then((data) => {

            if (data.hasRights) {
               return res.render('admin/index');
            }

            return res.redirect('/oauth/login');
        })
        .catch((err) => {
            console.log('check_rights: ', err);
            return res.redirect('/oauth/userBlocked');
        });

};

module.exports = admin;
