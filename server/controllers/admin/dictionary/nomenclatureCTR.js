var api = require('../../../api'),
    error = require('../../../errors/index');

var nomenclatureCTR = (req, res) => {
    api.admin_api.nomenclature(req, res)
        .then((data) => {

            // if (!data.data) { //404 error prevent
            //     return res.json({data: []})
            // }

            res.json(data);
        })
        .catch((err) => {
            console.log(err);
            var errorObj = error.apiErrorHandler.parser(err);
            res.status(errorObj.statusCode).json(errorObj);
        })
};

module.exports = nomenclatureCTR;
