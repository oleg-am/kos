var api = require('../../../api'),
    error = require('../../../errors/index');

var nosology_GET = (req, res) => {
    api.admin_api.nosology(req, res)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            console.log(err);
            var errorObj = error.apiErrorHandler.parser(err);
            res.status(errorObj.statusCode).json(errorObj);
        })
};

module.exports = nosology_GET;
