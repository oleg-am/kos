var api = require('../../../api'),
    error = require('../../../errors/index');

var pharmacyCTR = (req, res) => {
    api.admin_api.pharmacy(req, res)
        .then((data) => {

            // if (!data.data) { //404 error prevent
            //     return res.json({data: []})
            // }

            res.json(data);
        })
        .catch((err) => {
            console.log(err);
            var errorObj = error.apiErrorHandler.parser(err);
            res.status(errorObj.statusCode).json(errorObj);
        })
};

module.exports = pharmacyCTR;
