var api = require('../../../api'),
    error = require('../../../errors/index');

var pharmacyItemCTR = (req, res) => {
    api.admin_api.pharmacyItem(req, res)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            console.log(err);
            var errorObj = error.apiErrorHandler.parser(err);
            res.status(errorObj.statusCode).json(errorObj);
        })
};

module.exports = pharmacyItemCTR;