var config = require('../../../../config/config'),
    rest = require('restler'),
    errors = require('../../../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

var region_DELETE = (req, res) => {

    var token = req.session.token ? `?access_token=${req.session.token}` : '';
    rest.del(`${configLocal.api.admin}regions/${req.params.id}${token}`).on('success', function(data, response) {
        res.send(data);
    }).on('fail', function (err, response) {
        console.log(err);
        res.status(response.statusCode).send(err);
    });
};

module.exports = region_DELETE;
