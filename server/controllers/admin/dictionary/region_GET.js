var api = require('../../../api'),
    error = require('../../../errors/index');

var region_GET = (req, res) => {
    api.admin_api.regionsItem(req, res)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            console.log(err);
            var errorObj = error.apiErrorHandler.parser(err);
            res.status(errorObj.statusCode).json(errorObj);
        })
};

module.exports = region_GET;
