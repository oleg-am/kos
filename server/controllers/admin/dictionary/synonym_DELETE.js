var config = require('../../../../config/config'),
    rest = require('restler'),
    errors = require('../../../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

var synonym_DELETE = (req, res) => {

    var token = req.session.token ? `?access_token=${req.session.token}` : '';
    rest.del(`${configLocal.api.admin}synonyms/${req.params.id}${token}`).on('success', function(data, response) {
        res.send(data);
    }).on('fail', function (err, response) {
        console.log(err);
        res.status(response.statusCode).send(err);
    });
};

module.exports = synonym_DELETE;
