var api = require('../../../api'),
    error = require('../../../errors/index');

var synonym_GET = (req, res) => {
    api.admin_api.synonymItem(req, res)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            console.log(err);
            var errorObj = error.apiErrorHandler.parser(err);
            res.status(errorObj.statusCode).json(errorObj);
        })
};

module.exports = synonym_GET;
