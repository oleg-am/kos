var config = require('../../../../config/config'),
    rest = require('restler'),
    errors = require('../../../errors/index');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

var synonym_PUT = (req, res) => {

    var token = req.session.token ? `?access_token=${req.session.token}` : '';
    var data = req.body;
    rest.putJson(`${configLocal.api.admin}synonyms/${req.params.id}${token}`, data).on('success', function(data, response) {
        res.send(data);
    }).on('fail', function (err, response) {
        console.log(err);
        res.status(response.statusCode).send(err);
    });
};

module.exports = synonym_PUT;
