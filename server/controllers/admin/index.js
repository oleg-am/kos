
var admin = require('./admin');
var nomenclatureCTR = require('./dictionary/nomenclatureCTR');
var nomenclatureItemCTR = require('./dictionary/nomenclatureItemCTR');
var login = require('./login/login');
var userBlocked = require('./login/userBlocked');
var logout = require('./login/logout');
var login_POST = require('./login/login_POST');
var pharmacyCTR = require('./dictionary/pharmacyCTR');
var pharmacyItemCTR = require('./dictionary/pharmacyItemCTR');
var pharmacyItem_PUT = require('./dictionary/pharmacyItemCTR_PUT');
var citiesCTR = require('./dictionary/citiesCTR');
var cities_PUT = require('./dictionary/citiesCTR_PUT');
var regionsCTR = require('./dictionary/regionsCTR');
var nosologiesCTR = require('./dictionary/nosologiesCTR');
var city_GET = require('./dictionary/city_GET');
var city_POST = require('./dictionary/city_POST');
var region_POST = require('./dictionary/regions_POST');
var region_GET = require('./dictionary/region_GET');
var region_PUT = require('./dictionary/region_PUT');
var region_DELETE = require('./dictionary/region_DELETE');
var nosology_GET = require('./dictionary/nosology_GET');
var nosology_POST = require('./dictionary/nosology_POST');
var nosology_PUT = require('./dictionary/nosology_PUT');
var nosology_DELETE = require('./dictionary/nosology_DELETE');
var synonymsCTR = require('./dictionary/synonymsCTR');
var synonym_GET = require('./dictionary/synonym_GET');
var synonym_POST = require('./dictionary/synonym_POST');
var synonym_PUT = require('./dictionary/synonym_PUT');
var synonym_DELETE = require('./dictionary/synonym_DELETE');
var link_POST = require('./dictionary/link_POST');
var unlink_POST = require('./dictionary/unlink_POST');

module.exports = {
    admin: admin,
    login: login,
    userBlocked: userBlocked,
    logout: logout,
    login_POST: login_POST,
    nomenclatureCTR: nomenclatureCTR,
    nomenclatureItemCTR: nomenclatureItemCTR,
    pharmacyCTR: pharmacyCTR,
    pharmacyItemCTR: pharmacyItemCTR,
    pharmacyItem_PUT: pharmacyItem_PUT,
    citiesCTR: citiesCTR,
    cities_PUT: cities_PUT,
    regionsCTR: regionsCTR,
    nosologiesCTR: nosologiesCTR,
	nosology_GET: nosology_GET,
	nosology_POST: nosology_POST,
	nosology_PUT: nosology_PUT,
	nosology_DELETE: nosology_DELETE,
    city_POST: city_POST,
    city_GET: city_GET,
	region_POST: region_POST,
	region_GET: region_GET,
	region_PUT: region_PUT,
	region_DELETE: region_DELETE,
	synonymsCTR: synonymsCTR,
	synonym_GET: synonym_GET,
	synonym_POST: synonym_POST,
	synonym_PUT: synonym_PUT,
	synonym_DELETE: synonym_DELETE,
	link_POST: link_POST,
	unlink_POST: unlink_POST
};
