
var login = (req, res) => {
    if (req.session.token) {
      return res.redirect('/admin')
    }
    // console.log('Cookies: ', req.cookies)
    res.render('admin/login');
};

module.exports = login;
