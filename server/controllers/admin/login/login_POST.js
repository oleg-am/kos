var config = require('../../../../config/config'),
    rest = require('restler');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;


var login_POST = (req, res) => {

    var url = `${configLocal.oauth.url}token`;

    rest.post(url, {
        data: {
            client_id: configLocal.oauth.client_id,
            client_secret: configLocal.oauth.client_secret,
            grant_type: configLocal.oauth.grant_type,
            username: req.body.username,
            password: req.body.password
        }
    }).on('success', function(data) {

        req.session.token = data.token.access_token;

        console.log(data);

        req.session.user = {
            name:  data.name,
            avatar: data.avatar
        };

        req.session.save();

        res.redirect('/admin');

    }).on('fail', function (err, response) {
        console.log(err);
        res.redirect('/admin/login');
    });

};

module.exports = login_POST;
