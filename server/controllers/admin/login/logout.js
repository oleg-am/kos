
var logout = (req, res) => {
    req.session.destroy();
    res.redirect('/admin');
};

module.exports = logout;