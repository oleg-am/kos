
var apiErrorHandler = {
    handler: function (err, response) {
        return new Error( JSON.stringify({errors: err.errors, statusCode: response.statusCode}))
    },

    parser: function (errObj) {
        return JSON.parse(errObj.message);
    }
};

module.exports = apiErrorHandler;