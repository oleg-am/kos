// # json
// Usage: `{{json data}}`
//
// Returns json data

var hbs = require('hbs');

hbs.registerHelper('json', function(data) {
  if (!data) { return ;}
  return JSON.stringify(data);
});
