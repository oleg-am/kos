// # jsonQuote
// Usage: `{{jsonQuote data}}`//
// Returns jsonQuote

var hbs = require('hbs');

hbs.registerHelper('jsonQuote', function(context) {
  if(!context) {        return;    }
  var json = JSON.stringify(context);
  json = json.replace(/'/g, "\\'");
  json = json.replace(/\\"/g, "\\'");
  return json;
});
