// # menuArranger
// Usage: `{{menuArranger index operator number}}`
//
// Returns boolean

var hbs = require('hbs');

hbs.registerHelper('menuArranger', function(a, operator, b, opts) {

  var even = (b%2 == 0);
  var bool = false;

  if (even) {
    switch(operator) {
      case '===':
        bool = (a === b/2 - 1) || (a === b/2);
        break;
      case '<':
        bool = a < b/2 - 1;
        break;
    }
  } else {
    switch(operator) {
      case '===':
        bool = (a === Math.round(b/2)-1);
        break;
      case '<':
        bool = a < Math.round(b/2)-1;
        break;
    }
  }


  if (bool) {
    return opts.fn(this);
  } else {
    return opts.inverse(this);
  }
});