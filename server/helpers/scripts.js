// # scripts
// Usage: `{{scripts n.js m.js}}`
//
// Returns scripts tags

var hbs = require('hbs');
var _ = require('lodash');

hbs.registerHelper('scripts', function(scripts) {

  if(!scripts) return;

  var arr = scripts.split('|');

  var result = '';

  _.forEach(arr, (item) => {
    result += `<script src='${item}'></script>`
  });

  return result;
});
