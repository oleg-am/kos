// # Frontend routes
var express   = require('express'),
  controllers = require('../controllers/index'),
  imgUrl      = require('../api/imgUrl'),
  postImg     = require('../api/postImg'),
  get         = require('../api/get'),
  post        = require('../api/post'),
  put         = require('../api/put'),
  del         = require('../api/del'),
  getUserInfo = require('../api/getUserInfo'),
  router      = express.Router(),
  adminRoutes;

adminRoutes = () => {
  // ## Login page
  router.get('/oauth/login', controllers.adminControllers.login);
  router.get('/oauth/userBlocked', controllers.adminControllers.userBlocked);

  // ## Logout
  router.get('/oauth/logout', controllers.adminControllers.logout);

  // ## Post login page
  router.post('/oauth/login', controllers.adminControllers.login_POST);

  // ## admin page
  // router.get('*',       controllers.adminControllers.admin );

  router.get('/admin', controllers.adminControllers.admin);
  router.get('/admin/*', controllers.adminControllers.admin);

  // ## api data delivery
  router.get('/api/img-url', imgUrl);

  // ## api log_receivers
  router.get('/api/admin/log_receivers', get);
  router.post('/api/admin/log_receivers', post);
  router.get('/api/admin/log_receivers/:id', get);
  router.put('/api/admin/log_receivers/:id', put);
  router.delete('/api/admin/log_receivers/:id', del);

  // ## api Internet Grounds
  router.get('/api/admin/internet_grounds', get);
  router.post('/api/admin/internet_grounds', post);

  router.get('/api/admin/internet_grounds/:id', get);
  router.put('/api/admin/internet_grounds/:id', put);
  router.delete('/api/admin/internet_grounds/:id', del);

  router.post('/api/admin/internet_ground_contacts/', post);
  router.get('/api/admin/internet_ground_contacts/:id', get);
  router.put('/api/admin/internet_ground_contacts/:id', put);
  router.delete('/api/admin/internet_ground_contacts/:id', del);

  router.post('/api/admin/status_aliases/', post);
  router.get('/api/admin/status_aliases/:id', get);
  router.put('/api/admin/status_aliases/:id', put);
  router.delete('/api/admin/status_aliases/:id', del);

  router.get('/api/admin/remainders_fields/:id', get);
  router.put('/api/admin/remainders_fields/:id', put);
  router.delete('/api/admin/remainders_fields/:id', del);

  router.post('/api/admin/remainders_local_diff_fields', post);
  router.get('/api/admin/remainders_local_diff_fields/:id', get);
  router.put('/api/admin/remainders_local_diff_fields/:id', put);
  router.delete('/api/admin/remainders_local_diff_fields/:id', del);

  router.get('/api/admin/remainders_chain', get);

  router.post('/api/admin/remainders_fields', post);
  router.get('/api/admin/remainders_fields', get);

  router.get('/api/admin/order_statuses', get);

  router.post('/api/admin/order_fields', post);
  router.put('/api/admin/order_fields/:id', put);

  router.post('/api/admin/status_fields', post);
  router.put('/api/admin/status_fields/:id', put);

  // ## END api Internet Grounds

  router.get('/api/admin/orders', get);
  router.get('/api/admin/orders/:id', get);
  router.post('/api/admin/orders', post);
  router.put('/api/admin/orders/:id', put);
  router.put('/api/admin/orders/:id/cancel', put);
  router.put('/api/admin/orders/:id/pause', put);
  router.put('/api/admin/orders/:id/status', put);

  router.get('/api/admin/nomenclature', get);
  router.get('/api/admin/nomenclature/:id', get);
  router.get('/api/admin/nomenclature/:id/remainders', get);
  router.put('/api/admin/nomenclature/:id', put);
  router.get('/api/admin/nomenclature/:id/remainders', get);
  router.post('/api/admin/nomenclature/:id/image', postImg);
  router.post('/api/admin/nomenclature/:id/instruction', postImg);

  router.get('/api/admin/pharmacies', get);
  router.get('/api/admin/pharmacies/:id', get);
  router.put('/api/admin/pharmacies/:id', put);
  router.post('/api/admin/pharmacies/:id/image', postImg);
  router.get('/api/admin/pharmacies/:id/remainders', get);

  router.post('/api/admin/pharmacy_phones', post);
  router.put('/api/admin/pharmacy_phones/:id', put);
  router.delete('/api/admin/pharmacy_phones/:id', del);

  router.post('/api/admin/delivery_schedules', post);
  router.put('/api/admin/delivery_schedules/:id', put);
  router.delete('/api/admin/delivery_schedules/:id', del);

  router.get('/api/admin/manufacturers', get);
  router.get('/api/admin/cities', get);
  router.get('/api/admin/cities/:id', get);
  router.get('/api/admin/regions', get);
  router.get('/api/admin/networks', get);

  router.get('/api/admin/clients', get);
  router.get('/api/admin/clients/:id', get);
  router.put('/api/admin/clients/:id', put);

  router.get('/api/admin/users', get);
  router.post('/api/admin/users', post);

  router.get('/api/admin/users/:id', get);
  router.put('/api/admin/users/:id', put);

  router.post('/api/admin/users/:id/link', post);
  router.post('/api/admin/users/:id/unlink', post);

  router.get('/api/admin/user_roles', get);
  router.get('/api/admin/user_roles/:id', get);
  router.post('/api/admin/user_roles', post);
  router.put('/api/admin/user_roles/:id', put);
  router.delete('/api/admin/user_roles/:id', del);

  router.get('/api/admin/configs', get);
  router.put('/api/admin/configs/:id', put);

  router.get('/api/me/check_rights', getUserInfo);
  router.get('/api/me/info', getUserInfo);

  router.post('/api/admin/take_order', post);
  router.get('/api/admin/order_reports', get);
  return router;
};

module.exports = adminRoutes;
