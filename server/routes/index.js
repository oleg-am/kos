var api = require('./api');
var admin = require('./admin');
var frontend = require('./frontend');

module.exports = {
    api: api,
    admin: admin,
    frontend: frontend
};
