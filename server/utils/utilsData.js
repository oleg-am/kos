var config = require('../../config/config');

const configLocal = process.env.NODE_ENV === 'development' ? config.development : config.production;

module.exports = {
    hostImages: configLocal.api.hostImages
};