// global.Promise = require('bluebird'); // for node 0.10
/* eslint-disable */
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');
var fs = require('fs');
var entry = {};

var files = fs.readdirSync(path.resolve('./client/js/pages'));


var filepath = './client/js/pages/';

files.forEach(function (filename) {
    filename = filename.slice(0, -3); //remove .js
    entry[filename] = filepath + filename
});

module.exports = [
    // {
    //     name: 'styles',
    //     entry: {
    //         styles: './client/styles/style.scss'
    //     },
    //     output: {
    //         path: 'client/public/build/styles',
    //         filename: "[name].js"
    //         // publicPath: "build/"
    //     },
    //     devtool: 'source-map',
    //     module: {
    //         loaders: [
    //             {
    //                 test: /\.scss$/,
    //                 loader: ExtractTextPlugin.extract("style", "css?sourceMap!sass?sourceMap&resolve url")
    //             },
    //             { test: /\.gif$/, loader: "url-loader?limit=10000&mimetype=image/gif&name=../images/[name].[ext]" },
    //             { test: /\.jpg$/, loader: "url-loader?limit=10000&mimetype=image/jpg&name=../images/[name].[ext]" },
    //             { test: /\.png$/, loader: "url-loader?limit=10000&mimetype=image/png&name=../images/[name].[ext]" },
    //             { test: /\.svg/, loader: "url-loader?limit=26000&mimetype=image/svg+xml&name=../images/[name].[ext]" },
    //             { test: /\.(woff|woff2|ttf|eot)/, loader: "url-loader?limit=1&name=../fonts/[name].[ext]" }
    //         ]
    //     },
    //     plugins: [
    //         new ExtractTextPlugin("[name].css", {allChunks: true}),
    //         new CopyWebpackPlugin([
    //             { from: './client/images', to: '../images' }
    //         ])
    //     ]
    // },
    {
        name: 'js',
        entry: entry,
        output: {
            path: 'client/public/build/js',
            filename: "[name].js",
            publicPath: "build/js/"
        },
        devtool: "eval",
        module: {
            loaders: [
                { test: /\.js$/, loader: "react-hot!babel", exclude: [/node_modules/] },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel',
                    query: {
                        presets: ['es2015', 'react', 'stage-0']
                    }
                }
            ]
        },
        plugins: [
            // new webpack.DefinePlugin({
            //     "process.env": {
            //         BROWSER: JSON.stringify(true),
            //         NODE_ENV: JSON.stringify( process.env.NODE_ENV || 'development' )
            //     }
            // }),
            // new ExtractTextPlugin("[name].css", {allChunks: true})
        ],
        eslint: {
            configFile: path.resolve('.eslintrc')
        }
    },
    {
        // debug: true,
        // devtool: 'cheap-module-eval-source-map',
        name: 'admin-spa',
        entry: {
            index: './client/admin/index'
        },
        output: {
            path: 'client/public/build/admin',
            filename: "[name].js",
            publicPath: "build/admin/"
        },
        devtool: "eval",
        module: {
            loaders: [
                { test: /\.js$/, loader: "react-hot!babel", exclude: [/node_modules/] },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel',
                    query: {
                        presets: ['es2015', 'react', 'stage-0']
                    }
                },
                {
                  test: /\.css$/,
                  loader: 'style!css',
                }
                // {test: /(\.css|\.scss)$/, loaders: ['style', 'css?sourceMap', 'sass?sourceMap']}
            ],
            preLoaders: [
              {
                test: /\.json$/,
                loader: 'json-loader',
                include: '../../config/version.json',
              },
            ]
        }
    },
    {
        name: 'admin-styles',
        entry: {
            styles: './client/admin/styles/style.scss'
        },
        output: {
            path: 'client/public/build/admin/styles',
            filename: "[name].js"
        },
        devtool: 'source-map',
        module: {
            loaders: [
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract("style", "css?sourceMap!sass?sourceMap&resolve url")
                },
                { test: /\.gif$/, loader: "url-loader?limit=10000&mimetype=image/gif&name=../images/[name].[ext]" },
                { test: /\.jpg$/, loader: "url-loader?limit=10000&mimetype=image/jpg&name=../images/[name].[ext]" },
                { test: /\.png$/, loader: "url-loader?limit=10000&mimetype=image/png&name=../images/[name].[ext]" },
                { test: /\.svg/, loader: "url-loader?limit=26000&mimetype=image/svg+xml&name=../images/[name].[ext]" },
                { test: /\.(woff|woff2|ttf|eot)/, loader: "url-loader?limit=1&name=../fonts/[name].[ext]" }
            ]
        },
        plugins: [
            new ExtractTextPlugin("[name].css", {allChunks: true}),
            new CopyWebpackPlugin([
                { from: './client/admin/images', to: '../images' }
            ])
        ]
    }];
