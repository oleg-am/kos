/* eslint-disable */
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var fs = require('fs');
var path = require('path');
var entry = {};

var files = fs.readdirSync(path.resolve('./client/js/pages'));
var filepath = './client/js/pages/';

files.forEach(function (filename) {
    filename = filename.slice(0, -3); //remove .js
    entry[filename] = filepath + filename
});

module.exports = [
    {
        name: 'js',
        entry: entry,
        output: {
            path: 'client/public/build/js',
            filename: "[name].js",
            publicPath: "build/js/"
        },
        plugins: [
            new webpack.NoErrorsPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor'
            }),
            new webpack.optimize.UglifyJsPlugin({
                mangle: true,
                compress: {
                    sequences: true,
                    dead_code: true,
                    conditionals: true,
                    booleans: true,
                    unused: true,
                    if_return: true,
                    join_vars: true,
                    drop_console: true,
                    warnings: false
                }
            }),
            new webpack.optimize.DedupePlugin()
        ],
        module: {
            loaders: [
                { test: /\.js$/, loader: "react-hot!babel", exclude: [/node_modules/] },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel',
                    query: {
                        presets: ['es2015', 'react', 'stage-0'],
                        plugins: [
                            'transform-decorators-legacy',
                            'transform-react-constant-elements',
                            'transform-react-inline-elements'
                        ]
                    }
                }
            ]
        },
        eslint: {
            configFile: path.resolve('.eslintrc')
        }
    },
    // {
    //     name: 'styles',
    //     entry: {
    //         styles: './client/styles/style.scss'
    //     },
    //     output: {
    //         path: 'client/public/build/styles',
    //         filename: "[name].js"
    //         // publicPath: "build/"
    //     },
    //     module: {
    //         loaders: [
    //             {
    //                 test: /\.scss$/,
    //                 loader: ExtractTextPlugin.extract("style", "css?minimize&autoprefixer!sass?resolve url")
    //             },
    //             { test: /\.gif$/, loader: "url-loader?limit=10000&mimetype=image/gif&name=../images/[name].[ext]" },
    //             { test: /\.jpg$/, loader: "url-loader?limit=10000&mimetype=image/jpg&name=../images/[name].[ext]" },
    //             { test: /\.png$/, loader: "url-loader?limit=10000&mimetype=image/png&name=../images/[name].[ext]" },
    //             { test: /\.svg/, loader: "url-loader?limit=26000&mimetype=image/svg+xml&name=../images/[name].[ext]" },
    //             { test: /\.(woff|woff2|ttf|eot)/, loader: "url-loader?limit=1&name=../fonts/[name].[ext]" }
    //         ]
    //     },
    //     plugins: [
    //         new ExtractTextPlugin("[name].css", {allChunks: true}),
    //         new CopyWebpackPlugin([
    //             { from: './client/images', to: '../images' }
    //         ])
    //     ]
    // },
    {
        name: 'admin-spa',
        entry: {
            index: './client/admin/index'
        },
        output: {
            path: 'client/public/build/admin',
            filename: "[name].js",
            publicPath: "build/admin/"
        },
        plugins: [
            new webpack.NoErrorsPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor'
            }),
            new webpack.optimize.UglifyJsPlugin({
                mangle: true,
                compress: {
                    sequences: true,
                    dead_code: true,
                    conditionals: true,
                    booleans: true,
                    unused: true,
                    if_return: true,
                    join_vars: true,
                    drop_console: true,
                    warnings: false
                }
            }),
            new webpack.optimize.DedupePlugin()
        ],
        module: {
            loaders: [
                { test: /\.js$/, loader: "react-hot!babel", exclude: [/node_modules/] },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel',
                    query: {
                        presets: ['es2015', 'react', 'stage-0'],
                        plugins: [
                            'transform-decorators-legacy',
                            'transform-react-constant-elements',
                            'transform-react-inline-elements'
                        ]
                    }
                  },
                  {
                    test: /\.css$/,
                    loader: 'style!css',
                  },
            ],
            preLoaders: [
              {
                test: /\.json$/,
                loader: 'json-loader',
                include: '../../config/version.json',
              },
            ]
        }
    },
    {
        name: 'admin-styles',
        entry: {
            styles: './client/admin/styles/style.scss'
        },
        output: {
            path: 'client/public/build/admin/styles',
            filename: "[name].js"
        },
        module: {
            loaders: [
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract("style", "css?minimize&autoprefixer!sass?resolve url")
                },
                { test: /\.gif$/, loader: "url-loader?limit=10000&mimetype=image/gif&name=../images/[name].[ext]" },
                { test: /\.jpg$/, loader: "url-loader?limit=10000&mimetype=image/jpg&name=../images/[name].[ext]" },
                { test: /\.png$/, loader: "url-loader?limit=10000&mimetype=image/png&name=../images/[name].[ext]" },
                { test: /\.svg/, loader: "url-loader?limit=26000&mimetype=image/svg+xml&name=../images/[name].[ext]" },
                { test: /\.(woff|woff2|ttf|eot)/, loader: "url-loader?limit=1&name=../fonts/[name].[ext]" }
            ]
        },
        plugins: [
            new ExtractTextPlugin("[name].css", {allChunks: true}),
            new CopyWebpackPlugin([
                { from: './client/admin/images', to: '../images' }
            ])
        ]
    }
];
